truncate table [dbo].[Permission]
SET IDENTITY_INSERT [dbo].[Permission] ON 

GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (1, N'AllAccountController', N'0000001   ', N'Account', N'UserList^SaveUser^DeleteUser^ChangePassword^Login^Logout^PasswordGenerator', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (2, N'AllAccountingController', N'0000002   ', N'Accounting', N'LedgerGroupList^SaveLedgerGroup^DeleteLedgerGroup^LedgerList^SaveLedger^DeleteLedger', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (3, N'AllAccountingReport', N'0000003   ', N'AccountingReport', N'AccountsLedger', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (4, N'AllCategoryController', N'0000004   ', N'Category', N'List^Register^Delete', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (5, N'AllHomeController', N'0000005   ', N'Home', N'Index^AuthFailed', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (6, N'AllInventoryController', N'0000006   ', N'Inventory', N'ProductSettingsList^SaveProductSettings^DeleteProductSettings^StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (7, N'AllPaymentController', N'0000007   ', N'Payment', N'DealerPaymentModule^SupplierPaymentModule^SavePayment^DeletePayment', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (8, N'AllPurchaseController', N'0000008   ', N'Purchase', N'PurchaseEntryList^SavePurchaseEntry^OnChangLoadProduct^LoadProduct^DeleteInvoiceInfo^GetProductListByCategory^GetNewInvoiceInfoDetail', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (9, N'AllReportController', N'0000009   ', N'Report', N'Index^SalesReport^PurchaseReport^OpeningStock^ClosingStock^AccountsLedger^PartyLedger^IncomeStatement^BalanceSheet^TrialBalanceReport^InvoiceReport', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (10, N'AllSalaryController', N'0000010   ', N'Salary', N'SalaryList^SaveSalary^DeleteSalary^SaveAdvancedSalary^LoadAllowanceByEmployee^CalCulateEmployeePercentage', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (11, N'AllSalesController', N'0000011   ', N'Sales', N'SalesInvoiceList^AddSalesInvoice^AddReturnInvoice^SaveSalesEntry^LoadProductByCategory^LoadProduct^EditInvoiceInfo^PrintBill^LoadPayemntByParty^DeleteInvoiceInfo^GetProductListByCategory^GetNewInvoiceInfoDetail^GetNewInvoiceInfoDetail', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (12, N'AllSettingController', N'0000012   ', N'Settings', N'EmployeeList^SaveEmployee^DeleteEmployee^SupplierPartyList^DealerPartyList^SaveParty^DeleteParty^AreaList^SaveArea^DeleteArea^BankList^SaveBank^DeleteBank', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (13, N'AllStockController', N'0000013   ', N'Stock', N'Index^CurrentStockList^GEtProductStock', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (14, N'AllTemplateController', N'0000014   ', N'Template', N'List^Register^Modify^GetPermissionList', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (15, N'AllTransactionController', N'0000015   ', N'Transaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (16, N'AllUserController', N'0000016   ', N'UserCommon', N'List^Register', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (17, N'AllJsonController', N'0000016   ', N'JsonList', N'Index^LoadProductByCategory^LoadProductStock^LoadProductStockSell', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (18, N'InventoryWithoutProduct', NULL, N'Inventory', N'StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (19, N'AllJobExpenseController', N'0000019   ', N'JobExpense', N'JobExpenseList^SaveExpense^SaveExpense^DeleteExpense^OnChangeLoadDetail', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (20, N'AllowedReport', N'0000020   ', N'Report', N'SalesReport^PurchaseReport^OpeningStock^ClosingStock^PartyLedgerJobReport^JobCostingReport^InvoiceReport', 1, NULL)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (21, N'AllTransactionController', N'0000015   ', N'OpeningTransaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Permission] OFF
GO
