USE [RawWorld]
GO

/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 24-Apr-17 12:01:01 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO
drop table [dbo].[InvoiceDetails]
CREATE TABLE [dbo].[InvoiceDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[PropertyId] [bigint] NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[ProductSerial] [nvarchar](50) NOT NULL,
	[Warranty] [int] NOT NULL,
	[SalesFlag] [bigint] NOT NULL,
 CONSTRAINT [PK_BillDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO

ALTER TABLE [dbo].[InvoiceDetails] ADD  CONSTRAINT [DF_InvoiceDetails_ProductSerial]  DEFAULT ((0)) FOR [ProductSerial]
GO
ALTER TABLE [dbo].[InvoiceDetails] ADD  CONSTRAINT [DF_InvoiceDetails_SalesFlag]  DEFAULT ((0)) FOR [SalesFlag]
GO


