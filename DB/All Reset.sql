USE [RedGreen]
GO
Truncate table [dbo].[Category]
Truncate table [dbo].[Product]
Truncate table [dbo].[Currency]
Truncate table [dbo].[Employee]
Truncate table [dbo].[InvoiceDetails]
Truncate table [dbo].[InvoiceInfo]
Truncate table [dbo].[Payment]
Truncate table [dbo].[Transaction]

truncate table [dbo].[GroupHeader]
truncate table [dbo].[Ledger]
truncate table [dbo].[LedgerGroup]


SET IDENTITY_INSERT [dbo].[GroupHeader] ON 

GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (1, 1, 1, CAST(N'2016-07-17 02:35:39.107' AS DateTime), CAST(N'2016-07-17 02:35:39.260' AS DateTime), 1, 0, N'Fixed Assets', NULL, 17, 24, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (2, 1, 1, CAST(N'2016-07-17 02:36:23.837' AS DateTime), CAST(N'2016-07-17 02:36:23.837' AS DateTime), 1, 0, N'Current Assets', NULL, 17, 24, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (3, 1, 1, CAST(N'2016-07-17 02:36:32.543' AS DateTime), CAST(N'2016-07-17 02:36:32.543' AS DateTime), 1, 0, N' Liabilities', NULL, 18, 25, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (4, 1, 1, CAST(N'2016-07-17 02:36:40.777' AS DateTime), CAST(N'2016-07-17 02:36:40.777' AS DateTime), 1, 0, N'Equity', NULL, 18, 26, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (5, 1, 1, CAST(N'2016-07-17 02:36:48.910' AS DateTime), CAST(N'2016-07-17 02:36:48.910' AS DateTime), 1, 0, N'Revenue', NULL, 18, 27, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (6, 1, 1, CAST(N'2016-07-17 02:36:56.413' AS DateTime), CAST(N'2016-07-17 02:36:56.413' AS DateTime), 1, 0, N'Expense', NULL, 17, 28, 1)
GO
SET IDENTITY_INSERT [dbo].[GroupHeader] OFF
GO
SET IDENTITY_INSERT [dbo].[Ledger] ON 

GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (1, 3, 3, CAST(N'2017-02-22 00:51:46.450' AS DateTime), CAST(N'2017-02-22 00:51:46.450' AS DateTime), 1, 0, N'Sales', 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (2, 3, 3, CAST(N'2017-02-22 00:52:01.147' AS DateTime), CAST(N'2017-02-22 00:52:01.147' AS DateTime), 1, 0, N'Purchase', 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (3, 3, 3, CAST(N'2017-02-22 00:52:12.483' AS DateTime), CAST(N'2017-02-22 00:52:12.483' AS DateTime), 1, 0, N'Current Stock', 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (4, 3, 3, CAST(N'2017-02-22 01:09:57.753' AS DateTime), CAST(N'2017-02-22 01:12:03.123' AS DateTime), 1, 0, N'Bonus Income', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (5, 3, 3, CAST(N'2017-02-22 01:12:42.950' AS DateTime), CAST(N'2017-02-22 01:12:42.950' AS DateTime), 1, 0, N'Product Expire Expense', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (6, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Profit Income', 6, NULL, 0, N'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (7, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Purchase Discount', 8, NULL, 0, N'dfgdfg', NULL, NULL, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (8, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Sales Discount', 7, NULL, 0, N'dfgdfg', NULL, NULL, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (9, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Purchase Vat', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (10, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Sales Vat', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (11, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Purchase Service Charge', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (12, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Sales Service Charge', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Ledger] OFF
GO
SET IDENTITY_INSERT [dbo].[LedgerGroup] ON 

GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (1, 3, 3, CAST(N'2017-02-22 00:43:38.843' AS DateTime), CAST(N'2017-02-22 00:43:38.843' AS DateTime), 1, 0, N'Creditors', NULL, NULL, 3, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (2, 3, 3, CAST(N'2017-02-22 00:43:56.537' AS DateTime), CAST(N'2017-02-22 00:43:56.537' AS DateTime), 1, 0, N'Debtors', NULL, NULL, 2, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (3, 3, 3, CAST(N'2017-02-22 00:44:33.793' AS DateTime), CAST(N'2017-02-22 00:44:33.793' AS DateTime), 1, 0, N'Sales', NULL, NULL, 5, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (4, 3, 3, CAST(N'2017-02-22 00:44:50.727' AS DateTime), CAST(N'2017-02-22 00:44:50.727' AS DateTime), 1, 0, N'Purchase', NULL, NULL, 6, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (5, 3, 3, CAST(N'2017-02-22 00:45:14.387' AS DateTime), CAST(N'2017-02-22 00:45:14.387' AS DateTime), 1, 0, N'Current Stock', NULL, NULL, 2, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (6, 3, 3, CAST(N'2017-02-22 00:45:49.553' AS DateTime), CAST(N'2017-02-22 00:45:49.553' AS DateTime), 1, 0, N'Profit and Loss A/C', NULL, NULL, 4, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (7, 3, 3, CAST(N'2017-02-22 01:11:34.557' AS DateTime), CAST(N'2017-02-22 01:11:34.557' AS DateTime), 1, 0, N'Others Revenue', NULL, NULL, 5, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (8, 3, 3, CAST(N'2017-02-22 01:11:51.787' AS DateTime), CAST(N'2017-02-22 01:11:51.787' AS DateTime), 1, 0, N'Others Expense', NULL, NULL, 6, 0)
GO
SET IDENTITY_INSERT [dbo].[LedgerGroup] OFF
GO


SET IDENTITY_INSERT [dbo].[Currency] ON 

GO
INSERT [dbo].[Currency] ([ID], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [Status], [CurrencyName], [ShortName], [Symbol], [Country], [ConversionRate]) VALUES (1, 1, CAST(N'2017-05-08' AS Date), 1, CAST(N'2017-05-08' AS Date), 1, N'Taka', N'BDT', N'?', N'Bangladesh', CAST(1.00 AS Decimal(18, 2)))
GO
SET IDENTITY_INSERT [dbo].[Currency] OFF
GO
