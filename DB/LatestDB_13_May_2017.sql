USE [RedGreen]
GO
/****** Object:  Table [dbo].[Application]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AppName] [nvarchar](50) NOT NULL,
	[LogoUrl] [nvarchar](50) NULL,
	[Slogon] [nvarchar](max) NULL,
	[ValidateDate] [date] NOT NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Application_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[IsParent] [bit] NOT NULL,
	[ParentId] [bigint] NULL,
	[BrandName] [nvarchar](200) NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyName] [nvarchar](50) NOT NULL,
	[ShortName] [nchar](10) NOT NULL,
	[Symbol] [nvarchar](15) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Currency] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeName] [nvarchar](100) NOT NULL,
	[EmployeeCode] [nvarchar](100) NULL,
	[Designation] [nvarchar](100) NULL,
	[Department] [nvarchar](100) NULL,
	[Location] [nvarchar](100) NULL,
	[DateOfJoining] [datetime] NULL,
	[DateOfBirth] [datetime] NULL,
	[PFNo] [nvarchar](50) NULL,
	[ESINo] [nvarchar](50) NULL,
	[IsSR] [bit] NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupHeader]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupHeader](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[AccountsType] [bigint] NULL,
	[IsParent] [bit] NOT NULL,
 CONSTRAINT [PK_GroupHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[PropertyId] [bigint] NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[ProductSerial] [nvarchar](50) NOT NULL CONSTRAINT [DF_InvoiceDetails_ProductSerial]  DEFAULT ((0)),
	[Warranty] [int] NOT NULL,
	[SalesFlag] [bigint] NOT NULL CONSTRAINT [DF_InvoiceDetails_SalesFlag]  DEFAULT ((0)),
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_BillDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceInfo]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceInfo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceNo] [nvarchar](100) NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[SubTotal] [decimal](18, 0) NOT NULL,
	[ServiceCharge] [decimal](18, 0) NULL,
	[Discount] [decimal](18, 2) NULL,
	[Vat] [decimal](18, 2) NULL,
	[GrandTotal] [decimal](18, 0) NOT NULL,
	[InvoiceReferenceId] [bigint] NULL,
	[PropertyId] [bigint] NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[InvoiceDate] [datetime] NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
	[PartyName] [nvarchar](500) NULL,
	[PartyAddress] [nvarchar](2000) NULL,
	[ProcessBy] [bigint] NOT NULL,
	[ApprovedBy] [bigint] NOT NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_BillingTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ledger]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledger](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[LedgerGroupId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NOT NULL,
	[Code] [nvarchar](max) NULL,
	[ContactPerson] [nvarchar](max) NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[Mobile1] [nvarchar](max) NULL,
	[Mobile2] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Visible] [bigint] NULL,
 CONSTRAINT [PK_Ledger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LedgerGroup]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LedgerGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[GroupHeaderId] [bigint] NOT NULL,
	[IsParent] [bit] NOT NULL CONSTRAINT [DF_TransactionHeader_IsParent]  DEFAULT ((0)),
 CONSTRAINT [PK_LedgerGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[PaymentType] [bigint] NOT NULL,
	[Remarks] [nvarchar](200) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
	[BankName] [nvarchar](100) NULL,
	[AccountNo] [nvarchar](100) NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[CheckNo] [nvarchar](100) NULL,
	[CheckDate] [date] NULL,
	[PaymentStatus] [bigint] NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_Payment_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[PermissionCode] [nchar](10) NULL,
	[ControllerName] [nvarchar](20) NULL,
	[ActionName] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[ProductCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Salary]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeId] [bigint] NOT NULL,
	[SalryType] [bigint] NULL,
	[Month] [datetime] NULL,
	[TotalSalary] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[FestivalBonus] [decimal](18, 0) NULL,
	[TotalBill] [decimal](18, 0) NULL,
	[MedicalAllowance] [decimal](18, 0) NULL,
	[OtherAllowance] [decimal](18, 0) NULL,
	[OtherDeduction] [decimal](18, 0) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TableProperty]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableProperty](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](50) NULL,
	[PropertyName] [nvarchar](50) NULL,
	[PropertyValue] [int] NULL,
	[ViewName] [nvarchar](100) NULL,
 CONSTRAINT [PK_TableProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Template]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Template](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[PermissionIds] [nvarchar](max) NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Role_Status]  DEFAULT ((1)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[DebitHeaderId] [bigint] NOT NULL,
	[CreditHeaderId] [bigint] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionType] [bigint] NOT NULL,
	[CheckNo] [nvarchar](50) NULL,
	[EmployeeId] [bigint] NULL,
	[Remarks] [nvarchar](max) NULL,
	[BankId] [bigint] NULL,
	[CashType] [int] NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Unit]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unit](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Symbol] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_User_IsActive]  DEFAULT ((1)),
	[Islogin] [bit] NOT NULL CONSTRAINT [DF_User_Islogin]  DEFAULT ((0)),
	[LastLogin] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_User_Status]  DEFAULT ((1)),
	[PhotoUrl] [nvarchar](50) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[TemplateId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[ApplicationId] [bigint] NOT NULL,
	[UserMessage] [nvarchar](max) NULL,
	[Name] [nvarchar](200) NULL,
	[IsCreator] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 13/05/2017 00:09:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[ProfileId] [bigint] NULL,
	[RoleId] [bigint] NULL,
	[LoginDateTime] [datetime] NULL,
	[IpAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Application] ON 

GO
INSERT [dbo].[Application] ([Id], [AppName], [LogoUrl], [Slogon], [ValidateDate], [Status]) VALUES (1, N'DentalCorner', N'dental.jpg', N'Rifat Abdullah', CAST(N'2016-08-26' AS Date), 1)
GO
SET IDENTITY_INSERT [dbo].[Application] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId], [BrandName]) VALUES (1, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'sfdfsdfs', NULL, 0, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Currency] ON 

GO
INSERT [dbo].[Currency] ([ID], [CurrencyName], [ShortName], [Symbol], [Country], [ConversionRate]) VALUES (1, N'Taka', N'BDT       ', N't', N'Bd', CAST(1.00 AS Decimal(18, 2)))
GO
SET IDENTITY_INSERT [dbo].[Currency] OFF
GO
SET IDENTITY_INSERT [dbo].[GroupHeader] ON 

GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (1, 1, 1, CAST(N'2016-07-17 02:35:39.107' AS DateTime), CAST(N'2016-07-17 02:35:39.260' AS DateTime), 1, 0, N'Fixed Assets', NULL, 12, 24, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (2, 1, 1, CAST(N'2016-07-17 02:36:23.837' AS DateTime), CAST(N'2016-07-17 02:36:23.837' AS DateTime), 1, 0, N'Current Assets', NULL, 12, 24, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (3, 1, 1, CAST(N'2016-07-17 02:36:32.543' AS DateTime), CAST(N'2016-07-17 02:36:32.543' AS DateTime), 1, 0, N' Liabilities', NULL, 13, 25, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (4, 1, 1, CAST(N'2016-07-17 02:36:40.777' AS DateTime), CAST(N'2016-07-17 02:36:40.777' AS DateTime), 1, 0, N'Equity', NULL, 13, 26, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (5, 1, 1, CAST(N'2016-07-17 02:36:48.910' AS DateTime), CAST(N'2016-07-17 02:36:48.910' AS DateTime), 1, 0, N'Revenue', NULL, 13, 27, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (6, 1, 1, CAST(N'2016-07-17 02:36:56.413' AS DateTime), CAST(N'2016-07-17 02:36:56.413' AS DateTime), 1, 0, N'Expense', NULL, 12, 28, 1)
GO
SET IDENTITY_INSERT [dbo].[GroupHeader] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetails] ON 

GO
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [SalesFlag], [ConversionRate], [Currency]) VALUES (1, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, 1, 1, CAST(0.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(12.00 AS Decimal(18, 2)), 5, CAST(12.00 AS Decimal(18, 2)), N'aaaaaaa', 12, 1, CAST(1.00 AS Decimal(18, 2)), 1)
GO
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [SalesFlag], [ConversionRate], [Currency]) VALUES (2, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, 2, 1, CAST(1212.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(1212.00 AS Decimal(18, 2)), 2, CAST(0.00 AS Decimal(18, 2)), N'aaaaaaa', 12, 0, CAST(1.00 AS Decimal(18, 2)), 1)
GO
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice], [ProductSerial], [Warranty], [SalesFlag], [ConversionRate], [Currency]) VALUES (3, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, 3, 1, CAST(0.00 AS Decimal(18, 2)), CAST(1.00 AS Decimal(18, 2)), CAST(12.00 AS Decimal(18, 2)), 5, CAST(12.00 AS Decimal(18, 2)), N'ryytryt', 0, 0, CAST(1.00 AS Decimal(18, 2)), 1)
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceInfo] ON 

GO
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SubTotal], [ServiceCharge], [Discount], [Vat], [GrandTotal], [InvoiceReferenceId], [PropertyId], [Remarks], [InvoiceDate], [DebitId], [CreditId], [PartyName], [PartyAddress], [ProcessBy], [ApprovedBy], [ReferenceNo], [ConversionRate], [Currency]) VALUES (1, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'1', 6, CAST(12 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(12 AS Decimal(18, 0)), NULL, 5, NULL, CAST(N'2017-05-08 00:00:00.000' AS DateTime), 2, 6, NULL, NULL, 0, 0, N'', CAST(1.00 AS Decimal(18, 2)), 1)
GO
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SubTotal], [ServiceCharge], [Discount], [Vat], [GrandTotal], [InvoiceReferenceId], [PropertyId], [Remarks], [InvoiceDate], [DebitId], [CreditId], [PartyName], [PartyAddress], [ProcessBy], [ApprovedBy], [ReferenceNo], [ConversionRate], [Currency]) VALUES (2, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'2', 7, CAST(1212 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(1212 AS Decimal(18, 0)), NULL, 2, NULL, CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 7, NULL, NULL, 0, 0, NULL, CAST(1.00 AS Decimal(18, 2)), 1)
GO
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SubTotal], [ServiceCharge], [Discount], [Vat], [GrandTotal], [InvoiceReferenceId], [PropertyId], [Remarks], [InvoiceDate], [DebitId], [CreditId], [PartyName], [PartyAddress], [ProcessBy], [ApprovedBy], [ReferenceNo], [ConversionRate], [Currency]) VALUES (3, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'2', 6, CAST(12 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0.00 AS Decimal(18, 2)), CAST(0.00 AS Decimal(18, 2)), CAST(12 AS Decimal(18, 0)), NULL, 5, NULL, CAST(N'2017-05-08 00:00:00.000' AS DateTime), 2, 6, NULL, NULL, 0, 0, N'', CAST(1.00 AS Decimal(18, 2)), 1)
GO
SET IDENTITY_INSERT [dbo].[InvoiceInfo] OFF
GO
SET IDENTITY_INSERT [dbo].[Ledger] ON 

GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (1, 3, 3, CAST(N'2017-02-22 00:51:46.450' AS DateTime), CAST(N'2017-02-22 00:51:46.450' AS DateTime), 1, 0, N'Sales', 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (2, 3, 3, CAST(N'2017-02-22 00:52:01.147' AS DateTime), CAST(N'2017-02-22 00:52:01.147' AS DateTime), 1, 0, N'Purchase', 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (3, 3, 3, CAST(N'2017-02-22 00:52:12.483' AS DateTime), CAST(N'2017-02-22 00:52:12.483' AS DateTime), 1, 0, N'Current Stock', 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (4, 3, 3, CAST(N'2017-02-22 01:09:57.753' AS DateTime), CAST(N'2017-02-22 01:12:03.123' AS DateTime), 1, 0, N'Bonus Income', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (5, 3, 3, CAST(N'2017-02-22 01:12:42.950' AS DateTime), CAST(N'2017-02-22 01:12:42.950' AS DateTime), 1, 0, N'Product Expire Expense', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (6, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'asdasd', 1, NULL, 13, N'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (7, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'dfgdfg', 2, NULL, 14, N'dfgdfg', NULL, NULL, NULL, NULL, NULL, NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[Ledger] OFF
GO
SET IDENTITY_INSERT [dbo].[LedgerGroup] ON 

GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (1, 3, 3, CAST(N'2017-02-22 00:43:38.843' AS DateTime), CAST(N'2017-02-22 00:43:38.843' AS DateTime), 1, 0, N'Creditors', NULL, NULL, 3, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (2, 3, 3, CAST(N'2017-02-22 00:43:56.537' AS DateTime), CAST(N'2017-02-22 00:43:56.537' AS DateTime), 1, 0, N'Debtors', NULL, NULL, 2, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (3, 3, 3, CAST(N'2017-02-22 00:44:33.793' AS DateTime), CAST(N'2017-02-22 00:44:33.793' AS DateTime), 1, 0, N'Sales', NULL, NULL, 5, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (4, 3, 3, CAST(N'2017-02-22 00:44:50.727' AS DateTime), CAST(N'2017-02-22 00:44:50.727' AS DateTime), 1, 0, N'Purchase', NULL, NULL, 6, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (5, 3, 3, CAST(N'2017-02-22 00:45:14.387' AS DateTime), CAST(N'2017-02-22 00:45:14.387' AS DateTime), 1, 0, N'Current Stock', NULL, NULL, 2, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (6, 3, 3, CAST(N'2017-02-22 00:45:49.553' AS DateTime), CAST(N'2017-02-22 00:45:49.553' AS DateTime), 1, 0, N'Profit and Loss A/C', NULL, NULL, 4, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (7, 3, 3, CAST(N'2017-02-22 01:11:34.557' AS DateTime), CAST(N'2017-02-22 01:11:34.557' AS DateTime), 1, 0, N'Others Revenue', NULL, NULL, 5, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (8, 3, 3, CAST(N'2017-02-22 01:11:51.787' AS DateTime), CAST(N'2017-02-22 01:11:51.787' AS DateTime), 1, 0, N'Others Expense', NULL, NULL, 6, 0)
GO
SET IDENTITY_INSERT [dbo].[LedgerGroup] OFF
GO
SET IDENTITY_INSERT [dbo].[Permission] ON 

GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (1, N'AllAccountController', N'0000001   ', N'Account', N'UserList^SaveUser^DeleteUser^ChangePassword^Login^Logout^PasswordGenerator', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (2, N'AllAccountingController', N'0000002   ', N'Accounting', N'LedgerGroupList^SaveLedgerGroup^DeleteLedgerGroup^LedgerList^SaveLedger^DeleteLedger', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (3, N'AllAccountingReport', N'0000003   ', N'AccountingReport', N'AccountsLedger', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (4, N'AllCategoryController', N'0000004   ', N'Category', N'List^Register^Delete', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (5, N'AllHomeController', N'0000005   ', N'Home', N'Index^AuthFailed', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (6, N'AllInventoryController', N'0000006   ', N'Inventory', N'ProductSettingsList^SaveProductSettings^DeleteProductSettings^StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (7, N'AllPaymentController', N'0000007   ', N'Payment', N'DealerPaymentModule^SupplierPaymentModule^SavePayment^DeletePayment', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (8, N'AllPurchaseController', N'0000008   ', N'Purchase', N'PurchaseEntryList^SavePurchaseEntry^OnChangLoadProduct^LoadProduct^DeleteInvoiceInfo^GetProductListByCategory^GetNewInvoiceInfoDetail', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (9, N'AllReportController', N'0000009   ', N'Report', N'Index^SalesReport^PurchaseReport^OpeningStock^ClosingStock^AccountsLedger^PartyLedger^IncomeStatement^BalanceSheet^TrialBalanceReport^InvoiceReport', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (10, N'AllSalaryController', N'0000010   ', N'Salary', N'SalaryList^SaveSalary^DeleteSalary^SaveAdvancedSalary^LoadAllowanceByEmployee^CalCulateEmployeePercentage', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (11, N'AllSalesController', N'0000011   ', N'Sales', N'SalesInvoiceList^AddSalesInvoice^AddReturnInvoice^SaveSalesEntry^LoadProductByCategory^LoadProduct^EditInvoiceInfo^PrintBill^LoadPayemntByParty^DeleteInvoiceInfo^GetProductListByCategory^GetNewInvoiceInfoDetail^GetNewInvoiceInfoDetail', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (12, N'AllSettingController', N'0000012   ', N'Settings', N'EmployeeList^SaveEmployee^DeleteEmployee^SupplierPartyList^DealerPartyList^SaveParty^DeleteParty^AreaList^SaveArea^DeleteArea^BankList^SaveBank^DeleteBank', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (13, N'AllStockController', N'0000013   ', N'Stock', N'Index^CurrentStockList^GEtProductStock', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (14, N'AllTemplateController', N'0000014   ', N'Template', N'List^Register^Modify^GetPermissionList', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (15, N'AllTransactionController', N'0000015   ', N'Transaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (16, N'AllUserController', N'0000016   ', N'UserCommon', N'List^Register', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (17, N'AllJsonController', N'0000016   ', N'JsonList', N'Index^LoadProductByCategory^LoadProductStock^LoadProductStockSell', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (18, N'InventoryWithoutProduct', NULL, N'Inventory', N'StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (19, N'AllJobExpenseController', N'0000019   ', N'JobExpense', N'JobExpenseList^SaveExpense^SaveExpense^DeleteExpense^OnChangeLoadDetail', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (20, N'AllowedReport', N'0000020   ', N'Report', N'SalesReport^PurchaseReport^OpeningStock^ClosingStock^PartyLedgerJobReport^JobCostingReport^InvoiceReport', 1, NULL)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (21, N'AllTransactionController', N'0000015   ', N'OpeningTransaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Permission] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

GO
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode]) VALUES (1, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, 1, N'assdasd', CAST(12.00 AS Decimal(18, 2)), NULL, CAST(1212.00 AS Decimal(18, 2)), N'assdasd')
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[TableProperty] ON 

GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (1, N' Ledger ', N'TrialVisible', 1, N' InVisible on Ledger ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (2, N' InvoiceInfo ', N'SalesInvoiceType', 1, N' Sales ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (3, N' InvoiceInfo ', N'SalesInvoiceType', 2, N' SalesReplace ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (4, N' InvoiceInfo ', N'SalesInvoiceType', 3, N' Sales Return ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (5, N' InvoiceInfo ', N'PurchaseInvoiceType', 1, N' Purchase ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (6, N' InvoiceInfo ', N'PurchaseInvoiceType', 2, N' Purchase Return ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (7, N' InvoiceInfo ', N'PurchaseInvoiceType', 3, N' Expire Date ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (8, N' InvoiceInfo ', N'PurchaseInvoiceType', 4, N' Bonus ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (9, N' Salary ', N'SalaryType', 1, N' Current Salary ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (10, N' Salary ', N'SalaryType', 2, N' Advance Salary ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (11, N' Product ', N'ProductType', 1, N' Finished Product ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (12, N' Product ', N'ProductType', 2, N' Raw Rpoduct ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (13, N' Ledger ', N'PartyType', 1, N' Supplier Party ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (14, N' Ledger ', N'PartyType', 2, N' Client Party ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (15, N' Ledger ', N'PartyType', 3, N' Local Supplier ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (16, N' Ledger ', N'PartyType', 4, N' Local Client ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (17, N' GroupHeader ', N'HeaderType', 1, N' Debit/To ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (18, N' GroupHeader ', N'HeaderType', 2, N' Credit/From ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (19, N' LedgerGroup ', N'GroupType', 1, N' Cash Transaction ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (20, N' LedgerGroup ', N'GroupType', 2, N' Purchase Ldegers ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (21, N' LedgerGroup ', N'GroupType', 3, N' Sales Ledger ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (22, N' LedgerGroup ', N'GroupType', 4, N' SalaryLedger ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (23, N' LedgerGroup ', N'GroupType', 5, N' Inventory ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (24, N' LedgerGroup ', N'GroupType', 6, N' Profit/ Loss ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (25, N' LedgerGroup ', N'GroupType', 7, N' Capital ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (26, N'Payment ', N'PaymentStatus', 1, N'Paid')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (27, N'Payment ', N'PaymentStatus', 2, N'Pending')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (28, N'Payment', N'TransactionType', 1, N'Payment')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (29, N'Payment', N'TransactionType', 2, N'Receive')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (30, N' GroupHeader ', N'AccountsType', 1, N' Asset ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (31, N' GroupHeader ', N'AccountsType', 2, N' Liability ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (32, N' GroupHeader ', N'AccountsType', 3, N' Equity ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (33, N' GroupHeader ', N'AccountsType', 4, N' Revenue ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (34, N' GroupHeader ', N'AccountsType', 5, N' Expense ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (35, N' GroupHeader ', N'AccountsType', 6, N' Drwaings ')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (36, N'InvoiceDetails', N'SalesType', 1, N'Gift')
GO
SET IDENTITY_INSERT [dbo].[TableProperty] OFF
GO
SET IDENTITY_INSERT [dbo].[Template] ON 

GO
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (1, N'Admiin', N'1^2^3^4^5^6^7^8^9^10^11^12^13^15^17^21', 1, 0, CAST(N'2016-08-25' AS Date), 0, CAST(N'2016-09-04' AS Date), 1)
GO
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (2, N'testAdmin', N'1^2^3^4', 1, 0, CAST(N'2016-09-02' AS Date), NULL, NULL, 1)
GO
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (3, N'Operator', N'1^2^3^4^5^7^9^10^11^12^15^17^18', 1, 1, CAST(N'2016-09-02' AS Date), 1, CAST(N'2016-09-02' AS Date), 1)
GO
SET IDENTITY_INSERT [dbo].[Template] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

GO
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (1, N'ROOT', N'123456', 1, 0, NULL, 1, NULL, 1, CAST(N'2016-03-24' AS Date), NULL, NULL, 0, 0, 0, NULL, N'Admin', NULL)
GO
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (2, N'Badal', N'123', 1, 1, CAST(N'2016-04-07 17:45:45.760' AS DateTime), 1, NULL, 1, CAST(N'2016-03-28' AS Date), NULL, NULL, 1, 1, 1, NULL, N'Badal', 1)
GO
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (3, N'a', N'1', 1, 1, CAST(N'2017-04-26 00:00:00.000' AS DateTime), 1, N'null', 1, CAST(N'2016-08-08' AS Date), 0, CAST(N'2016-09-02' AS Date), 1, 1, 1, NULL, N'Operator', NULL)
GO
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [Name], [IsCreator]) VALUES (4, N'Operator', N'op123456', 1, 1, CAST(N'2017-01-16 10:41:09.367' AS DateTime), 1, NULL, 0, CAST(N'2016-09-01' AS Date), NULL, NULL, 3, 1, 1, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
