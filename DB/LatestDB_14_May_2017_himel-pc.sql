USE [master]
GO
/****** Object:  Database [RedGreen]    Script Date: 5/14/2017 12:49:57 AM ******/
CREATE DATABASE [RedGreen]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'RedGreen', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\RedGreen.mdf' , SIZE = 4096KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'RedGreen_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.SQL2014\MSSQL\DATA\RedGreen_log.ldf' , SIZE = 1024KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [RedGreen] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [RedGreen].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [RedGreen] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [RedGreen] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [RedGreen] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [RedGreen] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [RedGreen] SET ARITHABORT OFF 
GO
ALTER DATABASE [RedGreen] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [RedGreen] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [RedGreen] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [RedGreen] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [RedGreen] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [RedGreen] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [RedGreen] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [RedGreen] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [RedGreen] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [RedGreen] SET  DISABLE_BROKER 
GO
ALTER DATABASE [RedGreen] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [RedGreen] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [RedGreen] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [RedGreen] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [RedGreen] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [RedGreen] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [RedGreen] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [RedGreen] SET RECOVERY FULL 
GO
ALTER DATABASE [RedGreen] SET  MULTI_USER 
GO
ALTER DATABASE [RedGreen] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [RedGreen] SET DB_CHAINING OFF 
GO
ALTER DATABASE [RedGreen] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [RedGreen] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [RedGreen] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'RedGreen', N'ON'
GO
USE [RedGreen]
GO
/****** Object:  Table [dbo].[Application]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AppName] [nvarchar](50) NOT NULL,
	[LogoUrl] [nvarchar](50) NULL,
	[Slogon] [nvarchar](max) NULL,
	[ValidateDate] [date] NOT NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Application_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](200) NOT NULL,
	[Description] [nvarchar](200) NULL,
	[IsParent] [bit] NOT NULL,
	[ParentId] [bigint] NULL,
	[BrandName] [nvarchar](200) NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Currency]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Currency](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[CurrencyName] [nvarchar](50) NOT NULL,
	[ShortName] [nchar](10) NOT NULL,
	[Symbol] [nvarchar](15) NOT NULL,
	[Country] [nvarchar](50) NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Currencies] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeName] [nvarchar](100) NOT NULL,
	[EmployeeCode] [nvarchar](100) NULL,
	[Designation] [nvarchar](100) NULL,
	[Department] [nvarchar](100) NULL,
	[Location] [nvarchar](100) NULL,
	[DateOfJoining] [datetime] NULL,
	[DateOfBirth] [datetime] NULL,
	[PFNo] [nvarchar](50) NULL,
	[ESINo] [nvarchar](50) NULL,
	[IsSR] [bit] NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupHeader]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupHeader](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[AccountsType] [bigint] NULL,
	[IsParent] [bit] NOT NULL,
 CONSTRAINT [PK_GroupHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[PropertyId] [bigint] NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[ProductSerial] [nvarchar](50) NOT NULL CONSTRAINT [DF_InvoiceDetails_ProductSerial]  DEFAULT ((0)),
	[Warranty] [int] NOT NULL,
	[SalesFlag] [bigint] NOT NULL CONSTRAINT [DF_InvoiceDetails_SalesFlag]  DEFAULT ((0)),
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_BillDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceInfo]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceInfo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceNo] [nvarchar](100) NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[SubTotal] [decimal](18, 0) NOT NULL,
	[ServiceCharge] [decimal](18, 0) NULL,
	[Discount] [decimal](18, 2) NULL,
	[Vat] [decimal](18, 2) NULL,
	[GrandTotal] [decimal](18, 0) NOT NULL,
	[InvoiceReferenceId] [bigint] NULL,
	[PropertyId] [bigint] NOT NULL,
	[Remarks] [nvarchar](max) NULL,
	[InvoiceDate] [datetime] NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
	[PartyName] [nvarchar](500) NULL,
	[PartyAddress] [nvarchar](2000) NULL,
	[ProcessBy] [bigint] NOT NULL,
	[ApprovedBy] [bigint] NOT NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_BillingTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ledger]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledger](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[LedgerGroupId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NOT NULL,
	[Code] [nvarchar](max) NULL,
	[ContactPerson] [nvarchar](max) NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[Mobile1] [nvarchar](max) NULL,
	[Mobile2] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Visible] [bigint] NULL,
 CONSTRAINT [PK_Ledger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LedgerGroup]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LedgerGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[GroupHeaderId] [bigint] NOT NULL,
	[IsParent] [bit] NOT NULL CONSTRAINT [DF_TransactionHeader_IsParent]  DEFAULT ((0)),
 CONSTRAINT [PK_LedgerGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[PaymentType] [bigint] NOT NULL,
	[Remarks] [nvarchar](200) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
	[BankName] [nvarchar](100) NULL,
	[AccountNo] [nvarchar](100) NULL,
	[ReferenceNo] [nvarchar](100) NULL,
	[CheckNo] [nvarchar](100) NULL,
	[CheckDate] [date] NULL,
	[PaymentStatus] [bigint] NOT NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_Payment_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[PermissionCode] [nchar](10) NULL,
	[ControllerName] [nvarchar](20) NULL,
	[ActionName] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[ProductCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Salary]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeId] [bigint] NOT NULL,
	[SalryType] [bigint] NULL,
	[Month] [datetime] NULL,
	[TotalSalary] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[FestivalBonus] [decimal](18, 0) NULL,
	[TotalBill] [decimal](18, 0) NULL,
	[MedicalAllowance] [decimal](18, 0) NULL,
	[OtherAllowance] [decimal](18, 0) NULL,
	[OtherDeduction] [decimal](18, 0) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TableProperty]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableProperty](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](50) NULL,
	[PropertyName] [nvarchar](50) NULL,
	[PropertyValue] [int] NULL,
	[ViewName] [nvarchar](100) NULL,
 CONSTRAINT [PK_TableProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Template]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Template](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[PermissionIds] [nvarchar](max) NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Role_Status]  DEFAULT ((1)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[DebitHeaderId] [bigint] NOT NULL,
	[CreditHeaderId] [bigint] NOT NULL,
	[Amount] [decimal](18, 2) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionType] [bigint] NOT NULL,
	[CheckNo] [nvarchar](50) NULL,
	[EmployeeId] [bigint] NULL,
	[Remarks] [nvarchar](max) NULL,
	[BankId] [bigint] NULL,
	[CashType] [int] NULL,
	[ConversionRate] [decimal](18, 2) NOT NULL,
	[Currency] [bigint] NOT NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Unit]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unit](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Symbol] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_User_IsActive]  DEFAULT ((1)),
	[Islogin] [bit] NOT NULL CONSTRAINT [DF_User_Islogin]  DEFAULT ((0)),
	[LastLogin] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_User_Status]  DEFAULT ((1)),
	[PhotoUrl] [nvarchar](50) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[TemplateId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[ApplicationId] [bigint] NOT NULL,
	[UserMessage] [nvarchar](max) NULL,
	[Name] [nvarchar](200) NULL,
	[IsCreator] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 5/14/2017 12:49:57 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[ProfileId] [bigint] NULL,
	[RoleId] [bigint] NULL,
	[LoginDateTime] [datetime] NULL,
	[IpAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
USE [master]
GO
ALTER DATABASE [RedGreen] SET  READ_WRITE 
GO
