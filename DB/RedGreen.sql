USE [RedGreen]
GO
/****** Object:  Table [dbo].[Application]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AppName] [nvarchar](50) NOT NULL,
	[LogoUrl] [nvarchar](50) NULL,
	[Slogon] [nvarchar](max) NULL,
	[ValidateDate] [date] NOT NULL,
	[Status] [bit] NOT NULL CONSTRAINT [DF_Application_Status]  DEFAULT ((1)),
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Category]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[IsParent] [bit] NOT NULL,
	[ParentId] [bigint] NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Employee]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeName] [nvarchar](100) NOT NULL,
	[EmployeeCode] [nvarchar](100) NULL,
	[Designation] [nvarchar](100) NULL,
	[Department] [nvarchar](100) NULL,
	[Location] [nvarchar](100) NULL,
	[DateOfJoining] [datetime] NULL,
	[DateOfBirth] [datetime] NULL,
	[PFNo] [nvarchar](50) NULL,
	[ESINo] [nvarchar](50) NULL,
	[IsSR] [bit] NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GroupHeader]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupHeader](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[AccountsType] [bigint] NULL,
	[IsParent] [bit] NOT NULL,
 CONSTRAINT [PK_GroupHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[PropertyId] [bigint] NOT NULL CONSTRAINT [DF_InvoiceDetails_PropertyId]  DEFAULT ((0)),
	[BuyingPrice] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_BillDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InvoiceInfo]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceInfo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceNo] [nvarchar](100) NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[SRId] [bigint] NOT NULL,
	[SubTotal] [decimal](18, 0) NOT NULL,
	[GrandTotal] [decimal](18, 0) NOT NULL,
	[InvoiceReferenceId] [bigint] NULL,
	[PropertyId] [bigint] NOT NULL,
	[ReturnTotal] [decimal](18, 0) NULL,
	[Remarks] [nvarchar](max) NULL,
	[InvoiceDate] [datetime] NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
 CONSTRAINT [PK_BillingTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Ledger]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledger](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
	[LedgerGroupId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NOT NULL,
	[Code] [nvarchar](max) NULL,
	[ContactPerson] [nvarchar](max) NULL,
	[Address1] [nvarchar](max) NULL,
	[Address2] [nvarchar](max) NULL,
	[Mobile1] [nvarchar](max) NULL,
	[Mobile2] [nvarchar](max) NULL,
	[Email] [nvarchar](max) NULL,
	[Visible] [bigint] NULL,
 CONSTRAINT [PK_Ledger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LedgerGroup]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LedgerGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[GroupHeaderId] [bigint] NOT NULL,
	[IsParent] [bit] NOT NULL CONSTRAINT [DF_TransactionHeader_IsParent]  DEFAULT ((0)),
 CONSTRAINT [PK_LedgerGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Payment]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [decimal](18, 2) NOT NULL,
	[PaymentType] [bigint] NOT NULL,
	[Remarks] [nvarchar](200) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
 CONSTRAINT [PK_Payment_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Permission]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[PermissionCode] [nchar](10) NULL,
	[ControllerName] [nvarchar](20) NULL,
	[ActionName] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Product]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NULL,
	[SellingPrice] [decimal](18, 2) NOT NULL,
	[ProductCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Salary]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeId] [bigint] NOT NULL,
	[SalryType] [bigint] NULL,
	[Month] [datetime] NULL,
	[TotalSalary] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[FestivalBonus] [decimal](18, 0) NULL,
	[TotalBill] [decimal](18, 0) NULL,
	[MedicalAllowance] [decimal](18, 0) NULL,
	[OtherAllowance] [decimal](18, 0) NULL,
	[OtherDeduction] [decimal](18, 0) NULL,
	[DebitId] [bigint] NOT NULL,
	[CreditId] [bigint] NOT NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[TableProperty]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableProperty](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](50) NULL,
	[PropertyName] [nvarchar](50) NULL,
	[PropertyValue] [int] NULL,
	[ViewName] [nvarchar](100) NULL,
 CONSTRAINT [PK_TableProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Template]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Template](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[PermissionIds] [nvarchar](max) NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_Role_Status]  DEFAULT ((1)),
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[DebitHeaderId] [bigint] NOT NULL,
	[CreditHeaderId] [bigint] NOT NULL,
	[Amount] [decimal](18, 0) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionType] [bigint] NOT NULL,
	[CheckNo] [nvarchar](50) NULL,
	[EmployeeId] [bigint] NULL,
	[Remarks] [nvarchar](max) NULL,
	[BankId] [bigint] NULL,
	[CashType] [int] NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Unit]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Unit](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](20) NOT NULL,
	[Description] [nvarchar](100) NOT NULL,
	[Symbol] [nvarchar](10) NOT NULL,
 CONSTRAINT [PK_Unit] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[User]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL CONSTRAINT [DF_User_IsActive]  DEFAULT ((1)),
	[Islogin] [bit] NOT NULL CONSTRAINT [DF_User_Islogin]  DEFAULT ((0)),
	[LastLogin] [datetime] NULL,
	[Status] [int] NOT NULL CONSTRAINT [DF_User_Status]  DEFAULT ((1)),
	[PhotoUrl] [nvarchar](50) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[TemplateId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[ApplicationId] [bigint] NOT NULL,
	[UserMessage] [nvarchar](max) NULL,
	[PasswordAttempt] [int] NULL,
	[IsCreator] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 20/04/2017 23:12:48 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[ProfileId] [bigint] NULL,
	[RoleId] [bigint] NULL,
	[LoginDateTime] [datetime] NULL,
	[IpAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET IDENTITY_INSERT [dbo].[Application] ON 

GO
INSERT [dbo].[Application] ([Id], [AppName], [LogoUrl], [Slogon], [ValidateDate], [Status]) VALUES (1, N'DentalCorner', N'dental.jpg', N'Rifat Abdullah', CAST(N'2016-08-26' AS Date), 1)
GO
SET IDENTITY_INSERT [dbo].[Application] OFF
GO
SET IDENTITY_INSERT [dbo].[Category] ON 

GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (1, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Glass Innomer  Filling                                 ', N'GI', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (2, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Light Cure Filling Materials', N'LC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (3, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Temporary Filling Materials', N'TF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (4, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Permanent  Filling Materials', N'PF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (5, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Bonding Agent', N'BA', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (6, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Etching Gel', N'EG', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (7, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Cavity Linner', N'CL', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (8, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Cavity Vernisher', N'CV', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (9, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Glass Innomer Cement', N'GC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (10, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Pulp Devitalizer Materials', N'PD', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (11, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Infacted Root Canal Materials', N'IRC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (12, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Severe Infacted Root Canal Materials', N'SIRC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (13, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Canal Enlarger/ Widder', N'CE', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (14, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Root Canal Sealer', N'RCS', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (15, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Gutta Percha Point', N'GP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (16, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Reamer File (SS) 21 mm, Mani', N'RF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (17, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Reamer File (SS), 25mm, Mani', N'RF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (18, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Reamer File (SS) 21mm, Diadent', N'RF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (19, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Reamer File (SS) 25mm, Diadent', N'RF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (20, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Reamer File (Niti) 25mm, Diadent', N'RF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (21, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Reamer File (Niti) 21mm, Diadent', N'RF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (22, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'H- File (SS) 21mm,Mani', N'H-File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (23, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'H- File (SS) 25mm,Mani', N'H-File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (24, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'H- File (SS) 21mm,Diadent', N'H- File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (25, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'H- File (SS) 25mm,Diadent', N'H- File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (26, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'H- File (Niti) 21mm,Diadent', N'H-  File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (27, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'H- File (Niti) 25mm,Diadent', N'H-File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (28, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'K- File (SS) 21mm,Diadent', N'K-File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (29, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'K- File (SS) 21mm, Mani', N'K-File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (30, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'K- File (SS) 25mm, Mani', N'K- File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (31, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'K- File (SS) 25mm, Diadent', N'K- File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (32, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'K- File (Niti) 21mm, Diadent', N'K-File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (33, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'K- File (Niti) 25mm, Diadent', N'K- File', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (34, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Broach, Mani', N'BR', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (35, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Broach, Diadent', N'BR', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (36, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Broach, Germany', N'BR', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (37, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Broach, China', N'BR', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (38, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Spreader, Mani', N'SP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (39, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Spreader, Diadent', N'SP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (40, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Round), Mani, Japan', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (41, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Inverted), Mani, Japan', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (42, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Fissher), Mani, Japan', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (43, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Round), Mani, China', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (44, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Fissher), Mani, China', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (45, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Inverted), Mani, China', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (46, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Round) USA', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (47, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Inverted) USA', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (48, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Dia Bur (Fissher) USA', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (49, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Polishing Bur, Mani, China', N'PB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (50, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Polishing Bur, Mani, Japan', N'PB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (51, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Disk Bur, Mani', N'DB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (52, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Turbain Bur', N'TB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (53, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Polishing Paste, Propol, DPI', N'PP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (54, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Polishing Brush', N'PB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (55, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Tooth Stain Remover, Sumit', N'TSR', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (56, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Scaler Tips, ART, Taiwan', N'ST', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (57, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Scaler Tips, Woodpaker, China', N'ST', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (58, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Scaler Tips,  P4, China', N'ST', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (59, 3, 0, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Scaler Tips, Scalex, USA', N'ST', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (60, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Alginate, Lygin', N'ALG', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (61, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Alginate, Tropicalgin', N'ALG', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (62, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Die Stone, Dentamerica', N'DS', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (63, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Die Stone, Snow Rock', N'DS', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (64, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Normal Plaster', N'NP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (65, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Impression Tray (SS)', N'IT', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (66, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Impression Tray (Plastic)', N'IT', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (67, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Impression Tray (Plastis Set), 08 Set', N'IT', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (68, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Side Tray Plastic', N'ST', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (69, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Denture Plate', N'DP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (70, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Local Cartridge (Glass), Zeyco', N'LC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (71, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Local Cartridge (Plastic), Zeyco', N'LC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (72, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Local Vial ( Jesocaine A 2 %)', N'LV', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (73, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Local Vial ( G- Lidocaine 2%)', N'LV', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (74, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Local Vial ( Lignox 2 %)', N'LV', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (75, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Anesthesia Gel, Easy Caine 2 %', N'ANG', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (76, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Anesthesia Gel, Easy Caine 5 %', N'ANG', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (77, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Lidayn Spray', N'LS', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (78, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Nummit Spray', N'NS', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (79, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'BD Com', N'BDC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (80, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Face Mask, Dochem', N'FC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (81, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Face Mask, Mediklin', N'FC', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (82, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Suction Tips, Dentamerica, USA', N'ST', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (83, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Suction Tips, Dochem (Blue), China', N'ST', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (84, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Silk 3.0 + Needle, Mani', N'SN', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (85, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Silk 3.0 (Focal)', N'SF', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (86, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Suture Needle', N'SN', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (87, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Cellophane Stips, Dentamerica', N'CS', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (88, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Screw Post, China', N'SP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (89, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Screw Post, Japan', N'SP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (90, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'BP Blade / Surgical Blade', N'BPB', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (91, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Cotton (BSMI)', N'CT', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (92, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Dental Needle, Japan', N'DN', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (93, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Dental Needle, Korea', N'DN', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (94, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Hydrogen Peroxide, Momtaj', N'HGP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (95, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Hydrogen Peroxide, Jolly', N'HGP', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (96, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth (06 Set), PAK', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (97, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth (08 Set), PAK', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (98, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( Full Set), PAK', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (99, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( 04 Set), China', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (100, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth (06 Set), China', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (101, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth (08 Set), China', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (102, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( Full Set), China', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (103, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( 04 Set), Japan', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (104, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( 06 Set), Japan', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (105, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( 08 Set), Japan', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (106, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( Full Set), Japan', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (107, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( 04 Set), India', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (108, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( 06 Set), India', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (109, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( 08 Set), India', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (110, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Teeth ( Full Set), India', N'T', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (111, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Composition Materials', N'CM', 0, NULL)
GO
INSERT [dbo].[Category] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [IsParent], [ParentId]) VALUES (112, 3, 0, CAST(N'2017-03-05 00:00:00.000' AS DateTime), CAST(N'2017-03-05 00:00:00.000' AS DateTime), 1, 0, N'Modeling Wax', N'MW', 0, NULL)
GO
SET IDENTITY_INSERT [dbo].[Category] OFF
GO
SET IDENTITY_INSERT [dbo].[Employee] ON 

GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (1, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, N'Ahsanul Habib', N'001', N'Manager, Adminstration', N'Administration', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (2, 3, 3, CAST(N'2016-09-06 21:44:17.810' AS DateTime), CAST(N'2016-09-06 21:44:17.810' AS DateTime), 1, 0, N'Md.Nuruzzaman', N'002', N'Manager, Accounts and Finance', N'Accounts and Finance', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, CAST(2222 AS Decimal(18, 0)), CAST(3333 AS Decimal(18, 0)), CAST(4444 AS Decimal(18, 0)), CAST(1111 AS Decimal(18, 0)))
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (3, 3, 3, CAST(N'2016-09-18 16:26:57.527' AS DateTime), CAST(N'2016-09-18 16:26:57.527' AS DateTime), 1, 0, N'Alok kumar Ghosh', N'004', N'Executive, marketing', N'Marketing', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (4, 3, 3, CAST(N'2016-09-18 16:26:43.107' AS DateTime), CAST(N'2016-09-18 16:26:43.107' AS DateTime), 1, 0, N'Moniruzzaman', N'003', N'Senior Executive, Marketing', N'Marketing', N'faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (5, 1, 1, CAST(N'2016-06-11 15:16:07.187' AS DateTime), CAST(N'2016-06-11 15:16:07.187' AS DateTime), 1, 0, N'Prosanto Halder', N'005', N'Lab In charge', N'Lab', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (6, 1, 1, CAST(N'2016-06-11 15:17:16.323' AS DateTime), CAST(N'2016-06-11 15:17:16.323' AS DateTime), 1, 0, N'Shamim', N'006', N'Assistant Lab In charge', N'Lab', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (7, 1, 1, CAST(N'2016-06-11 15:18:51.107' AS DateTime), CAST(N'2016-06-11 15:18:51.107' AS DateTime), 1, 0, N'Victor Sarker', N'007', N'Wax pattern Technician', N'Lab', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (8, 1, 1, CAST(N'2016-06-11 15:20:06.017' AS DateTime), CAST(N'2016-06-11 15:20:06.017' AS DateTime), 1, 0, N'S M Sariful Islam', N'008', N'Officer, Accounts', N'Lab', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (9, 1, 1, CAST(N'2016-06-11 15:21:26.247' AS DateTime), CAST(N'2016-06-11 15:21:26.247' AS DateTime), 1, 0, N'Md.Jahidul Islam', N'009', N'Office Assistant cum Driver', N'Lab', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (10, 1, 1, CAST(N'2016-06-11 15:22:35.943' AS DateTime), CAST(N'2016-06-11 15:22:35.943' AS DateTime), 1, 0, N'Alam', N'011', N'Office Attandent', N'Lab', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (11, 1, 1, CAST(N'2016-06-11 15:24:23.033' AS DateTime), CAST(N'2016-06-11 15:24:23.033' AS DateTime), 1, 0, N'Minal Sheikh', N'012', N'Office Attendant', N'Admin', N'Faridpur', CAST(N'2016-06-11 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (12, 1, 1, CAST(N'2016-06-12 15:51:18.647' AS DateTime), CAST(N'2016-06-12 15:51:18.647' AS DateTime), 1, 0, N'Md.Jahidul Islam', N'010', N'Office Assistant cum Driver', N'Lab', N'Faridpur', CAST(N'2016-06-12 00:00:00.000' AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Employee] OFF
GO
SET IDENTITY_INSERT [dbo].[GroupHeader] ON 

GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (1, 1, 1, CAST(N'2016-07-17 02:35:39.107' AS DateTime), CAST(N'2016-07-17 02:35:39.260' AS DateTime), 1, 0, N'Fixed Assets', NULL, 12, 24, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (2, 1, 1, CAST(N'2016-07-17 02:36:23.837' AS DateTime), CAST(N'2016-07-17 02:36:23.837' AS DateTime), 1, 0, N'Current Assets', NULL, 12, 24, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (3, 1, 1, CAST(N'2016-07-17 02:36:32.543' AS DateTime), CAST(N'2016-07-17 02:36:32.543' AS DateTime), 1, 0, N' Liabilities', NULL, 13, 25, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (4, 1, 1, CAST(N'2016-07-17 02:36:40.777' AS DateTime), CAST(N'2016-07-17 02:36:40.777' AS DateTime), 1, 0, N'Equity', NULL, 13, 26, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (5, 1, 1, CAST(N'2016-07-17 02:36:48.910' AS DateTime), CAST(N'2016-07-17 02:36:48.910' AS DateTime), 1, 0, N'Revenue', NULL, 13, 27, 1)
GO
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (6, 1, 1, CAST(N'2016-07-17 02:36:56.413' AS DateTime), CAST(N'2016-07-17 02:36:56.413' AS DateTime), 1, 0, N'Expense', NULL, 12, 28, 1)
GO
SET IDENTITY_INSERT [dbo].[GroupHeader] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetails] ON 

GO
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice]) VALUES (1, 3, 3, CAST(N'2017-04-11 00:00:00.000' AS DateTime), CAST(N'2017-04-11 00:00:00.000' AS DateTime), 1, 0, 1, 1, CAST(650.00 AS Decimal(18, 2)), CAST(12.00 AS Decimal(18, 2)), CAST(6600.00 AS Decimal(18, 2)), 1, CAST(550.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice]) VALUES (2, 3, 3, CAST(N'2017-04-11 00:00:00.000' AS DateTime), CAST(N'2017-04-11 00:00:00.000' AS DateTime), 1, 0, 2, 1, CAST(650.00 AS Decimal(18, 2)), CAST(12.00 AS Decimal(18, 2)), CAST(6600.00 AS Decimal(18, 2)), 1, CAST(550.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice]) VALUES (3, 3, 3, CAST(N'2017-04-11 00:00:00.000' AS DateTime), CAST(N'2017-04-11 00:00:00.000' AS DateTime), 1, 0, 3, 1, CAST(650.00 AS Decimal(18, 2)), CAST(5.00 AS Decimal(18, 2)), CAST(3250.00 AS Decimal(18, 2)), 2, CAST(550.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice]) VALUES (4, 3, 3, CAST(N'2017-04-20 00:00:00.000' AS DateTime), CAST(N'2017-04-20 00:00:00.000' AS DateTime), 1, 0, 4, 1, CAST(650.00 AS Decimal(18, 2)), CAST(10.00 AS Decimal(18, 2)), CAST(5500.00 AS Decimal(18, 2)), 1, CAST(550.00 AS Decimal(18, 2)))
GO
INSERT [dbo].[InvoiceDetails] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceId], [ProductId], [SellingPrice], [Quantity], [TotalPrice], [PropertyId], [BuyingPrice]) VALUES (5, 3, 3, CAST(N'2017-04-20 00:00:00.000' AS DateTime), CAST(N'2017-04-20 00:00:00.000' AS DateTime), 1, 0, 4, 3, CAST(250.00 AS Decimal(18, 2)), CAST(15.00 AS Decimal(18, 2)), CAST(3000.00 AS Decimal(18, 2)), 1, CAST(200.00 AS Decimal(18, 2)))
GO
SET IDENTITY_INSERT [dbo].[InvoiceDetails] OFF
GO
SET IDENTITY_INSERT [dbo].[InvoiceInfo] ON 

GO
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SRId], [SubTotal], [GrandTotal], [InvoiceReferenceId], [PropertyId], [ReturnTotal], [Remarks], [InvoiceDate], [DebitId], [CreditId]) VALUES (1, 3, 3, CAST(N'2017-04-11 00:00:00.000' AS DateTime), CAST(N'2017-04-11 00:00:00.000' AS DateTime), 1, 0, N'1', 22, 0, CAST(6600 AS Decimal(18, 0)), CAST(6600 AS Decimal(18, 0)), NULL, 1, NULL, NULL, CAST(N'2017-04-11 00:00:00.000' AS DateTime), 2, 22)
GO
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SRId], [SubTotal], [GrandTotal], [InvoiceReferenceId], [PropertyId], [ReturnTotal], [Remarks], [InvoiceDate], [DebitId], [CreditId]) VALUES (2, 3, 3, CAST(N'2017-04-11 00:00:00.000' AS DateTime), CAST(N'2017-04-11 00:00:00.000' AS DateTime), 1, 0, N'2', 22, 0, CAST(6600 AS Decimal(18, 0)), CAST(6600 AS Decimal(18, 0)), NULL, 1, NULL, NULL, CAST(N'2017-04-11 00:00:00.000' AS DateTime), 2, 22)
GO
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SRId], [SubTotal], [GrandTotal], [InvoiceReferenceId], [PropertyId], [ReturnTotal], [Remarks], [InvoiceDate], [DebitId], [CreditId]) VALUES (3, 3, 3, CAST(N'2017-04-11 00:00:00.000' AS DateTime), CAST(N'2017-04-11 00:00:00.000' AS DateTime), 1, 0, N'1', 23, 0, CAST(3250 AS Decimal(18, 0)), CAST(3250 AS Decimal(18, 0)), NULL, 2, NULL, NULL, CAST(N'2017-04-11 00:00:00.000' AS DateTime), 23, 1)
GO
INSERT [dbo].[InvoiceInfo] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [InvoiceNo], [PartyId], [SRId], [SubTotal], [GrandTotal], [InvoiceReferenceId], [PropertyId], [ReturnTotal], [Remarks], [InvoiceDate], [DebitId], [CreditId]) VALUES (4, 3, 3, CAST(N'2017-04-20 00:00:00.000' AS DateTime), CAST(N'2017-04-20 00:00:00.000' AS DateTime), 1, 0, N'3', 22, 0, CAST(8500 AS Decimal(18, 0)), CAST(8500 AS Decimal(18, 0)), NULL, 1, NULL, NULL, CAST(N'2017-04-20 00:00:00.000' AS DateTime), 2, 22)
GO
SET IDENTITY_INSERT [dbo].[InvoiceInfo] OFF
GO
SET IDENTITY_INSERT [dbo].[Ledger] ON 

GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (3, 1, 1, CAST(N'2016-08-20 20:55:33.887' AS DateTime), CAST(N'2016-08-20 20:55:33.887' AS DateTime), 1, 0, N'Sales', 10, NULL, 18, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (4, 1, 1, CAST(N'2016-08-20 20:55:48.787' AS DateTime), CAST(N'2016-08-20 20:55:48.787' AS DateTime), 1, 0, N'Purchase', 11, NULL, 17, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (5, 1, 1, CAST(N'2016-08-21 00:09:15.057' AS DateTime), CAST(N'2016-08-21 00:09:15.057' AS DateTime), 1, 0, N'Salary Expense', 12, NULL, 19, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (9, 1, 1, CAST(N'2016-08-22 00:15:42.983' AS DateTime), CAST(N'2016-08-22 00:15:42.983' AS DateTime), 1, 0, N'Opening Stock', 13, NULL, 22, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (14, 1, 1, CAST(N'2016-08-25 15:39:51.237' AS DateTime), CAST(N'2016-08-25 15:39:51.237' AS DateTime), 1, 0, N'Profit and Loss A/C', 14, NULL, 23, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (17, 1, 1, CAST(N'2016-08-26 11:38:41.020' AS DateTime), CAST(N'2016-08-26 11:38:41.020' AS DateTime), 1, 0, N'Product Lost', 17, NULL, 30, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (18, 1, 1, CAST(N'2016-08-26 11:38:51.800' AS DateTime), CAST(N'2016-08-26 11:38:51.800' AS DateTime), 1, 0, N'Product Bonus', 19, NULL, 31, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (20, 1, 1, CAST(N'2016-08-28 19:35:18.497' AS DateTime), CAST(N'2016-08-28 19:35:18.497' AS DateTime), 1, 0, N'Capital Drawing', 21, NULL, 32, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (21, 3, 3, CAST(N'2017-03-30 00:00:00.000' AS DateTime), CAST(N'2017-03-30 00:00:00.000' AS DateTime), 0, 0, N'Monojit ', 2, NULL, 11, N'Monojit   ', NULL, NULL, NULL, NULL, NULL, NULL, 0)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (22, 3, 3, CAST(N'2017-03-30 00:00:00.000' AS DateTime), CAST(N'2017-03-30 00:00:00.000' AS DateTime), 1, 0, N'Raw World', 1, NULL, 10, N'Raw World ', N'Zannatul Ferdousi', N'Dhaka', NULL, N'01716603392', NULL, NULL, 0)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (23, 3, 3, CAST(N'2017-03-31 00:00:00.000' AS DateTime), CAST(N'2017-03-31 00:00:00.000' AS DateTime), 1, 0, N'Test ', 2, NULL, 11, N'Test      ', N'Test', N'Test', NULL, N'Test', NULL, N'Test', 0)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (24, 3, 3, CAST(N'2017-04-01 00:00:00.000' AS DateTime), CAST(N'2017-04-01 00:00:00.000' AS DateTime), 1, 0, N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', 1, NULL, 10, N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', NULL, N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', NULL, N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', 0)
GO
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (25, 3, 3, CAST(N'2017-04-01 00:00:00.000' AS DateTime), CAST(N'2017-04-01 00:00:00.000' AS DateTime), 1, 0, N'This is  the new tessdf t for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', 2, NULL, 11, N'This is  the new tessdf t for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', NULL, N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', NULL, N'This is  the new test for adding supplier with large text  for checking   long text getting inputted lkdfjsd flsdkjf sdkfj slsdkjfsl dfjsdkfj ', 0)
GO
SET IDENTITY_INSERT [dbo].[Ledger] OFF
GO
SET IDENTITY_INSERT [dbo].[LedgerGroup] ON 

GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (5, 1, 1, CAST(N'2016-07-17 02:35:39.107' AS DateTime), CAST(N'2016-07-17 02:35:39.107' AS DateTime), 1, 0, N'Cash', N'Cash', 16, 1, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (6, 1, 1, CAST(N'2016-08-20 18:15:25.567' AS DateTime), CAST(N'2016-08-20 18:15:25.567' AS DateTime), 1, 0, N'Cash In Bank', NULL, 16, 1, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (8, 1, 1, CAST(N'2016-08-20 19:29:42.257' AS DateTime), CAST(N'2016-08-20 19:29:42.257' AS DateTime), 1, 0, N'Creditors', NULL, 10, 3, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (9, 1, 1, CAST(N'2016-08-20 19:30:37.870' AS DateTime), CAST(N'2016-08-20 19:30:37.870' AS DateTime), 1, 0, N'Debtors', NULL, 11, 2, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (10, 1, 1, CAST(N'2016-08-20 19:35:49.337' AS DateTime), CAST(N'2016-08-20 19:35:49.337' AS DateTime), 1, 0, N'Sales', NULL, 18, 5, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (11, 1, 1, CAST(N'2016-08-20 19:40:55.700' AS DateTime), CAST(N'2016-08-20 19:40:55.700' AS DateTime), 1, 0, N'Purchase', NULL, 17, 6, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (12, 1, 1, CAST(N'2016-08-21 00:07:04.517' AS DateTime), CAST(N'2016-08-21 00:07:04.517' AS DateTime), 1, 0, N'Salary', NULL, 19, 6, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (13, 1, 1, CAST(N'2016-08-22 00:14:16.427' AS DateTime), CAST(N'2016-08-22 00:14:16.427' AS DateTime), 1, 0, N'Opening Stock', NULL, 22, 2, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (14, 1, 1, CAST(N'2016-08-22 00:18:43.803' AS DateTime), CAST(N'2016-08-22 00:18:43.803' AS DateTime), 1, 0, N'Profit and Loss A/C', NULL, 23, 4, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (17, 1, 1, CAST(N'2016-08-26 11:35:26.453' AS DateTime), CAST(N'2016-08-26 11:35:26.453' AS DateTime), 1, 0, N'Stock Loss', NULL, 30, 6, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (19, 1, 1, CAST(N'2016-08-26 11:36:47.677' AS DateTime), CAST(N'2016-08-26 11:36:47.677' AS DateTime), 1, 0, N'Stock Bonus', NULL, 31, 5, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (21, 1, 1, CAST(N'2016-08-28 19:34:52.333' AS DateTime), CAST(N'2016-08-28 19:34:52.333' AS DateTime), 1, 0, N'Drawning', NULL, 32, 6, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (22, 3, 3, CAST(N'2016-09-18 19:34:04.983' AS DateTime), CAST(N'2016-12-14 13:52:10.437' AS DateTime), 1, 0, N'Sale', NULL, NULL, 5, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (23, 3, 3, CAST(N'2016-09-20 16:35:45.607' AS DateTime), CAST(N'2016-09-20 16:35:45.607' AS DateTime), 1, 0, N'Stationary', NULL, NULL, 6, 0)
GO
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (24, 3, 3, CAST(N'2016-09-25 02:21:03.837' AS DateTime), CAST(N'2016-09-25 02:21:11.693' AS DateTime), 0, 0, N'Test', N'ZZZ', NULL, 1, 0)
GO
SET IDENTITY_INSERT [dbo].[LedgerGroup] OFF
GO
SET IDENTITY_INSERT [dbo].[Permission] ON 

GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (1, N'AllAccountController', N'0000001   ', N'Account', N'UserList^SaveUser^DeleteUser^ChangePassword^Login^Logout^PasswordGenerator', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (2, N'AllAccountingController', N'0000002   ', N'Accounting', N'LedgerGroupList^SaveLedgerGroup^DeleteLedgerGroup^LedgerList^SaveLedger^DeleteLedger', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (3, N'AllAccountingReport', N'0000003   ', N'AccountingReport', N'AccountsLedger', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (4, N'AllCategoryController', N'0000004   ', N'Category', N'List^Register^Delete', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (5, N'AllHomeController', N'0000005   ', N'Home', N'Index^AuthFailed', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (6, N'AllInventoryController', N'0000006   ', N'Inventory', N'ProductSettingsList^SaveProductSettings^DeleteProductSettings^StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (7, N'AllPaymentController', N'0000007   ', N'Payment', N'DealerPaymentModule^SupplierPaymentModule^SavePayment^DeletePayment', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (8, N'AllPurchaseController', N'0000008   ', N'Purchase', N'PurchaseEntryList^SavePurchaseEntry^OnChangLoadProduct^LoadProduct^DeleteInvoiceInfo', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (9, N'AllReportController', N'0000009   ', N'Report', N'Index^SalesReport^PurchaseReport^OpeningStock^ClosingStock^AccountsLedger^PartyLedger^IncomeStatement^BalanceSheet^TrialBalanceReport^InvoiceReport', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (10, N'AllSalaryController', N'0000010   ', N'Salary', N'SalaryList^SaveSalary^DeleteSalary^SaveAdvancedSalary^LoadAllowanceByEmployee^CalCulateEmployeePercentage', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (11, N'AllSalesController', N'0000011   ', N'Sales', N'SalesInvoiceList^AddInvoice^AddReturnInvoice^SaveSalesEntry^LoadProductByCategory^LoadProduct^EditInvoiceInfo^PrintBill^LoadPayemntByParty^DeleteInvoiceInfo', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (12, N'AllSettingController', N'0000012   ', N'Settings', N'EmployeeList^SaveEmployee^DeleteEmployee^SupplierPartyList^DealerPartyList^SaveParty^DeleteParty^AreaList^SaveArea^DeleteArea^BankList^SaveBank^DeleteBank', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (13, N'AllStockController', N'0000013   ', N'Stock', N'Index^CurrentStockList^GEtProductStock', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (14, N'AllTemplateController', N'0000014   ', N'Template', N'List^Register^Modify^GetPermissionList', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (15, N'AllTransactionController', N'0000015   ', N'Transaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (16, N'AllUserController', N'0000016   ', N'UserCommon', N'List^Register', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (17, N'AllJsonController', N'0000016   ', N'JsonList', N'Index^LoadProductByCategory^LoadProductStock^LoadProductStockSell', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (18, N'InventoryWithoutProduct', NULL, N'Inventory', N'StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (19, N'AllJobExpenseController', N'0000019   ', N'JobExpense', N'JobExpenseList^SaveExpense^SaveExpense^DeleteExpense^OnChangeLoadDetail', 1, 1)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (20, N'AllowedReport', N'0000020   ', N'Report', N'SalesReport^PurchaseReport^OpeningStock^ClosingStock^PartyLedgerJobReport^JobCostingReport^InvoiceReport', 1, NULL)
GO
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (21, N'AllTransactionController', N'0000015   ', N'OpeningTransaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
GO
SET IDENTITY_INSERT [dbo].[Permission] OFF
GO
SET IDENTITY_INSERT [dbo].[Product] ON 

GO
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode]) VALUES (1, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, 1, N'Glass Inomer Cement Tpe II, China', CAST(550.00 AS Decimal(18, 2)), NULL, CAST(650.00 AS Decimal(18, 2)), N'Glass Inomer Cement Tpe II, China')
GO
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode]) VALUES (2, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, 2, N'Light Cure, GC Shade A2', CAST(1500.00 AS Decimal(18, 2)), NULL, CAST(1600.00 AS Decimal(18, 2)), N'Light Cure, GC Shade A2')
GO
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode]) VALUES (3, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, 3, N'Dia Etch, Diadent, Korea', CAST(200.00 AS Decimal(18, 2)), NULL, CAST(250.00 AS Decimal(18, 2)), N'Dia Etch, Diadent, Korea')
GO
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode]) VALUES (4, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, 3, N'Charmfil Plus, Korea', CAST(200.00 AS Decimal(18, 2)), NULL, CAST(250.00 AS Decimal(18, 2)), N'Charmfil Plus, Korea')
GO
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [SellingPrice], [ProductCode]) VALUES (5, 3, 3, CAST(N'2017-03-04 00:00:00.000' AS DateTime), CAST(N'2017-03-04 00:00:00.000' AS DateTime), 1, 0, 4, N'Euginol', CAST(50.00 AS Decimal(18, 2)), NULL, CAST(60.00 AS Decimal(18, 2)), N'Euginol')
GO
SET IDENTITY_INSERT [dbo].[Product] OFF
GO
SET IDENTITY_INSERT [dbo].[TableProperty] ON 

GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (1, N'InvoiceInfo', N'PurchaseInvoiceType', 1, N'Purchase')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (2, N'InvoiceInfo', N'SalesInvoiceType', 2, N'Sales')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (3, N'InvoiceInfo', N'PurchaseInvoiceType', 3, N'Purchase Return')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (4, N'InvoiceInfo', N'SalesInvoiceType', 4, N'Sales Return')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (5, N'Transaction', N'TransactionType', 1, N'Recieved')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (6, N'Transaction', N'TransactionType', 2, N'Payment')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (7, N'Transaction', N'TransactionType', 3, N'Journal')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (8, N'Transaction', N'CashType', 1, N'Cash')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (9, N'Transaction', N'CashType', 2, N'Check')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (10, N'Party', N'PartyType', 1, N'Supplier Party')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (11, N'Party', N'PartyType', 2, N'Delear Party')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (12, N'GroupHeader', N'HeaderType', 1, N'Debit/To')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (13, N'GroupHeader', N'HeaderType', 2, N'Credit/From')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (14, N'Salary', N'SalaryType', 1, N'Current Salary')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (15, N'Salary', N'SalaryType', 2, N'Advance Salary')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (16, N'LedgerGroup', N'GroupType', 1, N'Cash Transaction')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (17, N'LedgerGroup', N'GroupType', 2, N'Purchase Ldegers')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (18, N'LedgerGroup', N'GroupType', 3, N'Sales Ledger')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (19, N'LedgerGroup', N'GroupType', 4, N'SalaryLedger')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (20, N'Transaction', N'TransactionType', 3, N'Recieved Return')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (21, N'Transaction', N'TransactionType', 4, N'Payment Return')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (22, N'LedgerGroup', N'GroupType', 5, N'Stock in Hand')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (23, N'LedgerGroup', N'GroupType', 6, N'Profit/ Loss')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (24, N'GroupHeader', N'AccountsType', 1, N'Asset')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (25, N'GroupHeader', N'AccountsType', 2, N'Liability')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (26, N'GroupHeader', N'AccountsType', 3, N'Equity')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (27, N'GroupHeader', N'AccountsType', 4, N'Revenue')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (28, N'GroupHeader', N'AccountsType', 5, N'Expense')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (29, N'LedgerGroup', N'GroupType', 7, N'Capital')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (30, N'InvoiceInfo', N'PurchaseInvoiceType', 5, N'Expire Date')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (31, N'InvoiceInfo', N'PurchaseInvoiceType', 6, N'Bonus')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (32, N'GroupHeader', N'AccountsType', 6, N'Drwaings')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (33, N'Product', N'ProductType', 1, N'Finished Product')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (34, N'Product', N'ProductType', 2, N'Raw Rpoduct')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (35, N'Job', N'JobType', 1, N'Created')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (36, N'Job', N'JobType', 2, N'In Production')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (37, N'Job', N'JobType', 3, N'Closed')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (38, N'JobDetails', N'DetailType', 1, N'Estimated')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (39, N'JobDetails', N'DetailType', 2, N'StockIn')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (40, N'JobDetails', N'DetailType', 3, N'StockOut')
GO
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (41, N'Ledger', N'TrialVisible', 1, N'InVisible on Trial Balance')
GO
SET IDENTITY_INSERT [dbo].[TableProperty] OFF
GO
SET IDENTITY_INSERT [dbo].[Template] ON 

GO
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (1, N'Admiin', N'1^2^3^4^5^6^7^8^9^10^11^12^13^15^17^21', 1, 0, CAST(N'2016-08-25' AS Date), 0, CAST(N'2016-09-04' AS Date), 1)
GO
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (2, N'testAdmin', N'1^2^3^4', 1, 0, CAST(N'2016-09-02' AS Date), NULL, NULL, 1)
GO
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (3, N'Operator', N'1^2^3^4^5^7^9^10^11^12^15^17^18', 1, 1, CAST(N'2016-09-02' AS Date), 1, CAST(N'2016-09-02' AS Date), 1)
GO
SET IDENTITY_INSERT [dbo].[Template] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

GO
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [PasswordAttempt], [IsCreator]) VALUES (1, N'ROOT', N'123456', 1, 0, NULL, 1, NULL, 1, CAST(N'2016-03-24' AS Date), NULL, NULL, 0, 0, 0, NULL, NULL, NULL)
GO
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [PasswordAttempt], [IsCreator]) VALUES (2, N'boblin', N'123', 1, 1, CAST(N'2016-04-07 17:45:45.760' AS DateTime), 1, NULL, 1, CAST(N'2016-03-28' AS Date), NULL, NULL, 1, 1, 1, NULL, NULL, NULL)
GO
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [PasswordAttempt], [IsCreator]) VALUES (3, N'DentalAdmin', N'123456', 1, 1, CAST(N'2017-04-20 00:00:00.000' AS DateTime), 1, N'null', 1, CAST(N'2016-08-08' AS Date), 0, CAST(N'2016-09-02' AS Date), 1, 1, 1, NULL, NULL, NULL)
GO
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [PasswordAttempt], [IsCreator]) VALUES (4, N'Operator', N'op123456', 1, 1, CAST(N'2017-01-16 10:41:09.367' AS DateTime), 1, NULL, 0, CAST(N'2016-09-01' AS Date), NULL, NULL, 3, 1, 1, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[User] OFF
GO
