USE [RedGreen]
GO
/****** Object:  Table [dbo].[Employee]    Script Date: 07-Jun-17 8:58:31 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeName] [nvarchar](100) NULL,
	[EmployeeCode] [nvarchar](100) NULL,
	[Designation] [nvarchar](100) NULL,
	[Department] [nvarchar](100) NULL,
	[Location] [nvarchar](100) NULL,
	[JoiningDate] [datetime] NULL,
	[DateOfBirth] [datetime] NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileInternet] [decimal](18, 0) NULL,
	[Transport] [decimal](18, 0) NULL,
	[Wellbeing] [decimal](18, 0) NULL,
	[Others] [decimal](18, 0) NULL,
	[Advance] [decimal](18, 0) NULL,
	[LeaveDeduction] [decimal](18, 0) NULL,
	[LateDeduction] [decimal](18, 0) NULL,
	[MiscellaneousDeduction] [decimal](18, 0) NULL,
	[Tax] [decimal](18, 0) NULL,
	[ProvidentFund] [decimal](18, 0) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
