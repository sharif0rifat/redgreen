USE [RedGreen]
GO
/****** Object:  Table [dbo].[UserLog]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[UserLog](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserId] [bigint] NULL,
	[ProfileId] [bigint] NULL,
	[RoleId] [bigint] NULL,
	[LoginDateTime] [datetime] NULL,
	[IpAddress] [nvarchar](50) NULL,
 CONSTRAINT [PK_UserLog] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[UserName] [nvarchar](50) NOT NULL,
	[Password] [nvarchar](50) NOT NULL,
	[IsActive] [bit] NOT NULL,
	[Islogin] [bit] NOT NULL,
	[LastLogin] [datetime] NULL,
	[Status] [int] NOT NULL,
	[PhotoUrl] [nvarchar](50) NULL,
	[CreatedBy] [bigint] NOT NULL,
	[CreatedDate] [date] NOT NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[TemplateId] [bigint] NOT NULL,
	[GroupId] [bigint] NOT NULL,
	[ApplicationId] [bigint] NOT NULL,
	[UserMessage] [nvarchar](max) NULL,
	[PasswordAttempt] [int] NULL,
	[IsCreator] [bit] NULL,
 CONSTRAINT [PK_User] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[User] ON
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [PasswordAttempt], [IsCreator]) VALUES (1, N'ROOT', N'123456', 1, 0, NULL, 1, NULL, 1, CAST(0x2E3B0B00 AS Date), NULL, NULL, 0, 0, 0, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [PasswordAttempt], [IsCreator]) VALUES (2, N'boblin', N'123', 1, 1, CAST(0x0000A5E10124B870 AS DateTime), 1, NULL, 1, CAST(0x323B0B00 AS Date), NULL, NULL, 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [PasswordAttempt], [IsCreator]) VALUES (3, N'DentalAdmin', N'123456', 1, 0, CAST(0x0000A678001943B2 AS DateTime), 1, N'null', 1, CAST(0xB73B0B00 AS Date), 0, CAST(0xD03B0B00 AS Date), 1, 1, 1, NULL, NULL, NULL)
INSERT [dbo].[User] ([Id], [UserName], [Password], [IsActive], [Islogin], [LastLogin], [Status], [PhotoUrl], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [TemplateId], [GroupId], [ApplicationId], [UserMessage], [PasswordAttempt], [IsCreator]) VALUES (4, N'qwe', N'qwe', 1, 0, NULL, 1, NULL, 0, CAST(0xCF3B0B00 AS Date), NULL, NULL, 1, 1, 1, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[User] OFF
/****** Object:  Table [dbo].[Transaction]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[DebitHeaderId] [bigint] NOT NULL,
	[CreditHeaderId] [bigint] NOT NULL,
	[Amount] [decimal](18, 0) NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionType] [int] NOT NULL,
	[CheckNo] [nvarchar](50) NULL,
	[EmployeeId] [bigint] NULL,
	[Remarks] [nvarchar](max) NULL,
	[BankId] [bigint] NULL,
	[CashType] [int] NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Template]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Template](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[TemplateName] [nvarchar](50) NULL,
	[PermissionIds] [nvarchar](max) NULL,
	[Status] [int] NOT NULL,
	[CreatedBy] [bigint] NULL,
	[CreatedDate] [date] NULL,
	[ModifiedBy] [bigint] NULL,
	[ModifiedDate] [date] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Role] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Template] ON
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (1, N'Admiin', N'1^2^3^4^5^6^7^8^9^10^11^12^13^15^17', 1, 0, CAST(0xC83B0B00 AS Date), 0, CAST(0xD23B0B00 AS Date), 1)
INSERT [dbo].[Template] ([Id], [TemplateName], [PermissionIds], [Status], [CreatedBy], [CreatedDate], [ModifiedBy], [ModifiedDate], [ApplicationId]) VALUES (2, N'testAdmin', N'1^2^3^4', 1, 0, CAST(0xD03B0B00 AS Date), NULL, NULL, 1)
SET IDENTITY_INSERT [dbo].[Template] OFF
/****** Object:  Table [dbo].[TableProperty]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableProperty](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[TableName] [nvarchar](50) NULL,
	[PropertyName] [nvarchar](50) NULL,
	[PropertyValue] [int] NULL,
	[ViewName] [nvarchar](100) NULL,
 CONSTRAINT [PK_TableProperty] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[TableProperty] ON
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (1, N'InvoiceInfo', N'PurchaseInvoiceType', 1, N'Purchase')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (2, N'InvoiceInfo', N'SalesInvoiceType', 2, N'Sales')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (3, N'InvoiceInfo', N'PurchaseInvoiceType', 3, N'Purchase Return')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (4, N'InvoiceInfo', N'SalesInvoiceType', 4, N'Sales Return')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (5, N'Transaction', N'TransactionType', 1, N'Recieved')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (6, N'Transaction', N'TransactionType', 2, N'Payment')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (7, N'Transaction', N'TransactionType', 3, N'Journal')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (8, N'Transaction', N'CashType', 1, N'Cash')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (9, N'Transaction', N'CashType', 2, N'Check')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (10, N'Party', N'PartyType', 1, N'Supplier Party')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (11, N'Party', N'PartyType', 2, N'Delear Party')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (12, N'GroupHeader', N'HeaderType', 1, N'Debit/To')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (13, N'GroupHeader', N'HeaderType', 2, N'Credit/From')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (14, N'Salary', N'SalaryType', 1, N'Current Salary')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (15, N'Salary', N'SalaryType', 2, N'Advance Salary')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (16, N'LedgerGroup', N'GroupType', 1, N'Cash Transaction')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (17, N'LedgerGroup', N'GroupType', 2, N'Purchase Ldegers')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (18, N'LedgerGroup', N'GroupType', 3, N'Sales Ledger')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (19, N'LedgerGroup', N'GroupType', 4, N'SalaryLedger')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (20, N'Transaction', N'TransactionType', 3, N'Recieved Return')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (21, N'Transaction', N'TransactionType', 4, N'Payment Return')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (22, N'LedgerGroup', N'GroupType', 5, N'Stock in Hand')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (23, N'LedgerGroup', N'GroupType', 6, N'Profit/ Loss')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (24, N'GroupHeader', N'AccountsType', 1, N'Asset')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (25, N'GroupHeader', N'AccountsType', 2, N'Liability')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (26, N'GroupHeader', N'AccountsType', 3, N'Equity')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (27, N'GroupHeader', N'AccountsType', 4, N'Revenue')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (28, N'GroupHeader', N'AccountsType', 5, N'Expense')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (29, N'LedgerGroup', N'GroupType', 7, N'Capital')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (30, N'InvoiceInfo', N'PurchaseInvoiceType', 5, N'Loss')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (31, N'InvoiceInfo', N'PurchaseInvoiceType', 6, N'Profit')
INSERT [dbo].[TableProperty] ([Id], [TableName], [PropertyName], [PropertyValue], [ViewName]) VALUES (32, N'GroupHeader', N'AccountsType', 6, N'Drwaings')
SET IDENTITY_INSERT [dbo].[TableProperty] OFF
/****** Object:  Table [dbo].[Salary]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Salary](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeId] [bigint] NOT NULL,
	[SalryType] [int] NULL,
	[Month] [datetime] NULL,
	[TotalSalary] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[FestivalBonus] [decimal](18, 0) NULL,
	[Commission] [decimal](18, 0) NULL,
	[TotalBill] [decimal](18, 0) NULL,
	[CommissionFromDate] [datetime] NULL,
	[CommissionToDate] [datetime] NULL,
	[MedicalAllowance] [decimal](18, 0) NULL,
	[OtherAllowance] [decimal](18, 0) NULL,
	[OtherDeduction] [decimal](18, 0) NULL,
	[SubHeaderId] [bigint] NOT NULL,
	[LedgerId] [bigint] NULL,
 CONSTRAINT [PK_Salary] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Salary] ON
INSERT [dbo].[Salary] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeId], [SalryType], [Month], [TotalSalary], [BasicSalary], [HouseRent], [MobileAllowance], [TransportCost], [FestivalBonus], [Commission], [TotalBill], [CommissionFromDate], [CommissionToDate], [MedicalAllowance], [OtherAllowance], [OtherDeduction], [SubHeaderId], [LedgerId]) VALUES (2, 0, 0, CAST(0x0000A678000C3A00 AS DateTime), CAST(0x0000A678000C3A00 AS DateTime), 1, 0, 1, 1, CAST(0x0000A67800000000 AS DateTime), CAST(200 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), CAST(0 AS Decimal(18, 0)), NULL, NULL, NULL, CAST(0x0000A678000C3484 AS DateTime), CAST(0x0000A678000C3484 AS DateTime), NULL, NULL, NULL, 0, 21)
SET IDENTITY_INSERT [dbo].[Salary] OFF
/****** Object:  Table [dbo].[Product]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[CategoryId] [bigint] NOT NULL,
	[ProductName] [nvarchar](100) NOT NULL,
	[BuyingPrice] [decimal](18, 2) NOT NULL,
	[Commission] [decimal](18, 2) NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[ProductCode] [nvarchar](50) NULL,
 CONSTRAINT [PK_Model] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Product] ON
INSERT [dbo].[Product] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [CategoryId], [ProductName], [BuyingPrice], [Commission], [Price], [ProductCode]) VALUES (1, 3, 3, CAST(0x0000A67701328BF9 AS DateTime), CAST(0x0000A67701328BF9 AS DateTime), 1, 0, 4, N'Product 1', CAST(100.00 AS Decimal(18, 2)), NULL, CAST(110.00 AS Decimal(18, 2)), N'P1')
SET IDENTITY_INSERT [dbo].[Product] OFF
/****** Object:  Table [dbo].[Permission]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Permission](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](50) NULL,
	[PermissionCode] [nchar](10) NULL,
	[ControllerName] [nvarchar](20) NULL,
	[ActionName] [nvarchar](max) NULL,
	[Status] [bit] NULL,
	[ApplicationId] [bigint] NULL,
 CONSTRAINT [PK_Permission] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Permission] ON
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (1, N'AllAccountController', N'0000001   ', N'Account', N'UserList^SaveUser^DeleteUser^ChangePassword^Login^Logout^PasswordGenerator', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (2, N'AllAccountingController', N'0000002   ', N'Accounting', N'LedgerGroupList^SaveLedgerGroup^DeleteLedgerGroup^LedgerList^SaveLedger', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (3, N'AllAccountingReport', N'0000003   ', N'AccountingReport', N'AccountsLedger', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (4, N'AllCategoryController', N'0000004   ', N'Category', N'List^Register^Delete', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (5, N'AllHomeController', N'0000005   ', N'Home', N'Index^AuthFailed', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (6, N'AllInventoryController', N'0000006   ', N'Inventory', N'ProductSettingsList^SaveProductSettings^DeleteProductSettings^StockEntryList^SaveStockEntry^OnChangLoadProduct^LoadCurrentStockAmount^LoadQuantityForEdit^LoadSalesRepresentitive^LoadInvoiceNo^LoadAmountByQuantity', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (7, N'AllPaymentController', N'0000007   ', N'Payment', N'DealerPaymentModule^SupplierPaymentModule^SavePayment^DeletePayment', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (8, N'AllPurchaseController', N'0000008   ', N'Purchase', N'PurchaseEntryList^SavePurchaseEntry^OnChangLoadProduct^LoadProduct^DeleteInvoiceInfo', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (9, N'AllReportController', N'0000009   ', N'Report', N'Index^SalesReport^PurchaseReport^OpeningStock^ClosingStock^AccountsLedger^PartyLedger^IncomeStatement^BalanceSheet^TrialBalanceReport', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (10, N'AllSalaryController', N'0000010   ', N'Salary', N'SalaryList^SaveSalary^DeleteSalary^SaveAdvancedSalary^LoadAllowanceByEmployee^CalCulateEmployeePercentage', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (11, N'AllSalesController', N'0000011   ', N'Sales', N'SalesInvoiceList^AddInvoice^AddReturnInvoice^SaveSalesEntry^LoadProductByCategory^LoadProduct^EditInvoiceInfo^PrintBill^LoadPayemntByParty', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (12, N'AllSettingController', N'0000012   ', N'Settings', N'EmployeeList^SaveEmployee^DeleteEmployee^SupplierPartyList^DealerPartyList^SaveParty^DeleteParty^AreaList^SaveArea^DeleteArea^BankList^SaveBank^DeleteBank', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (13, N'AllStockController', N'0000013   ', N'Stock', N'Index^CurrentStockList^GEtProductStock', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (14, N'AllTemplateController', N'0000014   ', N'Template', N'List^Register^Modify^GetPermissionList', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (15, N'AllTransactionController', N'0000015   ', N'Transaction', N'TransactionList^TransactionForm^SaveTransactionForm^DeleteTransaction', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (16, N'AllUserController', N'0000016   ', N'UserCommon', N'List^Register', 1, 1)
INSERT [dbo].[Permission] ([Id], [Title], [PermissionCode], [ControllerName], [ActionName], [Status], [ApplicationId]) VALUES (17, N'AllJsonController', N'0000016   ', N'JsonList', N'Index^LoadProductByCategory^LoadProductStock^LoadProductStockSell', 1, 1)
SET IDENTITY_INSERT [dbo].[Permission] OFF
/****** Object:  Table [dbo].[Payment]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Payment](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [nvarchar](50) NOT NULL,
	[Date] [datetime] NOT NULL,
	[Amount] [decimal](18, 0) NOT NULL,
	[PaymentType] [int] NOT NULL,
	[SubHeaderId] [bigint] NOT NULL,
	[Remarks] [nvarchar](200) NULL,
	[LedgerId] [bigint] NULL,
 CONSTRAINT [PK_Payment_1] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Party]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Party](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[PartyName] [nvarchar](100) NOT NULL,
	[Code] [nvarchar](100) NULL,
	[ContactPerson] [nvarchar](100) NULL,
	[Address1] [nvarchar](100) NULL,
	[Address2] [nvarchar](100) NULL,
	[Mobile1] [nvarchar](100) NULL,
	[Mobile2] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[SRId] [bigint] NULL,
	[SRPercentage] [float] NOT NULL,
	[AreaId] [bigint] NULL,
	[PropertyId] [bigint] NULL,
 CONSTRAINT [PK_Party] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[LedgerGroup]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LedgerGroup](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[GroupHeaderId] [bigint] NOT NULL,
	[IsParent] [bit] NOT NULL,
 CONSTRAINT [PK_LedgerGroup] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[LedgerGroup] ON
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (5, 1, 1, CAST(0x0000A646002AC044 AS DateTime), CAST(0x0000A646002AC044 AS DateTime), 1, 0, N'Cash', N'Cash', 16, 1, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (6, 1, 1, CAST(0x0000A668012CDE26 AS DateTime), CAST(0x0000A668012CDE26 AS DateTime), 1, 0, N'Cash In Bank', NULL, 16, 1, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (8, 1, 1, CAST(0x0000A668014144D5 AS DateTime), CAST(0x0000A668014144D5 AS DateTime), 1, 0, N'Creditors', NULL, 10, 3, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (9, 1, 1, CAST(0x0000A66801418601 AS DateTime), CAST(0x0000A66801418601 AS DateTime), 1, 0, N'Debtors', NULL, 11, 2, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (10, 1, 1, CAST(0x0000A6680142F301 AS DateTime), CAST(0x0000A6680142F301 AS DateTime), 1, 0, N'Sales', NULL, 18, 5, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (11, 1, 1, CAST(0x0000A66801445A06 AS DateTime), CAST(0x0000A66801445A06 AS DateTime), 1, 0, N'Purchase', NULL, 17, 6, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (12, 1, 1, CAST(0x0000A6690001F17B AS DateTime), CAST(0x0000A6690001F17B AS DateTime), 1, 0, N'Salary', NULL, 19, 6, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (13, 1, 1, CAST(0x0000A66A0003EBA0 AS DateTime), CAST(0x0000A66A0003EBA0 AS DateTime), 1, 0, N'Opening Stock', NULL, 22, 2, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (14, 1, 1, CAST(0x0000A66A000524F5 AS DateTime), CAST(0x0000A66A000524F5 AS DateTime), 1, 0, N'Profit and Loss A/C', NULL, 23, 4, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (17, 1, 1, CAST(0x0000A66E00BF0230 AS DateTime), CAST(0x0000A66E00BF0230 AS DateTime), 1, 0, N'Stock Loss', NULL, 30, 6, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (19, 1, 1, CAST(0x0000A66E00BF615F AS DateTime), CAST(0x0000A66E00BF615F AS DateTime), 1, 0, N'Stock Bonus', NULL, 31, 5, 0)
INSERT [dbo].[LedgerGroup] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [GroupHeaderId], [IsParent]) VALUES (21, 1, 1, CAST(0x0000A6700142B034 AS DateTime), CAST(0x0000A6700142B034 AS DateTime), 1, 0, N'Drawning', NULL, 32, 6, 0)
SET IDENTITY_INSERT [dbo].[LedgerGroup] OFF
/****** Object:  Table [dbo].[Ledger]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ledger](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[LedgerGroupId] [bigint] NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
 CONSTRAINT [PK_Ledger] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Ledger] ON
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (3, 1, 1, CAST(0x0000A6680158D9E6 AS DateTime), CAST(0x0000A6680158D9E6 AS DateTime), 1, 0, N'Sales', 10, NULL, 18)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (4, 1, 1, CAST(0x0000A6680158EB5C AS DateTime), CAST(0x0000A6680158EB5C AS DateTime), 1, 0, N'Purchase', 11, NULL, 17)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (5, 1, 1, CAST(0x0000A66900028A75 AS DateTime), CAST(0x0000A66900028A75 AS DateTime), 1, 0, N'Salary Expense', 12, NULL, 19)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (6, 1, 1, CAST(0x0000A66900F6A984 AS DateTime), CAST(0x0000A66900F6A984 AS DateTime), 1, 0, N'Sundry Creditors', 8, NULL, 10)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (7, 1, 1, CAST(0x0000A66900F6B5B7 AS DateTime), CAST(0x0000A66900F6B5B7 AS DateTime), 1, 0, N'Sundry Debtors', 9, NULL, 11)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (9, 1, 1, CAST(0x0000A66A0004510F AS DateTime), CAST(0x0000A66A0004510F AS DateTime), 1, 0, N'Opening Stock', 13, NULL, 22)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (14, 1, 1, CAST(0x0000A66D0102237B AS DateTime), CAST(0x0000A66D0102237B AS DateTime), 1, 0, N'Profit and Loss A/C', 14, NULL, 23)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (17, 1, 1, CAST(0x0000A66E00BFE632 AS DateTime), CAST(0x0000A66E00BFE632 AS DateTime), 1, 0, N'Product Lost', 17, NULL, 30)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (18, 1, 1, CAST(0x0000A66E00BFF2D4 AS DateTime), CAST(0x0000A66E00BFF2D4 AS DateTime), 1, 0, N'Product Bonus', 19, NULL, 31)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (20, 1, 1, CAST(0x0000A6700142CEDD AS DateTime), CAST(0x0000A6700142CEDD AS DateTime), 1, 0, N'Capital Drawing', 21, NULL, 32)
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId]) VALUES (21, 3, 3, CAST(0x0000A677017736AA AS DateTime), CAST(0x0000A677017736AA AS DateTime), 1, 0, N'Petty Cash', 5, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Ledger] OFF
/****** Object:  Table [dbo].[InvoiceInfo]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceInfo](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceNo] [nvarchar](100) NOT NULL,
	[PartyId] [bigint] NOT NULL,
	[SRId] [bigint] NOT NULL,
	[SubTotal] [decimal](18, 0) NOT NULL,
	[GrandTotal] [decimal](18, 0) NOT NULL,
	[InvoiceReferenceId] [bigint] NULL,
	[PropertyIdId] [bigint] NULL,
	[ReturnTotal] [decimal](18, 0) NULL,
	[Remarks] [nvarchar](max) NULL,
	[InvoiceDate] [datetime] NULL,
 CONSTRAINT [PK_BillingTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[InvoiceDetails]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[InvoiceDetails](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[InvoiceId] [bigint] NOT NULL,
	[ProductId] [bigint] NOT NULL,
	[Price] [decimal](18, 2) NOT NULL,
	[Quantity] [decimal](18, 2) NOT NULL,
	[TotalPrice] [decimal](18, 2) NOT NULL,
	[PropertyId] [bigint] NOT NULL,
 CONSTRAINT [PK_BillDetails] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[GroupHeader]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[GroupHeader](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](max) NULL,
	[PropertyId] [bigint] NULL,
	[AccountsType] [bigint] NULL,
	[IsParent] [bit] NOT NULL,
 CONSTRAINT [PK_GroupHeader] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[GroupHeader] ON
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (1, 1, 1, CAST(0x0000A646002AC044 AS DateTime), CAST(0x0000A646002AC072 AS DateTime), 1, 0, N'Fixed Assets', NULL, 12, 24, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (2, 1, 1, CAST(0x0000A646002AF4AF AS DateTime), CAST(0x0000A646002AF4AF AS DateTime), 1, 0, N'Current Assets', NULL, 12, 24, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (3, 1, 1, CAST(0x0000A646002AFEE3 AS DateTime), CAST(0x0000A646002AFEE3 AS DateTime), 1, 0, N' Liabilities', NULL, 13, 25, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (4, 1, 1, CAST(0x0000A646002B0889 AS DateTime), CAST(0x0000A646002B0889 AS DateTime), 1, 0, N'Equity', NULL, 13, 26, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (5, 1, 1, CAST(0x0000A646002B1211 AS DateTime), CAST(0x0000A646002B1211 AS DateTime), 1, 0, N'Revenue', NULL, 13, 27, 1)
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (6, 1, 1, CAST(0x0000A646002B1ADC AS DateTime), CAST(0x0000A646002B1ADC AS DateTime), 1, 0, N'Expense', NULL, 12, 28, 1)
SET IDENTITY_INSERT [dbo].[GroupHeader] OFF
/****** Object:  Table [dbo].[Employee]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Employee](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[EmployeeName] [nvarchar](100) NOT NULL,
	[EmployeeCode] [nvarchar](100) NULL,
	[Designation] [nvarchar](100) NULL,
	[Department] [nvarchar](100) NULL,
	[Location] [nvarchar](100) NULL,
	[DateOfJoining] [datetime] NULL,
	[DateOfBirth] [datetime] NULL,
	[PFNo] [nvarchar](50) NULL,
	[ESINo] [nvarchar](50) NULL,
	[IsSR] [bit] NULL,
	[HouseRent] [decimal](18, 0) NULL,
	[MobileAllowance] [decimal](18, 0) NULL,
	[TransportCost] [decimal](18, 0) NULL,
	[BasicSalary] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Employee] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Employee] ON
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (1, 1, 1, CAST(0x0000A62200C1D819 AS DateTime), CAST(0x0000A62200C1D819 AS DateTime), 1, 0, N'Ahsanul Habib', N'001', N'Manager, Adminstration', N'Personel department', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (2, 1, 1, CAST(0x0000A62200C269DA AS DateTime), CAST(0x0000A62200C269DA AS DateTime), 1, 0, N'Md.Nuruzzaman', N'002', N'Manager, Accounts and Finance', N'Accounts and Finance', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (3, 1, 1, CAST(0x0000A62200FB3658 AS DateTime), CAST(0x0000A62200FB3658 AS DateTime), 1, 0, N'Alok kumar Ghosh', N'003', N'Executive, marketing', N'Marketing', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (4, 1, 1, CAST(0x0000A62200FB427E AS DateTime), CAST(0x0000A62200FB427E AS DateTime), 1, 0, N'Moniruzzaman', N'004', N'Senior Executive, Marketing', N'Marketing', N'faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (5, 1, 1, CAST(0x0000A62200FB9EAC AS DateTime), CAST(0x0000A62200FB9EAC AS DateTime), 1, 0, N'Prosanto Halder', N'005', N'Lab In charge', N'Lab', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (6, 1, 1, CAST(0x0000A62200FBEFB1 AS DateTime), CAST(0x0000A62200FBEFB1 AS DateTime), 1, 0, N'Shamim', N'006', N'Assistant Lab In charge', N'Lab', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (7, 1, 1, CAST(0x0000A62200FC5EC4 AS DateTime), CAST(0x0000A62200FC5EC4 AS DateTime), 1, 0, N'Victor Sarker', N'007', N'Wax pattern Technician', N'Lab', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (8, 1, 1, CAST(0x0000A62200FCB68D AS DateTime), CAST(0x0000A62200FCB68D AS DateTime), 1, 0, N'S M Sariful Islam', N'008', N'Officer, Accounts', N'Lab', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (9, 1, 1, CAST(0x0000A62200FD1492 AS DateTime), CAST(0x0000A62200FD1492 AS DateTime), 1, 0, N'Md.Jahidul Islam', N'009', N'Office Assistant cum Driver', N'Lab', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (10, 1, 1, CAST(0x0000A62200FD663F AS DateTime), CAST(0x0000A62200FD663F AS DateTime), 1, 0, N'Alam', N'011', N'Office Attandent', N'Lab', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (11, 1, 1, CAST(0x0000A62200FDE3BE AS DateTime), CAST(0x0000A62200FDE3BE AS DateTime), 1, 0, N'Minal Sheikh', N'012', N'Office Attendant', N'Admin', N'Faridpur', CAST(0x0000A62200000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
INSERT [dbo].[Employee] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [EmployeeName], [EmployeeCode], [Designation], [Department], [Location], [DateOfJoining], [DateOfBirth], [PFNo], [ESINo], [IsSR], [HouseRent], [MobileAllowance], [TransportCost], [BasicSalary]) VALUES (12, 1, 1, CAST(0x0000A6230105490A AS DateTime), CAST(0x0000A6230105490A AS DateTime), 1, 0, N'Md.Jahidul Islam', N'010', N'Office Assistant cum Driver', N'Lab', N'Faridpur', CAST(0x0000A62300000000 AS DateTime), NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL)
SET IDENTITY_INSERT [dbo].[Employee] OFF
/****** Object:  Table [dbo].[Category]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Category](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[CreatedBy] [bigint] NOT NULL,
	[ModifiedBy] [bigint] NOT NULL,
	[CreatedDate] [datetime] NOT NULL,
	[ModifiedDate] [datetime] NOT NULL,
	[Status] [int] NOT NULL,
	[BussinessId] [bigint] NOT NULL,
	[Name] [nvarchar](100) NOT NULL,
	[Description] [nvarchar](100) NULL,
	[IsParent] [bit] NOT NULL,
	[ParentId] [bigint] NULL,
 CONSTRAINT [PK_Brand] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Application]    Script Date: 09/05/2016 01:53:42 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Application](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[AppName] [nvarchar](50) NOT NULL,
	[LogoUrl] [nvarchar](50) NULL,
	[Slogon] [nvarchar](max) NULL,
	[ValidateDate] [date] NOT NULL,
	[Status] [bit] NOT NULL,
 CONSTRAINT [PK_Application] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Application] ON
INSERT [dbo].[Application] ([Id], [AppName], [LogoUrl], [Slogon], [ValidateDate], [Status]) VALUES (1, N'DentalCorner', N'dental.jpg', N'Rifat Abdullah', CAST(0xC93B0B00 AS Date), 1)
SET IDENTITY_INSERT [dbo].[Application] OFF
/****** Object:  Default [DF_Application_Status]    Script Date: 09/05/2016 01:53:42 ******/
ALTER TABLE [dbo].[Application] ADD  CONSTRAINT [DF_Application_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_InvoiceDetails_PropertyId]    Script Date: 09/05/2016 01:53:42 ******/
ALTER TABLE [dbo].[InvoiceDetails] ADD  CONSTRAINT [DF_InvoiceDetails_PropertyId]  DEFAULT ((0)) FOR [PropertyId]
GO
/****** Object:  Default [DF_TransactionHeader_IsParent]    Script Date: 09/05/2016 01:53:42 ******/
ALTER TABLE [dbo].[LedgerGroup] ADD  CONSTRAINT [DF_TransactionHeader_IsParent]  DEFAULT ((0)) FOR [IsParent]
GO
/****** Object:  Default [DF_Role_Status]    Script Date: 09/05/2016 01:53:42 ******/
ALTER TABLE [dbo].[Template] ADD  CONSTRAINT [DF_Role_Status]  DEFAULT ((1)) FOR [Status]
GO
/****** Object:  Default [DF_User_IsActive]    Script Date: 09/05/2016 01:53:42 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_IsActive]  DEFAULT ((1)) FOR [IsActive]
GO
/****** Object:  Default [DF_User_Islogin]    Script Date: 09/05/2016 01:53:42 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_Islogin]  DEFAULT ((0)) FOR [Islogin]
GO
/****** Object:  Default [DF_User_Status]    Script Date: 09/05/2016 01:53:42 ******/
ALTER TABLE [dbo].[User] ADD  CONSTRAINT [DF_User_Status]  DEFAULT ((1)) FOR [Status]
GO
