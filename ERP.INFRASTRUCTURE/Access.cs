﻿namespace ERP.INFRASTRUCTURE
{
    public class Access
    {
        public long Id { get; set; }
        public long UserTypeId { get; set; }
        public long PermissionId { get; set; }
    }
}
