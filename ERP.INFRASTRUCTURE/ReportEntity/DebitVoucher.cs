﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.INFRASTRUCTURE.ReportEntity
{
    public class DebitVoucher
    {
        public string PayeeName { get; set; }
        public string Address { get; set; }
        public string PhoneNo { get; set; }
        public DateTime Date { get; set; }
        public string Particulars { get; set; }
        public decimal Amount { get; set; }
    }
}
