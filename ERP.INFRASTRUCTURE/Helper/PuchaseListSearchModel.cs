﻿using System;

namespace ERP.INFRASTRUCTURE.Helper
{
    public class PuchaseListSearchModel
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public long Id { get; set; }
    }
}
