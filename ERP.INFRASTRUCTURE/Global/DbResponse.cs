﻿using System;

namespace ERP.INFRASTRUCTURE.Global
{
    public class DbResponse
    {
        public int MessageType { get; set; }
        public string MessageData { get; set; }
        public object ReturnValue { get; set; }
    }
}
