﻿using System;

namespace ERP.INFRASTRUCTURE
{
    public class InvoiceDetailViewModel
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public long BussinessId { get; set; }
        public long InvoiceId { get; set; }
        public long ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductSerial { get; set; }
        public decimal Quantity { get; set; }
        public int Warranty { get; set; }
        public long PropertyId { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal TotalPrice { get; set; }
        public decimal BuyingPrice { get; set; }
        public decimal ConversionRate { get; set; }
        public long Currency { get; set; }
        public long SalesFlag { get; set; }
        public int GiftId { get; set; }
    }
}
