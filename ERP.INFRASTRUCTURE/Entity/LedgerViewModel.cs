
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.INFRASTRUCTURE
{
    using System;
    using System.Collections.Generic;
    
    public class LedgerViewModel
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public long BussinessId { get; set; }
        [DisplayName("Ledger Name")]
        public string Name { get; set; }
        public long LedgerGroupId { get; set; }
        public string Description { get; set; }
        public string GroupName { get; set; }
        public bool IsEdit { get; set; }
        public long PropertyId { get; set; }
        public string Code { get; set; }
        public string ContactPerson { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Mobile1 { get; set; }
        public string Mobile2 { get; set; }
        public string Email { get; set; }
        public Nullable<long> Visible { get; set; }
    }
}
