﻿using System;

namespace ERP.INFRASTRUCTURE.Entity
{
    public class PartialApplication
    {
        public long Id { get; set; }
        public string AppName { get; set; }
        public string LogoUrl { get; set; }
        public string Slogon { get; set; }
        public DateTime ValidateDate { get; set; }
        public bool Status { get; set; }
    }
}
