﻿using System;
using System.Collections.Generic;

namespace ERP.INFRASTRUCTURE
{
    public class PartialTemplate
    {
        public long Id { get; set; }
        public string TemplateName { get; set; }
        public string PermissionIds { get; set; }
        public int Status { get; set; }
        public long? CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long? ApplicationId { get; set; }
        public List<PartialPermission> PartialPermissions { get; set; }
    }
}
