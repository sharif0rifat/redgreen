

namespace ERP.INFRASTRUCTURE
{
    using System;
    using System.Collections.Generic;
    
    public class Party
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public long BussinessId { get; set; }
        public string PartyName { get; set; }
        public string Code { get; set; }
        public string ContactPerson { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string Mobile1 { get; set; }
        public string Mobile2 { get; set; }
        public string Email { get; set; }
        public long? SRId { get; set; }
        public double SRPercentage { get; set; }
        public long? AreaId { get; set; }
        public long? PropertyId { get; set; }
        public int partyType { get; set; }
        public bool IsEdit { get; set; }
    }
}
