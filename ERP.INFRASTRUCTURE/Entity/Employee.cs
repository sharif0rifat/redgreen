

namespace ERP.INFRASTRUCTURE
{
    using System;

    public class Employee
    {
        public long Id { get; set; }
        public long BussinessId { get; set; }
        public string EmployeeName { get; set; }
        public string EmployeeCode { get; set; }
        public string Designation { get; set; }
        public string Department { get; set; }
        public string Location { get; set; }
        public DateTime? JoiningDate { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public decimal? BasicSalary { get; set; }
        public decimal? HouseRent { get; set; }
        public decimal? MobileInternet { get; set; }
        public decimal? Transport { get; set; }
        public decimal? Wellbeing { get; set; }
        public decimal? Others { get; set; }
        public decimal? Advance { get; set; }
        public decimal? LeaveDeduction { get; set; }
        public decimal? LateDeduction { get; set; }
        public decimal? MiscellaneousDeduction { get; set; }
        public decimal? Tax { get; set; }
        public decimal? ProvidentFund { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public bool IsEdit { get; set; }
    }
}
