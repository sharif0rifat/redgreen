﻿namespace ERP.INFRASTRUCTURE
{
    using System;
    using System.Collections.Generic;
    public class Accounts
    {
        public string HeaderName { get; set; }
        public long HeaderId { get; set; }
        public decimal CreditAmount { get; set; }
        public decimal DebitAmount { get; set; }
        public decimal Amount { get; set; }
        public DateTime Date { get; set; }
    }
}
