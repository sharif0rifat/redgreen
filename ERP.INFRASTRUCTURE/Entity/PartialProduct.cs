using System.ComponentModel.DataAnnotations;
using System;

namespace ERP.INFRASTRUCTURE
{
    public class PartialProduct
    {
        public long Id { get; set; }
        [Display(Name = "Created By")]
        public long CreatedBy { get; set; }
        [Display(Name = "Modified By")]
        public long? ModifiedBy { get; set; }
        [Display(Name = "Created Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM dd, yyyy}")]
        public DateTime CreatedDate { get; set; }
        [Display(Name = "Modified Date")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:MMM dd, yyyy}")]
        public DateTime? ModifiedDate { get; set; }
        public int Status { get; set; }
        public long BussinessId { get; set; }
        [Required(ErrorMessage = "Category name is required")]
        [Display(Name = "Category Name")]
        public long CategoryId { get; set; }
        [Required(ErrorMessage = "Product name is required")]
        [Display(Name = "Product Name")]
        public string ProductName { get; set; }
        [Required(ErrorMessage = "Give Buying Price for this Product.")]
        [Display(Name = "Buying Price")]
        [Range(typeof(decimal), "0", "79228162514264337593543950335", ErrorMessage = "Enter positive value")]
        public decimal BuyingPrice { get; set; }
        [Range(typeof(decimal), "0", "79228162514264337593543950335", ErrorMessage = "Enter positive value")]
        public decimal? Commission { get; set; }
        [Required(ErrorMessage = "Give Selling Price for this Product.")]
        [Display(Name = "Selling Price")]
        [Range(typeof(decimal), "0", "79228162514264337593543950335", ErrorMessage = "Enter positive value")]
        public decimal SellingPrice { get; set; }
        [Display(Name = "Category Name")]
        public string CategoryName { get; set; }
        [Required]
        [Range(typeof(long), "0", "9999999999")]
        public decimal Quantity { get; set; }
        [Display(Name = "Product Code")]
        public string ProductCode { get; set; }
        public bool IsEdit { get; set; }
        public DateTime? InvoiceDate { get; set; }
        public long? TablePropertyId { get; set; }
        public decimal PurchaseUnit { get; set; }

        [Required(ErrorMessage = "Give Serial Number for this Product.")]
        [Display(Name = "Product Serial")]
        public string ProductSerial { get; set; }

        [Required(ErrorMessage = "Give Warranty duration for this Product.")]
        [Range(typeof(int), "0", "36", ErrorMessage = "Enter positive value")]
        public int Warranty { get; set; }
    }
}
