
using System.ComponentModel.DataAnnotations;

namespace ERP.INFRASTRUCTURE
{
    using System;
    using System.Collections.Generic;

    public class LedgerGroupViewModel
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public long BussinessId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public Nullable<long> PropertyId { get; set; }
        [Required(ErrorMessage = "You Must Select a Header")]
        public long GroupHeaderId { get; set; }
        public string GroupName { get; set; }
        public bool IsEdit { get; set; }
    }
}
