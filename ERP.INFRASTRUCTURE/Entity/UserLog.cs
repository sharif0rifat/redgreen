
namespace ERP.INFRASTRUCTURE
{
    using System;
    using System.Collections.Generic;
    
    public class UserLog
    {
        public long Id { get; set; }
        public Nullable<long> UserId { get; set; }
        public Nullable<long> ProfileId { get; set; }
        public Nullable<long> RoleId { get; set; }
        public Nullable<System.DateTime> LoginDateTime { get; set; }
        public string IpAddress { get; set; }
    }
}
