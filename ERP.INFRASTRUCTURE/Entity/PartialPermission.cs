﻿namespace ERP.INFRASTRUCTURE
{
    public class PartialPermission
    {
        public long Id { get; set; }
        public string Title { get; set; }
        public string PermissionCode { get; set; }
        public string ControllerName { get; set; }
        public string ActionName { get; set; }
        public bool? Status { get; set; }
        public long? ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public bool CheckboxStatus { get; set; }
    }
}
