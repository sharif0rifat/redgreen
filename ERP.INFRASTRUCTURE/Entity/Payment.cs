

using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace ERP.INFRASTRUCTURE
{
    using System;

    public class Payment
    {
        public long Id { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public long PartyId { get; set; }
        [DisplayName("Party Name")]
        public string PartyName { get; set; }
        public System.DateTime CreatedDate { get; set; }
        public System.DateTime ModifiedDate { get; set; }
        public int Status { get; set; }
        public string BussinessId { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MMMM/yyyy}")]
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public long PaymentType { get; set; }
        public long DebitId { get; set; }
        public long CreditId { get; set; }
        public string BankName { get; set; }
        public string AccountNo { get; set; }
        public string ReferenceNo { get; set; }
        public string CheckNo { get; set; }
        public Nullable<System.DateTime> CheckDate { get; set; }
        public string SubHeader { get; set; }
        public bool IsEdit { get; set; }
        public string Remarks { get; set; }
        public long PaymentStatus { get; set; }
        public int CurrencyId { get; set; }
    }
}
