﻿using System;

namespace ERP.INFRASTRUCTURE
{
    public class PartialUser
    {
        public long Id { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public bool IsActive { get; set; }
        public bool Islogin { get; set; }
        public DateTime? LastLogin { get; set; }
        public int Status { get; set; }
        public string PhotoUrl { get; set; }
        public long CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public long? ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public long TemplateId { get; set; }
        public string TemplateName { get; set; }
        public long GroupId { get; set; }
        public string GroupName { get; set; }
        public long ApplicationId { get; set; }
        public string ApplicationName { get; set; }
        public string ConfirmPassword { get; set; }
        public string UserMessage { get; set; }
        public int? PasswordAttempt { get; set; }
        public bool? IsCreator { get; set; }
    }
}
