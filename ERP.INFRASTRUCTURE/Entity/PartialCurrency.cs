﻿using System;

namespace ERP.INFRASTRUCTURE.Entity
{
    public class PartialCurrency
    {
        public int ID { get; set; }
        public string CurrencyName { get; set; }
        public string ShortName { get; set; }
        public string Symbol { get; set; }
        public string Country { get; set; }
        public decimal ConversionRate { get; set; }

        public bool IsEdit { get; set; }
        public long CreatedBy { get; set; }
        public long ModifiedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ModifiedDate { get; set; }

        public int Status { get; set; }

    }
}
