﻿
namespace ERP.REPOSITORIES.Helper
{
    public static class NumberInWords
    {
        static string[] ones = new string[] { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine" };
        static string[] teens = new string[] { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen" };
        static string[] tens = new string[] { "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety" };
        static string[] thousandsGroups = { "", " Thousand", " Lac", " Crore" };

        private static string FriendlyInteger(long n, string leftDigits, int thousands)
        {
            if (n == 0)
            {
                return leftDigits;
            }

            string friendlyInt = leftDigits;

            if (friendlyInt.Length > 0)
            {
                friendlyInt += " ";
            }

            if (n < 10)
            {
                friendlyInt += ones[n];
            }
            else if (n < 20)
            {
                friendlyInt += teens[n - 10];
            }
            else if (n < 100)
            {
                friendlyInt += FriendlyInteger(n % 10, tens[n / 10 - 2], 0);
            }
            else if (n < 1000)
            {
                friendlyInt += FriendlyInteger(n % 100, (ones[n / 100] + " Hundred"), 0);
            }
            else if (n < 100000)
            {
                friendlyInt += FriendlyInteger(n % 1000, (FriendlyInteger(n / 1000, "", 0) + " Thousand"), 0);
            }
            else if (n < 10000000)
            {
                friendlyInt += FriendlyInteger(n % 100000, (FriendlyInteger(n / 100000, "", 0) + " Lac"), 0);
            }
            else
            {
                //friendlyInt += FriendlyInteger(n % 1000, FriendlyInteger(n / 1000, "", thousands + 1), 0);
                //if (n % 1000 == 0)
                //{
                //    return friendlyInt;
                //}

                friendlyInt += FriendlyInteger(n % 10000000, (FriendlyInteger(n / 10000000, "", 0) + " Crore"), 0);
            }

            return friendlyInt + thousandsGroups[thousands];
        }

        public static string IntegerToWritten(long n)
        {
            if (n == 0)
            {
                return "Zero";
            }
            else if (n < 0)
            {
                return "Negative " + IntegerToWritten(-n);
            }

            return FriendlyInteger(n, "", 0);
        }
    }
}
