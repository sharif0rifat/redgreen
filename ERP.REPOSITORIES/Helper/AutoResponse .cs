﻿using ERP.INFRASTRUCTURE.Global;

namespace ERP.REPOSITORIES.Helper
{
    public static class AutoResponse
    {
        public static DbResponse SuccessMessage()    
        {
            return new DbResponse{MessageType = 1,MessageData = "Job Complete.",ReturnValue = null};
        }
        public static DbResponse SuccessMessageWithValue(string message,object value)
        {
            return new DbResponse { MessageType = 1, MessageData = message, ReturnValue = value };
        }
        public static DbResponse SuccessMessageWithParam(object param)
        {
            return new DbResponse { MessageType = 1, MessageData = "Job Complete. ", ReturnValue = param };
        }
        public static DbResponse FailedMessage()
        {
            return new DbResponse { MessageType = 2, MessageData = "Job Failed. ", ReturnValue = null };
        }
        public static DbResponse FailedMessage(string Message)
        {
            return new DbResponse { MessageType = 2, MessageData = Message, ReturnValue = Message };
        }
        public static DbResponse FailedMessageWithParam(object param)
        {
            return new DbResponse { MessageType = 2, MessageData = param.ToString(), ReturnValue = param };
        }
        
        public static DbResponse ExistMessage()
        {
            return new DbResponse { MessageType = 3, MessageData = "Already Exist. ", ReturnValue = null };
        }
        public static DbResponse ExistMessageWithParam(object param)
        {
            return new DbResponse { MessageType = 3, MessageData = param.ToString(), ReturnValue = param };
        }
        public static DbResponse NotFoundMessage()
        {
            return new DbResponse { MessageType = 3, MessageData = "Data Not Found. ", ReturnValue = null };
        }
        public static DbResponse NotFoundWithParam(object param)
        {
            return new DbResponse { MessageType = 3, MessageData = "Data Not Found. ", ReturnValue = param };
        }
        public static DbResponse LoginExistMessage()
        {
            return new DbResponse { MessageType = 3, MessageData = "User Already Logged In.", ReturnValue = null };
        }
        public static DbResponse ExpiredMessage()
        {
            return new DbResponse { MessageType = 3, MessageData = "Application Expired. Please Renew...", ReturnValue = null };
        }
    }
}
