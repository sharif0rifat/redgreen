﻿using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Repositories;

namespace ERP.REPOSITORIES.Helper
{
    public static class PropertyConstants
    {
        public static string DateFormat;
        public static IPropertyManager PropertyManager;


        //==========PartyType==========
        public static long Supplier;
        public static long Dealer;

        //==========InvoiceType============
        public static long Purchase;
        public static long PurchaseReturn;
        public static long LostInvoice;
        public static long ProfitInvoice;

        public static long Sales;
        public static long SalesReturn;
        public static long SalesReplace;
        //==========SalesType========
        public static long Gift;
        //==========SalesType========
        public static long StockIn;
        public static long StockOut;

        //========TransactionType============
        public static long Receive;
        public static long Payment;
        public static long ReceiveReturn;
        public static long PaymentReturn;


        //==========Transaction Header Type============
        public static long Debit;
        public static long Credit;

        //==========Salary Type============
        public static long CurrentSalary;
        public static long AdvanceSalary;

        //==============Ledger Type==============
        public static long CashTransaction;
        public static long PurchaseLedger;
        public static long SalesLedger;
        public static long SalaryLedger;
        public static long StockLedger;
        public static long Capital;
        public static long ProfitLoss;
        //==========Ledger Visibility=======
        public static long LedgerVisible;

        //==============Accounts Type=======
        public static long Asset;
        public static long Liability;
        public static long Equity;
        public static long PositiveEquity;
        public static long NegativeEquity;

        //==============Job Type==========
        public static long CreatedJob;
        public static long InProductionJob;
        public static long ClosedJob;


        //=========Payment=======
        public static long Pending;
        public static long Paid;

        public static void Initialize()
        {
            DateFormat = "dd/MMM/yyyy";
            PropertyManager = new PropertyManager();
            //==========PartyType==========
            Supplier = PropertyManager.GetPropertyByNameAndValue("PartyType", (int)Constants.PartyType.SupplierParty).Id;
            Dealer = PropertyManager.GetPropertyByNameAndValue("PartyType", (int)Constants.PartyType.DealerParty).Id;

            //==========InvoiceType==========
            Sales = PropertyManager.GetPropertyByNameAndValue("SalesInvoiceType", (int)Constants.SalesInvoiceType.Sales).Id;
            SalesReturn = PropertyManager.GetPropertyByNameAndValue("SalesInvoiceType", (int)Constants.SalesInvoiceType.SalesReturn).Id;
            SalesReplace = PropertyManager.GetPropertyByNameAndValue("SalesInvoiceType", (int)Constants.SalesInvoiceType.SalesReplace).Id;

            Purchase = PropertyManager.GetPropertyByNameAndValue("PurchaseInvoiceType", (int)Constants.PurchaseInvoiceType.Purchase).Id;
            PurchaseReturn = PropertyManager.GetPropertyByNameAndValue("PurchaseInvoiceType", (int)Constants.PurchaseInvoiceType.PurchaseReturn).Id;
            LostInvoice = PropertyManager.GetPropertyByNameAndValue("PurchaseInvoiceType", (int)Constants.PurchaseInvoiceType.Lost).Id;
            ProfitInvoice = PropertyManager.GetPropertyByNameAndValue("PurchaseInvoiceType", (int)Constants.PurchaseInvoiceType.Profit).Id;
            //=============Sales Tyep=============
            Gift = PropertyManager.GetPropertyByNameAndValue("SalesType", 1).Id;
            //=============Sales Tyep=============
            StockIn = PropertyManager.GetPropertyByNameAndValue("StockType", 1).Id;
            StockOut = PropertyManager.GetPropertyByNameAndValue("StockType", 2).Id;

            //==========HeaderType==========
            Debit = PropertyManager.GetPropertyByNameAndValue("HeaderType", (int)Constants.HeaderType.Debit).Id;
            Credit = PropertyManager.GetPropertyByNameAndValue("HeaderType", (int)Constants.HeaderType.Credit).Id;

            //==========Salary Type==========
            CurrentSalary = PropertyManager.GetPropertyByNameAndValue("SalaryType", (int)Constants.SalaryType.Current).Id;
            AdvanceSalary = PropertyManager.GetPropertyByNameAndValue("SalaryType", (int)Constants.SalaryType.Advance).Id;

            //==============Ledger Type==============
            CashTransaction = PropertyManager.GetPropertyByNameAndValue("GroupType", (int)Constants.LedgerType.CashTransaction).Id;
            PurchaseLedger = PropertyManager.GetPropertyByNameAndValue("GroupType", (int)Constants.LedgerType.PurchaseLedger).Id;
            SalesLedger = PropertyManager.GetPropertyByNameAndValue("GroupType", (int)Constants.LedgerType.SalesLedger).Id;
            SalaryLedger = PropertyManager.GetPropertyByNameAndValue("GroupType", (int)Constants.LedgerType.SalaryLedger).Id;
            StockLedger = PropertyManager.GetPropertyByNameAndValue("GroupType", (int)Constants.LedgerType.StockLedger).Id;
            Capital = PropertyManager.GetPropertyByNameAndValue("GroupType", (int)Constants.LedgerType.Capital).Id;
            ProfitLoss = PropertyManager.GetPropertyByNameAndValue("GroupType", (int)Constants.LedgerType.ProfitLoss).Id;
            //===============ledger visible type================
            LedgerVisible = PropertyManager.GetPropertyByNameAndValue("TrialVisible", 1).Id;

            //==============Accounts Type==============
            Asset = PropertyManager.GetPropertyByNameAndValue("AccountsType", (int)Constants.AccountsTytpe.Asset).Id;
            Liability = PropertyManager.GetPropertyByNameAndValue("AccountsType", (int)Constants.AccountsTytpe.Liability).Id;
            Equity = PropertyManager.GetPropertyByNameAndValue("AccountsType", (int)Constants.AccountsTytpe.Equity).Id;
            PositiveEquity = PropertyManager.GetPropertyByNameAndValue("AccountsType", (int)Constants.AccountsTytpe.PositiveEquity).Id;
            NegativeEquity = PropertyManager.GetPropertyByNameAndValue("AccountsType", (int)Constants.AccountsTytpe.NegativeEquity).Id;

            //=========Payment=======
            Pending = PropertyManager.GetPropertyByNameAndValue("PaymentStatus", (int)Constants.Payment.Pending).Id;
            Paid = PropertyManager.GetPropertyByNameAndValue("PaymentStatus", (int)Constants.Payment.Paid).Id;

            //=========TransactionType=======
            Receive = PropertyManager.GetPropertyByNameAndValue("TransactionType", (int)Constants.TransactionType.Receive).Id;
            Payment = PropertyManager.GetPropertyByNameAndValue("TransactionType", (int)Constants.TransactionType.Payment).Id;
        }
    }
}
