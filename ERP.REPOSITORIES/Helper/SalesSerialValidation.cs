﻿using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.REPOSITORIES.Helper
{
    class SalesSerialValidation
    {
        #region Edit Sales Invoice
        public DbResponse ValidateSerialForSalesEditCheckReturnedOrNot(long propertyId, IList<InvoiceDetailViewModel> editedList, List<InvoiceDetail> mainInvoiceList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in editedList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = editedList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            //List<InvoiceDetailViewModel> newList = new List<InvoiceDetailViewModel>(editedList);
            //HashSet<string> productSerialList = new HashSet<string>(mainInvoiceList.Select(x => x.ProductSerial));

            //newList.RemoveAll(x => productSerialList.Contains(x.ProductSerial));

            // Entered Serial Is On Purchase
            if (propertyId == PropertyConstants.Sales || propertyId == PropertyConstants.SalesReplace)
                return CheckSerialReturnedOrNot(mainInvoiceList, propertyId);

            return AutoResponse.SuccessMessage();
        }

        public DbResponse ValidateSerialForSalesEditCheckStockForNewEntry(long propertyId, IList<InvoiceDetailViewModel> editedList, List<InvoiceDetail> mainInvoiceList)
        {
            List<InvoiceDetailViewModel> newList = new List<InvoiceDetailViewModel>(editedList);
            HashSet<string> productSerialList = new HashSet<string>(mainInvoiceList.Select(x => x.ProductSerial));

            newList.RemoveAll(x => productSerialList.Contains(x.ProductSerial));

            // Entered Serial Is On Purchase
            if (propertyId == PropertyConstants.Sales || propertyId == PropertyConstants.SalesReplace)
                //return CheckSerialIsInStockNewEntry(newList, propertyId);
                return CheckSerialExistForSales(newList, propertyId);

                return AutoResponse.SuccessMessage();
        }
                
        private DbResponse CheckSerialReturnedOrNot(List<InvoiceDetail> detailList, long propertyId)
        {
            using (var db = new ERPEntities())
            {
                InvoiceDetail existSerial = null;
                foreach (var invoiceDetailViewModel in detailList)
                {
                    //Check If It has Been purchased or not
                    existSerial = db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.SalesReturn && i.ProductId == invoiceDetailViewModel.ProductId && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                    if (existSerial != null)
                    {
                        return AutoResponse.FailedMessageWithParam("You Can't Modify This Invoice! The Product With Serial No: " + invoiceDetailViewModel.ProductSerial + " had been sold earlier and has been returned.");
                    }
                    
                }
                return AutoResponse.SuccessMessage();
            }
        }
        //Used In Add Sales Invoice Section Too
        private DbResponse CheckSerialExistForSales(IEnumerable<InvoiceDetailViewModel> detailList, long propertyId)
        {
            using (var db = new ERPEntities())
            {
                InvoiceDetail existSerial = null;
                foreach (var invoiceDetailViewModel in detailList)
                {
                    //Check If It has Been purchased or not
                    existSerial = db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && (i.PropertyId == PropertyConstants.Purchase || i.PropertyId == PropertyConstants.ProfitInvoice) && i.ProductId == invoiceDetailViewModel.ProductId && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                    if (existSerial == null)
                    {
                        return AutoResponse.FailedMessageWithParam("Serial No: " + invoiceDetailViewModel.ProductSerial + " is invalid.");
                    }
                    //Check if it Has been sold or not
                    else
                    {
                        var stockQuantity = GetStockBySerial(invoiceDetailViewModel, db);
                        if (stockQuantity.Quantity > 0)
                            invoiceDetailViewModel.BuyingPrice = stockQuantity.BuyingPrice;
                        else
                            return AutoResponse.FailedMessageWithParam("The product with the serial : " + invoiceDetailViewModel.ProductSerial + " ...is not in stock.");
                    }

                }
                return AutoResponse.SuccessMessage();
            }
        }
        #endregion


        #region Add Sales Invoice
        public DbResponse ValidateSerialForSales(long propertyId, IList<InvoiceDetailViewModel> detailList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in detailList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = detailList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            // Entered Serial Is On Purchase
            if (propertyId == PropertyConstants.Sales || propertyId == PropertyConstants.SalesReplace)
                return CheckSerialExistForSales(detailList, propertyId);

            return AutoResponse.SuccessMessage();
        }
        #endregion

        //private DbResponse CheckSerialIsInStockNewEntry(IEnumerable<InvoiceDetailViewModel> detailList, long propertyId)
        //{
        //    using (var db = new ERPEntities())
        //    {
        //        InvoiceDetail existSerial = null;
        //        foreach (var invoiceDetailViewModel in detailList)
        //        {
        //            //Check If It has Been purchased or not
        //            existSerial = db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.SalesReturn && i.ProductId == invoiceDetailViewModel.ProductId && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockOut);
        //            if (existSerial != null)
        //            {
        //                return AutoResponse.FailedMessageWithParam("You Can't Modify This Invoice! The Product With Serial No: " + invoiceDetailViewModel.ProductSerial + " had been sold earlier and had been returned.");
        //            }
        //            //Check if it Has been sold or not
        //            else
        //            {
        //                var stockQuantity = GetStockBySerial(invoiceDetailViewModel, db);
        //                if (stockQuantity.Quantity > 0)
        //                    invoiceDetailViewModel.BuyingPrice = stockQuantity.BuyingPrice;
        //                else
        //                    return AutoResponse.FailedMessageWithParam("The product with the serial : " + invoiceDetailViewModel.ProductSerial + " ...is not in stock.");
        //            }

        //        }
        //        return AutoResponse.SuccessMessage();
        //    }
        //}

        #region Add Sales Return Invoice
        public DbResponse ValidateSerialForSalesReturn(long propertyId, IList<InvoiceDetailViewModel> detailList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in detailList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = detailList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            // Entered Serial Is On Purchase
            if (propertyId == PropertyConstants.SalesReturn)
            {
                return CheckSameSerialExistForSalesReturn(detailList);
            }

            return AutoResponse.SuccessMessage();
        }

        private DbResponse CheckSameSerialExistForSalesReturn(IEnumerable<InvoiceDetailViewModel> detailList)
        {
            using (var db = new ERPEntities())
            {
                InvoiceDetail existSerial = null;

                foreach (var invoiceDetailViewModel in detailList)
                {
                    // Check If It has Been purchased or not
                    existSerial = db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && (i.PropertyId == PropertyConstants.Purchase || i.PropertyId == PropertyConstants.ProfitInvoice) && i.ProductId == invoiceDetailViewModel.ProductId && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                    if (existSerial == null)
                    {
                        return AutoResponse.FailedMessageWithParam("Serial No: " + invoiceDetailViewModel.ProductSerial + " is invalid.");
                    }
                    //Check If It's In Stock
                    var stockQuantity = GetStockBySerial(invoiceDetailViewModel, db);
                    if (stockQuantity.Quantity > 0)
                        return AutoResponse.FailedMessageWithParam("This Product With The Serial No: " + invoiceDetailViewModel.ProductSerial + " Has Not Been Sold Yet.");

                    //InvoiceDetail SerialExist = null;
                    else
                    {
                        existSerial = db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && (i.PropertyId == PropertyConstants.LostInvoice || i.PropertyId == PropertyConstants.PurchaseReturn) && i.StockIn == PropertyConstants.StockOut && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.ProductId == invoiceDetailViewModel.ProductId);

                        if (existSerial != null)
                        {
                            return AutoResponse.FailedMessageWithParam("You Can't Return This Product With Serial No: " + invoiceDetailViewModel.ProductSerial + ". It Has Been Damaged Or Returned To The Factory.");
                        }

                        //else
                        //{
                        //    existSerial = db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.Sales && i.StockIn == PropertyConstants.StockOut && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.ProductId == invoiceDetailViewModel.ProductId);

                        //    if (existSerial == null)
                        //    {
                        //        return AutoResponse.FailedMessageWithParam("Serial No: " + invoiceDetailViewModel.ProductSerial + " Is Invalid!");
                        //    }
                        //}

                    }
                }
                return AutoResponse.SuccessMessage();
            }
        }
        #endregion

        #region Edit Sales Return Invoice
        public DbResponse ValidateSerialEditingSalesReturnOldEntry(long propertyId, IList<InvoiceDetailViewModel> editedList, List<InvoiceDetail> mainInvoiceList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in editedList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = editedList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            //List<InvoiceDetailViewModel> newList = new List<InvoiceDetailViewModel>(editedList);
            //HashSet<string> productSerialList = new HashSet<string>(mainInvoiceList.Select(x => x.ProductSerial));

            //newList.RemoveAll(x => productSerialList.Contains(x.ProductSerial));
                        
            if (propertyId == PropertyConstants.SalesReturn)
                return CheckoldEntryStockForSalesReturnInvoiceEditing(mainInvoiceList);

            return AutoResponse.SuccessMessage();
        }

        private DbResponse CheckoldEntryStockForSalesReturnInvoiceEditing(List<InvoiceDetail> detailList)
        {
            using (var db = new ERPEntities())
            {
                //InvoiceDetail existSerial = null;
                foreach (var invoiceDetailViewModel in detailList)
                {
                   
                        var stockQuantity = GetStockBySerialOldEntry(invoiceDetailViewModel, db);
                        if (stockQuantity.Quantity > 0)
                            invoiceDetailViewModel.BuyingPrice = stockQuantity.BuyingPrice;
                        else
                            return AutoResponse.FailedMessageWithParam("You Can't Modify This Invoice! The product with the serial : " + invoiceDetailViewModel.ProductSerial + " ...is not in stock.");
                    
                }
                return AutoResponse.SuccessMessage();
            }
        }
        public DbResponse ValidateSerialEditingSalesReturnNewEntry(long propertyId, IList<InvoiceDetailViewModel> editedList, List<InvoiceDetail> mainInvoiceList)
        {
            List<InvoiceDetailViewModel> newList = new List<InvoiceDetailViewModel>(editedList);
            HashSet<string> productSerialList = new HashSet<string>(mainInvoiceList.Select(x => x.ProductSerial));

            newList.RemoveAll(x => productSerialList.Contains(x.ProductSerial));

            // Entered Serial Is On Purchase
            if (propertyId == PropertyConstants.SalesReturn)
                //return CheckSerialIsInStockNewEntry(newList, propertyId);
                return CheckSameSerialExistForSalesReturn(newList);

            return AutoResponse.SuccessMessage();
        }
        #endregion


        //Check Stock By Serial
        InvoiceDetail GetStockBySerial(InvoiceDetailViewModel invoiceDetailViewModel, ERPEntities db)
        {
            var stock = db.InvoiceDetails.Where(i => i.Status == 1 && i.ProductId == invoiceDetailViewModel.ProductId && i.ProductSerial == invoiceDetailViewModel.ProductSerial).Select(i => new
            {
                ProductId = i.ProductId,
                BuyingPrice = i.BuyingPrice,
                Quantity = i.StockIn == PropertyConstants.StockIn ? i.Quantity : -i.Quantity
            }).ToList();
            var stockQuantity = stock.GroupBy(i => i.ProductId).Select(i => new InvoiceDetail
            {
                ProductId = i.Key,
                BuyingPrice = i.FirstOrDefault().BuyingPrice,
                Quantity = i.Sum(j => j.Quantity)
            }).FirstOrDefault();
            return stockQuantity;
        }

        InvoiceDetail GetStockBySerialOldEntry(InvoiceDetail invoiceDetailViewModel, ERPEntities db)
        {
            var stock = db.InvoiceDetails.Where(i => i.Status == 1 && i.ProductId == invoiceDetailViewModel.ProductId && i.ProductSerial == invoiceDetailViewModel.ProductSerial).Select(i => new
            {
                ProductId = i.ProductId,
                BuyingPrice = i.BuyingPrice,
                Quantity = i.StockIn == PropertyConstants.StockIn ? i.Quantity : -i.Quantity
            }).ToList();
            var stockQuantity = stock.GroupBy(i => i.ProductId).Select(i => new InvoiceDetail
            {
                ProductId = i.Key,
                BuyingPrice = i.FirstOrDefault().BuyingPrice,
                Quantity = i.Sum(j => j.Quantity)
            }).FirstOrDefault();
            return stockQuantity;
        }
    }
}
