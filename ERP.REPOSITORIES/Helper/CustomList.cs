﻿using System.Collections.Generic;
using System.Web.Mvc;
using ERP.REPOSITORIES.System256;

namespace ERP.REPOSITORIES.Helper
{
    public class CustomList
    {
        public static SelectList GetMonths()
        {
            var values = new List<SelectListItem>
            {
                new SelectListItem {Value = "1-"+System128.GetCurrentDateTime().Year, Text = "January-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "2-"+System128.GetCurrentDateTime().Year, Text = "February-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "3-"+System128.GetCurrentDateTime().Year , Text = "March-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "4-"+System128.GetCurrentDateTime().Year, Text = "April-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "5-"+System128.GetCurrentDateTime().Year, Text = "May-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "6-"+System128.GetCurrentDateTime().Year, Text = "June-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "7-"+System128.GetCurrentDateTime().Year, Text = "July-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value ="8-"+System128.GetCurrentDateTime().Year , Text = "August-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "9-"+System128.GetCurrentDateTime().Year, Text = "September-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "10-"+System128.GetCurrentDateTime().Year, Text = "October-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "11-"+System128.GetCurrentDateTime().Year, Text = "November-"+System128.GetCurrentDateTime().Year},
                new SelectListItem {Value = "12-"+System128.GetCurrentDateTime().Year, Text = "December-"+System128.GetCurrentDateTime().Year}
            };
            return new SelectList(values, "Value", "Text");
        }
    }
}
