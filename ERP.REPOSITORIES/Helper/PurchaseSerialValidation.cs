﻿using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ERP.REPOSITORIES.Helper
{
    class PurchaseSerialValidation
    {
        #region Validate Serial For Purchase Edit

        //Checking Any Product Is Sold Or Not
        public DbResponse ValidateSerialForPurchaseEditCheckSold(long propertyId, List<InvoiceDetail> detailList)
        {
            List<InvoiceDetail> sameSerial = null;
            foreach (var invoiceDetailViewModel in detailList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = detailList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            // Entered Serial Is On Purchase
            if (propertyId == PropertyConstants.Purchase || propertyId == PropertyConstants.ProfitInvoice)
                return CheckSerialSoldOrNot(detailList, propertyId);

            return AutoResponse.SuccessMessage();
        }
        private DbResponse CheckSerialSoldOrNot(List<InvoiceDetail> detailList, long propertyId)
        {
            using (var db = new ERPEntities())
            {
                InvoiceDetail existSerial = null;
                foreach (var invoiceDetailViewModel in detailList)
                {
                    //Check If It has Been purchased or not
                    existSerial = db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockOut);
                    if (existSerial != null)
                    {
                        return AutoResponse.FailedMessageWithParam("Serial No: " + invoiceDetailViewModel.ProductSerial + " is Sold Or Returned. You Can't Modify This Invoice.");
                    }
                    
                }
                return AutoResponse.SuccessMessage();
            }
        }
        //Checking Any Serial Is Duplicate Or Not
        public DbResponse ValidateSerialForPurchaseEditCheckExist(long propertyId, IList<InvoiceDetailViewModel> editedList, List<InvoiceDetail> mainInvoiceList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in editedList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = editedList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            List<InvoiceDetailViewModel> newList = new List<InvoiceDetailViewModel>(editedList);
            HashSet<string> productSerialList = new HashSet<string>(mainInvoiceList.Select(x => x.ProductSerial));

            newList.RemoveAll(x => productSerialList.Contains(x.ProductSerial));

            DbResponse result = CheckSameSerialExistinDB(newList); //Same Serial Can't Exist On DB For Purchase And Bonus
            if (result.MessageType == 2)
            {
                return result;
            }

            return AutoResponse.SuccessMessage();
        }

        private DbResponse CheckSameSerialExistinDB(IEnumerable<InvoiceDetailViewModel> detailList)
        {
            using (var db = new ERPEntities())
            {
                InvoiceDetail existSerial = null;
                //var exist = false;
                var dbDetailList = db.InvoiceDetails.Where(i => i.Status == 1).ToList();
                foreach (var invoiceDetailViewModel in detailList)
                {
                    if (invoiceDetailViewModel.Id > 0)
                    {
                        existSerial =
                        db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.Purchase && i.Id != invoiceDetailViewModel.Id && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                    }
                    else
                        existSerial =
                            db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.ProductSerial == invoiceDetailViewModel.ProductSerial);
                    if (existSerial != null)
                    {
                        var invoice = db.InvoiceInfoes.FirstOrDefault(i => i.Status == 1 && i.Id == existSerial.InvoiceId);
                        return AutoResponse.FailedMessageWithParam("The Product Serial " + invoiceDetailViewModel.ProductSerial + " Is Duplicate In Invoice: " + invoice.InvoiceNo + ". You Can't Insert New Product With Existing Serial Number.");
                    }

                }
                return AutoResponse.SuccessMessage();
            }
        }

        #endregion


        #region Validate Serial For Purchase
        public DbResponse ValidateSerialForPurchase(long propertyId, IList<InvoiceDetailViewModel> detailList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in detailList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = detailList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            DbResponse result = CheckSameSerialExistinDB(detailList); //Same Serial Can't Exist On DB For Purchase And Bonus
            if (result.MessageType == 2)
            {
                return result;
            }

            return AutoResponse.SuccessMessage();
        }

        // Check Same Serial Exist On DB Or Not
        //private DbResponse CheckSameSerialExistinDB(IEnumerable<InvoiceDetailViewModel> detailList)
        //{
        //    using (var db = new ERPEntities())
        //    {
        //        InvoiceDetail existSerial = null;
        //        //var exist = false;
        //        var dbDetailList = db.InvoiceDetails.Where(i => i.Status == 1).ToList();
        //        foreach (var invoiceDetailViewModel in detailList)
        //        {
        //            if (invoiceDetailViewModel.Id > 0)
        //            {
        //                existSerial =
        //                db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.Purchase && i.Id != invoiceDetailViewModel.Id && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
        //            }
        //            else
        //                existSerial =
        //                    db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.ProductSerial == invoiceDetailViewModel.ProductSerial);
        //            if (existSerial != null)
        //            {
        //                var invoice = db.InvoiceInfoes.FirstOrDefault(i => i.Status == 1 && i.Id == existSerial.InvoiceId);
        //                return AutoResponse.FailedMessageWithParam("The Product Serial " + invoiceDetailViewModel.ProductSerial + " Is Duplicate In Invoice: " + invoice.InvoiceNo + ". You Can't Insert New Product With Existing Serial Number.");
        //            }

        //        }
        //        return AutoResponse.SuccessMessage();
        //    }
        //}
        // Check Same Serial Exist On DB Or Not [End]
        
        #endregion

        #region Validate Serial For Purchase Return
        //Validation With DB For Sales Return
        public DbResponse ValidateSerialForPurchaseReturn(long propertyId, IList<InvoiceDetailViewModel> detailList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in detailList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = detailList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            // Entered Serial Is On Purchase
            if (propertyId == PropertyConstants.PurchaseReturn || propertyId == PropertyConstants.LostInvoice)
                return CheckSerialExistForPurchaseReturn(detailList, propertyId);

            return AutoResponse.SuccessMessage();
        }
        private DbResponse CheckSerialExistForPurchaseReturn(IEnumerable<InvoiceDetailViewModel> detailList, long propertyId)
        {
            using (var db = new ERPEntities())
            {
                InvoiceDetail existSerial = null;
                foreach (var invoiceDetailViewModel in detailList)
                {
                    //Check If It has Been purchased or not
                    existSerial = db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && (i.PropertyId == PropertyConstants.Purchase || i.PropertyId == PropertyConstants.ProfitInvoice) && i.ProductId == invoiceDetailViewModel.ProductId && i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                    if (existSerial == null)
                    {
                        return AutoResponse.FailedMessageWithParam("Serial No: " + invoiceDetailViewModel.ProductSerial + " is invalid.");
                    }
                    //Check if it Has been sold or not
                    //======On selling time reserve the purchase price.========
                    else
                    {
                        var stockQuantity = GetStockBySerial(invoiceDetailViewModel, db);
                        if (stockQuantity.Quantity > 0)
                            invoiceDetailViewModel.BuyingPrice = stockQuantity.BuyingPrice;
                        else
                            return AutoResponse.FailedMessageWithParam("The product with the serial : " + invoiceDetailViewModel.ProductSerial + " is not in stock.");
                    }

                }
                return AutoResponse.SuccessMessage();
            }
        }
        //Validate Serial For Editing Purchase return Invoice
        public DbResponse ValidateSerialForPurchaseReturnEdit(long propertyId, IList<InvoiceDetailViewModel> editedList, List<InvoiceDetail> mainInvoiceList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in editedList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = editedList.Where(i => i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }

            List<InvoiceDetailViewModel> newList = new List<InvoiceDetailViewModel>(editedList);
            HashSet<string> productSerialList = new HashSet<string>(mainInvoiceList.Select(x => x.ProductSerial));

            newList.RemoveAll(x => productSerialList.Contains(x.ProductSerial));

            DbResponse result = CheckSerialExistForPurchaseReturn(newList, propertyId); //Same Serial Can't Exist On DB For Purchase And Bonus
            if (result.MessageType == 2)
            {
                return result;
            }

            return AutoResponse.SuccessMessage();
        }
        InvoiceDetail GetStockBySerial(InvoiceDetailViewModel invoiceDetailViewModel, ERPEntities db)
        {
            var stock = db.InvoiceDetails.Where(i => i.Status == 1 && i.ProductId == invoiceDetailViewModel.ProductId && i.ProductSerial == invoiceDetailViewModel.ProductSerial).Select(i => new
            {
                ProductId = i.ProductId,
                BuyingPrice = i.BuyingPrice,
                Quantity = i.StockIn == PropertyConstants.StockIn ? i.Quantity : -i.Quantity
            }).ToList();
            var stockQuantity = stock.GroupBy(i => i.ProductId).Select(i => new InvoiceDetail
            {
                ProductId = i.Key,
                BuyingPrice = i.FirstOrDefault().BuyingPrice,
                Quantity = i.Sum(j => j.Quantity)
            }).FirstOrDefault();
            return stockQuantity;
        }
        #endregion

        //I've No Idea What Is It, Or It's Usage...
        private DbResponse ValidatePurchaseReturn(PartialInvoiceInfo param, IEnumerable<PartialProduct> productlist, ERPEntities dbEntities)
        {
            var stockManager = new StockManager();
            if (param.PropertyId == PropertyConstants.PurchaseReturn)
            {
                foreach (var detail in productlist)
                {
                    var qty = stockManager.GetStockByProduct(detail.Id);
                    var Product = dbEntities.Products.FirstOrDefault(i => i.Id == detail.Id);
                    if (qty < detail.PurchaseUnit)
                        return new DbResponse
                        {
                            MessageType = 2,
                            MessageData = "Not Enough stock to return of " + Product.ProductName
                        };
                }
            }
            return new DbResponse { MessageType = 1 };
        }
    }
}
