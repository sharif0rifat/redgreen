﻿using ERP.DAL.Model;
using ERP.REPOSITORIES.Repositories;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.Mvc;

namespace ERP.REPOSITORIES.Helper
{
    public static class DropDownListClass
    {
        public static SelectList GetTemplates()
        {
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbTemplates = dbEntities.Templates.ToList();
                    var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
                    if (dbTemplates.Count == 0) return new SelectList(list);
                    foreach (var template in dbTemplates)
                    {
                        var addTo = new SelectListItem { Value = template.Id.ToString(CultureInfo.InvariantCulture), Text = template.TemplateName };
                        list.Add(addTo);
                    }
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static SelectList GetApplications()
        {
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbApplications = dbEntities.Applications.ToList();
                    var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
                    if (dbApplications.Count == 0) return new SelectList(list);
                    foreach (var application in dbApplications)
                    {
                        var addTo = new SelectListItem { Value = application.Id.ToString(CultureInfo.InvariantCulture), Text = application.AppName };
                        list.Add(addTo);
                    }
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public static SelectList GetCategories()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbCategories = dbEntities.Categories.Where(i => i.Status == 1).ToList();
                    //if (dbCategories.Count == 0) return new SelectList(list);
                    list.AddRange(dbCategories.Select(category => new SelectListItem { Value = category.Id.ToString(CultureInfo.InvariantCulture), Text = category.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetProducts()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbProducts = dbEntities.Products.Where(i => i.Status == 1).ToList();
                    //if (dbCategories.Count == 0) return new SelectList(list);
                    list.AddRange(dbProducts.Select(product => new SelectListItem { Value = product.Id.ToString(CultureInfo.InvariantCulture), Text = product.ProductName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetEmployees()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbEmployees = dbEntities.Employees.Where(i => i.Status == 1).ToList();
                    //if (dbCategories.Count == 0) return new SelectList(list);
                    list.AddRange(dbEmployees.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.EmployeeName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetPaymentSubHeaderList()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    long[] nonEditableLedgers = { 1, 2, 3, 4, 5 };
                    var trHeader =
                        dbEntities.LedgerGroups.Where(i => i.GroupHeaderId == 2).Select(i => i.Id).ToList();        //GroupHeaderId=  : Current Asset
                    var dbLedgers = dbEntities.Ledgers.Where(i => i.Status == 1 && trHeader.Contains(i.LedgerGroupId) 
                        && !nonEditableLedgers.Contains(i.Id)
                        && i.PropertyId!=PropertyConstants.Supplier
                        &&i.PropertyId!=PropertyConstants.Dealer).ToList();
                    list.AddRange(dbLedgers.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetTransactionLedgrList()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    long[] nonEditableLedgers = { 1, 2, 3, 4, 5 };
                    var dbLedgers = dbEntities.Ledgers.Where(i => i.Status == 1 && (i.PropertyId != 10 && i.PropertyId != 11) && !nonEditableLedgers.Contains(i.Id)).ToList();
                    list.AddRange(dbLedgers.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetOpeningTransactionLedgrList()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    long[] nonEditableLedgers = { 1, 2, 3, 4, 5 };
                    var dbLedgers = dbEntities.Ledgers.Where(i => i.Status == 1 && !nonEditableLedgers.Contains(i.Id)).ToList();
                    list.AddRange(dbLedgers.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetSubHeaderList()
        {
            var manager = new LedgerGroupManager();
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    //long[] reservedLedgers = { 1, 2,  4, 5, 6, 7, 8, 9, 10, 11, 12 };
                    long[] reservedLedgers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 };
                    var dbSubHeaders =
                        dbEntities.Ledgers.Where(
                            i =>
                                i.Status == 1 && i.PropertyId ==0 && !reservedLedgers.Contains(i.Id)).ToList();
                    list.AddRange(dbSubHeaders.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetLedgerGroupList()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbHeaders = dbEntities.GroupHeaders.Where(i => i.Status == 1).ToList();
                    //if (dbCategories.Count == 0) return new SelectList(list);
                    list.AddRange(dbHeaders.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetLedgerGroupListForLedger()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    long[] nonEditableLedgers = { 1, 2, 3, 4, 5, 6, 7, 8 };
                    var dbHeaders = dbEntities.LedgerGroups.Where(i => i.IsParent == false && !nonEditableLedgers.Contains(i.Id)).ToList();
                    list.AddRange(dbHeaders.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetLedgerHeader()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbHeaders = dbEntities.Ledgers.Where(i => i.Status == 1).ToList();
                    list.AddRange(dbHeaders.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetLedgerGroup()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbHeaders = dbEntities.LedgerGroups.Where(i => i.IsParent == false).ToList();
                    list.AddRange(dbHeaders.Select(i => new SelectListItem { Value = i.Id.ToString(CultureInfo.InvariantCulture), Text = i.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetParties()
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbPartyList = dbEntities.Ledgers.Where(x => x.Status == 1 && (x.PropertyId == PropertyConstants.Supplier || x.PropertyId == PropertyConstants.Dealer)).ToList();
                    list.AddRange(dbPartyList.Select(party => new SelectListItem { Value = party.Id.ToString(CultureInfo.InvariantCulture), Text = party.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetParties(long partyType)
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbPartyList = dbEntities.Ledgers.Where(x => x.Status == 1 && x.PropertyId == partyType).ToList();
                    if (dbPartyList.Count == 0) return new SelectList(list, "Value", "Text");
                    list.AddRange(dbPartyList.Select(party => new SelectListItem { Value = party.Id.ToString(CultureInfo.InvariantCulture), Text = party.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
        public static SelectList GetPartiesForSales(long partyType)
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbPartyList = dbEntities.Ledgers.Where(x => x.Status == 1 && x.PropertyId == partyType).ToList();
                    dbPartyList.Insert(0,new Ledger{Id = -1,Name="New party"});
                    if (dbPartyList.Count == 0) return new SelectList(list, "Value", "Text");
                    list.AddRange(dbPartyList.Select(party => new SelectListItem { Value = party.Id.ToString(CultureInfo.InvariantCulture), Text = party.Name }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }

        public static dynamic GetPaymentTypes()
        {
            var list = new List<SelectListItem>();
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var paymenTypeList = dbEntities.TableProperties.Where(i => i.PropertyName == "PaymentType").ToList();
                    if (paymenTypeList.Count == 0) return new SelectList(list);
                    list.AddRange(paymenTypeList.Select(type => new SelectListItem { Value = type.PropertyValue.ToString(), Text = type.ViewName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }



        public static dynamic GetInvoiceTypes(string propertyName)
        {
            var list = new List<SelectListItem>();
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var paymenTypeList = dbEntities.TableProperties.Where(i => i.PropertyName == propertyName).ToList();
                    if (paymenTypeList.Count == 0) return new SelectList(list);
                    list.AddRange(paymenTypeList.Select(type => new SelectListItem { Value = type.Id.ToString(), Text = type.ViewName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }

        public static object GetProductListByCategory(long categoryId)
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbProducts = dbEntities.Products.Where(i => i.Status == 1 && i.CategoryId == categoryId).ToList();
                    //if (dbCategories.Count == 0) return new SelectList(list);
                    list.AddRange(dbProducts.Select(product => new SelectListItem { Value = product.Id.ToString(CultureInfo.InvariantCulture), Text = product.ProductName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }





        public static object GetProductListByCategoryAndType(long categoryId)
        {
            var list = new List<SelectListItem> { new SelectListItem { Value = "0", Text = "Select One" } };
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbProducts = dbEntities.Products.Where(p => p.CategoryId == categoryId).ToList();
                    list.AddRange(dbProducts.Select(product => new SelectListItem { Value = product.Id.ToString(CultureInfo.InvariantCulture), Text = product.ProductName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text"); ;
            }
        }

        public static SelectList GetCurrencies()
        {
            //var list = new List<SelectListItem> { new SelectListItem { Value = "", Text = "Select One" } };
            var list = new List<SelectListItem>();
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbCurrencies = dbEntities.Currencies.ToList();
                    list.AddRange(dbCurrencies.Select(c => new SelectListItem { Value = c.ID.ToString(CultureInfo.InvariantCulture), Text = c.ShortName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }

        public static dynamic GetPaymentStatus()
        {
            var list = new List<SelectListItem>();
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var paymenTypeList = dbEntities.TableProperties.Where(i => i.PropertyName == "PaymentStatus").ToList();
                    if (paymenTypeList.Count == 0) return new SelectList(list);
                    list.AddRange(paymenTypeList.Select(type => new SelectListItem { Value = type.Id.ToString(), Text = type.ViewName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }

        public static dynamic SalesTypes()
        {
            var list = new List<SelectListItem>();
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var paymenTypeList = dbEntities.TableProperties.Where(i => i.PropertyName == "SalesType").ToList();
                    if (paymenTypeList.Count == 0) return new SelectList(list);
                    list.AddRange(paymenTypeList.Select(type => new SelectListItem { Value = type.Id.ToString(), Text = type.ViewName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }

        public static dynamic GetGiftProperty()
        {
            var list = new List<SelectListItem>();
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var giftList = dbEntities.TableProperties.Where(i => i.Id==36).ToList();
                    if (giftList.Count == 0) return new SelectList(list);
                    list.AddRange(giftList.Select(type => new SelectListItem { Value = type.Id.ToString(), Text = type.ViewName }));
                    return new SelectList(list, "Value", "Text");
                }
            }
            catch (Exception)
            {
                return new SelectList(list, "Value", "Text");
            }
        }
    }
    public class BooleanDdl
    {
        public bool Value { get; set; }
        public string Text { get; set; }
    }
}
