﻿using System;
using System.Collections.Generic;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using Party=ERP.INFRASTRUCTURE.Party;
namespace ERP.REPOSITORIES.IRepositories
{
    public interface IPartyManager : IGenericRepository<ERP.DAL.Model.Ledger>, IDisposable
    {
        List<LedgerViewModel> GetPartiesByType(int partyType);
        LedgerViewModel GetParty(long partyId);
        DbResponse AddParty(LedgerViewModel obj, LogInInfo logInInfo);
        DbResponse EditParty(LedgerViewModel obj, LogInInfo logInInfo);
        DbResponse DeleteParty(long id, LogInInfo logInInfo);
    }
}
