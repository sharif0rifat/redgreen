﻿using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IUserRepo
    {
        // get all users
        DbResponse GetUsers();
        // get users by param key and param value
        DbResponse GetUsers(string rootUser,long applicationId);
        DbResponse GetUser(long param);
        DbResponse AddUser(PartialUser partialUser);
        DbResponse EditUser(PartialUser partialUser);
        DbResponse DeleteUser(long paramId);
        DbResponse ResetPassword(long param);
        DbResponse ReleaseLogin(long param);
        DbResponse LockRelease(long param);
    }
}
