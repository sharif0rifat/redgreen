﻿using System;
using System.Collections.Generic;
using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Generics;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IStockManager : IGenericRepository<StockViewModel>, IDisposable
    {
        List<StockViewModel> GetStocks(string stockType);
        decimal GetStockByProduct(long productId);

        decimal LoadAmountByQuantity(long modelId, long quantity);

        decimal LoadAmountByQuantityForEdit(long modelId, long quantity, long billId);

        IEnumerable<StockViewModel> GetCurrentStocks();
    }
}
