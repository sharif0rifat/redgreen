﻿using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IAssignTemplateRepo
    {
        DbResponse Register(PartialUser param);
    }
}
