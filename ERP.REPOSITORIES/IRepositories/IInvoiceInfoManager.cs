﻿using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using System;
using System.Collections.Generic;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IInvoiceInfoManager : IGenericRepository<InvoiceInfo>, IDisposable
    {
        long LoadFinalInvoiceNo(int salesType);
        string GetInvoiceNo(long propertyId);
        List<PartialInvoiceInfo> GetInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null, DateTime? toDate = null);
        PartialInvoiceInfo GetInvoiceInfo(long billId);
        DbResponse DeleteInvoiceInfo(long billId, LogInInfo logInInfo);
        List<PartialInvoiceInfo> GetInvoiceInfoByDateRange(long propertyvalue, DateTime datefrom, DateTime dateto);
        void UpdateInvoice(InvoiceInfo invoiceInfo, LogInInfo logInInfo);
        PartialInvoiceInfo GetLastInvoice(Constants.PartyType partyType);
        PartialInvoiceInfo GetInvoiceByInvoiceId(long billId);

        List<PartialInvoiceInfo> GetPurchaseInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null,
            DateTime? toDate = null);

        List<PartialInvoiceInfo> GetSalesInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null,
            DateTime? toDate = null);

        DbResponse AddSalesInvoice(PartialInvoiceInfo invoiceInfo, List<PartialProduct> productlist, LogInInfo logInInfo);
        DbResponse EditSalesInvoice(PartialInvoiceInfo param, List<PartialProduct> productlist, LogInInfo loginInfo);

        string GetLastInvoiceNo(long invoiceType);

        DbResponse DeleteInvoiceFromDB(long id, LogInInfo logInInfo);

        InvoiceInfo GetDebitCreditByInvoiceType(InvoiceInfo invoiceInfo);

        InvoiceInfo AddInvoice(InvoiceInfo invoiceInfo, LogInInfo logInInfo);
        //bool IsExistSerial(IList<InvoiceDetailViewModel> invoiceDetailsList, long properyId);

        DbResponse ValidateSerial(long propertyId, IList<InvoiceDetailViewModel> list);

        string GetDateGeneratedInvoiceNo(long propertyId);
    }
}
