﻿using System;
using System.Collections.Generic;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using Employee=ERP.INFRASTRUCTURE.Employee;
namespace ERP.REPOSITORIES.IRepositories
{
    public interface IEmployeeManager : IGenericRepository<ERP.DAL.Model.Employee>, IDisposable
    {
        Employee GetEmployee(long employeeId);
        List<Employee> GetEmployees(bool isSr=false);
        DbResponse AddEmployee(Employee obj, LogInInfo logInInfo);
        DbResponse EditEmployee(Employee obj, LogInInfo logInInfo);
        DbResponse DeleteEmployee(long id, LogInInfo logInInfo);
    }
}
