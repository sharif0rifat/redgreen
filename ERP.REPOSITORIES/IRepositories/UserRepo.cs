﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EntityLayer.Models;
using Infrastructure.CoreModel;
using Infrastructure.ModelHelpers;
using Repository.Helpers;
using Repository.RepoInterfaces;

namespace Repository.Repos
{
    public class UserRepo : IUserRepo
    {
        public DbResponse GetUsers()
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var users = db.Users.Where(i=>i.Id >1).ToList();
                    if (users.Count == 0) return AutoResponse.FailedMessageWithParam("No user found.");
                    return AutoResponse.SuccessMessageWithParam(ConvertToLocalObjects(users, new List<PartialUser>()));
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse GetUsers(string rootUser, long applicationId)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var users = (rootUser == ConstantValue.Root) ? db.Users.Where(i => i.Id > 1).ToList() : db.Users.Where(i => i.Id > 1 && i.ApplicationId == applicationId).ToList();
                    if (users.Count == 0) return AutoResponse.FailedMessageWithParam("No user found.");
                    var dbUsers = ConvertToLocalObjects(users, new List<PartialUser>());
                    var dbGroups = db.Groups.ToList();
                    var dbApplications = db.Applications.ToList();
                    var dbTemplates = db.Templates.ToList();
                    foreach (var partialUser in dbUsers)
                    {
                        partialUser.GroupName = dbGroups.Where(i => i.Id == partialUser.GroupId).Select(i => i.GroupName).SingleOrDefault();
                        partialUser.ApplicationName = dbApplications.Where(i => i.Id == partialUser.ApplicationId).Select(i => i.AppName).SingleOrDefault();
                        partialUser.TemplateName = dbTemplates.Where(i => i.Id == partialUser.TemplateId).Select(i => i.TemplateName).SingleOrDefault();
                    }
                    return AutoResponse.SuccessMessageWithParam(dbUsers);
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse GetUser(long param)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var users = db.Users.FirstOrDefault(i=>i.Id == param);
                    return AutoResponse.SuccessMessageWithParam(ConvertToLocalObject(users, new PartialUser()));
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse AddUser(PartialUser partialUser)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    if (partialUser.Password != partialUser.ConfirmPassword)
                        return AutoResponse.FailedMessageWithParam("Password Does Not Matched.");
                    var exist = db.Users.Any(i => i.UserName == partialUser.UserName && i.Status == 1 );
                    if (exist) return AutoResponse.FailedMessageWithParam("UserName Already Exist.");

                    var register = db.Registers.FirstOrDefault(i => i.RegisterCode == partialUser.UserName && i.Status == true);
                    if (register == null) return AutoResponse.FailedMessageWithParam("User Registration Error.");
                    partialUser.UserName = register.RegisterCode;
                    partialUser.IsActive = true;
                    partialUser.Islogin = false;
                    partialUser.CreatedBy = 1;
                    partialUser.CreatedDate = DateTime.Now;
                    partialUser.GroupId = register.GroupId ?? 0;
                    partialUser.TemplateId = 0;
                    partialUser.ApplicationId = 1;
                    partialUser.Status = 1;
                    var dbUser = ConvertToEntityObject(partialUser, new User());
                    db.Users.Add(dbUser);
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse EditUser(PartialUser partialUser)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse DeleteUser(long paramId)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var dbRegister = db.Users.FirstOrDefault(i => i.Id == paramId);
                    if (dbRegister == null) return AutoResponse.FailedMessage();
                    dbRegister.Status = 0;
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }


        public DbResponse ResetPassword(long param)
        {
            using (var db = new TerminalEntities())
            {
                var users = db.Users.FirstOrDefault(i=>i.Id == param);
                if (users == null) return AutoResponse.FailedMessageWithParam("Data not exist.");
                users.Password = users.UserName;
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return AutoResponse.SuccessMessage();
            }
        }

        public DbResponse ReleaseLogin(long param)
        {
            using (var db = new TerminalEntities())
            {
                var users = db.Users.FirstOrDefault(i => i.Id == param);
                if (users == null) return AutoResponse.FailedMessageWithParam("Data not exist.");
                users.Islogin = false;
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return AutoResponse.SuccessMessage();
            }
        }

        public DbResponse LockRelease(long param)
        {
            using (var db = new TerminalEntities())
            {
                var users = db.Users.FirstOrDefault(i => i.Id == param);
                if (users == null) return AutoResponse.FailedMessageWithParam("Data not exist.");
                users.Status = Convert.ToInt32(AutoSequence.UserStatus.Active);
                db.Entry(users).State = EntityState.Modified;
                db.SaveChanges();
                return AutoResponse.SuccessMessage();
            }
        }

        // conversion terminal db list will converted to local object
        private List<PartialUser> ConvertToLocalObjects(List<User> convertedFrom,List<PartialUser> convertedTo)
        {
            var converter = new ConvertTypeInto<User, PartialUser>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
        // db object will converted to local object
        public PartialUser ConvertToLocalObject(User convertedFrom, PartialUser convertedTo)
        {
            var converter = new ConvertTypeInto<User, PartialUser>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
        // local object will converted to db object
        public User ConvertToEntityObject(PartialUser convertedFrom, User convertedTo)
        {
            var converter = new ConvertTypeInto<PartialUser, User>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
    }
}
