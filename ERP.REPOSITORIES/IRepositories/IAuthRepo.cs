﻿using ERP.INFRASTRUCTURE.Global;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IAuthRepo
    {
        DbResponse Login(string param1, string param2, long param3);
        DbResponse Logout(long param);
        DbResponse ResetPasswordRequest(long param);
    }
}
