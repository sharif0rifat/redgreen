﻿using System;
using System.Collections.Generic;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
namespace ERP.REPOSITORIES.IRepositories
{
    public interface ILedgerGroupManager : IGenericRepository<DAL.Model.LedgerGroup>, IDisposable
    {
        List<LedgerGroupViewModel> GetLedgerGroups(int? transactionType = null);
        List<LedgerViewModel> GetLedgers(int? transactionType = null);
        DbResponse DeleteLedgerGroup(long id, LogInInfo logInInfo);
        DbResponse EditLedgerGroup(LedgerGroupViewModel obj, LogInInfo logInInfo);
        DbResponse AddLedgerGroup(LedgerGroupViewModel headerModel,LogInInfo _logInInfo);
        LedgerGroupViewModel GetLedgerGroup(long id);
        DbResponse AddLedger(LedgerViewModel transactionSubHeaderModel, LogInInfo _logInInfo);
        DbResponse EditLedger(LedgerViewModel transactionSubHeaderModel, LogInInfo logInInfo);
        LedgerViewModel GetLedger(long id);

        DbResponse DeleteLedger(long id, LogInInfo logInInfo);
    }
}
