﻿using System;
using System.Collections.Generic;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IPurchaseInvoiceManager : IGenericRepository<InvoiceInfo>, IDisposable
    {
        List<PartialInvoiceInfo> GetPurchaseInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null,
            DateTime? toDate = null);
        DbResponse AddPurchaseInvoice(PartialInvoiceInfo invoiceInfo, LogInInfo logInInfo);
        DbResponse EditPurchaseInvoice(PartialInvoiceInfo param, LogInInfo loginInfo);


    }
}
