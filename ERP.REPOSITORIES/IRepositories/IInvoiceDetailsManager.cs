﻿using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using System;
using System.Collections.Generic;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IInvoiceDetailsManager : IGenericRepository<InvoiceDetail>, IDisposable
    {

        InvoiceDetail GetInvoiceDetailsById(long id);
        InvoiceDetail AddInvoiceDetails(InvoiceDetail invoiceDetail);
        void UpdateInvoiceDetails(InvoiceDetail invoiceDetail);
        List<InvoiceDetailViewModel> GetInvoiceDetails(long billId);
        List<InvoiceDetailViewModel> GetInvoiceDetailsWithProduct(long billId);

        DbResponse DeleteInvoiceDetailsFromDBByInvoiceId(long invoiceId, LogInInfo logInInfo);
        DbResponse AddInvoiceDetailListToDB(IList<InvoiceDetailViewModel> partialInvoiceDetail, long invoiceId, long propertyId, LogInInfo logInInfo);

        InvoiceDetail GetInvoiceDetailsBySerial(string serial);
                
        void DeleteDetailByInvoiceId(long invoiceId);
        //void ChangeStockFlagForInvoiceDetails(List<InvoiceDetailViewModel> detailList, long propertyId);
    }
}
