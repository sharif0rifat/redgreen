﻿using System.Collections.Generic;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using Infrastructure.ModelHelpers;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IApplicationRepo
    {
        // get all Applications
        DbResponse GetApplicaitons();
        // get Applications by param key and param value
        DbResponse GetApplicaitons(List<Searchy> searchies);
        DbResponse Register(PartialApplication partialApplication);
        DbResponse AddApplication(PartialApplication param);
    }
}
