﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EntityLayer.Models;
using Infrastructure.CoreModel;
using Infrastructure.ModelHelpers;
using Repository.Helpers;
using Repository.RepoInterfaces;

namespace Repository.Repos
{
    public class AssignTemplateRepo : IAssignTemplateRepo
    {
        public DbResponse Register(PartialUser param)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var dbUser = db.Users.FirstOrDefault(i => i.Id == param.Id);
                    if (dbUser == null) return AutoResponse.FailedMessage();
                    dbUser.TemplateId = param.TemplateId;
                    db.Entry(dbUser).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return AutoResponse.SuccessMessage();
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }

        
    }
}
