﻿using System;
using System.Collections.Generic;
using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Generics;
using ERP.INFRASTRUCTURE.Global;

namespace ERP.REPOSITORIES.IRepositories
{
    public interface IProductManager : IGenericRepository<DAL.Model.Product>, IDisposable
    {
        DbResponse AddProduct(PartialProduct productObj, LogInInfo logInInfo);
        DbResponse EditProduct(PartialProduct productObj, LogInInfo logInInfo);
        DbResponse DeleteProduct(long productId, LogInInfo logInInfo);
        PartialProduct GetProduct(long productId);
        List<PartialProduct> GetProducts();
        List<PartialProduct> GetModelsByCategory(long categoryId);
        PartialProduct GetCurrentQuantityByModel(PartialProduct param);
        DbResponse GetIndividualProductStock(long productId);
        DbResponse GetIndividualProductStockSell(long productId);
    }
}

