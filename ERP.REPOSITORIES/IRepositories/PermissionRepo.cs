﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using EntityLayer.Models;
using Infrastructure.CoreModel;
using Infrastructure.ModelHelpers;
using Repository.Helpers;
using Repository.RepoInterfaces;

namespace Repository.Repos
{
    public class PermissionRepo : IPermissionRepo
    {
        public DbResponse GetPremissions()
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var permissions = db.Permissions.ToList();
                    if (permissions.Count == 0) return AutoResponse.NotFoundMessage();
                    return AutoResponse.SuccessMessageWithParam(ConvertToLocalObjects(permissions, new List<PartialPermission>()));
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse GetPremissions(List<Searchy> searchies)
        {
            throw new NotImplementedException();
        }

        public DbResponse Register(PartialPermission permission)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var dbPermission =db.Permissions.FirstOrDefault(i =>i.PermissionCode == permission.PermissionCode && i.ApplicationId == permission.ApplicationId);
                    if (dbPermission == null)
                    {
                        var uploadData = ConvertToEntityObject(permission, new Permission());
                        uploadData.Status = true;
                        db.Permissions.Add(uploadData);
                        db.SaveChanges();
                        return AutoResponse.SuccessMessage();
                    }
                    dbPermission.PermissionCode = permission.PermissionCode;
                    dbPermission.ControllerName = permission.ControllerName;
                    dbPermission.ActionName = permission.ActionName;
                    dbPermission.Status = true;
                    dbPermission.Title = dbPermission.Title;
                    db.Entry(dbPermission).State = EntityState.Modified;
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse GetPremissions(long applicationId)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var dbPermissions = db.Permissions.Where(i => i.ApplicationId == applicationId).ToList();
                    return AutoResponse.SuccessMessageWithParam(dbPermissions);
                }
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage();
            }
        }

        public DbResponse GetPermission(string permissionCode)
        {
            try
            {
                using (var db = new TerminalEntities())
                {
                    var permissions = db.Permissions.FirstOrDefault(i=>i.PermissionCode == permissionCode);
                    return permissions == null ? AutoResponse.NotFoundMessage() : AutoResponse.SuccessMessageWithParam(ConvertToLocalObject(permissions, new PartialPermission()));
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public List<PartialPermission> ConvertToLocalObjects(List<Permission> convertedFrom, List<PartialPermission> convertedTo)
        {
            var converter = new ConvertTypeInto<Permission, PartialPermission>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
        // db object will converted to local object
        public PartialPermission ConvertToLocalObject(Permission convertedFrom, PartialPermission convertedTo)
        {
            var converter = new ConvertTypeInto<Permission, PartialPermission>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
        // local object will converted to db object
        public Permission ConvertToEntityObject(PartialPermission convertedFrom, Permission convertedTo)
        {
            var converter = new ConvertTypeInto<PartialPermission, Permission>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
    }
}
