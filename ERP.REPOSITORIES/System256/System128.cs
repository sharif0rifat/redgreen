﻿using System;
using System.Globalization;
using System.Linq;

namespace ERP.REPOSITORIES.System256
{
    public static class System128
    {
        public static DateTime GetToday { get { return DateTime.Today; } }
        public static string GenerateGuid()
        {
            const string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            var random = new Random();
            var guid = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return guid;
        }

        public static string PasswordGenerator()
        {
            const string chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789@#*$!";
            var random = new Random();
            var passwordGenerator = new string(
                Enumerable.Repeat(chars, 8)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());
            return passwordGenerator;
        }
    

        public static string GenerateCode(string title, long lastId)
        {
            var serial = lastId.ToString(CultureInfo.InvariantCulture).PadLeft(lastId.ToString(CultureInfo.InvariantCulture).Length + 6 - lastId.ToString(CultureInfo.InvariantCulture).Length, '0');
            return title + serial;
        }
        
        public static DateTime GetCurrentDateTime()
        {
            return DateTime.Today.AddHours(13);
        }
    }
}
