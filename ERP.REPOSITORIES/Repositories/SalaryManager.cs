﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using Salary = ERP.INFRASTRUCTURE.Salary;

namespace ERP.REPOSITORIES.Repositories
{
    public class SalaryManager : GenericRepository<ERPEntities, DAL.Model.Salary>, ISalaryManager
    {
        private ERPEntities dbEntities;
        public SalaryManager()
        {
            dbEntities = new ERPEntities();
        }

        public List<Salary> GetSalaries()
        {
            var startDate = DateTime.Today.AddMonths(-3);
            var endDate = DateTime.Today;
            var dbObjList = dbEntities.Salaries.Where(x => x.Status == 1 &&(x.Month>=startDate && x.Month<=endDate)).Select(i => i).ToList();

            return (from dbObj in dbObjList
                    let employee = dbEntities.Employees.FirstOrDefault(i => i.Id == dbObj.EmployeeId)
                    where employee != null
                    let employeeName = employee.EmployeeName
                    where employeeName != null
                    select new Salary
                    {
                        Id = dbObj.Id,
                        EmployeeId = dbObj.EmployeeId,
                        SalryType = dbObj.SalryType,
                        TotalSalary = dbObj.TotalSalary,
                        Month = dbObj.Month,
                        SalaryTypeName = dbEntities.TableProperties.FirstOrDefault(p=>p.Id== dbObj.SalryType).ViewName,
                        EmployeeName = employeeName
                    }).ToList();
        }

        public Salary GetSalary(long id, ERPEntities dbEntities)
        {
            var dbObj = dbEntities.Salaries.SingleOrDefault(x => x.Id == id && x.Status == 1);
            var obj = new Salary();
            ConvertDbSalaryToSalary(dbObj, obj);

            return obj;

        }

        public DbResponse AddSalary(Salary obj, ERPEntities dbEntities)
        {

            try
            {

                if (obj.SalryType == (int)Constants.SalaryType.Current)
                {
                    var employeeSalary =
                        dbEntities.Salaries.Where(
                            x =>
                                x.EmployeeId == obj.EmployeeId && x.Month.Value.Month == obj.Month.Value.Month &&
                                x.Month.Value.Year == obj.Month.Value.Year).ToList();
                    if (employeeSalary.Count > 0)
                    {
                        return AutoResponse.FailedMessageWithParam("Salary is already given.");
                    }
                }
                if (obj.SalryType == (int)Constants.SalaryType.Advance)
                {

                    if (obj.Month.Value.Month < DateTime.Today.Month && obj.Month.Value.Year <= DateTime.Today.Year)
                    {
                        return AutoResponse.FailedMessageWithParam("Can not give advance in previous date.");
                    }
                }
                var dbObj = ConvertSalaryToDbSalary(obj, new DAL.Model.Salary());
                if (obj.CreditId < 1)
                    return AutoResponse.FailedMessageWithParam("Select a Credit for salary");
                dbObj.DebitId = 5;       //Lehger 5  is salary Expense;
                dbObj.Status = 1;
                dbObj.CreatedDate = DateTime.Today;
                dbObj.ModifiedDate = DateTime.Today;
                dbEntities.Salaries.Add(dbObj);
                dbEntities.SaveChanges();
                return AutoResponse.SuccessMessage();
            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }

        }

        public DbResponse DeleteSalary(long id, ERPEntities dbEntities)
        {
            try
            {
                var dbObj = dbEntities.Salaries.FirstOrDefault(i => i.Id == id);
                if (dbObj == null) return AutoResponse.NotFoundMessage();
                dbObj.Status = 0;
                Edit(dbObj); Save();
                return AutoResponse.SuccessMessageWithParam("Salary Has been Deleted");
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse EditSalary(Salary obj, ERPEntities dbEntities)
        {
            try
            {
                if (obj.SalryType == (int)Constants.SalaryType.Current)
                {
                    var employeeSalary =
                        dbEntities.Salaries.Where(
                            x => x.Id != obj.Id &&
                                x.EmployeeId == obj.EmployeeId &&
                                x.SalryType == (int)Constants.SalaryType.Current &&
                                x.Month.Value.Month == obj.Month.Value.Month &&
                                x.Month.Value.Year == obj.Month.Value.Year).ToList();
                    if (employeeSalary.Count > 0)
                    {
                        return AutoResponse.FailedMessageWithParam("Salary is already given.");
                    }
                }
                if (obj.SalryType == (int)Constants.SalaryType.Advance)
                {

                    if (obj.Month.Value.Month < DateTime.Today.Month && obj.Month.Value.Year <= DateTime.Today.Year)
                    {
                        return AutoResponse.FailedMessageWithParam("Can not give advance in previous date.");
                    }
                }
                if (obj.CreditId<1)
                    return AutoResponse.FailedMessageWithParam("Selecta Credit for salary");
                obj.DebitId = 5;       //Lehger 5  is salary Expense;
                var dbObj = dbEntities.Salaries.FirstOrDefault(i => i.Id == obj.Id);
                if (dbObj == null) return AutoResponse.NotFoundMessage();
                dbObj.EmployeeId = obj.EmployeeId;
                dbObj.SalryType = obj.SalryType;
                dbObj.TotalSalary = obj.TotalSalary;
                dbObj.BasicSalary = obj.BasicSalary;
                dbObj.HouseRent = obj.HouseRent;
                dbObj.MobileAllowance = obj.MobileAllowance;
                dbObj.TransportCost = obj.TransportCost;
                dbObj.FestivalBonus = obj.FestivalBonus;

                dbObj.MedicalAllowance = obj.MedicalAllowance;
                dbObj.OtherAllowance = obj.OtherAllowance;
                dbObj.OtherDeduction = obj.OtherDeduction;

                dbObj.DebitId = 5;       //Lehger 5  is salary Expense;
                dbObj.CreditId = obj.CreditId;
                dbObj.TotalBill = obj.TotalBill;
                dbObj.Month = obj.Month;
                dbObj.ModifiedDate = DateTime.Today;
                dbObj.Status = 1;
                dbEntities.Entry(dbObj).State = EntityState.Modified;
                dbEntities.SaveChanges();
                return AutoResponse.SuccessMessage();
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DAL.Model.Salary ConvertSalaryToDbSalary(Salary obj, DAL.Model.Salary dbObj)
        {
            var objConvert = new ConvertTypeInto<Salary, DAL.Model.Salary>();
            return objConvert.ConvertInto(obj, dbObj);
        }
        public Salary ConvertDbSalaryToSalary(DAL.Model.Salary dbObj, Salary obj)
        {
            var objConvert = new ConvertTypeInto<DAL.Model.Salary, Salary>();
            return objConvert.ConvertInto(dbObj, obj);
        }

    }
}
