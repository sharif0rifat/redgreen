﻿using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using Employee = ERP.INFRASTRUCTURE.Employee;

namespace ERP.REPOSITORIES.Repositories
{
    public class EmployeeManager : GenericRepository<ERPEntities, DAL.Model.Employee>, IEmployeeManager
    {
        public Employee GetEmployee(long employeeId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbEmployee = db.Employees.Where(x => x.Id == employeeId).ToList().SingleOrDefault();
                    return Mapper.Map<Employee>(dbEmployee);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<Employee> GetEmployees(bool isSr = false)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbEmployees = isSr ? db.Employees.Where(x => x.Status == 1).ToList() : db.Employees.Where(x => x.Status == 1).ToList();
                    return Mapper.Map<List<Employee>>(dbEmployees);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public DbResponse AddEmployee(Employee obj, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    if (obj.Id > 0)
                    {
                        var isExist = db.Employees.FirstOrDefault(i => i.Id == obj.Id);
                        if (isExist == null) return AutoResponse.NotFoundWithParam("Employee not found.");
                        isExist.EmployeeName = obj.EmployeeName;
                        isExist.EmployeeCode = obj.EmployeeCode;
                        isExist.Location = obj.Location;
                        isExist.Department = obj.Department;
                        isExist.Designation = obj.Designation;
                        isExist.DateOfBirth = obj.DateOfBirth;
                        isExist.BasicSalary = obj.BasicSalary;
                        isExist.MobileInternet = obj.MobileInternet;
                        isExist.Transport = obj.Transport;
                        isExist.Wellbeing = obj.Wellbeing;
                        isExist.Others = obj.Others;
                        isExist.Advance = obj.Advance;
                        isExist.LeaveDeduction = obj.LeaveDeduction;
                        isExist.LateDeduction = obj.LateDeduction;
                        isExist.MiscellaneousDeduction = obj.MiscellaneousDeduction;
                        isExist.Tax = obj.Tax;
                        isExist.ProvidentFund = obj.ProvidentFund;
                        isExist.ModifiedBy = logInInfo.UserId;
                        isExist.ModifiedDate = DateTime.Today;
                        isExist.Status = 1;
                        Edit(isExist);
                        Save();
                        return AutoResponse.SuccessMessage();
                    }
                    var dbEmployee = Mapper.Map<DAL.Model.Employee>(obj);
                    var checkObj = db.Employees.Any(i => i.EmployeeCode == dbEmployee.EmployeeCode);
                    if (checkObj) return AutoResponse.ExistMessageWithParam("Duplicate Code..Employee with this Code Already Exist. ");
                    dbEmployee.Status = 1;
                    dbEmployee.CreatedBy = dbEmployee.ModifiedBy = logInInfo.UserId;
                    dbEmployee.ModifiedDate = dbEmployee.CreatedDate = DateTime.Today;
                    db.Employees.Add(dbEmployee);
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (DbEntityValidationException e)
            {
                return AutoResponse.FailedMessageWithParam(e.ToString());
            }
            catch (Exception e)
            {

                return AutoResponse.FailedMessageWithParam(e.ToString());
            }
        }
        public DbResponse EditEmployee(Employee obj, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var isExist = db.Employees.FirstOrDefault(i => i.Id == obj.Id);
                    if (isExist == null) return AutoResponse.NotFoundWithParam("Employee not found.");
                    isExist.EmployeeName = obj.EmployeeName;
                    isExist.EmployeeCode = obj.EmployeeCode;
                    isExist.Location = obj.Location;
                    isExist.Department = obj.Department;
                    isExist.Designation = obj.Designation;
                    isExist.DateOfBirth = obj.DateOfBirth;
                    isExist.BasicSalary = obj.BasicSalary;
                    isExist.MobileInternet = obj.MobileInternet;
                    isExist.Transport = obj.Transport;
                    isExist.Wellbeing = obj.Wellbeing;
                    isExist.Others = obj.Others;
                    isExist.Advance = obj.Advance;
                    isExist.LeaveDeduction = obj.LeaveDeduction;
                    isExist.LateDeduction = obj.LateDeduction;
                    isExist.MiscellaneousDeduction = obj.MiscellaneousDeduction;
                    isExist.Tax = obj.Tax;
                    isExist.ProvidentFund = obj.ProvidentFund;
                    isExist.ModifiedBy = logInInfo.UserId;
                    isExist.ModifiedDate = DateTime.Today;
                    isExist.Status = 1;
                    Edit(isExist);
                    Save();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }

        public DbResponse DeleteEmployee(long id, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbObj = db.Employees.FirstOrDefault(i => i.Id == id);
                    if (dbObj == null) return AutoResponse.NotFoundMessage();
                    dbObj.Status = 0;
                    dbObj.ModifiedBy = logInInfo.UserId;
                    dbObj.ModifiedDate = DateTime.Today;
                    Edit(dbObj);
                    Save();
                    return AutoResponse.SuccessMessageWithParam("Employee Deleted successfully.");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }
    }
}
