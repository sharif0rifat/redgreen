﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using Infrastructure.ModelHelpers;

namespace ERP.REPOSITORIES.Repositories
{
    public class ApplicationRepo : IApplicationRepo
    {
        public DbResponse GetApplicaitons()
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbApplications = db.Applications.ToList();
                    return AutoResponse.SuccessMessageWithParam(ConvertToLocalObjects(dbApplications, new List<PartialApplication>()));
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse GetApplicaitons(List<Searchy> searchies)
        {
            throw new NotImplementedException();
        }

        public DbResponse Register(PartialApplication partialApplication)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbApplication = db.Applications.FirstOrDefault(i => i.AppName == partialApplication.AppName);
                    if (dbApplication == null)
                    {
                        var uploadData = ConvertToEntityObject(partialApplication, new Application());
                        db.Applications.Add(uploadData);
                        db.SaveChanges();
                        return AutoResponse.SuccessMessage();
                    }
                    dbApplication.AppName = partialApplication.AppName;
                    dbApplication.ValidateDate = partialApplication.ValidateDate;
                    dbApplication.Status = true;
                    db.Entry(dbApplication).State = EntityState.Modified;
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse AddApplication(PartialApplication param)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var ifany = db.Applications.Any(i => i.AppName == param.AppName);
                    if (ifany) return AutoResponse.ExistMessage();
                    var dbApplication = ConvertToEntityObject(param, new Application());
                    dbApplication.ValidateDate = DateTime.Today.AddDays(30);
                    dbApplication.Status = true;
                    db.Applications.Add(dbApplication);
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }


        private List<PartialApplication> ConvertToLocalObjects(List<Application> convertedFrom, List<PartialApplication> convertedTo)
        {
            var converter = new ConvertTypeInto<Application, PartialApplication>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
        // db object will converted to local object
        public PartialApplication ConvertToLocalObject(Application convertedFrom, PartialApplication convertedTo)
        {
            var converter = new ConvertTypeInto<Application, PartialApplication>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
        // local object will converted to db object
        public Application ConvertToEntityObject(PartialApplication convertedFrom, Application convertedTo)
        {
            var converter = new ConvertTypeInto<PartialApplication, Application>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
    }
}
