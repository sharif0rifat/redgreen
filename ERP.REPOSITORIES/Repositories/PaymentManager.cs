﻿using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using Payment = ERP.INFRASTRUCTURE.Payment;

namespace ERP.REPOSITORIES.Repositories
{
    public class PaymentManager : GenericRepository<ERPEntities, DAL.Model.Payment>, IPaymentManager
    {
        private ERPEntities _dbEntities;
        private readonly ICurrencyManager _currencyManager;
        public PaymentManager()
        {
            _dbEntities = new ERPEntities();
            _currencyManager = new CurrencyManager();
        }
        public decimal GetPaymentInfo(long partyId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    decimal ledger = 0;
                    if (db.Payments.Any(x => x.PartyId == partyId))
                    {
                        var bill = db.InvoiceInfoes.Where(i => i.PartyId == partyId && i.Status == 1).Select(i => i.GrandTotal).DefaultIfEmpty(0).Sum();
                        //var x = dbEntities.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.CashReceive && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).ToList();
                        //var y = dbEntities.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.CashReturn && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).ToList();
                        //var z = dbEntities.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.SalesReturn && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).ToList();
                        //var payment =
                        //    db.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.CashReceive && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).DefaultIfEmpty(0).Sum()
                        //    - db.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.CashReturn && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).DefaultIfEmpty(0).Sum()
                        //    + db.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.SalesReturn && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).DefaultIfEmpty(0).Sum();
                        //ledger = payment - bill;
                    }
                    else
                    {
                        if (db.InvoiceInfoes.Where(i => i.PartyId == partyId && i.Status == 1).ToList() != null && db.InvoiceInfoes.Where(i => i.PartyId == partyId && i.Status == 1).ToList().Count > 0)
                        {
                            var billSum = db.InvoiceInfoes.Where(i => i.PartyId == partyId && i.Status == 1).Select(i => i.GrandTotal).DefaultIfEmpty(0).Sum();
                            ledger = billSum != null ? -billSum : 0;
                        }
                        else
                        {
                            ledger = 0;
                        }
                    }
                    return ledger;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public decimal GetPaymentInfo(long partyId, DateTime bllingDate)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    decimal ledger = 0;
                    if (db.Payments.Any(x => x.PartyId == partyId))
                    {
                        var bill = db.InvoiceInfoes.Where(i => i.PartyId == partyId && i.Status == 1).Select(i => i.GrandTotal).DefaultIfEmpty(0).Sum();
                        //var x = dbEntities.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.CashReceive && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).ToList();
                        //var y = dbEntities.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.CashReturn && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).ToList();
                        //var z = dbEntities.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.SalesReturn && i.PartyId == partyId && i.Status == 1).Select(i => i.Amount).ToList();
                        //var payment =
                        //    db.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.CashReceive && i.PartyId == partyId && i.Status == 1 && i.Date <= bllingDate).Select(i => i.Amount).DefaultIfEmpty(0).Sum()
                        //    - db.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.CashReturn && i.PartyId == partyId && i.Status == 1 && i.Date <= bllingDate).Select(i => i.Amount).DefaultIfEmpty(0).Sum()
                        //    + db.Payments.Where(i => i.PaymentOption == (int)Constants.PaymentOption.SalesReturn && i.PartyId == partyId && i.Status == 1 && i.Date <= bllingDate).Select(i => i.Amount).DefaultIfEmpty(0).Sum();
                        ledger = 0 - bill;
                    }
                    else
                    {
                        if (db.InvoiceInfoes.Where(i => i.PartyId == partyId && i.Status == 1).ToList() != null && db.InvoiceInfoes.Where(i => i.PartyId == partyId && i.Status == 1).ToList().Count > 0)
                        {
                            var billSum = db.InvoiceInfoes.Where(i => i.PartyId == partyId && i.Status == 1).Select(i => i.GrandTotal).DefaultIfEmpty(0).Sum();
                            ledger = billSum != null ? -billSum : 0;
                        }
                        else
                        {
                            ledger = 0;
                        }
                    }
                    return ledger;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }
        public Payment GetLastPayment(long partyId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    Payment lastPayment;
                    if (db.Payments.Any(x => x.PartyId == partyId))
                    {
                        var lastPayDate = db.Payments.Where(i => i.PartyId == partyId).Select(i => i.Date).SingleOrDefault();
                        //var lastDatePay = db.Payments.Where(i => i.Status == 1 && i.PartyId == partyId && i.Date == lastPayDate && i.PaymentOption == (int)Constants.PaymentOption.CashReceive).Select(i => i.Amount).DefaultIfEmpty(0).Sum();
                        lastPayment = new Payment { Date = lastPayDate, Amount = 0 };
                    }
                    else
                    {
                        lastPayment = new Payment { Date = DateTime.Today, Amount = 0 };
                    }
                    return lastPayment;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<Payment> GetPayments(long? partyId = null)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var paymentList = new List<Payment>();
                    var dbPaymentList = new List<DAL.Model.Payment>();
                    if (partyId == null)
                    {
                        dbPaymentList = db.Payments.Where(x => x.Status == 1).Select(i => i).ToList();
                    }
                    if (partyId != null)
                    {
                        dbPaymentList = db.Payments.Where(x => x.Status == 1 && x.PartyId == partyId).Select(i => i).ToList();
                    }
                    foreach (var dbpay in dbPaymentList)
                    {
                        var payment = Mapper.Map<Payment>(dbpay);
                        payment.PartyName =
                            _dbEntities.Ledgers.Where(i => i.Id == dbpay.PartyId)
                                .Select(p => p.Name)
                                .FirstOrDefault();
                        paymentList.Add(payment);
                    }
                    return paymentList;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<Payment> GetPaymentsByDate(long? partyId, DateTime? datefrom, DateTime? datetto)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    List<DAL.Model.Payment> dbPaymentList = partyId != null ? db.Payments.Where(x => x.Status == 1 && x.PartyId == partyId).Select(i => i).ToList()
                        : db.Payments.Where(x => x.Status == 1).Select(i => i).ToList();
                    if (datefrom != null || datetto != null)
                    {
                        dbPaymentList = dbPaymentList.Where(x => x.Status == 1 && x.Date >= datefrom && x.Date <= datetto).Select(i => i).ToList();
                    }
                    var paymentList = Mapper.Map<List<Payment>>(dbPaymentList);
                    return paymentList;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public List<Payment> GetPaymentsByDate(long? partyId, long paymentType, DateTime? datefrom, DateTime? datetto)
        {
            var paymentList = new List<Payment>();
            var payments = this.GetPaymentsByDate(partyId, datefrom, datetto);
            if (payments != null)
            {
                paymentList = payments.Where(i => i.PaymentType == paymentType).ToList();
                paymentList.ForEach(payment =>
                {
                    var transactionSubHeader = paymentType == PropertyConstants.Payment ? _dbEntities.Ledgers.FirstOrDefault(i => i.Id == payment.CreditId) : _dbEntities.Ledgers.FirstOrDefault(i => i.Id == payment.DebitId);
                    if (transactionSubHeader != null)
                        payment.SubHeader = transactionSubHeader.Name;
                    var firstOrDefault = _dbEntities.Ledgers.FirstOrDefault(i => i.Id == payment.PartyId);
                    if (firstOrDefault != null)
                        payment.PartyName = firstOrDefault.Name;

                });
            }
            return paymentList;
        }
        public List<Payment> GetPaymentsByType(long? partyId, long paymentType)
        {
            var paymentTypeId = PropertyConstants.PropertyManager.GetPropertyByNameAndValue("TransactionType", paymentType).Id;
            var payments = this.GetPayments(partyId);
            var paymentList = payments.Where(i => i.PaymentType == paymentTypeId).ToList();
            paymentList.ForEach(payment =>
            {
                var transactionSubHeader = _dbEntities.Ledgers.FirstOrDefault(i => i.Id == payment.DebitId);
                if (transactionSubHeader != null)
                    payment.SubHeader = transactionSubHeader.Name;
                var firstOrDefault = _dbEntities.Ledgers.FirstOrDefault(i => i.Id == payment.PartyId);
                if (firstOrDefault != null)
                    payment.PartyName = firstOrDefault.Name;
            });
            return paymentList;
        }
        public Payment GetPaymentById(long id)
        {
            Payment payment = _dbEntities.Payments.Where(i => i.Id == id).Select(pay => new Payment
            {
                Id = pay.Id,
                PaymentType = pay.PaymentType,
                BussinessId = pay.BussinessId,
                CreatedBy = pay.CreatedBy,
                Amount = pay.Amount,
                DebitId = pay.DebitId,
                CreditId = pay.CreditId,
                CurrencyId = (int)pay.Currency,
                PartyId = pay.PartyId,
                ModifiedBy = pay.ModifiedBy,
                CreatedDate = pay.CreatedDate,
                Status = pay.Status,
                Date = pay.Date,
                BankName = pay.BankName,
                AccountNo = pay.AccountNo,
                ReferenceNo = pay.ReferenceNo,
                CheckNo = pay.CheckNo,
                CheckDate = pay.CheckDate,
                PaymentStatus = pay.PaymentStatus,
                SubHeader = _dbEntities.GroupHeaders.FirstOrDefault(i => i.Id == pay.CreditId).Name,
                Remarks = pay.Remarks
            }).FirstOrDefault();

            return payment;
        }

        public Payment GetPayment(long id)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbPayment = db.Payments.Where(x => x.Status == 1 && x.Id == id).Select(i => i).SingleOrDefault();
                    var payment = ConvertDbPaymentToPayment(dbPayment, new Payment());
                    return payment;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DbResponse EditPayment(Payment payment, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var
                    previousPayment = _dbEntities.Payments.FirstOrDefault(i => i.Id == payment.Id);
                    if (previousPayment == null)
                        return AutoResponse.NotFoundMessage();
                    var dbPayment = Mapper.Map<DAL.Model.Payment>(payment);
                    if (payment.PaymentType == PropertyConstants.Payment)
                    {
                        dbPayment.DebitId = payment.PartyId;
                        if (dbPayment.CreditId < 1)
                            return AutoResponse.FailedMessageWithParam("You Mus Select a credit from.");
                    }
                    else
                    {
                        dbPayment.CreditId = payment.PartyId;
                        if (dbPayment.DebitId < 1)
                            return AutoResponse.FailedMessageWithParam("You Mus Select a debit from.");
                    }
                    dbPayment.CreatedBy = previousPayment.CreatedBy;
                    dbPayment.CreatedDate = previousPayment.CreatedDate;
                    dbPayment.Status = previousPayment.Status;
                    dbPayment.ModifiedBy = logInInfo.UserId;
                    dbPayment.ModifiedDate = DateTime.Today;
                    dbPayment.BussinessId = "0";

                    // Currency
                    var currencyInfo = _currencyManager.GetCurrency(payment.CurrencyId);
                    if (currencyInfo != null)
                    {
                        dbPayment.Currency = currencyInfo.ID;
                        dbPayment.ConversionRate = currencyInfo.ConversionRate;
                    }

                    Edit(dbPayment);
                    Save();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (DbEntityValidationException e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }
        public DbResponse DeletePayment(long id, LogInInfo _logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbPayment = db.Payments.FirstOrDefault(i => i.Id == id);
                    if (dbPayment == null) return AutoResponse.NotFoundMessage();
                    dbPayment.Status = 0;
                    dbPayment.ModifiedDate = DateTime.Today;
                    dbPayment.ModifiedBy = _logInInfo.UserId;
                    Edit(dbPayment);
                    Save();
                    return AutoResponse.SuccessMessageWithParam("Amount has been Deleted");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }
        public DbResponse AddPayment(Payment payment, LogInInfo _logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    using (var dbEntitiesTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            var dbPayment = Mapper.Map<DAL.Model.Payment>(payment);
                            #region========validation==========
                            if (payment.PaymentType == PropertyConstants.Payment)
                            {
                                dbPayment.DebitId = payment.PartyId;
                                if (dbPayment.CreditId < 1)
                                    return AutoResponse.FailedMessageWithParam("You Must Select a credit from.");
                            }
                            else
                            {
                                dbPayment.CreditId = payment.PartyId;
                                if (dbPayment.DebitId < 1)
                                    return AutoResponse.FailedMessageWithParam("You Must Select a debit from.");
                            }
                            if (dbPayment.Amount <= 0)
                                return AutoResponse.FailedMessageWithParam("You Must give a valid amount.");

                            //==========pending check no validation=======
                            if(payment.PaymentStatus==PropertyConstants.Pending && payment.CheckNo.Trim()=="")
                                return AutoResponse.FailedMessageWithParam("You Must give a CheckNo and Bank information.");
                            #endregion
                            

                            dbPayment.CreatedDate = DateTime.Today;
                            dbPayment.ModifiedDate = DateTime.Today;
                            dbPayment.BussinessId = "0";
                            dbPayment.Status = 1;
                            dbPayment.CreatedBy = _logInInfo.UserId;
                            dbPayment.ModifiedBy = _logInInfo.UserId;

                            // Currency
                            var currencyInfo = _currencyManager.GetCurrency(payment.CurrencyId);
                            if (currencyInfo != null)
                            {
                                dbPayment.Currency = currencyInfo.ID;
                                dbPayment.ConversionRate = currencyInfo.ConversionRate;
                            }

                            db.Payments.Add(dbPayment);
                            db.SaveChanges();
                            dbEntitiesTransaction.Commit();
                            return AutoResponse.SuccessMessage();
                        }
                        catch (DbEntityValidationException e)
                        {
                            return AutoResponse.FailedMessageWithParam(e.ToString());
                        }
                        catch (Exception e)
                        {
                            dbEntitiesTransaction.Rollback();
                            return AutoResponse.FailedMessageWithParam(e);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage();
            }
        }
        public ERP.DAL.Model.Payment ConvertPaymentToDbPayment(Payment payemnt, ERP.DAL.Model.Payment dbPayment)
        {
            var paymentConvert = new ConvertTypeInto<Payment, ERP.DAL.Model.Payment>();
            return paymentConvert.ConvertInto(payemnt, dbPayment);
        }
        public Payment ConvertDbPaymentToPayment(ERP.DAL.Model.Payment dbPayment, Payment payemnt)
        {
            var paymentConvert = new ConvertTypeInto<ERP.DAL.Model.Payment, Payment>();
            return paymentConvert.ConvertInto(dbPayment, payemnt);
        }

        public List<Payment> GetPaymentInfoByStatus(long paymentType, long paymentFlag = 0)
        {
            var paymentList = new List<Payment>();
            var payments = GetPayments();
            if (paymentFlag != 0)
            {
                payments = payments.Where(p => p.PaymentStatus == paymentFlag).ToList();
            }

            if (payments != null)
            {
                paymentList = payments.Where(i => i.PaymentType == paymentType).ToList();
                paymentList.ForEach(payment =>
                {
                    var transactionSubHeader = paymentType == PropertyConstants.Payment ? _dbEntities.Ledgers.FirstOrDefault(i => i.Id == payment.CreditId) : _dbEntities.Ledgers.FirstOrDefault(i => i.Id == payment.DebitId);
                    if (transactionSubHeader != null)
                        payment.SubHeader = transactionSubHeader.Name;
                    var firstOrDefault = _dbEntities.Ledgers.FirstOrDefault(i => i.Id == payment.PartyId);
                    if (firstOrDefault != null)
                        payment.PartyName = firstOrDefault.Name;

                });
            }
            return paymentList;
        }
    }
}
