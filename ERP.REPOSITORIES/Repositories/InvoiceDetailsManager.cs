﻿using System.Data.Entity;
using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ERP.REPOSITORIES.Repositories
{
    public class InvoiceDetailsManager : GenericRepository<ERPEntities, InvoiceDetail>, IInvoiceDetailsManager
    {

        public InvoiceDetail GetInvoiceDetailsById(long id)
        {
            InvoiceDetail result = default(InvoiceDetail);
            try
            {
                using (var db = new ERPEntities())
                {
                    result = db.InvoiceDetails.Find(id);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return result;
        }
        public InvoiceDetail GetInvoiceDetailsBySerial(string serial)
        {
            InvoiceDetail result = default(InvoiceDetail);
            try
            {
                using (var db = new ERPEntities())
                {
                    result = db.InvoiceDetails.FirstOrDefault(i => i.ProductSerial == serial && (i.PropertyId == PropertyConstants.Purchase || i.PropertyId==PropertyConstants.ProfitInvoice) && i.Status==1);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        public InvoiceDetail AddInvoiceDetails(InvoiceDetail invoiceDetail)
        {
            InvoiceDetail result = default(InvoiceDetail);
            try
            {
                using (var db = new ERPEntities())
                {
                    result = db.InvoiceDetails.Add(invoiceDetail);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        public void UpdateInvoiceDetails(InvoiceDetail invoiceDetail)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    db.Entry(invoiceDetail).State = System.Data.Entity.EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InvoiceDetailViewModel> GetInvoiceDetails(long billId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbInvoiceDetails = db.InvoiceDetails.Where(x => x.Status == 1 && x.InvoiceId == billId).Select(i => i).ToList();
                    var invoiceDetailList = Mapper.Map<List<InvoiceDetailViewModel>>(dbInvoiceDetails);
                    invoiceDetailList.ForEach(i =>
                    {
                        i.ProductName = db.Products.FirstOrDefault(p => p.Id == i.ProductId).ProductName;
                        i.GiftId = (int)i.PropertyId;
                    });
                    return invoiceDetailList;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<InvoiceDetailViewModel> GetInvoiceDetailsWithProduct(long billId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbInvoiceDetails = db.InvoiceDetails.Where(x => x.Status == 1 && x.InvoiceId == billId).Select(i => i).ToList();
                    var invoiceDetailsLSit = new List<InvoiceDetailViewModel>();
                    foreach (var invoiceDetail in dbInvoiceDetails.Select(Mapper.Map<InvoiceDetailViewModel>))
                    {
                        invoiceDetail.ProductName =
                            db.Products.Where(i => i.Id == invoiceDetail.ProductId)
                                .Select(i => i.ProductName)
                                .FirstOrDefault();
                        invoiceDetailsLSit.Add(invoiceDetail);
                    }
                    return invoiceDetailsLSit;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }


        public DbResponse DeleteInvoiceDetailsFromDBByInvoiceId(long invoiceId, LogInInfo logInInfo)
        {
            using (ERPEntities dbEntities = new ERPEntities())
            {
                List<InvoiceDetail> invoiceDetailsList = dbEntities.InvoiceDetails.Where(id => id.Status == 1 && id.InvoiceId == invoiceId).ToList();
                foreach (InvoiceDetail invoiceDetail in invoiceDetailsList)
                {
                    invoiceDetail.Status = 0;
                    invoiceDetail.ModifiedBy = logInInfo.UserId;
                    invoiceDetail.ModifiedDate = DateTime.Today;
                    dbEntities.Entry(invoiceDetail).State = System.Data.Entity.EntityState.Modified;
                }
                dbEntities.SaveChanges();
                //====Stock in Flag change========
            }

            return AutoResponse.SuccessMessage();
        }
        

        public DbResponse AddInvoiceDetailListToDB(IList<InvoiceDetailViewModel> partialInvoiceDetailList, long invoiceId, long propertyId, LogInInfo logInInfo)
        {
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    foreach (var invoiceDetailViewModel in partialInvoiceDetailList)
                    {
                        InvoiceDetail invoiceDetail = new InvoiceDetail();
                        Mapper.Map(invoiceDetailViewModel, invoiceDetail);
                        invoiceDetail.InvoiceId = invoiceId;
                        invoiceDetail.CreatedBy = invoiceDetail.ModifiedBy = logInInfo.UserId;
                        invoiceDetail.CreatedDate = invoiceDetail.ModifiedDate = DateTime.Today;
                        invoiceDetail.Status = 1;
                        if (propertyId == PropertyConstants.Purchase || propertyId == PropertyConstants.SalesReturn || propertyId == PropertyConstants.ProfitInvoice)
                        {

                            invoiceDetail.StockIn = PropertyConstants.StockIn;
                        }
                        if (propertyId == PropertyConstants.Sales || propertyId == PropertyConstants.PurchaseReturn || propertyId == PropertyConstants.SalesReplace || propertyId == PropertyConstants.LostInvoice || propertyId == PropertyConstants.Gift)
                        {

                            invoiceDetail.StockIn = PropertyConstants.StockOut;
                        }
                        dbEntities.InvoiceDetails.Add(invoiceDetail);

                    }
                    dbEntities.SaveChanges();
                }


            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }

            return AutoResponse.SuccessMessage();
        }
                      
        public void DeleteDetailByInvoiceId(long invoiceId)
        {
            using (var db = new ERPEntities())
            {
                var detialList = db.InvoiceDetails.Where(i => i.InvoiceId == invoiceId).ToList();
                detialList.ForEach(i =>
                {
                    db.Entry(i).State = EntityState.Deleted;
                });
                db.SaveChanges();
            }

        }
    }
}
