﻿using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;


namespace ERP.REPOSITORIES.Repositories
{
    public class TransactionManager : GenericRepository<ERPEntities, Transaction>, ITransactionManager
    {
        private readonly ICurrencyManager _currencyManager;
        public TransactionManager()
        {
            _currencyManager = new CurrencyManager();
        }
        public List<TransactionViewModel> GetTransactions()
        {
            var startDate = DateTime.Today.AddMonths(-1);
            var endDate = DateTime.Today;
            try
            {
                using (var db = new ERPEntities())
                {
                    var transactionList =
                        db.Transactions.Where(x => x.Status == 1 && x.TransactionDate >= startDate && x.TransactionDate <= endDate)
                            .Select(i => new TransactionViewModel
                            {
                                Id = i.Id,
                                DebitHeaderName = db.Ledgers.FirstOrDefault(th => th.Id == i.DebitHeaderId).Name,
                                CreditHeaderName = db.Ledgers.FirstOrDefault(th => th.Id == i.CreditHeaderId).Name,
                                TransactionDate = i.TransactionDate,
                                Remarks = i.Remarks,
                                PartyId = i.PartyId,
                                Amount = i.Amount
                            })
                            .OrderBy(i => i.TransactionDate)
                            .ToList();
                    return transactionList;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<TransactionViewModel> GetTransactionsByDate(DateTime startDate, DateTime endDate)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var transactionList =
                        db.Transactions.Where(x => x.Status == 1 && x.TransactionDate >= startDate && x.TransactionDate <= endDate)
                            .Select(i => new TransactionViewModel
                            {
                                Id = i.Id,
                                DebitHeaderName = db.Ledgers.FirstOrDefault(th => th.Id == i.DebitHeaderId).Name,
                                CreditHeaderName = db.Ledgers.FirstOrDefault(th => th.Id == i.CreditHeaderId).Name,
                                TransactionDate = i.TransactionDate,
                                Remarks = i.Remarks,
                                PartyId = i.PartyId,
                                Amount = i.Amount
                            })
                            .OrderBy(i => i.TransactionDate)
                            .ToList();
                    return transactionList;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public List<TransactionViewModel> GetTransactionsBySubHeader(long ledgerId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var transactionList =
                        db.Transactions.Where(x => x.Status == 1 && (x.DebitHeaderId == ledgerId || x.CreditHeaderId == ledgerId))
                            .Select(i => new TransactionViewModel
                            {
                                DebitHeaderName = db.Ledgers.FirstOrDefault(th => th.Id == i.DebitHeaderId).Name,
                                CreditHeaderName = db.Ledgers.FirstOrDefault(th => th.Id == i.CreditHeaderId).Name,
                                TransactionDate = i.TransactionDate,
                                Remarks = i.Remarks,
                                PartyId = i.PartyId,
                                CreditAmount = i.DebitHeaderId == ledgerId ? i.Amount ?? 0 : 0,
                                DebitAmount = i.CreditHeaderId == ledgerId ? i.Amount ?? 0 : 0,
                            })
                            .OrderBy(i => i.TransactionDate)
                            .ToList();
                    //Get Balance Amount
                    var balance = transactionList.GroupBy(i => i.DebitHeaderId).Select(i => i.Sum(x => x.Amount ?? 0)).FirstOrDefault()
                        - transactionList.GroupBy(i => i.CreditHeaderId).Select(i => i.Sum(x => x.Amount ?? 0)).FirstOrDefault();
                    transactionList.Add(new TransactionViewModel
                    {
                        TransactionDate = DateTime.Today,
                        Remarks = "Balance",
                        CreditAmount = balance < 0 ? balance : 0,
                        DebitAmount = balance > 0 ? balance : 0
                    });
                    return transactionList;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public TransactionViewModel GetTransaction(long objId)
        {
            using (var dbEntities = new ERPEntities())
            {
                var dbobj = dbEntities.Transactions.FirstOrDefault(i => i.Id == objId);
                if (dbobj == null) return null;
                var viewObj = Mapper.Map<TransactionViewModel>(dbobj);
                viewObj.DebitAmount = viewObj.Amount ?? 0;
                viewObj.CreditAmount = viewObj.Amount ?? 0;
                return viewObj;
            }
        }

        public DbResponse AddTransaction(TransactionViewModel param)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    ValidateObject(param);
                    //var cashTypeId = db.TableProperties.Where(i => i.ViewName == param.CashTypeValue).Select(i => i.Id).SingleOrDefault();
                    //param.CashType = cashTypeId;
                    //=====Need to remove this
                    param.TransactionDate = param.TransactionDate == DateTime.MinValue ? DateTime.Today : param.TransactionDate;
                    //==========

                    var dbObj = Mapper.Map<Transaction>(param);
                    dbObj.BussinessId = "1";
                    dbObj.Status = 1;
                    dbObj.CreatedBy = dbObj.ModifiedBy = param.LogInInfo.UserId;
                    dbObj.CreatedDate = dbObj.ModifiedDate = DateTime.Today;

                    // Currency
                    var currencyInfo = _currencyManager.GetCurrency(param.CurrencyId);
                    if (currencyInfo != null)
                    {
                        dbObj.Currency = currencyInfo.ID;
                        dbObj.ConversionRate = currencyInfo.ConversionRate;
                    }
                    db.Transactions.Add(dbObj);
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (DbEntityValidationException)
            {
                return AutoResponse.FailedMessage();
            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessage(e.Message);
            }

        }

        private void ValidateObject(TransactionViewModel param)
        {
            if (param.Amount == null || param.Amount < 0)
                throw new Exception("Amount should be Greater than 0");
            if (param.DebitHeaderId == null || param.DebitHeaderId < 1)
                throw new Exception("Select a Debit Ledger");
            if (param.CreditHeaderId == null || param.CreditHeaderId < 1)
                throw new Exception("Select a Credit Ledger");
            if (param.TransactionDate == null)
                throw new Exception("Set a Date");
        }
        public DbResponse EditTransaction(TransactionViewModel obj)
        {
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbObj = dbEntities.Transactions.FirstOrDefault(i => i.Id == obj.Id);
                    if (dbObj == null) return AutoResponse.NotFoundMessage();
                    dbObj.DebitHeaderId = obj.DebitHeaderId;
                    dbObj.CreditHeaderId = obj.CreditHeaderId;
                    dbObj.Amount = obj.Amount;
                    dbObj.TransactionDate = obj.TransactionDate;
                    dbObj.Remarks = obj.Remarks;
                    dbObj.ModifiedDate = DateTime.Today;
                    dbObj.BussinessId = "1";
                    // Currency
                    var currencyInfo = _currencyManager.GetCurrency(obj.CurrencyId);
                    if (currencyInfo != null)
                    {
                        dbObj.Currency = currencyInfo.ID;
                        dbObj.ConversionRate = currencyInfo.ConversionRate;
                    }

                    //dbEntities.Entry(dbObj).State = EntityState.Modified;
                    dbEntities.SaveChanges();
                    return AutoResponse.SuccessMessageWithParam("Transaction Updated Successfully");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }
        public DbResponse DeleteTransaction(long id, ERPEntities dbEntities)
        {
            try
            {
                var dbObj = dbEntities.Transactions.FirstOrDefault(i => i.Id == id);
                if (dbObj == null) return AutoResponse.NotFoundMessage();
                dbObj.Status = 0;
                Edit(dbObj);
                Save();
                return AutoResponse.SuccessMessageWithParam("Transaction has been deleted.");
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }
    }
}
