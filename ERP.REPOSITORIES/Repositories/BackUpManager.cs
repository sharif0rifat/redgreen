﻿using System;
using System.Linq;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Global;
using System.Data.Entity;
using ERP.REPOSITORIES.Helper;

namespace ERP.REPOSITORIES.Repositories
{
    public class BackUpManager
    {


        public DbResponse TakeBackUp(string backUpFolder)
        {
            try
            {
                var dbPath = string.Format(@"{0}RedGreen_{1}.bak", backUpFolder, DateTime.Today.ToString("dd-MMM-yyyy"));
                using (var db = new ERPEntities())
                {
                    var cmd = String.Format("BACKUP DATABASE {0} TO DISK='{1}' WITH FORMAT, MEDIANAME='DbBackups', MEDIADESCRIPTION='Media set for {0} database';"
                        , "RedGreen", dbPath);
                    db.Database.ExecuteSqlCommand(TransactionalBehavior.DoNotEnsureTransaction, cmd);
                    return new DbResponse
                    {
                        MessageType = 1,
                        MessageData = "Database Back up on folder:  " + backUpFolder
                    };
                }

            }
            catch (Exception)
            {
                throw;
            }

        }

        public DbResponse ClearAllData()
        {
            using (var db = new ERPEntities())
            {
                var blogNames = db.Database.SqlQuery<string>(@"Truncate table [dbo].[Category]
Truncate table [dbo].[Product]
Truncate table [dbo].[Currency]
Truncate table [dbo].[Employee]
Truncate table [dbo].[InvoiceDetails]
Truncate table [dbo].[InvoiceInfo]
Truncate table [dbo].[Payment]
Truncate table [dbo].[Transaction]

truncate table [dbo].[GroupHeader]
truncate table [dbo].[Ledger]
truncate table [dbo].[LedgerGroup]


SET IDENTITY_INSERT [dbo].[GroupHeader] ON 

 
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (1, 1, 1, CAST(N'2016-07-17 02:35:39.107' AS DateTime), CAST(N'2016-07-17 02:35:39.260' AS DateTime), 1, 0, N'Fixed Assets', NULL, 17, 24, 1)
 
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (2, 1, 1, CAST(N'2016-07-17 02:36:23.837' AS DateTime), CAST(N'2016-07-17 02:36:23.837' AS DateTime), 1, 0, N'Current Assets', NULL, 17, 24, 1)
 
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (3, 1, 1, CAST(N'2016-07-17 02:36:32.543' AS DateTime), CAST(N'2016-07-17 02:36:32.543' AS DateTime), 1, 0, N' Liabilities', NULL, 18, 25, 1)
 
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (4, 1, 1, CAST(N'2016-07-17 02:36:40.777' AS DateTime), CAST(N'2016-07-17 02:36:40.777' AS DateTime), 1, 0, N'Equity', NULL, 18, 26, 1)
 
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (5, 1, 1, CAST(N'2016-07-17 02:36:48.910' AS DateTime), CAST(N'2016-07-17 02:36:48.910' AS DateTime), 1, 0, N'Revenue', NULL, 18, 27, 1)
 
INSERT [dbo].[GroupHeader] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [Description], [PropertyId], [AccountsType], [IsParent]) VALUES (6, 1, 1, CAST(N'2016-07-17 02:36:56.413' AS DateTime), CAST(N'2016-07-17 02:36:56.413' AS DateTime), 1, 0, N'Expense', NULL, 17, 28, 1)
 
SET IDENTITY_INSERT [dbo].[GroupHeader] OFF
 
SET IDENTITY_INSERT [dbo].[Ledger] ON 

 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (1, 3, 3, CAST(N'2017-02-22 00:51:46.450' AS DateTime), CAST(N'2017-02-22 00:51:46.450' AS DateTime), 1, 0, N'Sales', 3, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (2, 3, 3, CAST(N'2017-02-22 00:52:01.147' AS DateTime), CAST(N'2017-02-22 00:52:01.147' AS DateTime), 1, 0, N'Purchase', 4, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (3, 3, 3, CAST(N'2017-02-22 00:52:12.483' AS DateTime), CAST(N'2017-02-22 00:52:12.483' AS DateTime), 1, 0, N'Current Stock', 5, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (4, 3, 3, CAST(N'2017-02-22 01:09:57.753' AS DateTime), CAST(N'2017-02-22 01:12:03.123' AS DateTime), 1, 0, N'Bonus Income', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (5, 3, 3, CAST(N'2017-02-22 01:12:42.950' AS DateTime), CAST(N'2017-02-22 01:12:42.950' AS DateTime), 1, 0, N'Product Expire Expense', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (6, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Profit Income', 6, NULL, 0, N'asdasd', NULL, NULL, NULL, NULL, NULL, NULL, 0)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (7, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Purchase Discount', 8, NULL, 0, N'dfgdfg', NULL, NULL, NULL, NULL, NULL, NULL, 0)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (8, 0, 0, CAST(N'2017-05-08 00:00:00.000' AS DateTime), CAST(N'2017-05-08 00:00:00.000' AS DateTime), 1, 0, N'Sales Discount', 7, NULL, 0, N'dfgdfg', NULL, NULL, NULL, NULL, NULL, NULL, 0)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (9, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Purchase Vat', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (10, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Sales Vat', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (11, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Purchase Service Charge', 8, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
INSERT [dbo].[Ledger] ([Id], [CreatedBy], [ModifiedBy], [CreatedDate], [ModifiedDate], [Status], [BussinessId], [Name], [LedgerGroupId], [Description], [PropertyId], [Code], [ContactPerson], [Address1], [Address2], [Mobile1], [Mobile2], [Email], [Visible]) VALUES (12, 0, 0, CAST(N'2017-05-17 00:00:00.000' AS DateTime), CAST(N'2017-05-17 00:00:00.000' AS DateTime), 1, 0, N'Sales Service Charge', 7, NULL, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL)
 
SET IDENTITY_INSERT [dbo].[Ledger] OFF
 
");
            }
            return AutoResponse.SuccessMessageWithParam("All Data has been cleared.");
        }
    }
}
