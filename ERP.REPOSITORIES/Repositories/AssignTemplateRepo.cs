﻿using System;
using System.Data.Entity;
using System.Linq;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.REPOSITORIES.Repositories
{
    public class AssignTemplateRepo : IAssignTemplateRepo
    {
        public DbResponse Register(PartialUser param)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbUser = db.Users.FirstOrDefault(i => i.Id == param.Id);
                    if (dbUser == null) return AutoResponse.FailedMessage();
                    dbUser.TemplateId = param.TemplateId;
                    db.Entry(dbUser).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return AutoResponse.SuccessMessage();
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }

        
    }
}
