﻿using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Entity;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;


namespace ERP.REPOSITORIES.Repositories
{
    public class StockManager : GenericRepository<ERPEntities, StockViewModel>, IStockManager
    {
        private readonly IPropertyManager _propertyManager;
        public StockManager()
        {
            _propertyManager = new PropertyManager();
        }

        public IEnumerable<StockViewModel> GetCurrentStocks()
        {
            var stockInIds = new long[] {PropertyConstants.Purchase,PropertyConstants.ProfitInvoice};
            using (var dbEntries = new ERPEntities())
            {
                var stock = dbEntries.InvoiceDetails.Where(i => i.Status == 1).Select(i => new StockViewModel
                {
                    ProductId = i.ProductId,
                    Quantity = i.StockIn==PropertyConstants.StockIn ? i.Quantity : -i.Quantity
                }).ToList();
                stock = stock.GroupBy(i => i.ProductId).Select(i => new StockViewModel
                {
                    ProductId = i.Key,
                    Quantity = i.Sum(j => j.Quantity)
                }).ToList();
                stock.ForEach(i=>
                {
                    i.ProductName = dbEntries.Products.FirstOrDefault(p => p.Id == i.ProductId).ProductName;
                    i.Price = dbEntries.Products.FirstOrDefault(p => p.Id == i.ProductId).BuyingPrice;
                    i.Amount = i.Quantity*dbEntries.Products.FirstOrDefault(p => p.Id == i.ProductId).BuyingPrice;
                });
                return stock;
            }
        }
        //public IEnumerable<StockViewModel> GetCurrentStocks2()
        //{
        //    var stockInIds = new long[] { PropertyConstants.Purchase, PropertyConstants.ProfitInvoice, PropertyConstants.SalesReturn };
        //    using (var dbEntries = new ERPEntities())
        //    {
        //        var stock = dbEntries.InvoiceDetails.Where(i => i.Status == 1).Select(i => new StockViewModel
        //        {
        //            ProductId = i.ProductId,
        //            Quantity = stockInIds.Contains(i.PropertyId) ? i.Quantity : -i.Quantity
        //        }).ToList();
        //        stock = stock.GroupBy(i => i.ProductId).Select(i => new StockViewModel
        //        {
        //            ProductId = i.Key,
        //            Quantity = i.Sum(j => j.Quantity)
        //        }).ToList();
        //        stock.ForEach(i =>
        //        {
        //            i.ProductName = dbEntries.Products.FirstOrDefault(p => p.Id == i.ProductId).ProductName;
        //            i.Price = dbEntries.Products.FirstOrDefault(p => p.Id == i.ProductId).BuyingPrice;
        //            i.Amount = i.Quantity * dbEntries.Products.FirstOrDefault(p => p.Id == i.ProductId).BuyingPrice;
        //        });
        //        return stock;
        //    }
        //}
        public List<StockViewModel> GetStocks(string stockType)
        {
            var salesTypeId = _propertyManager.GetPropertyByNameAndValue("Sales", 1).Id;
            var purchaseTypeId = _propertyManager.GetPropertyByNameAndValue("Purchase", 1).Id;
            using (var dbEntries = new ERPEntities())
            {
                try
                {
                    var salesQuantity = dbEntries.InvoiceDetails.Where(i => i.PropertyId == salesTypeId).Select(st => new
                                        {
                                            productId = st.ProductId,
                                            Quantity = -st.Quantity,
                                            Amount = -st.TotalPrice
                                        }).ToList();
                    var purchaseQuantity =
                        dbEntries.InvoiceDetails.Where(i => i.PropertyId == purchaseTypeId).Select(st => new
                        {
                            productId = st.PropertyId,
                            Quantity = st.Quantity,
                            Amount = st.TotalPrice
                        }).ToList();
                    var currentStock =
                        salesQuantity.Union(purchaseQuantity).GroupBy(i => i.productId).Select(st => new StockViewModel
                        {
                            ProductName = dbEntries.Products.Where(p => p.Id == st.Key).Select(i => i.ProductName).SingleOrDefault(),
                            BuyngPrice = dbEntries.Products.Select(i => i.BuyingPrice).SingleOrDefault() ,
                            SellingPrice = dbEntries.Products.Select(i => i.SellingPrice).SingleOrDefault(),
                            Price = stockType == "Opening"
                                    ? dbEntries.Products.FirstOrDefault(p => p.Id == st.Key).BuyingPrice 
                                    : stockType == "Closing" ? dbEntries.Products.FirstOrDefault(p => p.Id == st.Key).SellingPrice : 0,
                            Quantity = st.Sum(q => q.Quantity),
                            Amount =
                                stockType == "Opening"
                                    ? st.Sum(q => q.Quantity) *
                                      dbEntries.Products.FirstOrDefault(p => p.Id == st.Key).BuyingPrice 
                                    : stockType == "Closing" ? st.Sum(q => q.Quantity) * dbEntries.Products.FirstOrDefault(p => p.Id == st.Key).SellingPrice : 0
                        }).ToList();
                    return currentStock;
                }
                catch (Exception ex)
                {
                    throw;
                }
            }
        }

        public decimal GetStockByProduct(long productId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var stockOut = db.InvoiceDetails.Where(i => (i.PropertyId == PropertyConstants.Sales || i.PropertyId==PropertyConstants.PurchaseReturn||i.PropertyId==PropertyConstants.LostInvoice) && i.ProductId == productId).Select(i => i.Quantity).DefaultIfEmpty(0).Sum();
                    var stockIn = db.InvoiceDetails.Where(i => (i.PropertyId == PropertyConstants.Purchase || i.PropertyId == PropertyConstants.SalesReturn || i.PropertyId == PropertyConstants.ProfitInvoice) && i.ProductId == productId).Select(i => i.Quantity).DefaultIfEmpty(0).Sum();
                    return stockIn - stockOut;
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }


        public decimal LoadAmountByQuantity(long modelId, long quantity)
        {
            throw new NotImplementedException();
        }

        public decimal LoadAmountByQuantityForEdit(long modelId, long quantity, long billId)
        {
            throw new NotImplementedException();
        }

    }
}

