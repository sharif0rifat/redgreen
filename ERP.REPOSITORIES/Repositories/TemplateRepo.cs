﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using Infrastructure.ModelHelpers;

namespace ERP.REPOSITORIES.Repositories
{
    public class TemplateRepo : ITemplateRepo
    {
        public DbResponse GetTemplates()
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var templates = db.Templates.ToList();
                    return AutoResponse.SuccessMessageWithParam(ConvertToLocalObjects(templates, new List<PartialTemplate>()));
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse GetTemplates(string root, long applicationId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var templates = (root == ConstantValue.Root) ? db.Templates.ToList() : db.Templates.Where(i=>i.ApplicationId == applicationId).ToList();
                    return AutoResponse.SuccessMessageWithParam(ConvertToLocalObjects(templates, new List<PartialTemplate>()));
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse Register(PartialTemplate param)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbTemplate = ConvertToEntityObject(param, new Template());
                    db.Templates.Add(dbTemplate);
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }

        public DbResponse GetTemplate(long param,long applicationId)
        {
            try
            {
                var permissionrepo = new PermissionRepo();
                using (var db = new ERPEntities())
                {
                    var dbTemplate = db.Templates.FirstOrDefault(i => i.Id == param);
                    var pTemplate = ConvertToLocalObject(dbTemplate, new PartialTemplate());
                    var permissionArr = pTemplate.PermissionIds.Split('^').Select(Int64.Parse).ToList();
                    var dbPermissions = (applicationId == 0) ? db.Permissions.ToList() : db.Permissions.Where(i=>i.ApplicationId == applicationId).ToList();
                    if (dbPermissions.Count < 1) return AutoResponse.NotFoundMessage();
                    var localPermisssions = permissionrepo.ConvertToLocalObjects(dbPermissions, new List<PartialPermission>());
                    foreach (var partialPermission in localPermisssions.Where(partialPermission => permissionArr.Contains(partialPermission.Id)))
                    {
                        partialPermission.CheckboxStatus = true;
                    }
                    pTemplate.PartialPermissions = localPermisssions;
                    return AutoResponse.SuccessMessageWithParam(pTemplate);
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }

        public DbResponse Modify(PartialTemplate param)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var x = param.PartialPermissions.Where(i => i.CheckboxStatus).ToList();
                    if (x.Count < 1) return AutoResponse.FailedMessageWithParam("Check atleast one.");
                    var y="";
                    var count = 1;
                    foreach (var partialPermission in x)
                    {
                        y += Convert.ToString(partialPermission.Id);
                        if (count < x.Count)
                        {
                            y += "^";
                        }
                        count++;
                    }
                    var dbTemplate = db.Templates.FirstOrDefault(i => i.Id == param.Id);
                    if (dbTemplate == null) return AutoResponse.NotFoundMessage();
                    dbTemplate.TemplateName = param.TemplateName;
                    dbTemplate.PermissionIds = y;
                    dbTemplate.ModifiedBy = param.ModifiedBy;
                    dbTemplate.ModifiedDate = param.ModifiedDate;
                    db.Entry(dbTemplate).State = EntityState.Modified;
                    db.SaveChanges();
                }
                return AutoResponse.SuccessMessage();
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }

        private List<PartialTemplate> ConvertToLocalObjects(List<Template> convertedFrom, List<PartialTemplate> convertedTo)
        {
            var converter = new ConvertTypeInto<Template, PartialTemplate>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
        // db object will converted to local object
        public PartialTemplate ConvertToLocalObject(Template convertedFrom, PartialTemplate convertedTo)
        {
            var converter = new ConvertTypeInto<Template, PartialTemplate>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
        // local object will converted to db object
        public Template ConvertToEntityObject(PartialTemplate convertedFrom, Template convertedTo)
        {
            var converter = new ConvertTypeInto<PartialTemplate, Template>();
            return converter.ConvertInto(convertedFrom, convertedTo);
        }
    }
}
