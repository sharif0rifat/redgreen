﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.REPOSITORIES.Repositories
{
    public class PartyManager : GenericRepository<ERPEntities, DAL.Model.Ledger>, IPartyManager
    {
        private IPropertyManager propertyManager;
        public PartyManager()
        {
            propertyManager = new PropertyManager();
        }
        public List<LedgerViewModel> GetPartiesByType(int partyType)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var propertyId = propertyManager.GetPropertyByNameAndValue("PartyType", partyType).Id;
                    var dbPartyList = db.Ledgers.Where(x => x.Status == 1 && x.PropertyId == propertyId).Select(i => i).OrderBy(i => i.Name).ToList();
                    return Mapper.Map<List<LedgerViewModel>>(dbPartyList);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public LedgerViewModel GetParty(long partyId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbParty = db.Ledgers.FirstOrDefault(i => i.Id == partyId);
                    if (dbParty == null) return null;
                    var party = ConvertDbPartyToParty(dbParty, new LedgerViewModel());
                    return party;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public DbResponse AddParty(LedgerViewModel obj, LogInInfo logInInfo)
        {
            try
            {
                obj.LedgerGroupId = obj.PropertyId == PropertyConstants.Supplier ? LedgerConstants.CreditorsGroupLedgerId : LedgerConstants.DebtorsGroupLedgerId;
                obj.Code = obj.Name;
                obj.Visible = 0;
                using (var db = new ERPEntities())
                {
                    bool isDuplicateCodeExist;
                    if (obj.Id > 0)
                    {
                        isDuplicateCodeExist = db.Ledgers.Any(i => i.Code == obj.Code && i.Id != obj.Id && i.Status == 1);
                        if (!isDuplicateCodeExist)
                        {
                            var dbParty = db.Ledgers.FirstOrDefault(i => i.Id == obj.Id);
                            if (dbParty == null) return AutoResponse.NotFoundMessage();
                            var dbObjChanged = Mapper.Map<Ledger>(obj);
                            dbObjChanged.PropertyId = obj.PropertyId;
                            dbObjChanged.ModifiedDate = DateTime.Today;
                            dbObjChanged.CreatedDate = dbParty.CreatedDate;
                            dbObjChanged.Status = 1;
                            Edit(dbObjChanged);
                            Save();
                            return AutoResponse.SuccessMessage();
                        }
                        return AutoResponse.FailedMessageWithParam("Name already  exist !!!");
                    }
                    isDuplicateCodeExist = db.Ledgers.Any(i => i.Code == obj.Code && i.Status == 1);
                    if (!isDuplicateCodeExist)
                    {
                        var dbObj = Mapper.Map<DAL.Model.Ledger>(obj);
                        var checkObj = db.Ledgers.Any(a => a.Code == dbObj.Code && a.Name == dbObj.Name && a.Status == 1);
                        if (checkObj) return AutoResponse.ExistMessage();
                        dbObj.Status = 1;
                        dbObj.PropertyId = obj.PropertyId;
                        dbObj.CreatedBy = dbObj.ModifiedBy = logInInfo.UserId;
                        dbObj.ModifiedDate = dbObj.CreatedDate = DateTime.Today;
                        db.Ledgers.Add(dbObj);
                        db.SaveChanges();
                        return AutoResponse.SuccessMessageWithValue("Party Saved successfully", dbObj.Id);
                    }
                    return AutoResponse.FailedMessageWithParam("Name already  exist !!!");

                }
            }
            catch (DbEntityValidationException e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }
        }
        public DbResponse DeleteParty(long id, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbObj = db.Ledgers.FirstOrDefault(i => i.Id == id);
                    if (dbObj == null) return AutoResponse.NotFoundMessage();
                    if (GetAnyTransaction(id))
                        return AutoResponse.FailedMessage("Cannot Delete party...Some transaction/payment/invoice is under this party.");
                    dbObj.Status = 0;
                    dbObj.ModifiedBy = logInInfo.UserId;
                    dbObj.ModifiedDate = DateTime.Today;
                    Edit(dbObj);
                    Save();
                    return AutoResponse.SuccessMessageWithParam("Party has been Deleted");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }
        private bool GetAnyTransaction(long id)
        {
            using (var db = new ERPEntities())
            {
                var isAnyTransaction = db.Transactions.Where(i => i.PartyId == id).ToList();
                var isAnyPayment = db.Payments.Where(i => i.PartyId == id).ToList();
                var isAnyInvoice = db.InvoiceInfoes.Where(i => i.PartyId == id).ToList();
                return isAnyTransaction.Count > 0 || isAnyPayment.Count > 0 || isAnyInvoice.Count > 0;
            }
        }
        public Ledger ConvertPartyToDbParty(LedgerViewModel party, DAL.Model.Ledger dbParty)
        {
            var partyConvert = new ConvertTypeInto<LedgerViewModel, Ledger>();
            return partyConvert.ConvertInto(party, dbParty);
        }
        public LedgerViewModel ConvertDbPartyToParty(Ledger dbParty, LedgerViewModel party)
        {
            var stockConvert = new ConvertTypeInto<Ledger, LedgerViewModel>();
            return stockConvert.ConvertInto(dbParty, party);
        }


        public DbResponse EditParty(LedgerViewModel obj, LogInInfo logInInfo)
        {
            throw new NotImplementedException();
        }
    }
}
