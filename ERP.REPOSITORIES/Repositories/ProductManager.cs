﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.IRepositories;
using ERP.INFRASTRUCTURE.Global;
using System;
using ERP.REPOSITORIES.Helper;


namespace ERP.REPOSITORIES.Repositories
{
    public class ProductManager : GenericRepository<ERPEntities, Product>, IProductManager
    {
        public DbResponse AddProduct(PartialProduct productObj,LogInInfo logInInfo)
        {
            try
            {
                if (productObj.CategoryId == 0)
                {
                    return AutoResponse.FailedMessageWithParam("Category must be selected.");
                }
                using (var db = new ERPEntities())
                {
                    if (productObj.BuyingPrice <= 0 || productObj.SellingPrice <= 0)
                        return AutoResponse.FailedMessageWithParam("You must give a buying and selling price.");
                    var checkproduct = db.Products.Any(i => (i.ProductName == productObj.ProductName) && i.Status == 1 && i.Id != productObj.Id);
                    if (checkproduct) return AutoResponse.ExistMessage();
                    if (productObj.Id > 0)
                    {
                        var dbProduct = db.Products.FirstOrDefault(i => i.Id == productObj.Id);
                        if (dbProduct == null) return AutoResponse.FailedMessage();
                        dbProduct.CategoryId = productObj.CategoryId;
                        dbProduct.ProductCode = productObj.ProductName;
                        dbProduct.ProductName = productObj.ProductName;
                        dbProduct.SellingPrice = productObj.SellingPrice;
                        dbProduct.BuyingPrice = productObj.BuyingPrice;
                        dbProduct.BuyingPrice = productObj.BuyingPrice;
                        dbProduct.ModifiedDate = DateTime.Today;
                        db.Entry(dbProduct).State = EntityState.Modified;
                    }
                    else
                    {
                        var dbProduct = Mapper.Map<Product>(productObj);
                        dbProduct.Status = 1;
                        dbProduct.ProductCode = productObj.ProductName;
                        dbProduct.CreatedBy = dbProduct.ModifiedBy = logInInfo.UserId;
                        dbProduct.ModifiedDate = dbProduct.CreatedDate = DateTime.Today;
                        db.Products.Add(dbProduct);
                    }
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }

        }
        public DbResponse EditProduct(PartialProduct productObj, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbProduct = db.Products.FirstOrDefault(i => i.Id == productObj.Id);
                    if (dbProduct == null) return AutoResponse.NotFoundMessage();
                    var checkProduct =
                        db.Products.Any(
                            a => a.ProductName == productObj.ProductName && a.Id != productObj.Id && a.Status == 1);
                    if (checkProduct) return AutoResponse.ExistMessage();
                    dbProduct.CategoryId = productObj.CategoryId;
                    dbProduct.ProductName = productObj.ProductName;
                    dbProduct.ProductCode = productObj.ProductCode;
                    dbProduct.BuyingPrice = productObj.BuyingPrice;
                    dbProduct.Commission = productObj.Commission;
                    dbProduct.SellingPrice = productObj.SellingPrice;
                    dbProduct.ModifiedBy = logInInfo.UserId;
                    dbProduct.ModifiedDate = DateTime.Today;
                    Edit(dbProduct);
                    Save();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }
        public DbResponse DeleteProduct(long productId,LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var isAny = db.InvoiceDetails.Where(i => i.ProductId == productId && i.Status == 1).ToList();
                    if (isAny.Count > 0)
                        return AutoResponse.FailedMessage("Cannot Delete Product... Some Invoice is made with this product.");
                    var dbProduct = db.Products.FirstOrDefault(i => i.Id == productId);
                    if (dbProduct == null) return AutoResponse.NotFoundMessage();
                    dbProduct.Status = 0;
                    dbProduct.ModifiedBy = logInInfo.UserId;
                    dbProduct.ModifiedDate = DateTime.Today;
                    Edit(dbProduct);
                    Save();
                    return AutoResponse.SuccessMessageWithParam("Product Deleted.");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }
        public PartialProduct GetProduct(long productId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbProduct = db.Products.FirstOrDefault(i => i.Id == productId);
                    return dbProduct == null ? null : Mapper.Map<PartialProduct>(dbProduct);
                }
            }
            catch (Exception)
            {
                return new PartialProduct();
            }
        }
        public List<PartialProduct> GetProducts()
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbProductList = db.Products.Where(x => x.Status == 1).Select(i => i).OrderBy(i => i.ProductName).ToList();
                    var productList = Mapper.Map<List<PartialProduct>>(dbProductList);
                    var dbcategories = db.Categories.ToList();
                    foreach (var prod in productList)
                    {
                        var prod1 = prod;
                        prod.CategoryName = dbcategories.Where(i => i.Id == prod1.CategoryId).Select(i => i.Name).SingleOrDefault();
                    }
                    return productList;
                }
            }
            catch (Exception ex)
            {
                return new List<PartialProduct>();
            }
        }
        public DbResponse GetIndividualProductStock(long productId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbProduct = db.Products.FirstOrDefault(i => i.Id == productId);
                    var invoices = db.InvoiceDetails.Where(i => i.ProductId == productId).ToList();
                    var dbTableProperty = db.TableProperties.ToList();
                    var purchasePropId = dbTableProperty.Where(i => i.TableName == "InvoiceInfo" && i.ViewName == "Purchase").Select(i => i.PropertyValue).SingleOrDefault() ?? 0;
                    var sellPropId = dbTableProperty.Where(i => i.TableName == "InvoiceInfo" && i.ViewName == "Sales").Select(i => i.PropertyValue).SingleOrDefault() ?? 0;
                    var product = Mapper.Map<PartialProduct>(dbProduct);
                    product.Quantity = invoices.Where(i => i.PropertyId == purchasePropId).Select(i => i.Quantity).Sum() -
                                       invoices.Where(i => i.PropertyId == sellPropId).Select(i => i.Quantity).Sum();
                    
                    return AutoResponse.SuccessMessageWithParam(product);
                }
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage();
            }
        }

        public DbResponse GetIndividualProductStockSell(long productId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbProduct = db.Products.FirstOrDefault(i => i.Id == productId);
                    var invoices = db.InvoiceDetails.Where(i => i.ProductId == productId).ToList();
                    var dbTableProperty = db.TableProperties.ToList();
                    var purchasePropId = dbTableProperty.Where(i => i.TableName == "InvoiceInfo" && i.ViewName == "Purchase").Select(i => i.PropertyValue).SingleOrDefault() ?? 0;
                    var sellPropId = dbTableProperty.Where(i => i.TableName == "InvoiceInfo" && i.ViewName == "Sales").Select(i => i.PropertyValue).SingleOrDefault() ?? 0;
                    var product = Mapper.Map<PartialProduct>(dbProduct);
                    product.Quantity = invoices.Where(i => i.PropertyId == purchasePropId).Select(i => i.Quantity).Sum() -
                                       invoices.Where(i => i.PropertyId == sellPropId).Select(i => i.Quantity).Sum();
                    if (product.Quantity < 1) return AutoResponse.NotFoundMessage();
                    return AutoResponse.SuccessMessageWithParam(product);
                }
            }
            catch (Exception)
            {
                return AutoResponse.NotFoundMessage();
            }
        }

        public List<PartialProduct> GetModelsByCategory(long categoryId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbProductList = (categoryId==0)? db.Products.Where(i=>i.Status==1).ToList():db.Products.Where(x => x.CategoryId == categoryId && x.Status == 1).Select(i => i).OrderBy(i => i.ProductName).ToList();
                    return dbProductList.Count < 1 ? null : Mapper.Map<List<PartialProduct>>(dbProductList);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
        public PartialProduct GetCurrentQuantityByModel(PartialProduct product)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    
                    var dbProductList = db.Products.Where(x => x.CategoryId == product.CategoryId && x.Status == 1).Select(i => i).OrderBy(i => i.ProductName).FirstOrDefault();
                    return dbProductList ==null? null : Mapper.Map<PartialProduct>(dbProductList);
                }
            }
            catch (Exception)
            {
                return null;
            }
        }
    }
}
