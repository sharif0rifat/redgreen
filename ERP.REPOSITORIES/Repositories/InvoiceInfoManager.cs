﻿using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Web.UI.WebControls;


namespace ERP.REPOSITORIES.Repositories
{
    public class InvoiceInfoManager : GenericRepository<ERPEntities, InvoiceInfo>, IInvoiceInfoManager
    {
        private readonly IProductManager _productManager;
        private readonly IInvoiceDetailsManager _detailsManager;
        //private long _propertyId;

        public InvoiceInfoManager()
        {
            _productManager = new ProductManager();
            _detailsManager = new InvoiceDetailsManager();
            //salesPropertyId=_propertyManager.GetPropertyByNameAndValue("Sales", 1).Id;
        }

        public InvoiceInfo AddInvoice(InvoiceInfo invoiceInfo, LogInInfo logInInfo)
        {
            invoiceInfo.CreatedBy = logInInfo.UserId;
            invoiceInfo.CreatedDate = DateTime.Today;
            invoiceInfo.ModifiedBy = logInInfo.UserId;
            invoiceInfo.ModifiedDate = DateTime.Today;
            invoiceInfo.Status = 1;
            invoiceInfo.ProcessBy = logInInfo.UserId;
            invoiceInfo.ApprovedBy = logInInfo.UserId;

            InvoiceInfo result = default(InvoiceInfo);
            try
            {
                using (var db = new ERPEntities())
                {
                    result = db.InvoiceInfoes.Add(invoiceInfo);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
        public void UpdateInvoice(InvoiceInfo invoiceInfo, LogInInfo logInInfo)
        {
            try
            {
                invoiceInfo.ModifiedBy = logInInfo.UserId;
                invoiceInfo.ModifiedDate = DateTime.Today;
                invoiceInfo.ProcessBy = logInInfo.UserId;
                invoiceInfo.ApprovedBy = logInInfo.UserId;
                using (var db = new ERPEntities())
                {
                    db.Entry(invoiceInfo).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private DbResponse ValidatePurchase(PartialInvoiceInfo param, IEnumerable<PartialProduct> productlist, ERPEntities dbEntities)
        {
            var stockManager = new StockManager();
            if (param.PropertyId == PropertyConstants.PurchaseReturn)
            {
                foreach (var detail in productlist)
                {
                    var qty = stockManager.GetStockByProduct(detail.Id);
                    var Product = dbEntities.Products.FirstOrDefault(i => i.Id == detail.Id);
                    if (qty < detail.PurchaseUnit)
                        return new DbResponse
                        {
                            MessageType = 2,
                            MessageData = "Not Enough stock to return of " + Product.ProductName
                        };
                }
            }
            return new DbResponse { MessageType = 1 };
        }

        private DbResponse ValidateSales(PartialInvoiceInfo param, IEnumerable<PartialProduct> productlist, ERPEntities dbEntities)
        {
            var stockManager = new StockManager();
            if (param.PropertyId == PropertyConstants.Sales)
            {
                foreach (var detail in productlist)
                {
                    var qty = stockManager.GetStockByProduct(detail.Id);
                    var Product = dbEntities.Products.FirstOrDefault(i => i.Id == detail.Id);
                    if (qty < detail.PurchaseUnit)
                        return new DbResponse
                        {
                            MessageType = 2,
                            MessageData = "Not enough stock for sale, of  " + Product.ProductName
                        };
                }
            }
            if (param.PropertyId == PropertyConstants.SalesReturn)
            {
                var previousinvoice = dbEntities.InvoiceInfoes.Where(i => i.PartyId == param.PartyId && i.PropertyId == PropertyConstants.Sales).Select(i => i.Id);
                foreach (var detail in productlist)
                {
                    var qty = dbEntities.InvoiceDetails.Where(i => previousinvoice.Contains(i.InvoiceId)).Sum(i => i.Quantity);
                    var Product = dbEntities.Products.FirstOrDefault(i => i.Id == detail.Id);
                    if (qty < detail.PurchaseUnit)
                        return new DbResponse
                        {
                            MessageType = 2,
                            MessageData = "Return quantity For the product " + Product.ProductName + " is not Correct. Check the sales for this client."
                        };
                }
            }
            return new DbResponse { MessageType = 1 };
        }

        private bool ValidatePurchaseReturn(PartialInvoiceInfo param)
        {
            if (param.PropertyId != PropertyConstants.PurchaseReturn)
                return false;
            else
            {
                if (param.PropertyId == PropertyConstants.PurchaseReturn)
                {
                    foreach (var partialInvoiceDetail in param.InvoiceDetailsList)
                    {

                    }
                }
            }
            return false;
        }
        public InvoiceInfo GetDebitCreditByInvoiceType(InvoiceInfo invoiceInfo)
        {
            if (invoiceInfo.PropertyId == PropertyConstants.Purchase)
            {
                invoiceInfo.DebitId = LedgerConstants.PurchaseLedgerId;
                invoiceInfo.CreditId = invoiceInfo.PartyId;
            }
            if (invoiceInfo.PropertyId == PropertyConstants.Sales)
            {
                invoiceInfo.DebitId = invoiceInfo.PartyId;
                invoiceInfo.CreditId = LedgerConstants.SalesLedgerId;
            }
            if (invoiceInfo.PropertyId == PropertyConstants.PurchaseReturn)
            {
                invoiceInfo.DebitId = invoiceInfo.PartyId;
                invoiceInfo.CreditId = LedgerConstants.PurchaseLedgerId;
            }
            if (invoiceInfo.PropertyId == PropertyConstants.SalesReturn)
            {
                invoiceInfo.DebitId = LedgerConstants.SalesLedgerId;
                invoiceInfo.CreditId = invoiceInfo.PartyId;
            }
            if (invoiceInfo.PropertyId == PropertyConstants.LostInvoice)
            {
                invoiceInfo.DebitId = LedgerConstants.ProductExpireExpenseLedgerId;
                invoiceInfo.CreditId = LedgerConstants.CurrentStockLedgerId;
            }
            if (invoiceInfo.PropertyId == PropertyConstants.ProfitInvoice)
            {
                invoiceInfo.DebitId = LedgerConstants.CurrentStockLedgerId;
                invoiceInfo.CreditId = LedgerConstants.BonusIncomeLedgerId;
            }
            return invoiceInfo;
        }

        public DbResponse DeleteInvoiceInfo(long billId, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    using (var dbEntitiesTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            DbResponse validResponse = ValidatePurchaseDelete(billId);
                            if (validResponse.MessageType == 2)
                                return validResponse;
                            var previousInvoiceInfo = db.InvoiceInfoes.FirstOrDefault(x => x.Id == billId);
                            if (previousInvoiceInfo == null) return AutoResponse.FailedMessage();
                            #region update billing info
                            previousInvoiceInfo.ModifiedBy = logInInfo.UserId;
                            previousInvoiceInfo.ModifiedDate = DateTime.Today;
                            previousInvoiceInfo.Status = 0;
                            Edit(previousInvoiceInfo);
                            Save();
                            #endregion
                            var invoiceDetails = db.InvoiceDetails.Where(i => i.InvoiceId == billId).ToList();
                            foreach (var invoiceDetail in invoiceDetails)
                            {
                                #region delete new InvoiceDetails and add stock in
                                var dbInvoiceDetail = invoiceDetail;
                                dbInvoiceDetail.Status = 0;
                                dbInvoiceDetail.ModifiedDate = DateTime.Today;
                                db.Entry(dbInvoiceDetail).State = EntityState.Modified;
                                db.SaveChanges();
                                #endregion
                            }
                            dbEntitiesTransaction.Commit();
                            return AutoResponse.SuccessMessageWithParam("Invoice Deleted.");

                        }
                        catch (Exception e)
                        {
                            dbEntitiesTransaction.Rollback();
                            return AutoResponse.FailedMessageWithParam(e);
                        }
                    }
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception.ToString());
            }
        }

        private DbResponse ValidatePurchaseDelete(long billId)
        {
            using (var dbEntities = new ERPEntities())
            {
                var invoiceDetails = dbEntities.InvoiceDetails.Where(i => i.InvoiceId == billId).ToList();
                return invoiceDetails.Any(invoiceDetail => invoiceDetail.StockIn == PropertyConstants.StockOut) 
                    ? AutoResponse.FailedMessageWithParam("Products may be sold already, you cannot delete the purchase.") 
                    : AutoResponse.SuccessMessage();
            }
        }

        #region======Read Functionality=====
        public PartialInvoiceInfo GetInvoiceByInvoiceId(long billId)
        {
            using (var dbEntities = new ERPEntities())
            {
                var dbInvoiceInfo = dbEntities.InvoiceInfoes.FirstOrDefault(i => i.Id == billId);
                var partialInvoiceInfo = Mapper.Map<PartialInvoiceInfo>(dbInvoiceInfo);
                if (partialInvoiceInfo == null) return new PartialInvoiceInfo();
                return partialInvoiceInfo;
            }
        }

        public List<PartialInvoiceInfo> GetPurchaseInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null,
            DateTime? toDate = null)
        {
            var startDate = fromDate ?? DateTime.Today.AddDays(-30);
            var endDate = toDate ?? DateTime.Today.AddDays(1);
            try
            {
                using (var db = new ERPEntities())
                {
                    var invoiceTypes = db.TableProperties.Where(i => i.PropertyName == "PurchaseInvoiceType").Select(i => i.Id);
                    var purchaseList = db.InvoiceInfoes.Where(invoiceInfo => invoiceInfo.Status == 1
                                                                             && invoiceInfo.InvoiceDate >= startDate &&
                                                                             invoiceInfo.InvoiceDate <= endDate && invoiceTypes.Contains(invoiceInfo.PropertyId))
                        .ToList();
                    if (!string.IsNullOrEmpty(invoiceNo))
                        purchaseList = purchaseList.Where(i => i.InvoiceNo == invoiceNo.Trim()).ToList();
                    else if (invoiceType > 0)
                        purchaseList = purchaseList.Where(i => i.PropertyId == invoiceType).ToList();
                    var result = Mapper.Map<List<PartialInvoiceInfo>>(purchaseList);
                    result.ForEach(i =>
                    {
                        i.PropertyName = db.TableProperties.FirstOrDefault(p => p.Id == i.PropertyId).ViewName;
                        i.CreatedByName = db.Users.FirstOrDefault(u => u.Id == i.CreatedBy).UserName;
                        i.PartyName = db.Ledgers.FirstOrDefault(pr => pr.Id == i.PartyId).Name;
                    });
                    return result;
                }

            }
            catch (Exception ex)
            {
                return new List<PartialInvoiceInfo>();
            }
        }

        public List<PartialInvoiceInfo> GetSalesInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null,
            DateTime? toDate = null)
        {
            var startDate = fromDate ?? DateTime.Today.AddDays(-30);
            var endDate = toDate ?? DateTime.Today.AddDays(1);
            try
            {
                using (var db = new ERPEntities())
                {
                    var invoiceTypes = db.TableProperties.Where(i => i.PropertyName == "SalesInvoiceType").Select(i => i.Id);
                    var purchaseList = db.InvoiceInfoes.Where(invoiceInfo => invoiceInfo.Status == 1
                                                                             && invoiceInfo.InvoiceDate >= startDate &&
                                                                             invoiceInfo.InvoiceDate <= endDate && invoiceTypes.Contains(invoiceInfo.PropertyId))
                        .ToList();
                    if (!string.IsNullOrEmpty(invoiceNo))
                        purchaseList = purchaseList.Where(i => i.InvoiceNo == invoiceNo.Trim()).ToList();
                    else if (invoiceType > 0)
                        purchaseList = purchaseList.Where(i => i.PropertyId == invoiceType).ToList();
                    var result = Mapper.Map<List<PartialInvoiceInfo>>(purchaseList);
                    result.ForEach(i =>
                    {
                        i.PropertyName = db.TableProperties.FirstOrDefault(p => p.Id == i.PropertyId).ViewName;
                        i.CreatedByName = db.Users.FirstOrDefault(u => u.Id == i.CreatedBy).UserName;
                        i.PartyName = db.Ledgers.FirstOrDefault(pr => pr.Id == i.PartyId).Name;
                    });
                    return result;
                }

            }
            catch (Exception)
            {
                return new List<PartialInvoiceInfo>();
            }
        }
        public List<PartialInvoiceInfo> GetInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null, DateTime? toDate = null)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var startDate = new DateTime();
                    var endDate = toDate ?? DateTime.Today.AddDays(1);
                    //var startDate = fromDate;
                    //var endDate = toDate;
                    var res = from invoiceInfo in db.InvoiceInfoes
                              orderby invoiceInfo.Id descending
                              where
                                  invoiceInfo.Status == 1
                                  && invoiceInfo.InvoiceDate >= startDate &&
                                  invoiceInfo.InvoiceDate <= endDate
                              select new PartialInvoiceInfo
                              {
                                  Id = invoiceInfo.Id,
                                  BussinessId = invoiceInfo.BussinessId,
                                  CreatedBy = invoiceInfo.CreatedBy,
                                  CreatedByName = db.Users.FirstOrDefault(u => u.Id == invoiceInfo.CreatedBy).UserName,
                                  CreatedDate = invoiceInfo.CreatedDate,
                                  GrandTotal = invoiceInfo.GrandTotal,
                                  InvoiceDate = invoiceInfo.InvoiceDate,
                                  InvoiceNo = invoiceInfo.InvoiceNo,
                                  InvoiceReferenceId = invoiceInfo.InvoiceReferenceId,
                                  ModifiedBy = invoiceInfo.ModifiedBy,
                                  ModifiedDate = invoiceInfo.ModifiedDate,
                                  PartyId = invoiceInfo.PartyId,
                                  PartyName = db.Ledgers.FirstOrDefault(l => l.Id == invoiceInfo.PartyId).Name,
                                  PropertyId = invoiceInfo.PropertyId,
                                  PropertyName = db.TableProperties.FirstOrDefault(p => p.Id == invoiceInfo.PropertyId).ViewName,
                                  Remarks = invoiceInfo.Remarks,
                                  ReturnTotal = invoiceInfo.ServiceCharge,
                                  Status = invoiceInfo.Status,
                                  SubTotal = invoiceInfo.SubTotal
                              };
                    var result = res.OrderByDescending(i => i.InvoiceNo).ToList();

                    //var dbInvoiceInfoList = db.InvoiceInfoes.Where(x => x.Status == 1 && x.CreatedDate >= startDate && x.CreatedDate <= endDate).ToList();
                    if (!string.IsNullOrEmpty(invoiceNo))
                    {
                        result = result.Where(i => i.InvoiceNo == invoiceNo.Trim()).ToList();
                        //dbInvoiceInfoList = dbInvoiceInfoList.Where(i => i.InvoiceNo == invoiceNo.Trim()).ToList();
                    }
                    else if (invoiceType > 0)
                    {
                        result = result.Where(i => i.PropertyId == invoiceType).ToList();
                        //dbInvoiceInfoList = dbInvoiceInfoList.Where(i => i.PropertyId == invoiceType).ToList();
                    }
                    return result;
                    //Mapper.Map<List<PartialInvoiceInfo>>(result).OrderByDescending(i => i.InvoiceNo).ToList();
                }
            }
            catch (Exception)
            {
                return new List<PartialInvoiceInfo>();
            }
        }
        public List<PartialInvoiceInfo> GetInvoiceInfoByDateRange(long propertyvalue, DateTime datefrom, DateTime dateto)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var res = from invoiceInfo in db.InvoiceInfoes
                              join user in db.Users on invoiceInfo.CreatedBy equals user.Id
                              join party in db.Ledgers on invoiceInfo.PartyId equals party.Id
                              join property in db.TableProperties on invoiceInfo.PropertyId equals property.Id


                              orderby invoiceInfo.Id descending
                              where
                                  invoiceInfo.Status == 1 && invoiceInfo.InvoiceDate >= datefrom &&
                                  invoiceInfo.InvoiceDate <= dateto && invoiceInfo.PropertyId == propertyvalue
                              select new PartialInvoiceInfo
                              {
                                  Id = invoiceInfo.Id,
                                  BussinessId = invoiceInfo.BussinessId,
                                  CreatedBy = invoiceInfo.CreatedBy,
                                  CreatedByName = user.UserName,
                                  CreatedDate = invoiceInfo.CreatedDate,
                                  GrandTotal = invoiceInfo.GrandTotal,
                                  InvoiceDate = invoiceInfo.InvoiceDate,
                                  InvoiceNo = invoiceInfo.InvoiceNo,
                                  InvoiceReferenceId = invoiceInfo.InvoiceReferenceId,
                                  ModifiedBy = invoiceInfo.ModifiedBy,
                                  ModifiedDate = invoiceInfo.ModifiedDate,
                                  PartyId = invoiceInfo.PartyId,
                                  PartyName = party.Name,
                                  PropertyId = invoiceInfo.PropertyId,
                                  PropertyName = property.ViewName,
                                  Remarks = invoiceInfo.Remarks,
                                  ReturnTotal = invoiceInfo.ServiceCharge,
                                  Status = invoiceInfo.Status,
                                  SubTotal = invoiceInfo.SubTotal
                              };
                    var result = res.ToList();
                    return result;
                }
            }
            catch (Exception)
            {
                return new List<PartialInvoiceInfo>();
            }
        }
        public PartialInvoiceInfo GetInvoiceInfo(long billId)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbPartialInvoiceInfo = db.InvoiceInfoes.SingleOrDefault(x => x.Id == billId);
                    var invoice = Mapper.Map<PartialInvoiceInfo>(dbPartialInvoiceInfo);
                    var partialInvoiceDetail = _detailsManager.GetInvoiceDetailsWithProduct(invoice.Id);
                    invoice.InvoiceDetailsList = Mapper.Map<List<InvoiceDetailViewModel>>(partialInvoiceDetail);
                    return invoice;
                }
            }
            catch (Exception)
            {
                return new PartialInvoiceInfo();
            }
        }
        public string GetLastInvoiceNo(long invoiceType)
        {
            string lastInvoiceNo = null;
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var invoicNums =
                        dbEntities.InvoiceInfoes.Where(i => i.PropertyId == invoiceType)
                            .Select(i => i.InvoiceNo).ToList();

                    int maxlen = invoicNums.Count > 0 ? invoicNums.Max(x => x.Length) : 0;
                    var invoiceNoList = dbEntities.InvoiceInfoes.Where(i => i.PropertyId == invoiceType)
                        .Select(i => i.InvoiceNo).ToList();
                    var orderedInvoiceNoList = invoiceNoList.OrderBy(x => x.PadLeft(maxlen, '0')).ToList();
                    lastInvoiceNo = orderedInvoiceNoList.LastOrDefault();
                    //dbEntities.InvoiceInfoes.Where(i => i.PropertyId == invoiceType)
                    //    .OrderBy(x => x.InvoiceNo.PadLeft(maxlen, '0'))
                    //    //.ThenByDescending(x => x)
                    //    .Select(s => s.InvoiceNo)
                    //    .LastOrDefault();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return GetDateGeneratedInvoiceNo(invoiceType);
        }
        #endregion

        #region====Helper Functions=======
        public long LoadFinalInvoiceNo(int salesType)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var lastInvoiceNo = db.InvoiceInfoes.Where(bill =>
                        (bill.Id ==
                         db.InvoiceInfoes.Where(x => x.Status == 1)
                             .Select(x => x.Id)
                             .Max()

                            )
                        )
                        .ToList().First().InvoiceNo;
                    return Convert.ToInt64(lastInvoiceNo);
                }
            }
            catch (Exception)
            {
                return 0;
            }
        }

        //public static string GetInvoiceNo(long propertyId)
        //{
        //    using (var db = new ERPEntities())
        //    {
        //        var invCount = db.InvoiceInfoes.Count(i => i.Status == 1 && i.PropertyId == propertyId) + 1;
        //        return invCount.ToString(CultureInfo.InvariantCulture);
        //    }

        //}

        public string GetInvoiceNo(long propertyId)
        {
            using (var db = new ERPEntities())
            {
                var invCount = db.InvoiceInfoes.Count(i => i.Status == 1 && i.PropertyId == propertyId) + 1;
                return invCount.ToString(CultureInfo.InvariantCulture);
            }

        }
        public PartialInvoiceInfo GetLastInvoice(Constants.PartyType partyType)
        {
            using (var db = new ERPEntities())
            {
                var lastInvoicedb = db.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == (Int64)partyType).OrderByDescending(x => x.InvoiceNo.Length).ThenByDescending(x => x).Select(i => i).FirstOrDefault();
                return Mapper.Map<PartialInvoiceInfo>(lastInvoicedb);
            }
        }

        public DbResponse AddPurchaseInvoice(PartialInvoiceInfo param, List<PartialProduct> productlist, LogInInfo logInInfo)
        {
            string invoiceType = "";
            try
            {
                using (var db = new ERPEntities())
                {
                    var invoiceInfo = Mapper.Map<InvoiceInfo>(param);
                    invoiceInfo.ModifiedBy = invoiceInfo.CreatedBy = logInInfo.UserId;
                    invoiceInfo.ModifiedDate = invoiceInfo.CreatedDate = DateTime.Today;
                    invoiceInfo.Status = 1;
                    invoiceInfo.InvoiceNo = GetInvoiceNo(param.PropertyId);

                    //==========Purchase  Operation=======
                    var purchaseValidate = ValidatePurchase(param, productlist, db);
                    if (purchaseValidate.MessageType != 1)
                        return purchaseValidate;

                    invoiceInfo.SubTotal = productlist.Sum(i => _productManager.GetProduct(i.Id).BuyingPrice * i.PurchaseUnit);
                    invoiceInfo.GrandTotal = productlist.Sum(i => _productManager.GetProduct(i.Id).BuyingPrice * i.PurchaseUnit);
                    invoiceType = "Purchase";

                    invoiceInfo = GetDebitCreditByInvoiceType(invoiceInfo);
                    db.InvoiceInfoes.Add(invoiceInfo);
                    db.SaveChanges();

                    var invoiceDetailList = new List<InvoiceDetail>();
                    foreach (var productObj in productlist)
                    {
                        var product = _productManager.GetProduct(productObj.Id);
                        // var invoiceDetail = Mapper.Map<InvoiceDetail>(productObj);
                        var invoiceDetail = new InvoiceDetail();
                        invoiceDetail.CreatedDate = invoiceInfo.CreatedDate;
                        invoiceDetail.ModifiedDate = invoiceInfo.ModifiedDate;
                        invoiceDetail.CreatedBy = invoiceInfo.CreatedBy;
                        invoiceDetail.ModifiedBy = invoiceInfo.ModifiedBy;
                        invoiceDetail.InvoiceId = invoiceInfo.Id;
                        invoiceDetail.ProductId = product.Id;
                        invoiceDetail.Quantity = productObj.PurchaseUnit;
                        invoiceDetail.BuyingPrice = product.BuyingPrice;
                        invoiceDetail.SellingPrice = product.SellingPrice;
                        invoiceDetail.Warranty = productObj.Warranty;
                        invoiceDetail.ProductSerial = productObj.ProductSerial;
                        invoiceDetail.StockIn = PropertyConstants.StockOut;

                        //==========Purchase operation=======
                        invoiceDetail.TotalPrice = productObj.PurchaseUnit * product.BuyingPrice;
                        invoiceDetail.PropertyId = param.PropertyId;
                        invoiceDetail.Status = 1;
                        invoiceDetail.BussinessId = 0;
                        invoiceDetailList.Add(invoiceDetail);
                    }

                    db.InvoiceDetails.AddRange(invoiceDetailList);
                    db.SaveChanges();
                }
                return AutoResponse.SuccessMessageWithParam(invoiceType + " invoice saved successfully");
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage(invoiceType + " invoice add fail.");
            }
        }

        public DbResponse EditPurchaseInvoice(PartialInvoiceInfo param, List<PartialProduct> productlist, LogInInfo loginInfo)
        {
            string invoiceType = "";
            try
            {

                using (var db = new ERPEntities())
                {

                    var dbInvoiceInfo = db.InvoiceInfoes.FirstOrDefault(i => i.Id == param.Id);
                    if (dbInvoiceInfo != null)
                    {
                        //==========Purchase Operation=======
                        if (ValidatePurchaseReturn(param))
                            dbInvoiceInfo.SubTotal = productlist.Sum(i => _productManager.GetProduct(i.Id).BuyingPrice * i.PurchaseUnit);
                        dbInvoiceInfo.GrandTotal = productlist.Sum(i => _productManager.GetProduct(i.Id).BuyingPrice * i.PurchaseUnit);
                        invoiceType = "Purchase";

                        dbInvoiceInfo = GetDebitCreditByInvoiceType(dbInvoiceInfo);
                        //====Edit Operation, Change the object value===========
                        dbInvoiceInfo.ModifiedBy = loginInfo.UserId;
                        dbInvoiceInfo.ModifiedDate = DateTime.Today;
                        dbInvoiceInfo.PartyId = param.PartyId;
                        dbInvoiceInfo.PropertyId = param.PropertyId;


                        db.Entry(dbInvoiceInfo).State = EntityState.Modified;
                        if (productlist.Any())
                        {
                            var dbProductList =
                                db.InvoiceDetails.Where(i => i.InvoiceId == dbInvoiceInfo.Id).ToList();
                            if (dbProductList.Any())
                            {
                                foreach (var detail in dbProductList)
                                {
                                    db.InvoiceDetails.Remove(detail);
                                }
                            }
                        }
                        else
                            AutoResponse.FailedMessageWithParam("No Product is added in this Invoice");
                        var invoiceDetailList = new List<InvoiceDetail>();
                        foreach (var productObj in productlist)
                        {
                            var product = _productManager.GetProduct(productObj.Id);
                            var invoiceDetail = new InvoiceDetail();
                            invoiceDetail.CreatedDate = dbInvoiceInfo.CreatedDate;
                            invoiceDetail.ModifiedDate = dbInvoiceInfo.ModifiedDate;
                            invoiceDetail.CreatedBy = dbInvoiceInfo.CreatedBy;
                            invoiceDetail.ModifiedBy = dbInvoiceInfo.ModifiedBy;
                            invoiceDetail.InvoiceId = dbInvoiceInfo.Id;
                            invoiceDetail.ProductId = product.Id;
                            invoiceDetail.Quantity = productObj.PurchaseUnit;
                            invoiceDetail.SellingPrice = product.SellingPrice;
                            invoiceDetail.BuyingPrice = product.BuyingPrice;
                            invoiceDetail.Warranty = productObj.Warranty;
                            invoiceDetail.ProductSerial = productObj.ProductSerial;
                            invoiceDetail.StockIn = PropertyConstants.StockOut;

                            //==========Purchase operation=======

                            // Purchase Operation
                            invoiceDetail.TotalPrice = productObj.PurchaseUnit * product.BuyingPrice;
                            invoiceDetail.PropertyId = param.PropertyId;
                            invoiceDetail.Status = 1;
                            invoiceDetail.BussinessId = 0;
                            invoiceDetailList.Add(invoiceDetail);
                        }

                        db.InvoiceDetails.AddRange(invoiceDetailList);
                        db.SaveChanges();
                    }
                    return AutoResponse.SuccessMessageWithParam(invoiceType + " invoice saved successfully");
                }
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage(invoiceType + " invoice add fail.");
            }
        }

        public DbResponse AddSalesInvoice(PartialInvoiceInfo param, List<PartialProduct> productlist, LogInInfo logInInfo)
        {
            string invoiceType = "";
            try
            {
                using (var db = new ERPEntities())
                {
                    var invoiceInfo = Mapper.Map<InvoiceInfo>(param);
                    invoiceInfo.ModifiedBy = invoiceInfo.CreatedBy = logInInfo.UserId;
                    invoiceInfo.ModifiedDate = invoiceInfo.CreatedDate = DateTime.Today;
                    invoiceInfo.Status = 1;
                    invoiceInfo.InvoiceNo = GetInvoiceNo(param.PropertyId);

                    //==========Purchase Operation=======
                    var salesValidate = ValidateSales(param, productlist, db);
                    if (salesValidate.MessageType != 1)
                        return salesValidate;

                    invoiceInfo.SubTotal = productlist.Sum(i => _productManager.GetProduct(i.Id).SellingPrice * i.PurchaseUnit);
                    invoiceInfo.GrandTotal = productlist.Sum(i => _productManager.GetProduct(i.Id).SellingPrice * i.PurchaseUnit);
                    invoiceType = "Sales";

                    invoiceInfo = GetDebitCreditByInvoiceType(invoiceInfo);
                    db.InvoiceInfoes.Add(invoiceInfo);
                    db.SaveChanges();

                    var invoiceDetailList = new List<InvoiceDetail>();
                    foreach (var productObj in productlist)
                    {
                        var product = _productManager.GetProduct(productObj.Id);
                        // var invoiceDetail = Mapper.Map<InvoiceDetail>(productObj);
                        var invoiceDetail = new InvoiceDetail();
                        invoiceDetail.CreatedDate = invoiceInfo.CreatedDate;
                        invoiceDetail.ModifiedDate = invoiceInfo.ModifiedDate;
                        invoiceDetail.CreatedBy = invoiceInfo.CreatedBy;
                        invoiceDetail.ModifiedBy = invoiceInfo.ModifiedBy;
                        invoiceDetail.InvoiceId = invoiceInfo.Id;
                        invoiceDetail.ProductId = product.Id;
                        invoiceDetail.Quantity = productObj.PurchaseUnit;
                        invoiceDetail.BuyingPrice = product.BuyingPrice;
                        invoiceDetail.SellingPrice = product.SellingPrice;
                        invoiceDetail.Warranty = productObj.Warranty;
                        invoiceDetail.ProductSerial = productObj.ProductSerial;

                        //==========Sales operation=======
                        invoiceDetail.TotalPrice = productObj.PurchaseUnit * product.SellingPrice;
                        invoiceDetail.PropertyId = param.PropertyId;
                        invoiceDetail.Status = 1;
                        invoiceDetail.BussinessId = 0;
                        invoiceDetailList.Add(invoiceDetail);
                    }

                    db.InvoiceDetails.AddRange(invoiceDetailList);
                    db.SaveChanges();
                }
                return AutoResponse.SuccessMessageWithParam(invoiceType + " invoice saved successfully");
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage(invoiceType + " invoice add fail.");
            }
        }

        public DbResponse EditSalesInvoice(PartialInvoiceInfo param, List<PartialProduct> productlist, LogInInfo loginInfo)
        {
            string invoiceType = "";
            try
            {

                using (var db = new ERPEntities())
                {

                    var dbInvoiceInfo = db.InvoiceInfoes.FirstOrDefault(i => i.Id == param.Id);
                    if (dbInvoiceInfo != null)
                    {
                        //==========Purchase Operation=======
                        dbInvoiceInfo.SubTotal = productlist.Sum(i => _productManager.GetProduct(i.Id).SellingPrice * i.PurchaseUnit);
                        dbInvoiceInfo.GrandTotal = productlist.Sum(i => _productManager.GetProduct(i.Id).SellingPrice * i.PurchaseUnit);
                        invoiceType = "Sales";

                        dbInvoiceInfo = GetDebitCreditByInvoiceType(dbInvoiceInfo);
                        //====Edit Operation, Change the object value===========
                        dbInvoiceInfo.ModifiedBy = loginInfo.UserId;
                        dbInvoiceInfo.ModifiedDate = DateTime.Today;
                        dbInvoiceInfo.PartyId = param.PartyId;
                        dbInvoiceInfo.PropertyId = param.PropertyId;


                        db.Entry(dbInvoiceInfo).State = EntityState.Modified;
                        if (productlist.Any())
                        {
                            var dbProductList =
                                db.InvoiceDetails.Where(i => i.InvoiceId == dbInvoiceInfo.Id).ToList();
                            if (dbProductList.Any())
                            {
                                foreach (var detail in dbProductList)
                                {
                                    db.InvoiceDetails.Remove(detail);
                                }
                            }
                        }
                        else
                            AutoResponse.FailedMessageWithParam("No Product is added in this Invoice");
                        var invoiceDetailList = new List<InvoiceDetail>();
                        foreach (var productObj in productlist)
                        {
                            var product = _productManager.GetProduct(productObj.Id);
                            var invoiceDetail = new InvoiceDetail();
                            invoiceDetail.CreatedDate = dbInvoiceInfo.CreatedDate;
                            invoiceDetail.ModifiedDate = dbInvoiceInfo.ModifiedDate;
                            invoiceDetail.CreatedBy = dbInvoiceInfo.CreatedBy;
                            invoiceDetail.ModifiedBy = dbInvoiceInfo.ModifiedBy;
                            invoiceDetail.InvoiceId = dbInvoiceInfo.Id;
                            invoiceDetail.ProductId = product.Id;
                            invoiceDetail.Quantity = productObj.PurchaseUnit;
                            invoiceDetail.SellingPrice = product.SellingPrice;
                            invoiceDetail.BuyingPrice = product.BuyingPrice;
                            invoiceDetail.Warranty = productObj.Warranty;
                            invoiceDetail.ProductSerial = productObj.ProductSerial;
                            invoiceDetail.StockIn = PropertyConstants.StockOut;


                            // Sales Operation
                            invoiceDetail.TotalPrice = productObj.PurchaseUnit * product.SellingPrice;
                            invoiceDetail.PropertyId = param.PropertyId;
                            invoiceDetail.Status = 1;
                            invoiceDetail.BussinessId = 0;
                            invoiceDetailList.Add(invoiceDetail);
                        }

                        db.InvoiceDetails.AddRange(invoiceDetailList);
                        db.SaveChanges();
                    }
                    return AutoResponse.SuccessMessageWithParam(invoiceType + " invoice saved successfully");
                }
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage(invoiceType + " invoice add fail.");
            }
        }


        #endregion



        public DbResponse DeleteInvoiceFromDB(long id, LogInInfo logInInfo)
        {
            using (var dbEntities = new ERPEntities())
            {
                var invoiceInfo = dbEntities.InvoiceInfoes.SingleOrDefault(i => i.Id == id);
                if (invoiceInfo != null)
                {
                    invoiceInfo.Status = 0;
                    invoiceInfo.ModifiedBy = logInInfo.UserId;
                    invoiceInfo.ModifiedDate = DateTime.Today;
                    dbEntities.Entry(invoiceInfo).State = System.Data.Entity.EntityState.Modified;
                    dbEntities.SaveChanges();
                }
            }

            return AutoResponse.SuccessMessage();
        }


        #region=========Validate Serial========

        public DbResponse ValidateSerial(long propertyId, IList<InvoiceDetailViewModel> detailList)
        {
            IList<InvoiceDetailViewModel> sameSerial = null;
            foreach (var invoiceDetailViewModel in detailList)
            {
                if (invoiceDetailViewModel.ProductSerial == null || invoiceDetailViewModel.ProductSerial.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You must give product Serial for each product");
                sameSerial = detailList.Where(i => i.Status == 1 && i.ProductSerial == invoiceDetailViewModel.ProductSerial).ToList();
                if (sameSerial != null && sameSerial.Count > 1)
                    return AutoResponse.FailedMessageWithParam("The Product Serial: " + invoiceDetailViewModel.ProductSerial + "  is Inserted twice here.");
            }
            if (sameSerial != null && sameSerial.Count > 1)
                return AutoResponse.FailedMessageWithParam("Serial No: exist! You cannot give product with same serial No:");
            if (propertyId == PropertyConstants.Purchase || propertyId == PropertyConstants.SalesReturn ||
                propertyId == PropertyConstants.ProfitInvoice)
                return CheckSameSerialExist(detailList);
            if (propertyId == PropertyConstants.Sales || propertyId == PropertyConstants.PurchaseReturn ||
                propertyId == PropertyConstants.LostInvoice)
                return CheckSerialExist(detailList, propertyId);

            return AutoResponse.SuccessMessage();
        }

        
        private DbResponse CheckSerialExist(IEnumerable<InvoiceDetailViewModel> detailList, long propertyId)
        {
            using (var db = new ERPEntities())
            {
                InvoiceDetail existSerial = null;
                foreach (var invoiceDetailViewModel in detailList)
                {
                    //For Edit
                    if (invoiceDetailViewModel.Id > 0 && (propertyId != PropertyConstants.Sales || propertyId == PropertyConstants.SalesReplace))
                        existSerial =
                        db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.Purchase && i.Id!=invoiceDetailViewModel.Id &&
                                                              i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                    //For Edit
                    else if (invoiceDetailViewModel.Id > 0 && propertyId == PropertyConstants.Sales)
                    {
                        existSerial =
                            db.InvoiceDetails.FirstOrDefault(
                                i =>
                                    i.Status == 1 &&
                                    ((i.StockIn == PropertyConstants.StockIn &&
                                      i.PropertyId == PropertyConstants.Purchase) ||
                                     i.PropertyId == PropertyConstants.Sales) &&
                                     i.ProductId == invoiceDetailViewModel.ProductId &&
                                    i.ProductSerial == invoiceDetailViewModel.ProductSerial);
                        if (existSerial == null)
                            return
                                AutoResponse.FailedMessageWithParam("The product with the serial : " +
                                                                    invoiceDetailViewModel.ProductSerial +
                                                                    " ...is invalid.");
                    }
                    else
                    {
                        existSerial =
                        db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.Purchase && i.ProductId == invoiceDetailViewModel.ProductId &&
                                                              i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                        //======On selling time reserve the purchase price.========
                        if (existSerial!=null)
                        invoiceDetailViewModel.BuyingPrice = existSerial.BuyingPrice;      
                        else
                        {
                            var firstOrDefault = db.Products.FirstOrDefault(p=>p.Id== invoiceDetailViewModel.ProductId);
                            if (firstOrDefault != null)
                                invoiceDetailViewModel.BuyingPrice = firstOrDefault.BuyingPrice;
                        }
                    }
                    
                    if (existSerial == null)
                        return AutoResponse.FailedMessageWithParam("The product with the serial : " + invoiceDetailViewModel.ProductSerial + " ...is not in stock.");
                }
                return AutoResponse.SuccessMessage();
            }
        }
        private DbResponse CheckSameSerialExist(IEnumerable<InvoiceDetailViewModel> detailList)
        {
            using (var db = new ERPEntities())
            {
                InvoiceDetail existSerial = null;
                var exist = false;
                var dbDetailList =
                    db.InvoiceDetails.Where(
                        i =>
                            i.Status == 1 && i.PropertyId == PropertyConstants.Purchase &&
                            i.StockIn == PropertyConstants.StockIn).ToList();
                foreach (var invoiceDetailViewModel in detailList)
                {
                    if (invoiceDetailViewModel.Id > 0)
                    {
                        existSerial =
                        db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.Purchase && i.Id != invoiceDetailViewModel.Id &&
                                                              i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                    }
                      else
                        existSerial =
                            db.InvoiceDetails.FirstOrDefault(i => i.Status == 1 && i.PropertyId == PropertyConstants.Purchase &&
                                                                  i.ProductSerial == invoiceDetailViewModel.ProductSerial && i.StockIn == PropertyConstants.StockIn);
                    if (existSerial != null)
                    {
                        var invoice = db.InvoiceInfoes.FirstOrDefault(i => i.Status == 1 && i.Id == existSerial.InvoiceId);
                        return AutoResponse.FailedMessageWithParam("The product serial" + invoiceDetailViewModel.ProductSerial + " is duplicate. in Invoice: " + invoice.InvoiceNo);
                    }
                    
                }
                return AutoResponse.SuccessMessage();
            }
        }
        #endregion


        public string GetDateGeneratedInvoiceNo(long propertyId)
        {
            using (var db = new ERPEntities())
            {
                var invoiceCountOfCurrentDate =
                    db.InvoiceInfoes.Count(i => i.Status == 1 && i.PropertyId == propertyId);
                var invoiceString = DateTime.Today.Year.ToString() + DateTime.Today.Month.ToString("D2") +
                                    DateTime.Today.Day.ToString("D2") + "I" + (invoiceCountOfCurrentDate + 1).ToString();
                return "RGC" + invoiceString;
            }
        }

    }
}
