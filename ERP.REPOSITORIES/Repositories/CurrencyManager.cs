﻿using ERP.DAL.Model;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP.INFRASTRUCTURE.Entity;
using AutoMapper;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using System.Data.Entity;

namespace ERP.REPOSITORIES.Repositories
{
    public class CurrencyManager : GenericRepository<ERPEntities, Currency>, ICurrencyManager
    {
        public List<PartialCurrency> GetCurrencies()
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    //var currencyListt = db.Currencies.OrderBy(i => i.CurrencyName).ToList();
                    var currencyListt = db.Currencies.Where(x => x.Status == 1).Select(i => i).OrderBy(i => i.CurrencyName).ToList();
                    return Mapper.Map<List<PartialCurrency>>(currencyListt);
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public PartialCurrency GetCurrency(int id)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var currency = db.Currencies.SingleOrDefault(x => x.ID == id);
                    return Mapper.Map<PartialCurrency>(currency);
                }
            }
            catch (Exception)
            {
                return new PartialCurrency();
            }
        }

        public PartialCurrency GetCurrencyByCountry(string country)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var currency = db.Currencies.SingleOrDefault(x => x.Country == country);
                    return Mapper.Map<PartialCurrency>(currency);
                }
            }
            catch (Exception)
            {
                return new PartialCurrency();
            }
        }

        public PartialCurrency GetCurrencyBySymbol(string symbol)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var currency = db.Currencies.SingleOrDefault(x => x.Symbol == symbol);
                    return Mapper.Map<PartialCurrency>(currency);
                }
            }
            catch (Exception)
            {
                return new PartialCurrency();
            }
        }

        //Add Currency Interface
        public DbResponse AddCurrency(PartialCurrency currencyObj)
        {
            try
            {
                using (var db = new ERPEntities())
                {

                    var checkCurrency = db.Currencies.Any(i => i.CurrencyName == currencyObj.CurrencyName && i.Status == 1 && i.ID != currencyObj.ID);
                    if (checkCurrency) return AutoResponse.ExistMessage();
                    if (currencyObj.ID > 0)
                    {
                        var dbCurrency = db.Currencies.FirstOrDefault(i => i.ID == currencyObj.ID);
                        if (dbCurrency == null) return AutoResponse.FailedMessage();
                        dbCurrency.CurrencyName = currencyObj.CurrencyName;
                        dbCurrency.Symbol = currencyObj.Symbol;
                        dbCurrency.ShortName = currencyObj.ShortName;
                        dbCurrency.Country = currencyObj.Country;
                        dbCurrency.ConversionRate = currencyObj.ConversionRate;
                        dbCurrency.ModifiedBy = currencyObj.ModifiedBy;
                        dbCurrency.ModifiedDate = currencyObj.ModifiedDate;
                        db.Entry(dbCurrency).State = EntityState.Modified;
                    }
                    else
                    {
                        var dbCurrency = Mapper.Map<Currency>(currencyObj);
                        dbCurrency.ModifiedDate = dbCurrency.ModifiedDate = DateTime.Today;
                        dbCurrency.Status = 1;
                        db.Currencies.Add(dbCurrency);
                    }
                    db.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }

        }

        //Delete

        public DbResponse DeleteCurrency(long currencyId, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    //var isAny = db.Products.Where(i => i.CategoryId == categoryId && i.Status == 1).ToList();
                    //if (isAny.Count > 0) return AutoResponse.FailedMessage("Cannot Delete Category...Some Product is udnder this category.");
                    var dbCurrency = db.Currencies.FirstOrDefault(i => i.ID == currencyId);
                    if (dbCurrency == null) return AutoResponse.NotFoundMessage();
                    dbCurrency.Status = 0;
                    dbCurrency.ModifiedBy = logInInfo.UserId;
                    dbCurrency.ModifiedDate = DateTime.Today;
                    Edit(dbCurrency);
                    Save();
                    return AutoResponse.SuccessMessageWithParam("Currency Deleted.");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

    }
}
