﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Security.Cryptography;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using ERP.REPOSITORIES.ReportRepositories;
using ERP.REPOSITORIES.System256;
using Accounts = ERP.INFRASTRUCTURE.Accounts;


namespace ERP.REPOSITORIES.Repositories
{
    public class AccountsManager : GenericRepository<ERPEntities, Accounts>, IAccountsManager
    {
        private readonly ERPEntities _dbEntiries;
        public AccountsManager(ERPEntities dbEntities)
        {
            if (dbEntities!=null)
            _dbEntiries = dbEntities;
            else
            _dbEntiries=new ERPEntities();
        }

        public List<AccountsLedgerRE> GetAccountsLedgerById(long ledgerId, DateTime startDate,
            DateTime endDate)
        {
            var ledger = _dbEntiries.Ledgers.FirstOrDefault(i => i.Id == ledgerId);
            var ledgerType = GetLedgerType(ledger);
            var ledgerByDate = GetAccountsLedgerData(ledger, startDate, endDate);
            if(ledgerType.AccountsType==PropertyConstants.Asset || ledgerType.AccountsType==PropertyConstants.Liability)
                ledgerByDate.Insert(0, GetAccountsLedgerOpeningData(ledger, startDate));
            decimal temp = 0;
            if(ledgerByDate.Count>0)
                temp = ledgerByDate[0].Balance;
            ledgerByDate.ForEach(i =>
            {
                i.Balance = temp + (ledgerType.PropertyId == PropertyConstants.Credit
                    ? (i.Credit - i.Debit)
                    : (i.Debit - i.Credit));
                temp = i.Balance;
            });
            return ledgerByDate;
        }

        private GroupHeader GetLedgerType(Ledger ledger)
        {
            var ledgerGroupId = _dbEntiries.LedgerGroups.FirstOrDefault(i => i.Id == ledger.LedgerGroupId).GroupHeaderId;
            var groupHeader = _dbEntiries.GroupHeaders.FirstOrDefault(i => i.Id == ledgerGroupId);
            return groupHeader;
        }
        public AccountsLedgerRE GetAccountsLedgerOpeningData(Ledger ledger, DateTime startDate)
        {
            var openinStartDate = new DateTime();
            var openingEndDate = startDate.AddDays(-1);
            var openingLedger = GetAccountsLedgerData(ledger, openinStartDate, openingEndDate);
            var ledgerOpening = new AccountsLedgerRE
            {
                ledgerId= ledger.Id,
                headerId = ledger.LedgerGroupId,
                Particular = "Opening",
                Balance = ledger.PropertyId == PropertyConstants.Supplier ? (openingLedger.Sum(i => i.Credit) - openingLedger.Sum(i => i.Debit))
                                : (openingLedger.Sum(i => i.Debit) - openingLedger.Sum(i => i.Credit))
            };
            return ledgerOpening;
        }
        internal List<AccountsLedgerRE> GetAccountsLedgerData(Ledger ledger, DateTime startDate, DateTime endDate)
        {
            var partyInvoice =
                _dbEntiries.InvoiceInfoes.Where(
                    i => i.Status == 1 && i.InvoiceDate >= startDate && i.InvoiceDate <= endDate && (i.DebitId == ledger.Id || i.CreditId == ledger.Id))
                    .Select(i => new AccountsLedgerRE
                    {
                        ledgerId = ledger.Id,
                        headerId = ledger.LedgerGroupId,
                        Particular = _dbEntiries.TableProperties.FirstOrDefault(pr => pr.Id == i.PropertyId).ViewName + " " + i.Remarks,
                        Date = i.InvoiceDate??DateTime.Today,
                        Debit = i.DebitId == ledger.Id ? i.GrandTotal : 0,
                        Credit = i.CreditId == ledger.Id ? i.GrandTotal : 0
                    }).ToList();
            var partyPayment = _dbEntiries.Payments.Where(
                    i => i.Status == 1 && i.Date >= startDate && i.Date <= endDate && (i.DebitId == ledger.Id || i.CreditId == ledger.Id))
                    .Select(i => new AccountsLedgerRE
                    {
                        ledgerId = ledger.Id,
                        headerId = ledger.LedgerGroupId,
                        Particular = _dbEntiries.TableProperties.FirstOrDefault(pr => pr.Id == i.PaymentType).ViewName + " " + i.Remarks,
                        Date = i.Date,
                        Debit = i.DebitId == ledger.Id ? i.Amount : 0,
                        Credit = i.CreditId == ledger.Id ? i.Amount : 0
                    }).ToList();
            var partyTransaction = _dbEntiries.Transactions.Where(
                    i => i.Status == 1 && i.TransactionDate >= startDate && i.TransactionDate <= endDate && (i.DebitHeaderId == ledger.Id || i.CreditHeaderId == ledger.Id))
                    .Select(i => new AccountsLedgerRE
                    {
                        ledgerId = ledger.Id,
                        headerId = ledger.LedgerGroupId,
                        Particular = i.Remarks,
                        Date = i.TransactionDate,
                        Debit = i.DebitHeaderId == ledger.Id ? (i.Amount ?? 0) : 0,
                        Credit = i.CreditHeaderId == ledger.Id ? (i.Amount ?? 0) : 0
                    }).ToList();
            var totalLedger = partyInvoice.Union(partyPayment).Union(partyTransaction).OrderBy(i => i.Date).ToList();
            return totalLedger;
        }

    }
    public class AccountsLedgerRE
    {
        public string Header { get; set; }
        public long ledgerId { get; set; }
        public long headerId { get; set; }
        public DateTime Date { get; set; }
        public string Particular { get; set; }
        public string InvoiceNo { get; set; }
        public Decimal Debit { get; set; }
        public Decimal Credit { get; set; }
        public long DebitId { get; set; }
        public long CreditId { get; set; }
        public Decimal Balance { get; set; }
        public string ReferenceNo { get; set; }
    }
}
