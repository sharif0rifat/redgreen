﻿using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;


namespace ERP.REPOSITORIES.Repositories
{
    public class SalesInvoiceManager : GenericRepository<ERPEntities, InvoiceInfo>, ISalesInvoiceManager
    {
        private readonly IProductManager _productManager;
        private readonly IInvoiceInfoManager _invoiceInfoManager;
        private readonly IInvoiceDetailsManager _invoiceDetailManager;
        private readonly IInvoiceDetailsManager _detailsManager;
        private readonly ICurrencyManager _currencyManager;
        private readonly IPartyManager _partyManager;
        private SalesSerialValidation _validateSales;
        public List<InvoiceDetail> invoiceDetailsbyID;

        //private long _propertyId;

        public SalesInvoiceManager()
        {
            _productManager = new ProductManager();
            _invoiceInfoManager = new InvoiceInfoManager();
            _invoiceDetailManager = new InvoiceDetailsManager();
            _detailsManager = new InvoiceDetailsManager();
            _currencyManager = new CurrencyManager();
            _partyManager = new PartyManager();
            _validateSales = new SalesSerialValidation();
            //salesPropertyId=_propertyManager.GetPropertyByNameAndValue("Sales", 1).Id;
        }


        public List<PartialInvoiceInfo> GetSalesInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null, DateTime? toDate = null)
        {
            var startDate = fromDate ?? DateTime.Today.AddDays(-30);
            var endDate = toDate ?? DateTime.Today.AddDays(1);
            try
            {
                using (var db = new ERPEntities())
                {
                    var invoiceTypes = db.TableProperties.Where(i => i.PropertyName == "SalesInvoiceType").Select(i => i.Id);
                    var purchaseList = db.InvoiceInfoes.Where(invoiceInfo => invoiceInfo.Status == 1
                                                                             && invoiceInfo.InvoiceDate >= startDate &&
                                                                             invoiceInfo.InvoiceDate <= endDate && invoiceTypes.Contains(invoiceInfo.PropertyId))
                        .ToList();
                    if (!string.IsNullOrEmpty(invoiceNo))
                        purchaseList = purchaseList.Where(i => i.InvoiceNo == invoiceNo.Trim()).ToList();
                    else if (invoiceType > 0)
                        purchaseList = purchaseList.Where(i => i.PropertyId == invoiceType).ToList();
                    var result = Mapper.Map<List<PartialInvoiceInfo>>(purchaseList);
                    result.ForEach(i =>
                    {
                        i.PropertyName = db.TableProperties.FirstOrDefault(p => p.Id == i.PropertyId).ViewName;
                        i.CreatedByName = db.Users.FirstOrDefault(u => u.Id == i.CreatedBy).UserName;
                        i.PartyName = db.Ledgers.FirstOrDefault(pr => pr.Id == i.PartyId).Name;
                    });
                    return result;
                }
            }
            catch (Exception ex)
            {
                return new List<PartialInvoiceInfo>();
            }
        }

        public DbResponse AddSalesInvoice(PartialInvoiceInfo partialInvoiceInfo, LogInInfo logInInfo)
        {
            //Validate Serial For Sales and Sales Replace
            if (partialInvoiceInfo.PropertyId == PropertyConstants.Sales || partialInvoiceInfo.PropertyId == PropertyConstants.SalesReplace)
            {
                DbResponse res = _validateSales.ValidateSerialForSales(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList);
                if (res.MessageType == 2)
                    return res;
            }
            //Validate Serial For Sales Return
            else if (partialInvoiceInfo.PropertyId == PropertyConstants.SalesReturn)
            {
                DbResponse res = _validateSales.ValidateSerialForSalesReturn(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList);
                if (res.MessageType == 2)
                    return res;

               var response = PartySerialVerification(partialInvoiceInfo);
                if (response.MessageType == 2)
                    return response;
            }



            if (partialInvoiceInfo.PartyId == -1)
            {
                if (partialInvoiceInfo.PartyName == null || partialInvoiceInfo.PartyName.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You Must give a Name for new Customer.");
                var partyRes = _partyManager.AddParty(
                    new LedgerViewModel
                    {
                        Name = partialInvoiceInfo.PartyName,
                        PropertyId = PropertyConstants.Dealer,
                        Address1 = partialInvoiceInfo.PartyAddress
                    }, logInInfo);
                if (partyRes.MessageType == 2)
                    return partyRes;
                partialInvoiceInfo.PartyId = (long)partyRes.ReturnValue;
            }

            try
            {
                if (partialInvoiceInfo.PartyId < 1)
                    return AutoResponse.FailedMessageWithParam("You must select or give a party.");
                var invoiceInfo = Mapper.Map<InvoiceInfo>(partialInvoiceInfo);
                UserRepo userManager = new UserRepo();
                var user = userManager.GetAdmin();
                invoiceInfo = _invoiceInfoManager.GetDebitCreditByInvoiceType(invoiceInfo); //Debit and Credit Header Id
                invoiceInfo.ReferenceNo = invoiceInfo.ReferenceNo ?? "";
                invoiceInfo.InvoiceNo = _invoiceInfoManager.GetDateGeneratedInvoiceNo(invoiceInfo.PropertyId);

                invoiceInfo.ProcessBy = user.Id;
                invoiceInfo.ApprovedBy = user.Id;


                // Currency
                var currencyInfo = _currencyManager.GetCurrency(partialInvoiceInfo.CurrencyId);
                if (currencyInfo != null)
                {
                    invoiceInfo.Currency = currencyInfo.ID;
                    invoiceInfo.ConversionRate = currencyInfo.ConversionRate;
                }

                //Save To Invoice
                invoiceInfo = _invoiceInfoManager.AddInvoice(invoiceInfo, logInInfo);
                //====Save Detail to invoice=====
                if (partialInvoiceInfo.InvoiceDetailsList != null)
                {
                    var detailList = (List<InvoiceDetailViewModel>)partialInvoiceInfo.InvoiceDetailsList;
                    detailList.ForEach(i =>
                    {
                        i.PropertyId = i.GiftId > 0 ? i.GiftId : invoiceInfo.PropertyId;
                        i.Currency = currencyInfo.ID;
                        i.ConversionRate = currencyInfo.ConversionRate;
                    });
                    _detailsManager.AddInvoiceDetailListToDB(detailList, invoiceInfo.Id, partialInvoiceInfo.PropertyId, logInInfo);
                    //_detailsManager.ChangeStockFlag(detailList, partialInvoiceInfo.PropertyId);
                }
                return AutoResponse.SuccessMessageWithValue("Sacved Successfully", invoiceInfo.Id);
            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }

            //return AutoResponse.SuccessMessage();

        }

        private DbResponse PartySerialVerification(PartialInvoiceInfo partialInvoiceInfo)
        {
           
            using (var db = new ERPEntities())
            {
                var inoviceIds = db.InvoiceInfoes.Where(i => i.PartyId == partialInvoiceInfo.PartyId && i.Status == 1).Select(x => x.Id).ToList();
                //HashSet<string> productSerialList = new HashSet<string>(invoiceDetailsParty.Select(x => x.Id));
                if (inoviceIds.Count == 0)
                {
                    return AutoResponse.FailedMessageWithParam("You Did Not Sell This Product To This Party. Please Choose The Correct Party For Sales Return.");
                }
                else
                {
                    var invoiceDetails = db.InvoiceDetails.Where(i => inoviceIds.Contains(i.InvoiceId) && i.Status == 1).ToList();
                    foreach (var invoiceDetial in partialInvoiceInfo.InvoiceDetailsList)
                    {
                        var serialCount = invoiceDetails.Where(i => i.ProductSerial == invoiceDetial.ProductSerial).ToList();
                        if (serialCount.Count < 1)
                            return AutoResponse.FailedMessageWithParam("The serial No: " + invoiceDetial.ProductSerial + " is not sold to this party.");
                    }
                }
            }
            return AutoResponse.SuccessMessage();
        }

        public DbResponse EditSalesInvoice(PartialInvoiceInfo partialInvoiceInfo, LogInInfo logInInfo)
        {
            //Validate Serial For Sales and Sales Replace
            if (partialInvoiceInfo.PropertyId == PropertyConstants.Sales || partialInvoiceInfo.PropertyId == PropertyConstants.SalesReplace)
            {
                using (var db = new ERPEntities())
                {
                    invoiceDetailsbyID = db.InvoiceDetails.Where(i => i.InvoiceId == partialInvoiceInfo.Id).ToList();

                }

                DbResponse res = new DbResponse();
                res = _validateSales.ValidateSerialForSalesEditCheckReturnedOrNot(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList, invoiceDetailsbyID);
                if (res.MessageType == 2)
                    return res;


                res = _validateSales.ValidateSerialForSalesEditCheckStockForNewEntry(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList, invoiceDetailsbyID);
                if (res.MessageType == 2)
                    return res;
            }

            //Validate Serial For Sales Return
            else if (partialInvoiceInfo.PropertyId == PropertyConstants.SalesReturn)
            {
                using (var db = new ERPEntities())
                {
                    invoiceDetailsbyID = db.InvoiceDetails.Where(i => i.InvoiceId == partialInvoiceInfo.Id).ToList();

                }

                DbResponse res = _validateSales.ValidateSerialEditingSalesReturnOldEntry(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList, invoiceDetailsbyID);
                if (res.MessageType == 2)
                    return res;

                DbResponse result = _validateSales.ValidateSerialEditingSalesReturnNewEntry(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList, invoiceDetailsbyID);
                if (result.MessageType == 2)
                    return result;

                PartySerialVerification(partialInvoiceInfo);
            }
            if (partialInvoiceInfo.PartyId == -1)
            {
                if (partialInvoiceInfo.PartyName == null || partialInvoiceInfo.PartyName.Trim() == "")
                    return AutoResponse.FailedMessageWithParam("You Must give a Name for new Customer.");
                var partyRes = _partyManager.AddParty(
                    new LedgerViewModel
                    {
                        Name = partialInvoiceInfo.PartyName,
                        PropertyId = PropertyConstants.Dealer,
                        Address1 = partialInvoiceInfo.PartyAddress
                    }, logInInfo);
                if (partyRes.MessageType == 2)
                    return partyRes;
                partialInvoiceInfo.PartyId = (long)partyRes.ReturnValue;
            }
            using (var dbEntities = new ERPEntities())
            {
                var invoiceInfo = new InvoiceInfo();
                Mapper.Map(partialInvoiceInfo, invoiceInfo);

                // Currency
                var currencyInfo = _currencyManager.GetCurrency(partialInvoiceInfo.CurrencyId);
                if (currencyInfo != null)
                {
                    invoiceInfo.Currency = currencyInfo.ID;
                    invoiceInfo.ConversionRate = currencyInfo.ConversionRate;
                }

                _invoiceInfoManager.UpdateInvoice(invoiceInfo, logInInfo);

                if (partialInvoiceInfo.InvoiceDetailsList == null) return AutoResponse.FailedMessageWithParam("No Product is Added to Update.");
                //Delete DetailList if removed or new added
                var dbDetailList = dbEntities.InvoiceDetails.Where(i => i.InvoiceId == partialInvoiceInfo.Id).ToList();
                if (partialInvoiceInfo.InvoiceDetailsList.Count != dbDetailList.Count)
                    return DeleteAndAddDetails(partialInvoiceInfo.InvoiceDetailsList, invoiceInfo, logInInfo);
                //Check two Lists Are Same
                var detailsId = partialInvoiceInfo.InvoiceDetailsList.Select(i => i.Id).ToList();
                var DBdetailsId = dbDetailList.Select(i => i.Id).ToList();
                if (DBdetailsId.Except(detailsId).Any())
                    return DeleteAndAddDetails(partialInvoiceInfo.InvoiceDetailsList, invoiceInfo, logInInfo);
                foreach (var partialInvoiceDetail in partialInvoiceInfo.InvoiceDetailsList)
                {
                    var invoiceDetail = partialInvoiceDetail.Id > 0 ?
                         _invoiceDetailManager.GetInvoiceDetailsById(partialInvoiceDetail.Id) :
                         new InvoiceDetail();

                    invoiceDetail.CreatedDate = invoiceInfo.CreatedDate;
                    invoiceDetail.ModifiedDate = invoiceInfo.ModifiedDate;
                    invoiceDetail.CreatedBy = invoiceInfo.CreatedBy;
                    invoiceDetail.ModifiedBy = invoiceInfo.ModifiedBy;
                    invoiceDetail.InvoiceId = invoiceInfo.Id;
                    invoiceDetail.ProductId = partialInvoiceDetail.ProductId;
                    invoiceDetail.Quantity = partialInvoiceDetail.Quantity;
                    invoiceDetail.BuyingPrice = partialInvoiceDetail.BuyingPrice;
                    invoiceDetail.SellingPrice = partialInvoiceDetail.SellingPrice;

                    invoiceDetail.TotalPrice = partialInvoiceDetail.Quantity * partialInvoiceDetail.SellingPrice;
                    invoiceDetail.ProductSerial = partialInvoiceDetail.ProductSerial;
                    invoiceDetail.Warranty = partialInvoiceDetail.Warranty;

                    // Currency
                    if (currencyInfo != null)
                    {
                        invoiceDetail.Currency = currencyInfo.ID;
                        invoiceDetail.ConversionRate = currencyInfo.ConversionRate;
                    }


                    if (partialInvoiceDetail.Id > 0)
                    {
                        // Update Invoice Details
                        _invoiceDetailManager.UpdateInvoiceDetails(invoiceDetail);
                    }
                    else
                    {
                        // Add Invoice Details
                        _invoiceDetailManager.AddInvoiceDetails(invoiceDetail);
                    }

                }
                //_detailsManager.ChangeStockFlag(partialInvoiceInfo.InvoiceDetailsList, partialInvoiceInfo.PropertyId);
            }

            return AutoResponse.SuccessMessage();
        }

        private DbResponse DeleteAndAddDetails(IList<InvoiceDetailViewModel> InvoiceDetailsList, InvoiceInfo invoiceInfo, LogInInfo logInInfo)
        {
            try
            {
                var dbDetailList = _detailsManager.GetInvoiceDetails(invoiceInfo.Id);
                //_detailsManager.ChangeStockFlag(dbDetailList, PropertyConstants.SalesReturn);
                _invoiceDetailManager.DeleteDetailByInvoiceId(invoiceInfo.Id);

                var currencyInfo = _currencyManager.GetCurrency((int)invoiceInfo.Currency);
                if (InvoiceDetailsList != null)
                {
                    List<InvoiceDetailViewModel> detailList = (List<InvoiceDetailViewModel>)InvoiceDetailsList;
                    detailList.ForEach(i =>
                    {
                        i.TotalPrice = i.BuyingPrice * i.Quantity;
                        i.PropertyId = invoiceInfo.PropertyId;
                        i.Currency = currencyInfo.ID;
                        i.ConversionRate = currencyInfo.ConversionRate;
                    });
                    _detailsManager.AddInvoiceDetailListToDB(detailList, invoiceInfo.Id, invoiceInfo.PropertyId, logInInfo);
                    //_detailsManager.ChangeStockFlag(detailList, invoiceInfo.PropertyId);
                }
                return AutoResponse.SuccessMessage();
            }
            catch (Exception ex)
            {
                return AutoResponse.FailedMessageWithParam(ex.Message);
            }

        }
        public int GetWarantyBySerial(string serialNo)
        {
            using (var dbEntities = new ERPEntities())
            {
                var detailsWithSerial = dbEntities.InvoiceDetails.Where(i => i.ProductSerial == serialNo).FirstOrDefault();
                if (detailsWithSerial != null)
                    return detailsWithSerial.Warranty;
                else
                    return -1;
            }

        }

        public DbResponse DeleteInvoice(long invoiceId, LogInInfo _logInInfo)
        {
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var invoiceInfo = dbEntities.InvoiceInfoes.SingleOrDefault(i => i.Id == invoiceId);
                    if (invoiceInfo != null)
                    {
                        invoiceInfo.Status = 0;
                        invoiceInfo.ModifiedBy = _logInInfo.UserId;
                        invoiceInfo.ModifiedDate = DateTime.Today;
                        dbEntities.Entry(invoiceInfo).State = System.Data.Entity.EntityState.Modified;
                    }
                    List<InvoiceDetail> invoiceDetailsList = dbEntities.InvoiceDetails.Where(id => id.Status == 1 && id.InvoiceId == invoiceId).ToList();
                    foreach (InvoiceDetail invoiceDetail in invoiceDetailsList)
                    {
                        invoiceDetail.Status = 0;
                        invoiceDetail.ModifiedBy = _logInInfo.UserId;
                        invoiceDetail.ModifiedDate = DateTime.Today;
                        dbEntities.Entry(invoiceDetail).State = System.Data.Entity.EntityState.Modified;
                    }
                    var detalViewList = Mapper.Map<List<InvoiceDetailViewModel>>(invoiceDetailsList);
                    //_detailsManager.ChangeStockFlag(detalViewList, PropertyConstants.SalesReturn);
                    dbEntities.SaveChanges();
                    return AutoResponse.SuccessMessageWithParam("Invoice Deleted");
                }
            }
            catch (Exception ex)
            {
                return AutoResponse.FailedMessageWithParam(ex.Message);
            }

        }
    }
}
;