﻿using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;


namespace ERP.REPOSITORIES.Repositories
{
    public class PurchaseInvoiceManager : GenericRepository<ERPEntities, InvoiceInfo>, IPurchaseInvoiceManager
    {
        private readonly IProductManager _productManager;
        private readonly IInvoiceInfoManager _invoiceInfoManager;
        private readonly IInvoiceDetailsManager _invoiceDetailManager;
        private readonly IInvoiceDetailsManager _detailsManager;
        private readonly ICurrencyManager _currencyManager;
        public List<InvoiceDetail> invoiceDetailsbyID;
        private PurchaseSerialValidation _validateSerial;

        public PurchaseInvoiceManager()
        {
            _productManager = new ProductManager();
            _invoiceInfoManager = new InvoiceInfoManager();
            _invoiceDetailManager = new InvoiceDetailsManager();
            _detailsManager = new InvoiceDetailsManager();
            _currencyManager = new CurrencyManager();
            _validateSerial = new PurchaseSerialValidation();
        }
        public List<PartialInvoiceInfo> GetPurchaseInvoiceInfoList(long invoiceType = 0, string invoiceNo = "", DateTime? fromDate = null, DateTime? toDate = null)
        {
            var startDate = fromDate ?? DateTime.Today.AddDays(-30);
            var endDate = toDate ?? DateTime.Today.AddDays(1);
            try
            {
                using (var db = new ERPEntities())
                {
                    var invoiceTypes = db.TableProperties.Where(i => i.PropertyName == "PurchaseInvoiceType").Select(i => i.Id);
                    var purchaseList = db.InvoiceInfoes.Where(invoiceInfo => invoiceInfo.Status == 1
                                                                             && invoiceInfo.InvoiceDate >= startDate &&
                                                                             invoiceInfo.InvoiceDate <= endDate && invoiceTypes.Contains(invoiceInfo.PropertyId))
                        .ToList();
                    if (!string.IsNullOrEmpty(invoiceNo))
                        purchaseList = purchaseList.Where(i => i.InvoiceNo == invoiceNo.Trim()).ToList();
                    else if (invoiceType > 0)
                        purchaseList = purchaseList.Where(i => i.PropertyId == invoiceType).ToList();
                    var result = Mapper.Map<List<PartialInvoiceInfo>>(purchaseList);
                    result.ForEach(i =>
                    {
                        i.PropertyName = db.TableProperties.FirstOrDefault(p => p.Id == i.PropertyId).ViewName;
                        i.CreatedByName = db.Users.FirstOrDefault(u => u.Id == i.CreatedBy).UserName;
                        i.PartyName = db.Ledgers.FirstOrDefault(pr => pr.Id == i.PartyId).Name;
                    });
                    return result;
                }

            }
            catch (Exception ex)
            {
                return new List<PartialInvoiceInfo>();
            }
        }
        public DbResponse AddPurchaseInvoice(PartialInvoiceInfo partialInvoiceInfo, LogInInfo logInInfo)
        {
            
            //Validate Serial For Purchase And Bonus
            if (partialInvoiceInfo.PropertyId == PropertyConstants.Purchase || partialInvoiceInfo.PropertyId == PropertyConstants.ProfitInvoice)
            {
                DbResponse res = _validateSerial.ValidateSerialForPurchase(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList);
                if (res.MessageType == 2)
                    return res;
            }

            //Validate Serial For Purchase Return And Damage
            else if (partialInvoiceInfo.PropertyId == PropertyConstants.PurchaseReturn || partialInvoiceInfo.PropertyId == PropertyConstants.LostInvoice)
            {
                DbResponse res = _validateSerial.ValidateSerialForPurchaseReturn(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList);
                if (res.MessageType == 2)
                    return res;
            }

            try
            {
                var invoiceInfo = Mapper.Map<InvoiceInfo>(partialInvoiceInfo);
                //===========
                invoiceInfo = _invoiceInfoManager.GetDebitCreditByInvoiceType(invoiceInfo); //Debit and Credit Header Id
                // TODO:
                invoiceInfo.ReferenceNo = invoiceInfo.ReferenceNo ?? "";
                invoiceInfo.InvoiceNo = _invoiceInfoManager.GetDateGeneratedInvoiceNo(invoiceInfo.PropertyId);

                // Currency
                var currencyInfo = _currencyManager.GetCurrency(partialInvoiceInfo.CurrencyId);
                if (currencyInfo != null)
                {
                    invoiceInfo.Currency = currencyInfo.ID;
                    invoiceInfo.ConversionRate = currencyInfo.ConversionRate;
                }

                // Save To Invoice
                invoiceInfo = AddInvoice(invoiceInfo, logInInfo);
                //====Save Detail to invoice=====
                if (partialInvoiceInfo.InvoiceDetailsList != null)
                {
                    List<InvoiceDetailViewModel> detailList = (List<InvoiceDetailViewModel>)partialInvoiceInfo.InvoiceDetailsList;
                    detailList.ForEach(i =>
                    {
                        i.PropertyId = invoiceInfo.PropertyId;
                        i.Currency = currencyInfo.ID;
                        i.ConversionRate = currencyInfo.ConversionRate;
                    });
                    _detailsManager.AddInvoiceDetailListToDB(detailList, invoiceInfo.Id, partialInvoiceInfo.PropertyId, logInInfo);

                    //_detailsManager.ChangeStockFlag(detailList, partialInvoiceInfo.PropertyId);
                }
            }
            catch (Exception e)
            {
                return AutoResponse.FailedMessageWithParam(e);
            }

            return AutoResponse.SuccessMessageWithParam("Operation comleted SuccessFully.");
        }

        // Save To Invoice Function
        public InvoiceInfo AddInvoice(InvoiceInfo invoiceInfo, LogInInfo logInInfo)
        {
            invoiceInfo.CreatedBy = logInInfo.UserId;
            invoiceInfo.CreatedDate = DateTime.Today;
            invoiceInfo.ModifiedBy = logInInfo.UserId;
            invoiceInfo.ModifiedDate = DateTime.Today;
            invoiceInfo.Status = 1;
            invoiceInfo.ProcessBy = logInInfo.UserId;
            invoiceInfo.ApprovedBy = logInInfo.UserId;

            InvoiceInfo result = default(InvoiceInfo);
            try
            {
                using (var db = new ERPEntities())
                {
                    result = db.InvoiceInfoes.Add(invoiceInfo);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }
                
        public DbResponse EditPurchaseInvoice(PartialInvoiceInfo partialInvoiceInfo, LogInInfo logInInfo)
        {
           //Validate Serial For Purchase And Bonus
            if (partialInvoiceInfo.PropertyId == PropertyConstants.Purchase || partialInvoiceInfo.PropertyId == PropertyConstants.ProfitInvoice)
            {
                using (var db = new ERPEntities())
                {
                    invoiceDetailsbyID = db.InvoiceDetails.Where(i => i.InvoiceId == partialInvoiceInfo.Id).ToList();
                    //ccc.InvoiceDetailsList
                }

                DbResponse res = _validateSerial.ValidateSerialForPurchaseEditCheckSold(partialInvoiceInfo.PropertyId, invoiceDetailsbyID);
                if (res.MessageType == 2)
                    return res;

                DbResponse result = _validateSerial.ValidateSerialForPurchaseEditCheckExist(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList, invoiceDetailsbyID);
                if (result.MessageType == 2)
                    return result;
            }

            //Validate Serial For Purchase Return And Damage
            else if (partialInvoiceInfo.PropertyId == PropertyConstants.PurchaseReturn || partialInvoiceInfo.PropertyId == PropertyConstants.LostInvoice)
            {
                using (var db = new ERPEntities())
                {
                    invoiceDetailsbyID = db.InvoiceDetails.Where(i => i.InvoiceId == partialInvoiceInfo.Id).ToList();
                }

                DbResponse res = _validateSerial.ValidateSerialForPurchaseReturnEdit(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList, invoiceDetailsbyID);
                if (res.MessageType == 2)
                    return res;

                //DbResponse result = _validateSerial.ValidateSerialForPurchaseReturn(partialInvoiceInfo.PropertyId, partialInvoiceInfo.InvoiceDetailsList);
                //if (result.MessageType == 2)
                //    return result;
            }

            using (var dbEntities = new ERPEntities())
            {
                var invoiceInfo = dbEntities.InvoiceInfoes.Find(partialInvoiceInfo.Id);
                Mapper.Map(partialInvoiceInfo, invoiceInfo);

                // Currency
                var currencyInfo = _currencyManager.GetCurrency(partialInvoiceInfo.CurrencyId);
                if (currencyInfo != null)
                {
                    invoiceInfo.Currency = currencyInfo.ID;
                    invoiceInfo.ConversionRate = currencyInfo.ConversionRate;
                }

                _invoiceInfoManager.UpdateInvoice(invoiceInfo,logInInfo);

                if (partialInvoiceInfo.InvoiceDetailsList == null) return AutoResponse.SuccessMessage();
                if (partialInvoiceInfo.InvoiceDetailsList == null) return AutoResponse.FailedMessageWithParam("No Products Added to update.");

                //Delete DetailList if removed or new added
                var dbDetailList = dbEntities.InvoiceDetails.Where(i => i.InvoiceId == partialInvoiceInfo.Id).ToList();
                if (partialInvoiceInfo.InvoiceDetailsList.Count != dbDetailList.Count)
                    return DeleteAndAddDetails(partialInvoiceInfo.InvoiceDetailsList, invoiceInfo, logInInfo);
                //Check two Lists Are Same
                var detailsId = partialInvoiceInfo.InvoiceDetailsList.Select(i => i.Id).ToList();
                var DBdetailsId = dbDetailList.Select(i => i.Id).ToList();
                if (DBdetailsId.Except(detailsId).Any())
                    return DeleteAndAddDetails(partialInvoiceInfo.InvoiceDetailsList, invoiceInfo, logInInfo);
                //=========Update Details======
                var detailList = Mapper.Map<List<InvoiceDetail>>(partialInvoiceInfo.InvoiceDetailsList);
                detailList.ForEach(i =>
                {
                    i.TotalPrice = i.BuyingPrice*i.Quantity; 
                    i.Currency = currencyInfo.ID;
                    i.ConversionRate = currencyInfo.ConversionRate;
                });
                foreach (var partialInvoiceDetail in partialInvoiceInfo.InvoiceDetailsList)
                {
                    var invoiceDetail = partialInvoiceDetail.Id > 0 ?
                         _invoiceDetailManager.GetInvoiceDetailsById(partialInvoiceDetail.Id) :
                         new InvoiceDetail();

                    invoiceDetail.CreatedDate = invoiceInfo.CreatedDate;
                    invoiceDetail.ModifiedDate = invoiceInfo.ModifiedDate;
                    invoiceDetail.CreatedBy = invoiceInfo.CreatedBy;
                    invoiceDetail.ModifiedBy = invoiceInfo.ModifiedBy;
                    invoiceDetail.InvoiceId = invoiceInfo.Id;
                    invoiceDetail.Quantity = partialInvoiceDetail.Quantity;

                    invoiceDetail.TotalPrice = partialInvoiceDetail.Quantity * partialInvoiceDetail.BuyingPrice;
                    invoiceDetail.ProductSerial = partialInvoiceDetail.ProductSerial;
                    invoiceDetail.Warranty = partialInvoiceDetail.Warranty;

                    // Currency
                    if (currencyInfo != null)
                    {
                        invoiceDetail.Currency = currencyInfo.ID;
                        invoiceDetail.ConversionRate = currencyInfo.ConversionRate;
                    }

                    if (partialInvoiceDetail.Id > 0)
                    {
                        // Update Invoice Details
                        _invoiceDetailManager.UpdateInvoiceDetails(invoiceDetail);
                    }
                    else
                    {
                        // Add Invoice Details
                        _invoiceDetailManager.AddInvoiceDetails(invoiceDetail);
                    }
                    
                }
              //  _detailsManager.ChangeStockFlag(partialInvoiceInfo.InvoiceDetailsList, partialInvoiceInfo.PropertyId);
            }

            return AutoResponse.SuccessMessage();
        }
        
        private DbResponse DeleteAndAddDetails(IList<InvoiceDetailViewModel> InvoiceDetailsList, InvoiceInfo invoiceInfo, LogInInfo logInInfo)
        {
            try
            {
                _invoiceDetailManager.DeleteDetailByInvoiceId(invoiceInfo.Id);
                var currencyInfo = _currencyManager.GetCurrency((int)invoiceInfo.Currency);
                if (InvoiceDetailsList != null)
                {
                    List<InvoiceDetailViewModel> detailList = (List<InvoiceDetailViewModel>)InvoiceDetailsList;
                    detailList.ForEach(i =>
                    {
                        i.TotalPrice = i.BuyingPrice * i.Quantity;
                        i.PropertyId = invoiceInfo.PropertyId;
                        i.Currency = currencyInfo.ID;
                        i.ConversionRate = currencyInfo.ConversionRate;
                    });
                    _detailsManager.AddInvoiceDetailListToDB(detailList, invoiceInfo.Id, invoiceInfo.PropertyId, logInInfo);
                    //_detailsManager.ChangeStockFlag(detailList, invoiceInfo.PropertyId);
                }
                return AutoResponse.SuccessMessage();
            }
            catch (Exception ex)
            {
                return AutoResponse.FailedMessageWithParam(ex.Message);
            }

        }
    }
}
