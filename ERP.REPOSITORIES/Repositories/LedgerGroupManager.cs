﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using AutoMapper;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Generics;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;



namespace ERP.REPOSITORIES.Repositories
{
    public class LedgerGroupManager : GenericRepository<ERPEntities, LedgerGroup>,
        ILedgerGroupManager
    {
        public List<LedgerGroupViewModel> GetLedgerGroups(int? transactionType = null)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    long[] nonEditableLedgerGroups = { 1, 2, 3, 4, 5, 6, 7, 8 };
                    var transactionHeaders =
                        db.LedgerGroups.Where(i => i.IsParent == false).Where(i => !nonEditableLedgerGroups.Contains(i.Id ) && i.Status == 1)
                            .Select(tr => new LedgerGroupViewModel
                            {
                                Id = tr.Id,
                                Name = tr.Name,
                                GroupName = db.GroupHeaders
                                    .Where(i => i.Id == tr.GroupHeaderId)
                                    .Select(i => i.Name).FirstOrDefault(),
                                Description = tr.Description
                            }).ToList();
                    return transactionHeaders;
                }
            }
            catch (Exception)
            {
                return new List<LedgerGroupViewModel>();
            }
        }
        public List<LedgerViewModel> GetLedgers(int? transactionType = null)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    long[] nonEditableLedgers = { 1, 2, 3, 4, 5,6,7,8,9,10,11,12 };
                    var transactionHeaders =
                        db.Ledgers.Where(i => !nonEditableLedgers.Contains(i.Id) &&(i.PropertyId!=PropertyConstants.Supplier &&i.PropertyId!=PropertyConstants.Dealer) && i.Status == 1).Select(tr => new LedgerViewModel
                        {
                            Id = tr.Id,
                            Name = tr.Name,
                            GroupName = db.LedgerGroups.FirstOrDefault(g => g.Id == tr.LedgerGroupId).Name,
                            Description = tr.Description
                        }).ToList();
                    return transactionHeaders;
                }
            }
            catch (Exception)
            {
                return new List<LedgerViewModel>();
            }
        }
        public List<LedgerViewModel> GetSortLedgersList()
        {
            try
            {

                using (var db = new ERPEntities())
                {

                    var cancelgroupId =
                        db.LedgerGroups.Where(i => i.PropertyId == PropertyConstants.Dealer || i.PropertyId == PropertyConstants.Supplier).Select(x => x.Id).ToList();
                    var transactionHeaders =
                        db.Ledgers.Where(i => !cancelgroupId.Contains(i.LedgerGroupId)).Select(tr => new LedgerViewModel
                        {
                            Id = tr.Id,
                            Name = tr.Name,
                            GroupName = db.LedgerGroups
                                .Where(i => i.Id == tr.LedgerGroupId)
                                .Select(i => i.Name).FirstOrDefault(),
                            Description = tr.Description
                        }).ToList();
                    return transactionHeaders;
                }
            }
            catch (Exception ex)
            {
                return new List<LedgerViewModel>();
            }
        }
        public DbResponse DeleteLedgerGroup(long id, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbLedger = db.LedgerGroups.FirstOrDefault(i => i.Id == id);
                    if (dbLedger == null)
                        return AutoResponse.NotFoundMessage();
                    var isAnyLedger = db.Ledgers.Where(i => i.Status == 1 && i.LedgerGroupId == id).ToList();
                    if (isAnyLedger.Count > 0)
                        return AutoResponse.FailedMessage("Cannot Delete Header...Some Ledger is under this Header.");
                    dbLedger.Status = 0;
                    dbLedger.ModifiedBy = logInInfo.UserId;
                    dbLedger.ModifiedDate = DateTime.Today;
                    db.Entry(dbLedger).State = EntityState.Modified;
                    db.SaveChanges();
                    return AutoResponse.SuccessMessageWithParam("Header Deleted.");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        public DbResponse EditLedgerGroup(LedgerGroupViewModel obj, LogInInfo logInInfo)
        {
            string errMsg = string.Empty;
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var editData = dbEntities.LedgerGroups.FirstOrDefault(i => i.Id == obj.Id);
                    if (editData != null)
                    {
                        editData.ModifiedDate = DateTime.Today;
                        editData.Description = obj.Description;
                        editData.GroupHeaderId = obj.GroupHeaderId;
                        editData.Name = obj.Name;
                        editData.ModifiedBy = logInInfo.UserId;
                        dbEntities.Entry(editData).State = EntityState.Modified;
                        dbEntities.SaveChanges();
                        return AutoResponse.SuccessMessage();
                    }
                }
            }
            catch (Exception e)
            {
                errMsg = e.ToString();
                return AutoResponse.FailedMessageWithParam(errMsg);
            }
            return AutoResponse.FailedMessage();
        }

        public DbResponse AddLedgerGroup(LedgerGroupViewModel headerModel, LogInInfo _logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    using (var dbEntitiesTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            if (headerModel.GroupHeaderId < 1)
                                return AutoResponse.FailedMessageWithParam("Select a Header type for this header.");
                            var dbTrHeader = Mapper.Map<DAL.Model.LedgerGroup>(headerModel);

                            if (headerModel.Id > 0)
                            {
                                dbTrHeader = db.LedgerGroups.FirstOrDefault(i => i.Id == headerModel.Id);
                                if (dbTrHeader == null)
                                    return AutoResponse.NotFoundWithParam("Header not found with this name.");
                                dbTrHeader.Description = headerModel.Description;
                                dbTrHeader.GroupHeaderId = headerModel.GroupHeaderId;
                                dbTrHeader.Name = headerModel.Name;
                                dbTrHeader.ModifiedDate = DateTime.Today;
                                dbTrHeader.ModifiedBy = _logInInfo.UserId;
                                dbTrHeader.IsParent = false;     //temporary
                                db.Entry(dbTrHeader).State = EntityState.Modified;
                                db.SaveChanges();
                                dbEntitiesTransaction.Commit();
                                return AutoResponse.SuccessMessageWithParam("Header Updated Successfully.");
                            }
                            else
                            {
                                dbTrHeader.CreatedDate = DateTime.Today;
                                dbTrHeader.ModifiedDate = DateTime.Today;
                                dbTrHeader.BussinessId = 0;
                                dbTrHeader.Status = 1;
                                dbTrHeader.CreatedBy = _logInInfo.UserId;
                                dbTrHeader.ModifiedBy = _logInInfo.UserId;
                                dbTrHeader.IsParent = false;     //temporary
                                db.LedgerGroups.Add(dbTrHeader);
                                db.SaveChanges();
                                dbEntitiesTransaction.Commit();
                                return AutoResponse.SuccessMessageWithParam("Header Created Successfully.");
                            }

                        }
                        catch (DbEntityValidationException e)
                        {
                            return AutoResponse.FailedMessageWithParam(e.ToString());
                        }
                        catch (Exception e)
                        {
                            dbEntitiesTransaction.Rollback();
                            return AutoResponse.FailedMessageWithParam(e);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage();
            }
        }

        public LedgerGroupViewModel GetLedgerGroup(long id)
        {
            try
            {
                using (var dbEntities = new ERPEntities())
                {
                    var dbData = dbEntities.LedgerGroups.Where(i => i.Id == id).Select(model => new LedgerGroupViewModel
                    {
                        Id = model.Id,
                        BussinessId = model.BussinessId,
                        CreatedBy = model.CreatedBy,
                        Description = model.Description,
                        GroupHeaderId = model.GroupHeaderId,
                        Name = model.Name,

                    }).FirstOrDefault();
                    return dbData;
                }

            }
            catch (Exception)
            {

                throw;
            }

        }




        public DbResponse AddLedger(LedgerViewModel ledgerViewModel, LogInInfo _logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    using (var dbEntitiesTransaction = db.Database.BeginTransaction())
                    {
                        try
                        {
                            {
                                if (ledgerViewModel.LedgerGroupId < 1)
                                    return AutoResponse.FailedMessageWithParam("Select a header for this ledger");
                                var dbTrHeader = Mapper.Map<DAL.Model.Ledger>(ledgerViewModel);

                                if (ledgerViewModel.Id > 0)
                                {
                                    dbTrHeader = db.Ledgers.FirstOrDefault(i => i.Id == ledgerViewModel.Id);
                                    if (dbTrHeader == null)
                                        return AutoResponse.NotFoundWithParam("Ledger not found with this name.");
                                    dbTrHeader.Description = ledgerViewModel.Description;
                                    dbTrHeader.LedgerGroupId = ledgerViewModel.LedgerGroupId;
                                    dbTrHeader.Name = ledgerViewModel.Name;
                                    dbTrHeader.ModifiedDate = DateTime.Today;
                                    dbTrHeader.ModifiedBy = _logInInfo.UserId;
                                    db.Entry(dbTrHeader).State = EntityState.Modified;
                                    db.SaveChanges();
                                    dbEntitiesTransaction.Commit();
                                    return AutoResponse.SuccessMessageWithParam("Ledger Updated Successfully.");
                                }
                                else
                                {
                                    dbTrHeader.CreatedDate = DateTime.Today;
                                    dbTrHeader.ModifiedDate = DateTime.Today;
                                    dbTrHeader.BussinessId = 0;
                                    dbTrHeader.Status = 1;
                                    dbTrHeader.CreatedBy = _logInInfo.UserId;
                                    dbTrHeader.ModifiedBy = _logInInfo.UserId;
                                    db.Ledgers.Add(dbTrHeader);
                                    db.SaveChanges();
                                    dbEntitiesTransaction.Commit();
                                    return AutoResponse.SuccessMessageWithParam("Ledger Created Successfully.");
                                }

                            }
                        }
                        catch (DbEntityValidationException e)
                        {
                            return AutoResponse.FailedMessageWithParam(e.ToString());
                        }
                        catch (Exception e)
                        {
                            dbEntitiesTransaction.Rollback();
                            return AutoResponse.FailedMessageWithParam(e);
                        }
                    }
                }
            }
            catch (Exception)
            {
                return AutoResponse.FailedMessage();
            }
        }

        public DbResponse EditLedger(LedgerViewModel transactionSubHeaderModel, LogInInfo logInInfo)
        {
            using (var dbEntities = new ERPEntities())
            {
                var data = dbEntities.Ledgers.FirstOrDefault(i => i.Id == transactionSubHeaderModel.Id);
                if (data != null)
                {
                    data.Name = transactionSubHeaderModel.Name;
                    data.Description = transactionSubHeaderModel.Description;
                    data.LedgerGroupId = transactionSubHeaderModel.LedgerGroupId;
                    data.ModifiedBy = logInInfo.UserId;
                    data.ModifiedDate = DateTime.Today;
                    dbEntities.Entry(data).State = EntityState.Modified;
                    dbEntities.SaveChanges();
                    return AutoResponse.SuccessMessage();
                }
            }
            return AutoResponse.FailedMessage();
        }
        public DbResponse DeleteLedger(long id, LogInInfo logInInfo)
        {
            try
            {
                using (var db = new ERPEntities())
                {
                    var dbLedger = db.Ledgers.FirstOrDefault(i => i.Id == id);
                    if (dbLedger == null)
                        return AutoResponse.NotFoundMessage();
                    if (GetAnyTransaction(id))
                        return AutoResponse.FailedMessage("Cannot Delete Ledger...Some transaction/payment is under this Ledger.");
                    dbLedger.Status = 0;
                    dbLedger.ModifiedBy = logInInfo.UserId;
                    dbLedger.ModifiedDate = DateTime.Today;
                    db.Entry(dbLedger).State = EntityState.Modified;
                    db.SaveChanges();
                    return AutoResponse.SuccessMessageWithParam("Ledger Deleted.");
                }
            }
            catch (Exception exception)
            {
                return AutoResponse.FailedMessageWithParam(exception);
            }
        }

        private bool GetAnyTransaction(long id)
        {
            using (var db = new ERPEntities())
            {
                var isAnyTransaction = db.Transactions.Where(i => i.DebitHeaderId == id || i.CreditHeaderId == id).ToList();
                var isAnyPayment = db.Payments.Where(i => i.DebitId == id || i.CreditId == id).ToList();
                return isAnyTransaction.Count > 0 || isAnyPayment.Count > 0;
            }
        }

        public LedgerViewModel GetLedger(long id)
        {
            using (var dbEntities = new ERPEntities())
            {
                var data = dbEntities.Ledgers.Where(i => i.Id == id).Select(model => new LedgerViewModel
                {
                    Id = model.Id,
                    Description = model.Description,
                    LedgerGroupId = model.LedgerGroupId,
                    Name = model.Name,
                }).FirstOrDefault();
                return data;
            }
        }
    }
}
