﻿using System.Collections;
using System.Runtime.Remoting.Messaging;
using ERP.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.Repositories;

namespace ERP.REPOSITORIES.ReportRepositories
{
    class TrialBalanceService
    {
        public DateTime fromDate = DateTime.Today;
        public DateTime toDate = DateTime.Today;
        internal System.Collections.IEnumerable GetTrialBalanceReport(DateTime _fromDate, DateTime _toDate)
        {
            fromDate = _fromDate;
            toDate = _toDate;
            using (var dbEntiries = new ERPEntities())
            {
                var accountsManager = new AccountsLedgerService();
                var ledgers = dbEntiries.Ledgers.Where(i => i.Status == 1 && i.Visible != PropertyConstants.LedgerVisible).ToList();
                var result = new List<TrialBalanceRE>();
                foreach (var ledger in ledgers)
                {
                    var accledgers = ledger.Id == LedgerConstants.CurrentStockLedgerId ? 
                        accountsManager.GetAccountsLedgerById(ledger.Id, new DateTime(), _fromDate.AddDays(-1)) 
                        : accountsManager.GetAccountsLedgerById(ledger.Id, fromDate, toDate);
                    if (accledgers.Count < 1)
                        continue;
                    //var sumLedger = accledgers[accledgers.Count - 1];
                    var groupHeader = dbEntiries.GroupHeaders.FirstOrDefault(gh => gh.Id == (dbEntiries.LedgerGroups.FirstOrDefault(lg => lg.Id == ledger.LedgerGroupId).GroupHeaderId));
                    result.Add(new TrialBalanceRE
                    {
                        HeaderId = ledger.LedgerGroupId,
                        Header = dbEntiries.LedgerGroups.FirstOrDefault(lg => lg.Id == ledger.LedgerGroupId).Name,
                        Debit = groupHeader.PropertyId == PropertyConstants.Debit ? accledgers.Sum(acc=>acc.Debit) : 0,
                        Credit = groupHeader.PropertyId == PropertyConstants.Credit ? accledgers.Sum(acc => acc.Credit) : 0,
                    });
                }
                //===========Profit Loss=========
                var incomeStatementService = new IncomeStatementService();
                var previousProfitLoss = incomeStatementService.GetIncomeStatement(new DateTime(), _fromDate.AddDays(-1));
                result.Add(new TrialBalanceRE
                {
                    HeaderId = 6,        //Profit Loss account
                    Header = "Profit and Loss A/C",
                    Debit = 0,
                    Credit = previousProfitLoss.Sum(i => i.Credit) - previousProfitLoss.Sum(i => i.Debit)
                });
                result = result.GroupBy(i => i.Header).Select(i => new TrialBalanceRE
                {
                    Header = i.Key,
                    Debit = i.Sum(j => j.Debit),
                    Credit = i.Sum(j => j.Credit)
                }).ToList();
                return result;
            }
        }


        private AccountsLedgerRE GetCurrentStock(ERPEntities dbEntiries, DateTime endDate)
        {
            var startDate = new DateTime();
            var partyInvoice =
                dbEntiries.InvoiceInfoes.Where(
                    i => i.Status == 1 && i.InvoiceDate >= startDate && i.InvoiceDate <= endDate)
                    .Select(i => new AccountsLedgerRE
                    {
                        headerId = 5,
                        Debit = (i.PropertyId == PropertyConstants.Purchase || i.PropertyId == PropertyConstants.SalesReturn || i.PropertyId == PropertyConstants.ProfitInvoice) ? i.GrandTotal : 0,
                        Credit = (i.PropertyId == PropertyConstants.Sales || i.PropertyId == PropertyConstants.PurchaseReturn || i.PropertyId == PropertyConstants.LostInvoice) ? i.GrandTotal : 0,
                    }).ToList();
            //partyInvoice= partyInvoice.GroupBy(i => i.headerId).Select(i => new AccountsLedgerRE
            //{
            //    Particular = "Current Stock",
            //    headerId = 5,
            //    Balance = i.Sum(j => j.Debit) - i.Sum(j => j.Credit),
            //    Credit = 0
            //}).ToList();
            return new AccountsLedgerRE
            {
                Particular = "Current Stock",
                headerId = 5,
                Balance = partyInvoice.Sum(j => j.Debit) - partyInvoice.Sum(j => j.Credit),
                Credit = 0
            };
        }

    }

    class TrialBalanceRE
    {
        public long HeaderId { get; set; }
        public string Header { get; set; }
        public decimal? Debit { get; set; }
        public decimal? Credit { get; set; }
        public decimal Balance { get; set; }
    }
}
