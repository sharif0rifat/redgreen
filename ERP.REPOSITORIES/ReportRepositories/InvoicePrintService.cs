﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.ReportEntity;
using ERP.REPOSITORIES.Helper;

namespace ERP.REPOSITORIES.ReportRepositories
{
    class InvoicePrintService
    {

        internal System.Collections.IEnumerable GetInvoiceDetail(long invoiceId)
        {
            using (var dbEntities = new ERPEntities())
            {
                var dbData =
                    dbEntities.InvoiceDetails.Where(i => i.InvoiceId == invoiceId).Select(j => new InvoicePrintDetail
                    {
                        Id = j.Id,
                        Gift = j.PropertyId,
                        Quantity = j.PropertyId == PropertyConstants.Gift ? 0 : j.Quantity,
                        ItemId = j.ProductId,
                        ItemName = dbEntities.Products.FirstOrDefault(i => i.Id == j.ProductId).ProductName,
                        UnitPrice = j.SellingPrice,
                        TotalPrice = j.TotalPrice,
                        WarrantyMonth = j.Warranty,
                        ProductSerial = j.ProductSerial+(j.PropertyId==PropertyConstants.Gift?"(gift)":"")
                    }).ToList();
                var detailList = dbData.GroupBy(i => i.ItemId).Select(i => new InvoicePrintDetail
                {
                    ItemId = i.Key,
                    ItemName = dbEntities.Products.FirstOrDefault(p => p.Id == i.Key).ProductName,
                    Quantity = i.Sum(j => j.Quantity),
                    TotalPrice = i.Sum(j => j.TotalPrice),
                    UnitPrice = i.First().UnitPrice,
                    ProductSerial = string.Join(","+Environment.NewLine,i.Select(j=>j.ProductSerial)),
                    Warranty = GetYearAndMonth(i.First().WarrantyMonth)
                }).ToList();
                return detailList;
            }
        }
        private string GetYearAndMonth(int warrantyMonths)
        {

            var years = (warrantyMonths / 12);
            var months = warrantyMonths - (years * 12);
            var warantyText = "";
            warantyText = years > 0 ? (years.ToString() + " Year") : "";
            warantyText += months > 0 ? (months.ToString() + " Month") : "";
            return warantyText;
        }
    }
}
