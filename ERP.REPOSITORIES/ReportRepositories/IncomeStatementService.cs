﻿using ERP.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.Repositories;

namespace ERP.REPOSITORIES.ReportRepositories
{
    public class IncomeStatementService
    {
        readonly ERPEntities _dbEntiries;
        public IncomeStatementService()
        {
            _dbEntiries = new ERPEntities();
        }
        internal IEnumerable<AccountsLedgerRE> GetIncomeStatement(DateTime startDate, DateTime endDate)
        {
            var revenue = GetInvoiceAmountByType(PropertyConstants.Sales, startDate, endDate) -
                          GetInvoiceAmountByType(PropertyConstants.SalesReturn, startDate, endDate);
            var expense = GetInvoiceCost(startDate, endDate);
            var incomeStatement = new List<AccountsLedgerRE>
            {
                new AccountsLedgerRE
                {
                    Header = "Revenue & Gains",
                    Particular = "Sales Revenue",
                    Credit = revenue,
                    Balance = revenue
                },
                new AccountsLedgerRE
                {
                    Header = "Expense & Losses",
                    Particular = "Cost Of Goods Sold",
                    Debit = expense,
                    Balance = expense
                }
            };
            var incomeLedgers = GetLegders(PropertyConstants.PositiveEquity, startDate, endDate);
            var expenseLedgers = GetLegders(PropertyConstants.NegativeEquity, startDate, endDate);
            return incomeStatement.Union(incomeLedgers).Union(expenseLedgers);
        }

        private decimal GetInvoiceCost(DateTime startDate, DateTime endDate)
        {
            var salesIds = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == PropertyConstants.Sales
                                                                && i.InvoiceDate >= startDate &&
                                                                i.InvoiceDate <= endDate).Select(i => i.Id);
            var salesReturnIds = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == PropertyConstants.SalesReturn
                                                                && i.InvoiceDate >= startDate &&
                                                                i.InvoiceDate <= endDate).Select(i => i.Id);
            var salesCost = _dbEntiries.InvoiceDetails.Where(i => i.Status == 1 && salesIds.Contains(i.InvoiceId)).Select(i => i.Quantity * i.BuyingPrice).DefaultIfEmpty(0).Sum();
            var salesReturnCost = _dbEntiries.InvoiceDetails.Where(i => i.Status == 1 && salesReturnIds.Contains(i.InvoiceId)).Select(i => i.Quantity * i.BuyingPrice).DefaultIfEmpty(0).Sum();
            return salesCost - salesReturnCost;
        }

        private decimal GetInvoiceAmountByType(long invoiceTypeType, DateTime startDate,
            DateTime endDate)
        {
            var invoiceAmount = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == invoiceTypeType
                                                                && i.InvoiceDate >= startDate &&
                                                                i.InvoiceDate <= endDate);
            var z = invoiceAmount.Select(i => i.SubTotal).DefaultIfEmpty(0).Sum();
            return z;
        }


        private IEnumerable<AccountsLedgerRE> GetLegders(long equityType, DateTime fromDate, DateTime toDate)
        {
            var headers = _dbEntiries.GroupHeaders.Where(i => i.AccountsType == equityType).Select(i => i.Id).ToList();
            var groups = _dbEntiries.LedgerGroups.Where(i => i.Status == 1 && headers.Contains(i.GroupHeaderId)).Select(i => i.Id).ToList();
            var ledgers = _dbEntiries.Ledgers.Where(i => i.Status == 1 && groups.Contains(i.LedgerGroupId) && (i.Id != LedgerConstants.PurchaseLedgerId && i.Id != LedgerConstants.SalesLedgerId)).ToList();
            var service = new AccountsLedgerService();
            ledgers.ForEach(i=>
            {
            var  t = service.GetAccountsLedgerById(i.Id, fromDate, toDate).ToList();
            }
        );
            
            var x = (from ledger in ledgers
                let transaction = service.GetAccountsLedgerById(ledger.Id, fromDate, toDate).ToList()
                select new AccountsLedgerRE
                {
                    Header = equityType == PropertyConstants.PositiveEquity ? "Revenue & Gains" : "Expense & Losses",
                    Particular = ledger.Name,
                    Debit = transaction.Sum(tr => tr.Debit),
                    Credit = transaction.Sum(tr => tr.Credit),
                    Balance =
                        equityType == PropertyConstants.PositiveEquity
                            ? transaction.Sum(tr => tr.Credit)
                            : transaction.Sum(tr => tr.Debit),
                }
                into transactionSum
                where transactionSum.Debit > 0 || transactionSum.Credit > 0
                select transactionSum).ToList();
            return (from ledger in ledgers
                    let transaction = service.GetAccountsLedgerById(ledger.Id, fromDate, toDate).ToList()
                    select new AccountsLedgerRE
                    {
                        Header = equityType == PropertyConstants.PositiveEquity ? "Revenue & Gains" : "Expense & Losses",
                        Particular = ledger.Name,
                        Debit = transaction.Sum(tr => tr.Debit),
                        Credit = transaction.Sum(tr => tr.Credit),
                        Balance =
                        equityType == PropertyConstants.PositiveEquity
                            ? transaction.Sum(tr => tr.Credit)
                            : transaction.Sum(tr => tr.Debit),
                    }
                        into transactionSum
                        where transactionSum.Debit > 0 || transactionSum.Credit > 0
                        select transactionSum).ToList();
        }
    }

    public class IncomeStatementRE
    {
        public int Serial { get; set; }
        public string Party { get; set; }
        public DateTime? Date { get; set; }
        public string Particular { get; set; }
        public string BilNo { get; set; }
        public Decimal Debit { get; set; }
        public Decimal Credit { get; set; }
        public Decimal Balance { get; set; }
    }
}
