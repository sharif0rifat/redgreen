﻿using System;
using System.Collections.Generic;
using System.Linq;
using ERP.DAL.Model;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.Repositories;

namespace ERP.REPOSITORIES.ReportRepositories
{
    public class AccountsLedgerService
    {
        private readonly ERPEntities _dbEntiries;
        public AccountsLedgerService()
        {
            _dbEntiries = new ERPEntities();
        }
        private GroupHeader GetLedgerType(Ledger ledger)
        {
            var ledgerGroupId = _dbEntiries.LedgerGroups.FirstOrDefault(i => i.Id == ledger.LedgerGroupId).GroupHeaderId;
            var groupHeader = _dbEntiries.GroupHeaders.FirstOrDefault(i => i.Id == ledgerGroupId);
            return groupHeader;
        }
        public List<AccountsLedgerRE> GetAccountsLedgerById(long ledgerId, DateTime startDate, DateTime endDate)
        {
            endDate.AddHours(23);
            endDate.AddMinutes(59);
            var ledger = _dbEntiries.Ledgers.FirstOrDefault(i => i.Id == ledgerId);
            var ledgerType = GetLedgerType(ledger);

            

            List<AccountsLedgerRE> ledgerByDate;
            var invoiceLedger = GetInvoiceTotalLedger(ledger, startDate, endDate);
            var transactionLedger = GetTransactionLedger(ledger, startDate, endDate);
            var paymentLedger = GetPaymentLedger(ledger, startDate, endDate);
            var stockLedger = GetStockLedger(ledger, startDate, endDate);
            ledgerByDate = invoiceLedger
                .Union(transactionLedger).Union(paymentLedger).Union(stockLedger)
                .ToList();
            //=========Opening======
            if (ledgerType.PropertyId == PropertyConstants.Asset || ledgerType.PropertyId == PropertyConstants.Liability)
            {
                var invoiceOpeningLedger = GetInvoiceTotalLedger(ledger, new DateTime(), startDate.AddDays(-1));
                var transactionOpeningLedger = GetTransactionLedger(ledger, new DateTime(), startDate.AddDays(-1));
                var paymentOpeningLedger = GetPaymentLedger(ledger, new DateTime(), startDate.AddDays(-1));
                var stockOpeningLedger = GetStockLedger(ledger, new DateTime(), startDate.AddDays(-1));
                ledgerByDate = invoiceLedger.Union(transactionLedger).Union(paymentLedger).Union(stockLedger)
                    .Union(invoiceOpeningLedger).Union(transactionOpeningLedger).Union(paymentOpeningLedger).Union(stockOpeningLedger).ToList();
            }
            decimal temp = 0;
            if (ledgerByDate.Count > 0)
                temp = ledgerByDate[0].Balance;
            ledgerByDate.ForEach(i =>
            {
                i.Balance = temp + (ledgerType.PropertyId == PropertyConstants.Credit
                    ? (i.Credit - i.Debit)
                    : (i.Debit - i.Credit));
                temp = i.Balance;
            });
            
            return ledgerByDate;
        }

        private IEnumerable<AccountsLedgerRE> GetStockLedger(Ledger ledger, DateTime startDate, DateTime endDate)
        {
            if(ledger.Id!=3)
                return new List<AccountsLedgerRE>();
            var ledgerData = _dbEntiries.InvoiceInfoes.Where(
                    i => i.Status == 1 && i.InvoiceDate >= startDate && i.InvoiceDate <= endDate )
                    .Select(i => new { i.Id,i.DebitId,i.CreditId,i.InvoiceDate,i.PropertyId}).Join(_dbEntiries.InvoiceDetails,inv=>inv.Id,invd=>invd.InvoiceId,(inv,invd)=>new {inv,invd})
                    .Where(invAndD=>invAndD.invd.Status==1)
                    .Select(ivd => new AccountsLedgerRE
                    {
                        Particular = _dbEntiries.TableProperties.FirstOrDefault(p => p.Id == ivd.invd.PropertyId).ViewName ,
                        Date = ivd.inv.InvoiceDate ?? DateTime.Today,
                        DebitId = 0,
                        CreditId = 0,
                        Debit = (ivd.inv.PropertyId == PropertyConstants.Purchase || ivd.inv.PropertyId == PropertyConstants.SalesReturn) ? (ivd.invd.Quantity * ivd.invd.BuyingPrice) : 0,
                        Credit = (ivd.inv.PropertyId == PropertyConstants.Sales
                                    || ivd.inv.PropertyId == PropertyConstants.Gift
                                    || ivd.inv.PropertyId == PropertyConstants.SalesReplace
                                    || ivd.inv.PropertyId == PropertyConstants.LostInvoice) ? (ivd.invd.Quantity * ivd.invd.BuyingPrice) : 0,
                    }).ToList();
            return ledgerData;
        }

        private IEnumerable<AccountsLedgerRE> GetPaymentLedger(Ledger ledger, DateTime startDate, DateTime endDate)
          {
            var ledgerData = _dbEntiries.Payments.Where(
                   i => i.Status == 1 && i.Date >= startDate && i.Date <= endDate 
                       && i.PaymentStatus==PropertyConstants.Paid
                       && (i.DebitId == ledger.Id || i.CreditId == ledger.Id))
                   .Select(i => new AccountsLedgerRE
                   {
                       Particular = (ledger.PropertyId == PropertyConstants.Dealer ? (i.CheckNo == ""||i.CheckNo==null ? "Received by cash" : "Received by check ") : "")
                       +i.Remarks,
                       Date = i.Date,
                       DebitId = i.DebitId,
                       CreditId = i.CreditId,
                       Debit = i.DebitId == ledger.Id ? i.Amount : 0,
                       Credit = i.CreditId == ledger.Id ? i.Amount : 0
                   }).ToList();
            return ledgerData;
        }

        private IEnumerable<AccountsLedgerRE> GetTransactionLedger(Ledger ledger, DateTime startDate, DateTime endDate)
        {
            var ledgerData = _dbEntiries.Transactions.Where(
                    i => i.Status == 1 && i.TransactionDate >= startDate && i.TransactionDate <= endDate && (i.DebitHeaderId == ledger.Id || i.CreditHeaderId == ledger.Id))
                    .Select(i => new AccountsLedgerRE
                    {
                        Particular = i.Remarks,
                        Date = i.TransactionDate,
                        DebitId = i.DebitHeaderId,
                        CreditId = i.CreditHeaderId,
                        Debit = i.DebitHeaderId == ledger.Id ? (i.Amount ?? 0) : 0,
                        Credit = i.CreditHeaderId == ledger.Id ? (i.Amount ?? 0) : 0
                    }).ToList();
            return ledgerData;
        }
        
        #region ===========Party Invoice===========
        private IEnumerable<AccountsLedgerRE> GetInvoiceTotalLedger(Ledger ledger, DateTime startDate, DateTime endDate)
        {
            var invoiceLedger = GetInvoiceLedger(ledger, startDate, endDate);
            var invoiceOthersLedger = GetInvoiceOtherLedger(ledger, startDate, endDate);
            invoiceOthersLedger.ForEach(i =>
            {
                if (i.Balance > 0 || i.Debit > 0 || i.Credit > 0)
                    invoiceLedger.Add(i);
            });
            return invoiceLedger;
        }
        private List<AccountsLedgerRE> GetInvoiceLedger(Ledger ledger, DateTime startDate, DateTime endDate)
        {
            var ledgerData = _dbEntiries.InvoiceInfoes.Where(
                    i => i.Status == 1 && i.InvoiceDate>=startDate && i.InvoiceDate<=endDate && (i.DebitId == ledger.Id || i.CreditId == ledger.Id))
                    .Select(i => new AccountsLedgerRE
                    {
                        Particular = _dbEntiries.TableProperties.FirstOrDefault(p => p.Id == i.PropertyId).ViewName + " " + i.Remarks,
                        InvoiceNo=i.InvoiceNo,
                        ReferenceNo = i.ReferenceNo,
                        Date = i.InvoiceDate ?? DateTime.Today,
                        DebitId = i.DebitId,
                        CreditId = i.CreditId,
                        Debit = i.DebitId == ledger.Id ? i.SubTotal : 0,
                        Credit = i.CreditId == ledger.Id ? i.SubTotal : 0
                    }).ToList();
            return ledgerData;
        }
        private List<AccountsLedgerRE> GetInvoiceOtherLedger(Ledger ledger, DateTime startDate, DateTime endDate)
        {
            var ledgerData = _dbEntiries.InvoiceInfoes.Where(
                    i => i.Status == 1 && i.InvoiceDate >= startDate && i.InvoiceDate <= endDate && (i.DebitId == ledger.Id || i.CreditId == ledger.Id))
                    .Select(i => new AccountsLedgerRE
                    {
                        Particular = "Vat+Service-Discount" + " on " + _dbEntiries.TableProperties.FirstOrDefault(p => p.Id == i.PropertyId).ViewName + " " + i.Remarks,
                        Date = i.InvoiceDate ?? DateTime.Today,
                        Balance = (i.Vat ?? 0) + (i.ServiceCharge ?? 0) - (i.Discount ?? 0),
                        DebitId = i.DebitId,
                        CreditId = i.CreditId,
                        Debit = i.DebitId == ledger.Id ? (i.Vat ?? 0) + (i.ServiceCharge ?? 0) - (i.Discount ?? 0) : 0,
                        Credit = i.CreditId == ledger.Id ? (i.Vat ?? 0) + (i.ServiceCharge ?? 0) - (i.Discount ?? 0) : 0
                    }).ToList();
            return ledgerData;
        }
        #endregion

    }
}
