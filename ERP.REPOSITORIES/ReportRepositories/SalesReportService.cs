﻿using System.Collections;
using System.Linq;
using ERP.DAL.Model;
using System;
using System.Collections.Generic;
using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.Repositories;

namespace ERP.REPOSITORIES.ReportRepositories
{
    public class SalesReportService
    {
        private readonly ERPEntities _entities;
        public SalesReportService()
        {
            _entities = new ERPEntities();
        }
        internal IEnumerable GetSalesReport(long categoryId, long productId, long partyId, long invoiceType, DateTime startDate, DateTime endDate)
        {
            var propertyManager = new PropertyManager();
            try
            {
                //=========Get Invoice by date=========
                var propertyId = invoiceType == PropertyConstants.Sales
                    ? propertyManager.GetPropertyByNameAndValue("SalesInvoiceType", 1).Id : propertyManager.GetPropertyByNameAndValue("PurchaseInvoiceType", 1).Id;
                var dateinvoices =
                   _entities.InvoiceInfoes.Where(
                       iv => iv.InvoiceDate >= startDate && iv.InvoiceDate <= endDate && iv.Status == 1 && iv.PropertyId == propertyId
                      ).ToList();
                var invoiceIds =
                    dateinvoices.Select(iv => iv.Id)
                        .ToList();

                //Get Invoice Details vy Invoice
                var invoiceDetail = _entities.InvoiceDetails.Where(
                    i => invoiceIds.Contains(i.InvoiceId)).ToList();

                //======filter by conditions========
                if (productId > 0)
                {
                    invoiceDetail = invoiceDetail.Where(i => i.Status == 1 && i.ProductId == productId).ToList();
                }
                if (categoryId > 0)
                {
                    var catProductIds = _entities.Products.Where(p => p.CategoryId == categoryId && p.Status == 1)
                        .Select(p => p.Id).ToList();
                    invoiceDetail = invoiceDetail.Where(
                        i => i.Status == 1 && catProductIds.Contains(i.ProductId)).ToList();
                }
                if (partyId > 0)
                {
                    var partyInvoiceIds = _entities.InvoiceInfoes.Where(iv => iv.PartyId == partyId && iv.Status == 1)
                        .Select(iv => iv.Id).ToList();
                    invoiceDetail =
                        invoiceDetail.Where(
                            i => i.Status == 1 && partyInvoiceIds.Contains(i.InvoiceId)).ToList();
                }

                //prepare object for view
                var reportData = invoiceDetail.Select(i => new SalesReportEntity
                {
                    Date = dateinvoices.FirstOrDefault(iv => iv.Id == i.InvoiceId).InvoiceDate ?? DateTime.Now,
                    InvoiceNo = dateinvoices.FirstOrDefault(iv => iv.Id == i.InvoiceId).InvoiceNo,
                    BillAmount = dateinvoices.FirstOrDefault(iv => iv.Id == i.InvoiceId).GrandTotal,
                    PartyName = dateinvoices.FirstOrDefault(iv => iv.Id == i.InvoiceId).PartyName,
                    ProductName = _entities.Products.FirstOrDefault(iv => iv.Id == i.ProductId).ProductName,
                    SellingPrice = i.SellingPrice,
                    Quantity = i.Quantity,
                }).ToList();
                var result = reportData.GroupBy(i => new
                {
                    i.Date,
                    i.InvoiceNo,
                    i.PartyName,
                    i.BillAmount,
                    i.ProductName,
                    i.SellingPrice,
                }).Select(r => new SalesReportEntity
                {
                    Date = r.Key.Date,
                    PartyName = r.Key.PartyName,
                    InvoiceNo = r.Key.InvoiceNo,
                    BillAmount = r.Key.BillAmount,
                    ProductName = r.Key.ProductName,
                    SellingPrice = r.Key.SellingPrice,
                    Quantity = r.Sum(id => id.Quantity)
                }).ToList();
               result[0].TotalBillAmount= dateinvoices.Select(r => r.GrandTotal).Sum();
                return result;
                //return reportData;
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        internal IEnumerable GetInvoiceReport(long categoryId, long productId, long partyId, DateTime startDate, DateTime endDate)
        {
            try
            {
                //=========Get Invoice by date=========
                var dateinvoiceIds =
                   _entities.InvoiceInfoes.Where(
                       iv => iv.InvoiceDate >= startDate && iv.InvoiceDate <= endDate && iv.Status == 1
                      ).Select(iv => iv.Id)
                       .ToList();

                //Get Invoice Details vy Invoice
                var invoiceDetail = _entities.InvoiceDetails.Where(
                    i => dateinvoiceIds.Contains(i.InvoiceId)).ToList();

                //======filter by conditions========
                if (productId > 0)
                {
                    invoiceDetail = invoiceDetail.Where(i => i.Status == 1 && i.ProductId == productId).ToList();
                }
                if (categoryId > 0)
                {
                    var catProductIds = _entities.Products.Where(p => p.CategoryId == categoryId && p.Status == 1)
                        .Select(p => p.Id).ToList();
                    invoiceDetail = invoiceDetail.Where(
                        i => i.Status == 1 && catProductIds.Contains(i.ProductId)).ToList();
                }
                if (partyId > 0)
                {
                    var partyInvoiceIds = _entities.InvoiceInfoes.Where(iv => iv.PartyId == partyId && iv.Status == 1)
                        .Select(iv => iv.Id).ToList();
                    invoiceDetail =
                        invoiceDetail.Where(
                            i => i.Status == 1 && partyInvoiceIds.Contains(i.InvoiceId)).ToList();
                }

                IList<dynamic> reportData = new List<dynamic>();
                foreach (var invoice in invoiceDetail)
                {
                    var productName = _entities.Products.FirstOrDefault(p => p.Id == invoice.ProductId).ProductName;
                    var date = _entities.InvoiceInfoes.FirstOrDefault(iv => iv.Id == invoice.InvoiceId).InvoiceDate;
                    var partyName = _entities.Ledgers.FirstOrDefault(
                        p => p.Id == _entities.InvoiceInfoes.FirstOrDefault(iv => iv.Id == invoice.InvoiceId).PartyId)
                        .Name;
                    decimal purchaseQty = 0;
                    decimal salesQty = 0;

                    if (invoice.PropertyId == PropertyConstants.Purchase)
                    {
                        purchaseQty = invoice.Quantity;
                    }
                    else if (invoice.PropertyId == PropertyConstants.Sales)
                    {
                        salesQty = invoice.Quantity;
                    }
                    reportData.Add(
                            new
                            {
                                ProductName = productName,
                                Date = date,
                                PartyName = partyName,
                                PurchaseQty = purchaseQty,
                                SalesQty = salesQty,
                                Balance = purchaseQty-salesQty
                            }
                        );
                }

                return reportData;
            }
            catch (Exception ex)
            {

                throw;
            }

        }
    }

    public class SalesReportEntity
    {
        public DateTime Date { get; set; }
        public string PartyName { get; set; }
        public long PartyId { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public decimal SellingPrice { get; set; }
        public decimal BillAmount { get; set; }
        public decimal TotalBillAmount { get; set; }
        public string InvoiceNo { get; set; }

    }
}
