﻿using System.Data.Entity.Core.Common.CommandTrees;
using ERP.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using ERP.REPOSITORIES.Helper;

namespace ERP.REPOSITORIES.ReportRepositories
{
    public class PartyLedgerService
    {
        readonly ERPEntities _dbEntiries;
        public PartyLedgerService(ERPEntities dbEntities)
        {
            if (dbEntities == null)
                _dbEntiries = new ERPEntities();
            else
                _dbEntiries = dbEntities;
        }

        public List<PartyLedgerRE> GetPartyLedgerByParty(long partyId, DateTime startDate, DateTime endDate)
        {
            DateTime newdate = endDate.AddHours(23);
            DateTime newenddate = newdate.AddMinutes(59);
            var party = _dbEntiries.Ledgers.FirstOrDefault(i => i.Id == partyId);
            var ledgerByDate = GetPartyLedgerData(partyId, startDate, newenddate);
            ledgerByDate.Insert(0, GetPartyLedgerOpeningData(party, startDate));
            ledgerByDate = ledgerByDate.OrderBy(i => i.Date).ToList();
            decimal temp = ledgerByDate[0].Balance;
            ledgerByDate.ForEach(i =>
            {
                
                i.Balance = temp+(party.PropertyId == PropertyConstants.Supplier
                    ? (i.Credit - i.Debit)
                    : (i.Debit - i.Credit));
                temp = i.Balance;
            });
            return ledgerByDate;
        }

        public List<PartyLedgerRE> GetAllPartyLedger(DateTime startDate, DateTime endDate)
        {
            var parties =
                _dbEntiries.Ledgers.Where(
                    i =>
                        i.Status == 1 &&
                        (i.PropertyId == PropertyConstants.Supplier || i.PropertyId == PropertyConstants.Dealer))
                    .ToList();
            var partyLedger = new List<PartyLedgerRE>();
            foreach (var party in parties)
            {
                //Opening
                partyLedger.Add(GetPartyLedgerOpeningData(party, startDate));
                //Current
                partyLedger.AddRange(GetPartyLedgerData(party.Id, startDate, endDate));
            }
            return partyLedger;
        }

        public PartyLedgerRE GetPartyLedgerOpeningData(Ledger party, DateTime startDate)
        {
            var openinStartDate = new DateTime();
            var openingEndDate = startDate.AddDays(-1);
            var openingLedger = GetPartyLedgerData(party.Id, openinStartDate, openingEndDate);
            var partyOpening = new PartyLedgerRE
            {
                Id = party.Id,
                Particular = "Opening",
                Balance = party.PropertyId == PropertyConstants.Supplier ? (openingLedger.Sum(i => i.Credit) - openingLedger.Sum(i => i.Debit)) 
                                : (openingLedger.Sum(i => i.Debit) - openingLedger.Sum(i => i.Credit))
            };
            return partyOpening;
        }

        public List<PartyLedgerRE> GetPartyLedgerData(long partyId, DateTime startDate, DateTime endDate)
        {
            var service= new AccountsLedgerService();
            var ledgers =service.GetAccountsLedgerById(partyId, startDate, endDate);
            return ledgers.Select(i => new PartyLedgerRE
            {
                Particular = i.Particular,
                InvoiceNo=i.InvoiceNo,
                ReferenceNo = i.ReferenceNo,
                Date = i.Date,
                Debit = i.Debit,
                Credit = i.Credit
            }).ToList();
        }


        internal List<CheckStatusRE> GetGetCheckStatusDataParty(long partyId, DateTime startDate, DateTime endDate)
        {
            var partyPayment = _dbEntiries.Payments.Where(
                    i => i.Status == 1 && i.PaymentStatus == PropertyConstants.Pending && i.Date >= startDate && i.CheckNo != null && i.Date <= endDate 
                        &&i.PaymentStatus== PropertyConstants.Pending
                        && (i.DebitId == partyId || i.CreditId == partyId))
                    .Select(i => new CheckStatusRE
                    {
                        Id = i.Id,
                        Amount = i.Amount,
                        BankName = i.BankName,
                        PaymentDate = i.Date,
                        CheckDate = i.CheckDate,
                        CheckNo = i.CheckNo,
                        CheckStatus = i.PaymentStatus == PropertyConstants.Paid ? "Paid" : "Pending"
                    }).ToList();
            return partyPayment;
        }
    }

    public class PartyLedgerRE
    {
        public long Id { get; set; }
        public int Serial { get; set; }
        public string Party { get; set; }
        public DateTime? Date { get; set; }
        public string Particular { get; set; }
        public string BilNo { get; set; }
        public Decimal Debit { get; set; }
        public Decimal Credit { get; set; }
        public Decimal Balance { get; set; }
        public string ReferenceNo { get; set; }
        public string InvoiceNo { get; set; }
    }
    public class CheckStatusRE
    {
        public long Id { get; set; }
        public decimal Amount { get; set; }
        public string CheckNo { get; set; }
        public string BankName { get; set; }
        public string CheckStatus { get; set; }
        public DateTime? PaymentDate { get; set; }
        public DateTime? CheckDate { get; set; }
    }
}
    
