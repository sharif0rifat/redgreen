﻿using ERP.DAL.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.Repositories;

namespace ERP.REPOSITORIES.ReportRepositories
{
    public class BalanceSheetService
    {
        readonly ERPEntities _dbEntiries;
        public BalanceSheetService()
        {
            _dbEntiries = new ERPEntities();
        }
        internal System.Collections.IEnumerable GetBalanceSheeet(DateTime startDate, DateTime endDate)
        {
            var trialBalanceServ = new TrialBalanceService();
            var result= (List<TrialBalanceRE>)trialBalanceServ.GetTrialBalanceReport(DateTime.Today, DateTime.Today);
            result.Add(GetCurrentStock(_dbEntiries, DateTime.Today));
            return result.Select(i => new AccountsLedgerRE
            {
                Particular = i.Header,
                Debit = i.Debit ?? 0,
                Credit = i.Credit ?? 0
            }).ToList();
            //var assets = GetBalanceLedger(PropertyConstants.Asset, PropertyConstants.Debit);
            //var liability = GetBalanceLedger(PropertyConstants.Liability, PropertyConstants.Credit);
            //var equity = GetBalanceLedger(PropertyConstants.Equity, PropertyConstants.Credit);
            //return assets.Union(liability).Union(equity).ToList();
        }
        private TrialBalanceRE GetCurrentStock(ERPEntities dbEntiries, DateTime endDate)
        {
            var startDate = new DateTime();
            var salesIds = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == PropertyConstants.Sales
                                                                && i.InvoiceDate >= startDate &&
                                                                i.InvoiceDate <= endDate).Select(i => i.Id);
            var salesReturnIds = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == PropertyConstants.SalesReturn
                                                                && i.InvoiceDate >= startDate &&
                                                                i.InvoiceDate <= endDate).Select(i => i.Id);
            //var purchaseIds = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == PropertyConstants.Purchase
            //                                                    && i.InvoiceDate >= startDate &&
            //                                                    i.InvoiceDate <= endDate).Select(i => i.Id);
            //var purchaseReturnIds = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == PropertyConstants.PurchaseReturn
            //                                                    && i.InvoiceDate >= startDate &&
            //                                                    i.InvoiceDate <= endDate).Select(i => i.Id);
            var salesCost = _dbEntiries.InvoiceDetails.Where(i => i.Status == 1 && salesIds.Contains(i.InvoiceId)).Select(i => i.Quantity * i.BuyingPrice).DefaultIfEmpty(0).Sum();
            var salesReturnCost = _dbEntiries.InvoiceDetails.Where(i => i.Status == 1 && salesReturnIds.Contains(i.InvoiceId)).Select(i => i.Quantity * i.BuyingPrice).DefaultIfEmpty(0).Sum();
            var purchaseCost = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == PropertyConstants.Purchase).Select(i => i.GrandTotal).DefaultIfEmpty(0).Sum();
            var purchaseReturnCost = _dbEntiries.InvoiceInfoes.Where(i => i.Status == 1 && i.PropertyId == PropertyConstants.PurchaseReturn).Select(i => i.GrandTotal).DefaultIfEmpty(0).Sum();
            //return salesCost - salesReturnCost;
            return new TrialBalanceRE { 
                Header="Current Stock",
                Debit=purchaseCost+salesReturnCost-purchaseReturnCost-salesCost
            };
        }
        private IEnumerable<AccountsLedgerRE> GetBalanceLedger(long accountsType, long headerType)
        {
            var groupHeaders =
                _dbEntiries.GroupHeaders.Where(i => i.AccountsType == accountsType).Select(i => i.Id).ToList();
            var balanceGroups =
                _dbEntiries.LedgerGroups.Where(
                    i =>
                        groupHeaders.Contains(i.GroupHeaderId))
                //.Select(i=>i.Id)
                    .ToList();
            var accounts = new List<AccountsLedgerRE>();
            balanceGroups.ForEach(i =>
            {
                var sum = GetSummaryByGroup(i);
                accounts.Add(new AccountsLedgerRE
                {
                    Particular = _dbEntiries.LedgerGroups.FirstOrDefault(x => x.Id == i.Id).Name,
                    Debit = headerType == PropertyConstants.Debit ? sum : 0,
                    Credit = headerType == PropertyConstants.Credit ? sum : 0,
                });
            });
            return accounts;
        }

        private decimal GetSummaryByGroup(LedgerGroup ledgerGroup)
        {
            //var serv = new TrialBalanceService { fromDate = DateTime.Today, toDate = DateTime.Today.AddDays(5) };
            //if (ledgerGroup != null && ledgerGroup.PropertyId == PropertyConstants.Supplier)
            //{
            //    return serv.GetCreditors();
            //}
            //if (ledgerGroup != null && ledgerGroup.PropertyId == PropertyConstants.Dealer)
            //{
            //    return serv.GetDebtors();
            //}
            //var manager = new AccountsManager(_dbEntiries);
            //var ledgers = _dbEntiries.Ledgers.Where(i => i.LedgerGroupId == ledgerGroup.Id).ToList();
            decimal sum = 0;
            //ledgers.ForEach(i =>
            //{
            //    var ledger = (List<AccountsLedgerRE>)manager.GetAccountsLedger(i.Id, DateTime.Today, DateTime.Today.AddDays(5));
            //    var accountsLedgerRE = ledger.LastOrDefault();
            //    if (accountsLedgerRE != null) sum = sum + accountsLedgerRE.Balance;
            //});
            return sum;
        }

    }

}
