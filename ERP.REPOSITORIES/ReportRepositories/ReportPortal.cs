﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity.Core.Common.CommandTrees;
using System.Dynamic;
using System.Linq;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.ReportEntity;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.Repositories;

namespace ERP.REPOSITORIES.ReportRepositories
{
    public class ReportPortal
    {
        readonly ERPEntities _dbEntiries;
        public ReportPortal()
        {
            _dbEntiries = new ERPEntities();
        }

        public IEnumerable GetSalesReportData(string splitobj, long invoiceType)
        {
            var salesReportService = new SalesReportService();
            var splitedObject = splitobj.Split('_');
            var startDate = splitedObject[0] == "" ? DateTime.Today : Convert.ToDateTime(splitedObject[0]);
            var endDate = splitedObject[1] == "" ? DateTime.Today : Convert.ToDateTime(splitedObject[1]);
            if (startDate > endDate)
                throw new Exception("Select Date properly...Start Date Cannot be grater than End Date.");
            var categoryId = Convert.ToInt64(splitedObject[2] == "" ? "0" : splitedObject[2]);
            var productId = Convert.ToInt64(splitedObject[3] == "" ? "0" : splitedObject[3]);
            var partyId = Convert.ToInt64(splitedObject[4] == "" ? "0" : splitedObject[4]);

            return salesReportService.GetSalesReport(categoryId, productId, partyId, invoiceType, startDate, endDate);
        }
        public IEnumerable GetInvoiceReportData(string splitobj)
        {
            var salesReportService = new SalesReportService();
            var splitedObject = splitobj.Split('_');
            var startDate = splitedObject[0] == "" ? DateTime.Today : Convert.ToDateTime(splitedObject[0]);
            var endDate = splitedObject[1] == "" ? DateTime.Today : Convert.ToDateTime(splitedObject[1]);
            if (startDate > endDate)
                throw new Exception("Select Date properly...Start Date Cannot be grater than End Date.");
            var categoryId = Convert.ToInt64(splitedObject[2] == "" ? "0" : splitedObject[2]);
            var productId = Convert.ToInt64(splitedObject[3] == "" ? "0" : splitedObject[3]);
            var partyId = Convert.ToInt64(splitedObject[4] == "" ? "0" : splitedObject[4]);
            return salesReportService.GetInvoiceReport(categoryId, productId, partyId, startDate, endDate);
        }
        public IEnumerable GetPartyLedgerData(string splitobj)
        {
            //return null;
            var partyService = new PartyLedgerService(null);
            var objSplitted = splitobj.Split('_');
            long partyId = Convert.ToInt64(objSplitted[0]);
            DateTime startDate = objSplitted[1] == "" ? DateTime.Today.AddDays(-30) : Convert.ToDateTime(objSplitted[1]);
            DateTime endDate = objSplitted[2] == "" ? DateTime.Today : Convert.ToDateTime(objSplitted[2]);
            if (startDate > endDate)
                throw new Exception("Select Date properly...Start Date Cannot be grater than End Date.");
            var currentData = partyService.GetPartyLedgerByParty(partyId, startDate, endDate);

            return currentData;
        }
        public IEnumerable GetVoucher(string splitobj)
        {
            long voucherId;
            long.TryParse(splitobj.Split('_')[0], out voucherId);
            string type = splitobj.Split('_')[1]; //Check string in payment module and transaction list
            using (var dbEntities = new ERPEntities())
            {
                List<DebitVoucher> voucher=null;
                
                if (type == "payment")
                {
                    voucher = dbEntities.Payments.Where(e => e.Id == voucherId && e.Status != 0)
                    .Select(p => new DebitVoucher
                    {
                        Date = p.Date,
                        Particulars = p.Remarks,
                        Amount = p.Amount
                    }).ToList();
                    var payment = dbEntities.Payments.Where(e => e.Id == voucherId && e.Status != 0).FirstOrDefault();
                    var party = dbEntities.Ledgers.Where(l => l.Id == payment.PartyId).FirstOrDefault();
                    voucher.ForEach(v =>
                    {
                        v.PayeeName = party.Name;
                        v.Address = party.Address1;
                        v.PhoneNo = party.Mobile1;
                    });
                }
                else if (type == "transaction")
                {
                    voucher = dbEntities.Transactions.Where(e => e.Id == voucherId && e.Status != 0)
                    .Select(p => new DebitVoucher
                    {
                        Date = p.TransactionDate,
                        Particulars = p.Remarks,
                        Amount = p.Amount??0
                    }).ToList();
                    var payment = dbEntities.Transactions.Where(e => e.Id == voucherId && e.Status != 0).FirstOrDefault();
                    var party = dbEntities.Ledgers.Where(l => l.Id == payment.DebitHeaderId).FirstOrDefault();
                    voucher.ForEach(v =>
                    {
                        v.PayeeName = party.Name;
                        v.Address = party.Address1;
                        v.PhoneNo = party.Mobile1;
                    });
                }
                
                return voucher;
            }
        }
        public IEnumerable GetAccountsLedgerData(string splitobj)
        {
            GetAdvancedDate(splitobj, true);
            if (DateRange.startDate > DateRange.endDate)
                throw new Exception("Select Date properly...Start Date Cannot be grater than End Date.");
            var accountsManager = new AccountsLedgerService();
            return accountsManager.GetAccountsLedgerById(DateRange.selectedId, DateRange.startDate, DateRange.endDate);
        }

        public IEnumerable GetTrialBalanceData(string splitobj)
        {
            DateTime startDate = splitobj.Split('_')[0] == "" ? DateTime.Today.AddMonths(-1) : Convert.ToDateTime(splitobj.Split('_')[0]);
            DateTime endDate = splitobj.Split('_')[1] == "" ? DateTime.Today : Convert.ToDateTime(splitobj.Split('_')[1]);
            if (startDate > endDate)
                throw new Exception("Select Date properly...Start Date Cannot be grater than End Date.");
            var trialBalanceServ = new TrialBalanceService();
            //return null;
            var x = trialBalanceServ.GetTrialBalanceReport(startDate.Date, endDate.Date);
            return x;
        }


        public IEnumerable GetClosingStockData(string splitobj)
        {
            var stockService = new StockManager();
            return stockService.GetStocks("Opening");
        }

        public IEnumerable GetOpeningStocktData(string splitobj)
        {
            var stockService = new StockManager();
            return stockService.GetStocks("Closing");
        }


        public IEnumerable GetBalanceSheetData(string splitobj, string header)
        {
            var service = new BalanceSheetService();
            return service.GetBalanceSheeet(DateRange.startDate, DateRange.endDate);
        }

        public IEnumerable GetStockListData(string splitobj, string header)
        {
            var service = new StockManager();
            return service.GetCurrentStocks();
        }

        public IEnumerable GetIncomeStatementData(string splitobj)
        {
            var service = new IncomeStatementService();
            GetSplittedObj(splitobj);
            return service.GetIncomeStatement(DateRange.startDate, DateRange.endDate);
        }

        private void GetSplittedObj(string splitobj)
        {
            DateRange.startDate = splitobj.Split('_')[0] == "" ? DateTime.Today.AddDays(-30) : Convert.ToDateTime(splitobj.Split('_')[0]);
            DateRange.endDate = splitobj.Split('_')[1] == "" ? DateTime.Today : Convert.ToDateTime(splitobj.Split('_')[1]);

        }
        private void GetAdvancedDate(string splitobj, bool hasId)
        {
            var startId = 0;
            var endId = 1;
            if (hasId)
            {
                DateRange.selectedId = Convert.ToInt64(splitobj.Split('_')[0]);
                startId = 1;
                endId = 2;
            }

            var openingIds =
                _dbEntiries.Ledgers.Where(
                    i =>
                        i.Id == DateRange.selectedId && (i.PropertyId == PropertyConstants.StockLedger ||
                        i.PropertyId == PropertyConstants.ProfitLoss)).ToList();

            DateRange.startDate = splitobj.Split('_')[startId] == "" ? DateTime.Today.AddDays(-30) : Convert.ToDateTime(splitobj.Split('_')[startId]);
            DateRange.endDate = splitobj.Split('_')[endId] == "" ? DateTime.Today : Convert.ToDateTime(splitobj.Split('_')[endId]);
            if (openingIds != null && openingIds.Count > 0)
            {
                var diff = DateRange.endDate - DateRange.startDate;
                var tempDate = DateRange.endDate;
                DateRange.endDate = DateRange.endDate + diff;
                DateRange.startDate = tempDate;
            }

        }


        public IEnumerable GetInvoiceById(string splitobj)
        {
            long invoiceId;
            long.TryParse(splitobj, out invoiceId);

            using (var dbEntities = new ERPEntities())
            {
                var currencyId = dbEntities.InvoiceInfoes.Where(i => i.Id == invoiceId).First().Currency;
                var currencyName = dbEntities.Currencies.Where(i => i.ID == currencyId).First().ShortName;
                var total = Convert.ToInt64(dbEntities.InvoiceInfoes.Where(i => i.Id == invoiceId).First().GrandTotal);
                var party = dbEntities.Ledgers.Where(p => p.Id == dbEntities.InvoiceInfoes.Where(i => i.Id == invoiceId).FirstOrDefault().PartyId).First();
                var process = dbEntities.Users.Where(i => i.Id == currencyId).First().Name;
                var approved = dbEntities.Users.Where(i => i.Id == currencyId).First().Name;
                var numberWords =
                    NumberInWords.IntegerToWritten(total);
                string now = DateTime.Today.ToString("dd-MM-yyyy");
                var invPrint = dbEntities.InvoiceInfoes.Where(i => i.Id == invoiceId).Select(i => new InvoicePrint
                {
                    Id = i.Id,
                    To = party.Name,
                    Address = party.Address1,
                    PartyId = i.PartyId,
                    PrintDate = now,
                    InvoiceDate = i.InvoiceDate??DateTime.Today,
                    InvoiceNo = i.InvoiceNo,
                    SubTotal = i.SubTotal,
                    ServiceCharge = i.ServiceCharge,
                    Discount = i.Discount,
                    Vat = i.Vat,
                    GrandTotal = i.GrandTotal,
                    Remarks = i.Remarks,
                    ProcessBy = process,
                    ApprovedBy = approved,
                    ReferenceNo = i.ReferenceNo,
                    Currency = currencyName,
                    NumberWords = numberWords
                }).ToList();
                invPrint.ForEach(i => i.PrintDate = i.InvoiceDate.ToString("dd MMM yyyy hh:mm tt"));
                return invPrint;
            }
        }

        public IEnumerable GetInvoiceDetailById(string splitobj)
        {
            long invoiceId;
            long.TryParse(splitobj, out invoiceId);
            //using (var dbEntities = new ERPEntities())
            //{
            //    var dbData = dbEntities.InvoiceDetails.Where(i => i.InvoiceId == invoiceId).Select(j => new InvoicePrintDetail
            //    {
            //        Id = j.Id,
            //        Gift = j.PropertyId,
            //        Quantity = j.Quantity,
            //        ItemName = dbEntities.Products.FirstOrDefault(i => i.Id == j.ProductId).ProductName,
            //        UnitPrice = j.SellingPrice,
            //        TotalPrice = j.TotalPrice,
            //        WarrantyMonth=j.Warranty,
            //        ProductSerial = j.ProductSerial
            //    }).ToList();
            //    dbData.ForEach(i =>
            //    {
                    //i.Warranty = GetYearAndMonth(i.WarrantyMonth);
            //        if (i.Gift == PropertyConstants.Gift)
            //        {
            //            i.UnitPrice = 0;
            //            i.TotalPrice = 0;
            //            i.Warranty = "Gift Item";
            //        }
            //    });
                var printService = new InvoicePrintService();
                return printService.GetInvoiceDetail(invoiceId);
                //return dbData;
            //}
        }
        public IEnumerable GetChallanDetaileById(string splitobj)
        {
            long invoiceId;
            long.TryParse(splitobj, out invoiceId);
            using (var dbEntities = new ERPEntities())
            {
                var dbData = dbEntities.InvoiceDetails.Where(i => i.InvoiceId == invoiceId).Select(j => new InvoicePrintDetail
                {
                    Id = j.Id,
                    Quantity = j.PropertyId == PropertyConstants.Gift ?0: j.Quantity,
                    ItemName = dbEntities.Products.FirstOrDefault(i => i.Id == j.ProductId).ProductName+(j.PropertyId==PropertyConstants.Gift?" (Gift)":""),
                    CategoryId = dbEntities.Products.FirstOrDefault(i => i.Id == j.ProductId).CategoryId,
                    UnitPrice = j.SellingPrice,
                    TotalPrice = j.PropertyId == PropertyConstants.Gift ? 0 : j.TotalPrice,
                    WarrantyMonth = j.Warranty,
                    ProductSerial = j.ProductSerial
                }).ToList();
                dbData = dbData.GroupBy(i => new { i.ItemName,i.CategoryId}).Select(i => new InvoicePrintDetail
                {
                    ItemName = i.Key.ItemName,
                    CategoryId = i.Key.CategoryId,
                    Quantity = i.Sum(p=>p.Quantity),
                    UnitPrice = i.First().UnitPrice,
                    TotalPrice = i.Sum(j=>j.TotalPrice)
                }).ToList();
                dbData.ForEach(i =>
                {
                    i.CategoryName = dbEntities.Categories.FirstOrDefault(j => j.Id == i.CategoryId).Name;
                });
                return dbData;
            }
        }

        public IEnumerable GetSalarySheet()
        {
            using (var dbEntities = new ERPEntities())
            {
                var employees = dbEntities.Employees.Where(e => e.Status != 0).ToList();
                return employees;
            }
        }

        public IEnumerable GetEmployeeById(string splitobj)
        {
            long employeeId;
            long.TryParse(splitobj, out employeeId);

            using (var dbEntities = new ERPEntities())
            {
                var employee = dbEntities.Employees.Where(e => e.Id == employeeId && e.Status != 0).ToList();
                return employee;
            }
        }

        public IEnumerable GetCheckStatusData(string splitobj, string p)
        {
            //return null;
            var partyService = new PartyLedgerService(null);
            var objSplitted = splitobj.Split('_');
            long partyId = Convert.ToInt64(objSplitted[0]);
            DateTime startDate = objSplitted[1] == "" ? DateTime.Today.AddDays(-30) : Convert.ToDateTime(objSplitted[1]);
            DateTime endDate = objSplitted[2] == "" ? DateTime.Today : Convert.ToDateTime(objSplitted[2]);
            if (startDate > endDate)
                throw new Exception("Select Date properly...Start Date Cannot be grater than End Date.");

            //var openingData = partyService.GetPartyLedgerOpeningData(partyId, startDate);
            var currentData = partyService.GetGetCheckStatusDataParty(partyId, startDate, endDate);

            return currentData;
        }

        public IEnumerable GetPartyHeaderData(string splitobj)
        {
            var objSplitted = splitobj.Split('_');
            long partyId = Convert.ToInt64(objSplitted[0]);
            DateTime startDate = objSplitted[1] == "" ? DateTime.Today.AddDays(-30) : Convert.ToDateTime(objSplitted[1]);
            DateTime endDate = objSplitted[2] == "" ? DateTime.Today : Convert.ToDateTime(objSplitted[2]);
            var party = _dbEntiries.Ledgers.FirstOrDefault(i => i.Id == partyId);
            IEnumerable obj = new[] { new { FromDate = startDate.ToString("dd-MM-yyyy"), ToDate = endDate.ToString("dd-MM-yyyy"), ReportName = party.Name, CompanyName = "", Remarks = "" } };
            return obj;
        }
    }

    public static class DateRange
    {
        public static long selectedId { get; set; }
        public static DateTime startDate;
        public static DateTime endDate;
    }
}
