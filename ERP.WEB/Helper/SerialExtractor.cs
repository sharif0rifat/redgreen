﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Configuration;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Repositories;
using WebGrease.Css.Extensions;
using Excel = Microsoft.Office.Interop.Excel;

namespace ERP.WEB.Helper
{
    public static class SerialExtractor
    {
        public static List<InvoiceDetail> invoiceDetailsbyID;
        public static IList<InvoiceDetailViewModel> SerialRead(long? invoiceInfoId, long productId, string serialString, int waranty)
        {
            IList<InvoiceDetailViewModel> detailList = new List<InvoiceDetailViewModel>();
            var _productManager = new ProductManager();

            var product = _productManager.GetProduct(productId);
            var serials = serialString.Split(new string[] { "\n", "\r\n" }, StringSplitOptions.RemoveEmptyEntries); ;
            serials.ForEach(i =>
                {
                    var detail = new InvoiceDetailViewModel
                    {
                        InvoiceId = invoiceInfoId.GetValueOrDefault(),
                        Status = 1,
                        ProductSerial = i,
                        ProductId = productId,
                        ProductName = product.ProductName,
                        SellingPrice = product.SellingPrice,
                        BuyingPrice = product.BuyingPrice,
                        Warranty = waranty,
                        Quantity = 1,
                        TotalPrice = (product.SellingPrice),
                    };
                    detailList.Add(detail);
                }
            );
            return detailList;
        }
    }
}