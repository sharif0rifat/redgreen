﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Configuration;
using ERP.INFRASTRUCTURE;
using Excel = Microsoft.Office.Interop.Excel;

namespace ERP.WEB.Helper
{
    public static class ExcelReader
    {
        public static IList<InvoiceDetailViewModel> CSVReader(long? invoiceInfoId, long productId, REPOSITORIES.IRepositories.IProductManager _productManager)
        {
            var path = WebConfigurationManager.AppSettings["ExcelFolder"];
            IList<InvoiceDetailViewModel> detailList = new List<InvoiceDetailViewModel>();
            using (var fs = File.OpenRead(path))
            using (var reader = new StreamReader(fs))
            {
                var product = _productManager.GetProduct(productId);
                
                //List<string> listA = new List<string>();
                //List<string> listB = new List<string>();
                int index = 0;
                while (!reader.EndOfStream)
                {
                    
                    
                    var detail = new InvoiceDetailViewModel
                    {
                        InvoiceId = invoiceInfoId.GetValueOrDefault(),
                        Status = 1,
                        ProductId = productId,
                        ProductName = product.ProductName,
                        SellingPrice = product.SellingPrice,
                        BuyingPrice = product.BuyingPrice,
                        Quantity = 1,
                        TotalPrice = (product.SellingPrice),
                    };
                    var line = reader.ReadLine();
                    if (index < 2)
                    {
                        index++;
                        continue;
                    }
                    
                    var values = line.Split(',');
                    detail.ProductSerial = values[0];
                    detail.Warranty =Convert.ToInt32(values[1]);
                    detailList.Add(detail);
                    
                }
            }
            return detailList;
        }

        public static IList<InvoiceDetailViewModel> ReadExcelAndInsertDetail(long? invoiceInfoId, long productId, REPOSITORIES.IRepositories.IProductManager _productManager)
        {

            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;

            string str;
            int rCnt;
            int cCnt;
            int rw = 0;
            int cl = 0;

            xlApp = new Excel.Application();
            var path = WebConfigurationManager.AppSettings["ExcelFolder"];
            xlWorkBook = xlApp.Workbooks.Open(path, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            range = xlWorkSheet.UsedRange;
            rw = range.Rows.Count;
            cl = range.Columns.Count;
            var product = _productManager.GetProduct(productId);
            IList<InvoiceDetailViewModel> detailList = new List<InvoiceDetailViewModel>();
            for (rCnt = 2; rCnt <= rw; rCnt++)
            {
                var detail = new InvoiceDetailViewModel
                {
                    InvoiceId = invoiceInfoId.GetValueOrDefault(),
                    Status = 1,
                    ProductId = productId,
                    ProductName = product.ProductName,
                    SellingPrice = product.SellingPrice,
                    BuyingPrice = product.BuyingPrice,
                    Quantity = 1,
                    TotalPrice = (product.SellingPrice),
                };
                for (cCnt = 1; cCnt <= cl; cCnt++)
                {
                    //str = (string)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;
                    if (cCnt == 1)
                        detail.ProductSerial = (string)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;
                    if (cCnt == 2)
                    {
                        //string wr = (string) (range.Cells[rCnt, cCnt] as Excel.Range).Value2;
                        //int wrn;
                        //Int32.TryParse(wr,out wrn);
                        detail.Warranty = (int)(range.Cells[rCnt, cCnt] as Excel.Range).Value2;
                    }

                    //MessageBox.Show(str);
                }
                detailList.Add(detail);
            }

            xlWorkBook.Close(true, null, null);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
            return detailList;
        }
    }
}