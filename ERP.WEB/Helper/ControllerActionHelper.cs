﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace ERP.WEB.Helper
{
    public class ControllerAction
    {
        public string Controller { get; set; }
        public string Action { get; set; }
        public string ReturnType { get; set; }
        public string Attributes { get; set; }
        public string Area { get; set; }

    }
    public static class ControllerActionHelper
    {
        public static List<ControllerAction> GetAllControllerAndAction()
        {
            Assembly asm = Assembly.GetAssembly(typeof(MvcApplication));
            var projectName = Assembly.GetExecutingAssembly().FullName.Split(',')[0];
            var sysList = asm.GetTypes().
                SelectMany(t => t.GetMethods(BindingFlags.Instance | BindingFlags.DeclaredOnly | BindingFlags.Public))
                .Where(d => d.ReturnType.Name == "ActionResult" || d.ReturnType.Name == "JsonResult").Select(n => new ControllerAction
                {
                    Controller = n.DeclaringType.Name.Replace("Controller", ""),
                    Action = n.Name,
                    ReturnType = n.ReturnType.Name,
                    Attributes = string.Join(",", n.GetCustomAttributes().Select(a => a.GetType().Name.Replace("Attribute", ""))),
                    Area = n.DeclaringType.Namespace.ToString().Replace(projectName + ".", "").Replace("Areas.", "").Replace(".Controllers", "").Replace("Controllers", "")
                }).ToList();
            return sysList;
        }
    }
}