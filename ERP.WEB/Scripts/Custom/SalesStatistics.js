﻿//$(document).ready(function LoadChart() {
//    $.jqplot.config.enablePlugins = true;
//    var s1 = [2, 6, 7, 10];
//    var ticks = ['a', 'b', 'c', 'd'];

//    plot1 = $.jqplot('chart1', [s1], {
//        // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
//        pointLabels: { show: true, stackedValue: true },
//        seriesDefaults: {
//            renderer: $.jqplot.BarRenderer,
//            showMarker: false,
//        },
//        axes: {
//            xaxis: {
//                renderer: $.jqplot.CategoryAxisRenderer,
//                ticks: ticks
//            }
//        },
//        highlighter: { show: false }
//    });
//});
//var onchagetextbox = function(ctrl, affectedctrls) {
//    var ctrls = affectedctrls.split('^');
//    $(ctrls).each(function() {
//        $(this).val(ctrl.val());
//    });
//};

var loadpermissions = function (control) {
    var param = $(control).val();
    var url = 'GetPermissionList';
    $.ajax({
        type: 'POST',
        cache: true,
        data: { 'param': param },
        url: url,
        success: function (result) {
            console.log(result);
            $('#tabledata').empty();
            if (result.MessageType == 1) {
                var tbodycontent = '';
                $.each(result.ReturnValue, function (key, permission) {
                    tbodycontent += '<tr><td><input type="checkbox" class="flat-red pcheck" value="' + permission.Id + '"></td><td>' + permission.Title + '</td><td>' + permission.PermissionCode + '</td></tr>';
                });
                $('#tabledata').append(tbodycontent);
            }
        },
        error: function (err) {
        }
    });
};

var submitTemplateRegister = function () {
    var checkeditem = '';
    var hasNext = false;
    $('tbody#tabledata').find('input:checkbox').each(function () {
        if ($(this).is(':checked')) {
            if (hasNext) checkeditem += '^';
            checkeditem += $(this).val();
            hasNext = true;
        }
    });
    var pdata = {};
    pdata['TemplateName'] = $('#TemplateName').val();
    pdata['ApplicationId'] = $('#ApplicationId').val();
    pdata['PermissionIds'] = checkeditem;
    submit_single(pdata, 'Register', '/Template/Register');
};

var search = function (control, url) {
    var param = $('#' + control).val();
    $.ajax({
        type: 'POST',
        cache: true,
        data: { 'param': param },
        url: url,
        success: function (result) {
            console.log(result);
            if (result.MessageType == 1) {
                var temp = {};
                temp['param'] = result.ReturnValue;
                SetObjects(temp);
            }
        },
        error: function (err) {
        }
    });
}
var clearvalidation = function(ctrl) {
    $('#'+ctrl+'-validation').empty();
};
var disableBtn = function(ctrl) {
    $('.' + ctrl).prop("disabled", true);
};
var updatecontrol = function (ctrl, buyingPricectrl, quantityctrl, totalctrl, grandTotalctrl, enableFilter) {
    var grandtotal = 0.0;
    var enablesubmit = true;
    $('.' + ctrl).find('tr').each(function () {
        var qtystr = $(this).find('td input:text').val();
        if (qtystr != '' && qtystr > 0 && qtystr != null) {
            var buyingqtyvalue = parseInt($(this).find('td.' + quantityctrl).html());
            if (enableFilter) {
                if (buyingqtyvalue < parseInt(qtystr)) {
                    alertify.error('you can sell maximum ' + buyingqtyvalue + ' quantity.');
                    enablesubmit = false;
                }
            }
            //var buyingPricevalue = parseInt($(this).find('td.' + buyingPricectrl).html());
            var total = parseInt(qtystr) * parseInt($(this).find('td.' + buyingPricectrl).html());
            console.log('total price is : ' + total + ' \n==================');
            $(this).find('.' + totalctrl).val(total);
            grandtotal += total;
        } else {
            enablesubmit = false;
        }
    });
    $('#' + grandTotalctrl).val(grandtotal);
    console.log(enablesubmit);
    if (!enablesubmit) {
        //$('.salesubmit').attr('disabled', 'disabled');
        $('.salesubmit').prop("disabled", true);
    } else {
        $('.salesubmit').prop("disabled", false);
    }
    
};
var removerow = function (thisobject, ctrl, buyingPricectrl, quantityctrl, totalctrl, grandTotalctrl) {
    $(thisobject).closest('tr').remove();
    updatecontrol(ctrl, buyingPricectrl, quantityctrl, totalctrl, grandTotalctrl);
};
$(function () {
    $('#fromDate').datepicker();
    $('#toDate').datepicker();
});
function LoadChart() {
    var startDate = $('#fromDate').val();
    var endDate = $('#toDate').val();
    $.ajax({
        url: "LoadData",
        data: { fromDate: startDate, toDate: endDate },
        success: function (result) {

            console.log(result);
            //=====Jqplot===============
            $.jqplot.config.enablePlugins = true;
            var s1 = result.yAxis;
            var ticks = result.xAxis;

            //var s1 = [2, 6, 7, 10];
            //var ticks = ['a', 'b', 'c', 'd'];

            plot1 = $.jqplot('chart1', [s1], {
                // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
                height: 600,
                width: 1500,
                pointLabels: { show: true, stackedValue: true },
                seriesDefaults: {
                    renderer: $.jqplot.BarRenderer,
                    labelPosition: 'middle',
                    showMarker: false,
                },
                axes: {
                    xaxis: {
                        renderer: $.jqplot.CategoryAxisRenderer,
                        ticks: ticks
                    }
                },
                highlighter: { show: false }
            });
            //=====Jqplot===============
            //$("#chart1").html(result);
        }
    });




    //$.jqplot.config.enablePlugins = true;
    //var s1 = [2, 6, 7, 10];
    //var ticks = ['a', 'b', 'c', 'd'];

    //plot1 = $.jqplot('chart1', [s1], {
    //    // Only animate if we're not using excanvas (not in IE 7 or IE 8)..
    //    pointLabels: { show: true, stackedValue: true },
    //    seriesDefaults: {
    //        renderer: $.jqplot.BarRenderer,
    //        showMarker: false,
    //    },
    //    axes: {
    //        xaxis: {
    //            renderer: $.jqplot.CategoryAxisRenderer,
    //            ticks: ticks
    //        }
    //    },
    //    highlighter: { show: false }
    //});


}