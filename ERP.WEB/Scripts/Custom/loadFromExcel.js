﻿function addProductByExcel(url) {
    var productId = parseInt($('#product-id option:selected').val());
    var invoiceInfoId = parseInt($('#invoiceInfo-id').val());
    invoiceInfoId = isNaN(invoiceInfoId) ? null : invoiceInfoId;
    if (productId > 0) {
        ShowLoader("Loading...");
        $.post(url, { invoiceInfoId: invoiceInfoId, serialproductId: productId }, function (data) {
            if (data.MessageType == 1) {
                $('#productBody').append(data.ReturnValue);
                updateTotals();
                HideLoader();
            } else {
                HideLoader();
                alertify.alert("Check the file name('Products.csv') and data properly.  Problem loading..." + data.MessageData);
            }
        });
    } else
        alertify.error("No Product Selected");
}

function addProductByserial(url) {
    var productId = parseInt($('#product-id option:selected').val());
    if (productId < 1) {
        alertify.error("Select A product for loading..");
        return;
    }
    var invoiceInfoId = parseInt($('#invoiceInfo-id').val());
    var waranty = parseInt($('#product-waranty').val());
    var serial = $('#product-serials').val();
    ShowLoader("Loading...");

    $.post(url, { invoiceId: invoiceInfoId, productId: productId, waranty: waranty, serial: serial }, function (data) {
        if (data.MessageType == 1) {
            $('#productBody').append(data.ReturnValue);
            updateTotals();
            HideLoader();
        } else {
            HideLoader();
            alertify.alert("Check the serial and waranty properly.  Problem loading..." + data.MessageData);
        }
    });
}