﻿
$(function() {
    $(".AddPartial").on("click", function(event) {
        var url = $(this).data('url');
        $.ajax({
            type: 'GET',
            cache: false,
            url: url,
            success: function(result) {
                $(".PartialModalContainer").html(result);
                $(".PartialModal").modal('show');
                $(".datepicker").datepicker({
                    inline: true,
                    changeMonth: true,
                    changeYear: true,
                    //dateFormat: 'dd-mm-yy',
                    yearRange: '-250:+250'                   
                });
                $.fn.modal.Constructor.prototype.enforceFocus = function () { };
            }
        });
        event.preventDefault();
    });

    $(".EditPartial").on("click", function(event) {
        var url = $(this).data('url');
        var id = $(this).data('id');
        $.ajax({
            data: { 'id': id },
            type: 'GET',
            cache: false,
            url: url,
            success: function(result) {
                $(".PartialModalContainer").html(result);
                $(".PartialModal").modal('show');
                $(".datepicker").datepicker({
                    inline: true,
                    changeMonth: true,
                    changeYear: true,
                    //dateFormat: 'dd-mm-yy',
                    yearRange: '-250:+250',
                });
                $.fn.modal.Constructor.prototype.enforceFocus = function () { };
            }
        });
        event.preventDefault();
    });
    $(".ViewDetails").on("click", function (event) {
        var url = $(this).data('url');
        var id = $(this).data('id');
        $.ajax({
            data: { 'id': id },
            type: 'GET',
            cache: false,
            url: url,
            success: function (result) {
                $(".PartialModalContainer").html(result);
                $(".PartialModal").modal('show');
                $.fn.modal.Constructor.prototype.enforceFocus = function () { };
            }
        });
        event.preventDefault();
    });

    $(".ChangePasswordPartial").on("click", function (event) {
        
        var url = $(this).data('url');
        var id = $(this).data('id');
        $.ajax({
            data: { 'id': id },
            type: 'GET',
            cache: false,
            url: url,
            success: function (result) {
                $(".PartialModalContainer").html(result);
                $(".PartialModal").modal('show');
            }
        });
        event.preventDefault();
    });
    
    
});
function ConfirmOperation(id, url,message) {
    console.log(id);
    console.log(url);
    alertify.confirm(message, function (e) {
        if (e) {
            // user clicked "ok"
            $.ajax({
                data: { 'id': id },
                type: 'GET',
                cache: false,
                url: url,
                success: function (result) {
                    console.log(result);
                    alertify.success("Database BackUp Successfull.");
                    location.reload();
                },
                error: function (error) {
                    console.log(error);
                }
            });
        } else {
            // user clicked "cancel"
            return false;
        }
    });
}
function ConfirmDelete(id, url) {
    console.log(id);
    console.log(url);
    alertify.confirm("Are you sure you want to delete?", function (e) {
        if (e) {
            // user clicked "ok"
            $.ajax({
                data: {'id': id },
                type: 'POST',
                cache: false,
                url: url,
                success: function (result) {
                    console.log("Success");
                    if (result.MessageType == 1) {
                        setTimeout(function () {
                            location.reload();
                            //your code to be executed after 1 second
                        }, 1500);
                    }
                    npAlert(result,true);
                },
                error: function (error) {
                    npAlert(error);
                }
            });
        } else {
            // user clicked "cancel"
            return false;
        }
    });
}

function ConfirmActivation(id,status, url) {
    if (status) {
        alertify.confirm("Are you sure you want to Active?", function (e) {
            if (e) {
                // user clicked "ok"
                $.ajax({
                    data: { 'id': id, 'status': status },
                    type: 'GET',
                    cache: false,
                    url: url,
                    success: function (result) {
                        console.log(result);
                        location.reload();
                    }
                });
            } else {
                // user clicked "cancel"
                return false;
            }
        });
    } else {
        alertify.confirm("Are you sure you want to Inactive?", function (e) {
            if (e) {
                // user clicked "ok"
                $.ajax({
                    data: { 'id': id, 'status': status },
                    type: 'GET',
                    cache: false,
                    url: url,
                    success: function (result) {
                        console.log(result);
                        location.reload();
                    }
                });
            } else {
                // user clicked "cancel"
                return false;
            }
        });
    }
    
}

function ConfirmUserDelete(id,status, url) {
    alertify.confirm("Are you sure you want to change status?", function (e) {
        if (e) {
            $.ajax({
                data: { 'id': id,'status':status },
                type: 'GET',
                cache: false,
                url: url,
                success: function (result) {
                    console.log(result);
                    location.reload();
                }
            });
        } else {
            return false;
        }
    });
}


