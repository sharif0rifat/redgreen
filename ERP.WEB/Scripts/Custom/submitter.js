﻿// load customer by CIF Number code.
// address information process
var submit_single = function (param, url, redirectUrl) {
    $.ajax({
        type: 'POST',
        cache: true,
        data: { 'param': param },
        url: url,
        success: function(result) {
            console.log(result);
            if (result.MessageType == 1) {
                npNotification(result);
                setTimeout(function () {
                    if (redirectUrl == null) {
                        window.location = window.location.href;
                    } else {
                        window.location = npGlobalProp.base_url + redirectUrl;
                    }
                }, 3000);
            }
        },
        error: function(err) {
            npNotification(err);
        }
    });
};
// end load customer code
var loadPartial = function (param, url) {
    if (param == "") param = "-1";
    $.ajax({
        data: { 'param': param },
        type: 'GET',
        cache: false,
        url: url,
        success: function (result) {
            $(".PartialModalContainer").html(result);
            $(".PartialModal").modal('show');
            $.fn.modal.Constructor.prototype.enforceFocus = function () { };
        }
    });
};

function FlashMessage(flashLabel, message) {
    if (flashLabel == 1) {
        $('.savebtn').attr('disabled', 'disable');
    }
    var $flash = $('<div id="flash" style="display:none;">');
    $flash.html(message);
    $flash.toggleClass('flash');
    $flash.toggleClass('flash-' + flashLabel);
    $('body').find('.FlashMessage').prepend($flash);
    $flash.slideDown('slow');
    $flash.delay(2000).slideToggle('highlight');
    $($flash).click(function () { $(this).slideToggle("highlight"); });
    if (flashLabel == 1) {
        setTimeout((function () {
            window.location.reload();
        }), 1500);
    }
}

var thirdPartySubmitter = function(formControl, formValidate, formUrl, redirectUrl, messageType) {
    var isSuccess = np_submit(formControl, formValidate, formUrl, messageType);
    isSuccess.done(function(data) {
        if (data.MessageType == 1) {
            setTimeout(function() {
                window.location = npGlobalProp.base_url + redirectUrl;
            }, 3000);
        }
        return false;
    });
};

var addtotable = function(loadingObjet) {
    var param = $('#' + loadingObjet.control).find(':selected').attr('value');
    $.ajax({
        type: 'POST',
        url: loadingObjet.url,
        data: { 'param': param },
        //dataType: 'json',
        //contentType: 'application/json; charset=utf-8',
        cache: false,
        //crossDomain: true,
        success: function (result) {
            console.log(result);
            GenerateTableRow(result, loadingObjet.affectedControls, loadingObjet.showableObjects, loadingObjet.hiddenObjects);
        },
        error: function (err) {
            console.log(result);
        }
    });
};

var onchangesubmitter = function (onchangeObject) {
    var param = $('#' + onchangeObject.control).find(':selected').attr('value');
    if (onchangeObject.validationcheck) {
        clearvalidation(onchangeObject.control);
    }
    console.log(onchangeObject.url);
    $.ajax({
        type: 'POST',
        url: onchangeObject.url,
        data: { param: param },
        dataType: 'json',
        //contentType: 'application/json; charset=utf-8',
        //cache: false,
        //crossDomain: true,
        success: function (result) {
            console.log(result);
            GenerateDropDownList(result, onchangeObject.affectedControls, onchangeObject.objectProperties);
        },
        error: function (err) {
            console.log(err);
        }
    });
};