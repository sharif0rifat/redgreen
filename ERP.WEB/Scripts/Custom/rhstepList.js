﻿var pdata = {};
var textData = {};
var ClientService = {};
var Client = {};
var obj = '';
function getObjectByRadioButton(tblContainer, client, id, isEdit,projectId) {
    $(tblContainer).find('input:radio').each(function () {
        if ($(this).is(':checked')) {
            var row = $(this).parent().parent();
            GetControlObject(row);
        }
    });
    pdata["Id"] = id;
    pdata["IsEdit"] = isEdit;
    pdata["ProjectId"] = projectId;
    if (client != undefined) {
        GetTextValues(client);
    }
    console.log(pdata);
    return pdata;
}

function GetControlObject(row) {
    $(row).find('input:text, input:password, input:file, select, textarea, input:radio, input:checkbox').each(function () {        
        if ($(this).is('select') || $(this).is('Select') || $(this).is('SELECT')) {
            pdata[$(this).attr('id')] = $(this).find(':selected').attr('value');            
        }
        if ($(this).is('input:radio')) {            
            pdata[$(this).attr('id')] = $(this).attr('value');
        }
        if ($(this).is('input:text')) {
            pdata[$(this).attr('id')] = $(this).val();
        }
        if ($(this).is('input:password')) {
            pdata[$(this).attr('id')] = $(this).attr('value');
        }
        
    });
}

function GetHiidentValue(tblContainer) {
    $(tblContainer).find('input:hidden').each(function () {
        if ($(this).is('input:hidden')) {
            pdata[$(this).attr('id').split("_")[1]] = $(this).val();
        }
        
    });
}
function GetTextValues(client) {
    $(client).find('input:hidden, select').each(function () {
        if ($(this).is('input:hidden')) {
             obj = $(this).attr('id').split("_")[0];
            if (obj == 'Client') {
                Client[$(this).attr('id').split("_")[1]] = $(this).val();
            } else {
                ClientService[$(this).attr('id').split("_")[1]] = $(this).val();
            }
        }
        if ($(this).is('select') || $(this).is('Select') || $(this).is('SELECT')) {
            obj = $(this).attr('id').split("_")[1];
            if (obj == undefined) {
                ClientService[$(this).attr('id')] = $(this).find(':selected').attr('value');
            } else {
                ClientService[$(this).attr('id').split("_")[1]] = $(this).find(':selected').attr('value');
            }
        }
    });
    console.log(ClientService);
        pdata["Client"] = Client;
        pdata["ClientService"] = ClientService;
    
    return textData;
}

$(function() {
    $("#SearchClient").on("change", function () {
        var url = "GetClientInfo";
        $.ajax({
            data: { 'clientSerial': $(this).val() },
            type: 'POST',
            cache: false,
            dataType: 'json',
            url: url,
            success: function (result) {
                console.log(result);
                $("#RegistrationNo").val(result.ClientSerial);
                $("#ClientName").val(result.ClientName);
                $("#ClientAddress").val(result.PresentAddress);
                $("#MobileNo").val(result.Contact);
                $("#ReferenceNo").val(result.ReferenceNo);
            },
            error: function (ex) {
                alertify.alert('Registration not found.' + ex);
            }
        });
    });
    
    

    $(".amount").on("change", function () {
        var totalAmount = 0.00;
        $("#bill-section").find(".amount").each(function() {
            totalAmount += parseFloat($(this).val());
        });
        $("#TotalAmount").val(totalAmount);
        $("#total").val(totalAmount);
    });
    $("#reverse-amount").on("change", function() {
        var grandTotal = parseFloat($("#TotalAmount").val()) - parseFloat($(this).val());
        $("#TotalAmount").val(grandTotal);
    });
    $("#AdvanceAmount").on("change", function() {
        var grandTotal = parseFloat($("#TotalAmount").val()) - parseFloat($(this).val());
        $("#DueAmount").val(grandTotal);
    });
    
    
});

function GetMessage(message, messageType) {
    //alert(message);
    if (messageType == 'error') {
        alertify.error(message);
    }
    if (messageType == 'success') {
        alertify.success(message);
    }
    if (messageType == 'warning') {
        alertify.error(message);
    }
}