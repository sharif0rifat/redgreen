﻿
function SaveInvoice(url) {
    ShowLoader("Processing......Wait..it will take some time.");
    var propList = 'ProductSerial^Warranty^Quantity^BuyingPrice^SellingPrice^TotalPrice^GiftId'.split('^');
    var invoiceInfo = {};
    //=====hidden Fields=======
    invoiceInfo.Id = $('#invoiceInfo-id').val();
    invoiceInfo.CreatedBy = $('#created-by').val();
    invoiceInfo.CreatedDate = $('#created-date').val();
    invoiceInfo.ModifiedBy = $('#modified-by').val();
    invoiceInfo.ModifiedDate = $('#modified-date').val();
    invoiceInfo.Status = $('#status').val();
    invoiceInfo.DebitId = $('#DebitId').val();
    invoiceInfo.CreditId = $('#CreditId').val();
    
    //===========
    invoiceInfo.InvoiceNo = $('#InvoiceNo').val();
    invoiceInfo.PartyName = $('#PartyName').val();
    invoiceInfo.PartyAddress = $('#PartyAddress').val();
    
    invoiceInfo.InvoiceDate = $('#InvoiceDate').val();
    invoiceInfo.PropertyId = $('#PropertyId').val();
    invoiceInfo.PartyId = $('#PartyId').val();
    invoiceInfo.SubTotal = $('#sub-total').val();
    invoiceInfo.GrandTotal = $('#grand-total').val();
    invoiceInfo.Remarks = $('#Remarks').val();
    invoiceInfo.Vat = $('#vat').val();
    invoiceInfo.Discount = $('#discount').val();
    invoiceInfo.ServiceCharge = $('#serviceCharge').val();
    invoiceInfo.PartyAddress = $('#PartyAddress').val();
    invoiceInfo.CurrencyId = $('#CurrencyId').val();
    var InvoiceDetails = [];
    var container = $('#productBody');
    container.find('tr').each(function () {
        var npTemp = {};
        npTemp.Id = $(this).find('#id').val();
        npTemp.ProductId =  $(this).find('#product-id').val();
        npTemp.InvoiceId = $('#invoiceInfo-id').val();
        npTemp.PropertyId = $('#Property-id').val();
        $(this).find('td').each(function () {
            var key = $(this).attr('np-key');
            if (key == 'Quantity')
                npTemp['Quantity'] = $(this).find('#product-quantity').text();
            if (key == 'TotalPrice')
                npTemp['TotalPrice'] = $(this).find('#product-total-price').text();
            
            
            if (key == 'textbox') {
                $(this).find('input').each(function () {
                    var clname = $(this).attr('class').split(' ')[1];
                    //npTemp[clname] = $(this).val();
                    if (_.indexOf(propList, clname) > -1) {
                        npTemp[clname] = $(this).val();
                    }
                });
            }
            if (key == 'dropdown') {
                $(this).find('select').each(function () {
                    var clname = $(this).attr('class').split(' ')[1];
                    //npTemp[clname] = $(this).val();
                    if (_.indexOf(propList, clname) > -1) {
                        npTemp[clname] = $(this).val();
                    }
                });
            }
        });
        InvoiceDetails.push(npTemp);
    });
    console.log(invoiceInfo);
    console.log(InvoiceDetails);
    
    //======save Invoice========
    $.post(url, { param: invoiceInfo, InvoiceDetailsList: InvoiceDetails }, function (data) {
        if (data.MessageType == 1) {
            //$('#saved-invoice').value = data.ReturnValue;
            if (invoiceInfo.PropertyId == 2 || invoiceInfo.PropertyId == 3 || invoiceInfo.PropertyId == 4)
            {
                document.getElementById("saved-invoice").value = data.ReturnValue + "";
            }
            console.log($('#saved-invoice').val());
            alertify.success(data.MessageData);
        }
        else
            alertify.alert(data.MessageData);
        console.log(data);
        HideLoader();
    });
}


function SavePayment(url) {
    ShowLoader("Processing......Wait..it will take some time.");
   
    //======save Invoice========
    $.post(url, function (data) {
        if (data.MessageType == 1)
            alertify.success(data.MessageData);
        else
            alertify.alert(data.MessageData);
        console.log(data);
        HideLoader();

    });

}
//==================

function ShowLoader(message) {
    var loader = document.getElementById("overlay");
    loader.innerHTML = '<div class="msg-loading">' +
                        message +
                        '</div>';
    loader.style.display = "block";
}
function HideLoader() {
    document.getElementById("overlay").style.display = "none";
}