﻿$("#SearchByServiceBillDue").on("change", function () {
    var url = "GetSeriviceBillInfo";
    $.ajax({
        data: { 'billNo': $(this).val() },
        type: 'POST',
        cache: false,
        dataType: 'json',
        url: url,
        success: function (result) {
            //-----------extract Client Info------------
            insertClientInfo(result);
            updatePayment(result.InvoicePayment);
        },
        error: function (ex) {
            alertify.alert('Registration not found.' + ex);
        }
    });
});
$(function () {
    var retbill = $('#ServiceBillReturn').val();
    if (retbill == 1) {
        $('#InvoiceNo').val('');
        $('#InvoiceDate').val('');
    }
});
var subTotal = 0;

function updatePayment(InvoicePayment) {
    $('#TotalAmount').val(InvoicePayment.BillSubTotal);
    $('#InvoicePayment_BillDue').val(InvoicePayment.BillDue);
}

function insertClientInfo(result) {
    $('#InvoiceNo').val(result.InvoiceNo);
    $('#invoiceId').val(result.Id);
    var date = ChangeDateFormat('date', result.CreatedDate);
    //$('#InvoiceDate').text(date);
    $('#InvoiceDate').val(date);
    $("#RegistrationNo").val(result.RegistrationNo);
    $("#ClientName").val(result.ClientName);
    $("#ReferenceNo").val(result.ReferenceNo);
    $("#ClientAddress").val(result.ClientAddress);
    $("#MobileNo").val(result.MobileNo);
    $("#ReferenceBy").val(result.ReferenceBy);
    $("#ReferenceByName").val(result.ReferenceByName);
    $("#ClientCategory").val(result.ClientCategory);
}

function SaveDueService() {
    console.log('This is service bill');
    console.log($('#invoiceId').val());
    var pdata = {};
    var invoicePayment = {};
    pdata['Id'] = $('#invoiceId').val();
    pdata['InvoiceNo'] = $('#InvoiceNo').val();
    pdata['InvoiceDate'] = $('#InvoiceDate').val();

    invoicePayment['BillPaid'] = $('#InvoicePayment_BillPaid').val();

    pdata['InvoicePayment'] = invoicePayment;
    console.log('The invoice:');
    console.log(pdata);
    var url = 'PayPathologyBillDue';
    $.ajax({
        data: { 'modelInvoice': pdata },
        type: 'POST',
        //dataType: 'json',
        cache: false,
        url: url,
        success: function (result) {
            window.location.href = "/ERPBilling/PathologyBillDue?billNo=" + $('#InvoiceNo').val();
        }
    }); 
}

function checkAmount() {
    var paidAmount = $('#InvoicePayment_BillPaid').val();
    var dueAmount = $('#InvoicePayment_BillDue').val();
    if (parseFloat(paidAmount) > parseFloat(dueAmount))
        alertify.alert("Paid amount must be equal or less than due amount.");
}