﻿/// <reference path="D:\Mostafiz's backup\Project E drive\Personal Projects\ERP-ONLINE\ERP.WEB\StaticHtml/Print.html" />
function GetReport() {
    var searchObj = "";
    $('.inventorySearch').find('input:text, input:password, input:file, select, textarea, input:radio, input:checkbox').each(function () {
        if ($(this).is('select') || $(this).is('Select') || $(this).is('SELECT')) {
            var inputObj = $(this).find(':selected').attr('value');
            inputObj = inputObj ? inputObj : "";
            console.log(inputObj);
            searchObj += inputObj + "_";
        }
        if ($(this).is('input:radio')) {
            var inputObj = $(this).attr('value');
            inputObj = inputObj ? inputObj : "";
            console.log(inputObj);
            searchObj += inputObj + "_";
        }
        if ($(this).is('input:text')) {
            var inputObj = $(this).val();
            inputObj = inputObj ? inputObj : "";
            console.log(inputObj);
            searchObj += inputObj + "_";
        }
        if ($(this).is('input:password')) {
            var inputObj = $(this).attr('value');
            inputObj = inputObj ? inputObj : "";
            console.log(inputObj);
            searchObj += inputObj + "_";
        }

    });
    var pathname = window.location.pathname; // Returns path only
    //var url = window.location.href;     // Returns full URL
    //alert(searchObj);
    //return false;
    pathname = pathname.split('/');
    console.log("pathname: " + pathname);
    console.log(searchObj);
    document.getElementById("myReport").src = "../../Report/ReportForm.aspx?ReportID=" + pathname[2] + "^" + searchObj;
}

function Print(id) {
    var invoiceId = id;
    var pathname = window.location.pathname;
    window.open("../StaticHtml/Print.html?invoiceId=" + invoiceId + "&pathname=" + pathname,'_blank');
}
function PrintChallan(id) {
    var invoiceId = id;
    var pathname = window.location.pathname;
    window.open("../StaticHtml/ChallanPrint.html?invoiceId=" + invoiceId + "&pathname=" + pathname, '_blank');
}
function InvoiceShortPrint(id) {
    var invoiceId = id;
    var pathname = window.location.pathname;
    window.open("../StaticHtml/InvoiceShortPrint.html?invoiceId=" + invoiceId + "&pathname=" + pathname, '_blank');
}
function PrintVoucher(id,type) {
    var voucherId = id;
    var pathname = window.location.pathname;
    window.open("../StaticHtml/VoucherPrint.html?voucherId=" + voucherId +"&voucherType=" +type+"&pathname=" + pathname, '_blank');
}
function PrintEmployee(id) {
    var employeeId = id;
    var pathname = window.location.pathname;
    window.open("../StaticHtml/EmployeePrint.html?employeeId=" + employeeId + "&pathname=" + pathname, '_blank');
}
function PrintSalarySheet() {
    var pathname = window.location.pathname;
    window.open("../StaticHtml/SalarySheetPrint.html?pathname=" + pathname, '_blank');
}
