﻿using AutoMapper;
using ERP.DAL.Model;
using ERP.REPOSITORIES.Helper;
using ERP.WEB.App_Start;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ERP.WEB.Startup))]
namespace ERP.WEB
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            AutoMapperWebConfiguration.Configure();
            ConfigureAuth(app);
            PropertyConstants.Initialize();
        }
    }
}
