﻿using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Helper;
using System.Web.Mvc;

namespace ERP.WEB.Controllers
{
    public class AccountingReportController : Controller
    {
        //
        // GET: /AccountingReport/
        public ActionResult AccountsLedger()
        {
            ViewBag.SubHeaderList = DropDownListClass.GetSubHeaderList();
            return View();
        }
	}
}