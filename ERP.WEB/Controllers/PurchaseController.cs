﻿using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.INFRASTRUCTURE.Helper;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ERPSales.Web.Helper;

namespace ERP.WEB.Controllers
{
    public class PurchaseController : Controller
    {
        #region Initial Objects
        private readonly LogInInfo _logInInfo;
        private readonly IInvoiceInfoManager _invoiceInfoManager;
        private readonly IPurchaseInvoiceManager _purchaseInvoiceInfoManager;
        private readonly IInvoiceDetailsManager _invoiceDetailsManager;
        private readonly IProductManager _productManager;
        #endregion

        public PurchaseController(HttpContext httpContext, IInvoiceInfoManager invoiceInfoManager, IInvoiceDetailsManager invoiceDetailsManager, IPurchaseInvoiceManager purchaseInvoiceInfoManager, IProductManager productManager)
        {
            _invoiceInfoManager = invoiceInfoManager;
            _invoiceDetailsManager = invoiceDetailsManager;
            _productManager = productManager;
            _invoiceInfoManager = invoiceInfoManager;
            _purchaseInvoiceInfoManager = purchaseInvoiceInfoManager;

            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }
        public ActionResult PurchaseEntryList()
        {
            var model = new PuchaseListSearchModel
            {
                Id = 0,
                FromDate = DateTime.Today.AddDays(-30).ToString("dd/MMMM/yyyy"),
                ToDate = DateTime.Today.AddDays(1).ToString("dd/MMMM/yyyy")
            };
            DateTime fromDate = Convert.ToDateTime(model.FromDate);
            DateTime toDate = Convert.ToDateTime(model.ToDate);
            var invoiceList = _invoiceInfoManager.GetInvoiceInfoList(PropertyConstants.Purchase, "", fromDate,
                toDate);
            //For Displaying Bonus Invoice In PurchaseReturn Invoice
            invoiceList.AddRange(_invoiceInfoManager.GetInvoiceInfoList(PropertyConstants.PurchaseReturn, "", fromDate,
                toDate));
            //For Displaying Bonus Invoice In Purchase Invoice
            invoiceList.AddRange(_invoiceInfoManager.GetInvoiceInfoList(PropertyConstants.ProfitInvoice, "", fromDate,
                toDate));
            //For Displaying Damage Invoice In Purchase Invoice
            invoiceList.AddRange(_invoiceInfoManager.GetInvoiceInfoList(PropertyConstants.LostInvoice, "", fromDate,
                toDate));
            ViewBag.BillList = invoiceList;
            return View(model);
        }
        public ActionResult SavePurchaseEntry(long billId = 0)
        {
            ViewBag.PartySelectList = DropDownListClass.GetParties(PropertyConstants.Supplier);
            ViewBag.CategorySelectList = DropDownListClass.GetCategories();
            ViewBag.ModelSelectList = DropDownListClass.GetProducts();
            ViewBag.PropertySelectList = DropDownListClass.GetInvoiceTypes("PurchaseInvoiceType");
            ViewBag.CurrencySelectList = DropDownListClass.GetCurrencies();
            PartialInvoiceInfo partialInvoiceInfo;
            if (billId > 0)
            {
                partialInvoiceInfo = _invoiceInfoManager.GetInvoiceByInvoiceId(billId);
                partialInvoiceInfo.IsEdit = true;
            }
            else
            {
                partialInvoiceInfo = new PartialInvoiceInfo
                {
                    InvoiceNo = _invoiceInfoManager.GetDateGeneratedInvoiceNo(PropertyConstants.Purchase),
                    InvoiceDate = DateTime.Today,
                    PropertyId = PropertyConstants.Purchase,
                    IsEdit=false,
                    InvoiceDetailsList = new List<InvoiceDetailViewModel>()
                };
            }
            return View(partialInvoiceInfo);
        }

        [HttpPost]
        public JsonResult OnChangLoadProduct(long param)
        {
            ViewBag.InvoiceType = PropertyConstants.Purchase;
            var products = _productManager.GetModelsByCategory(param);
            return Json(products, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult SavePurchaseEntry(PartialInvoiceInfo param, List<InvoiceDetailViewModel> InvoiceDetailsList)
        {
            if (param != null && InvoiceDetailsList.Count > 0)
                param.InvoiceDetailsList = InvoiceDetailsList;
            else
                return Json(AutoResponse.FailedMessageWithParam("Product List is not uploaded"));
            DbResponse result = default(DbResponse);
            if (param.Id > 0)
                result = _purchaseInvoiceInfoManager.EditPurchaseInvoice(param, _logInInfo);
            else
            {
                result = _purchaseInvoiceInfoManager.AddPurchaseInvoice(param, _logInInfo);
            }

            return Json(result);
        }
       
        [HttpPost]
        public JsonResult LoadProduct(string productId)
        {
            return string.IsNullOrEmpty(productId) ?
                Json("", JsonRequestBehavior.AllowGet) :
                Json(new { isSuccess = true, Product = _productManager.GetProduct(Convert.ToInt64(productId)) });
        }
        public ActionResult LoadInvoiceInfoDetail(long invoiceInfoId)
        {

            IList<InvoiceDetailViewModel> detailLis = _invoiceDetailsManager.GetInvoiceDetails(invoiceInfoId);
            ViewBag.GiftList = DropDownListClass.GetGiftProperty();

            var view = "";
            foreach (var invoiceDetailViewModel in detailLis)
            {

                view += PartialView("~/Views/Purchase/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
                    .RenderToString();
            }

            var result = Json(view, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [HttpPost]
        public ActionResult GetNewInvoiceInfoDetail(long? invoiceInfoId, long productId)
        {
            try
            {
                
                //=============
                PartialProduct product = _productManager.GetProduct(productId);
                var newInvoiceDetail = new InvoiceDetailViewModel
                {
                    InvoiceId = invoiceInfoId.GetValueOrDefault(),
                    Status = 1,
                    ProductId = productId,
                    ProductName = product.ProductName,
                    BuyingPrice = product.BuyingPrice,
                    Quantity = 1,
                    TotalPrice = (product.BuyingPrice),
                };
                return PartialView("~/Views/Purchase/Partial/_PartialInvoiceInfoDetail.cshtml", newInvoiceDetail);
            }
            catch (Exception ex)
            {
                throw;
            }

        }
        public JsonResult GetNewInvoiceInfoDetailBySerial(long invoiceId, long productId, int waranty, string serial)
        {
            try
            {
                if (productId < 1)
                    throw new Exception("No Product Is selected");
                IList<InvoiceDetailViewModel> detailLis = Helper.SerialExtractor.SerialRead(invoiceId,productId, serial, waranty);
                ViewBag.GiftList = DropDownListClass.GetGiftProperty();

                var view = "";
                foreach (var invoiceDetailViewModel in detailLis)
                {

                    view += PartialView("~/Views/Purchase/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
                        .RenderToString();
                }
                var response = AutoResponse.SuccessMessageWithValue("Loaded Successfully", view);
                var result = Json(response, JsonRequestBehavior.AllowGet);
                result.MaxJsonLength = int.MaxValue;
                return result;
            }
            catch (Exception ex)
            {
                var msg = AutoResponse.FailedMessageWithParam(ex.Message);
                return Json(msg, JsonRequestBehavior.AllowGet);
            }

        }
        //public JsonResult GetNewInvoiceInfoDetailByExcel(long? invoiceInfoId, long productId)
        //{
        //    try
        //    {
        //        if (productId < 1)
        //            throw new Exception("No Product Is selected");
        //        IList<InvoiceDetailViewModel> detailLis = Helper.ExcelReader.CSVReader(invoiceInfoId, productId, _productManager);
        //        ViewBag.GiftList = DropDownListClass.GetGiftProperty();

        //        var view = "";
        //        foreach (var invoiceDetailViewModel in detailLis)
        //        {

        //            view += PartialView("~/Views/Purchase/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
        //                .RenderToString();
        //        }
        //        var response = AutoResponse.SuccessMessageWithValue("Loaded Successfully", view);
        //        var result = Json(response, JsonRequestBehavior.AllowGet);
        //        result.MaxJsonLength = int.MaxValue;
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        var msg = AutoResponse.FailedMessageWithParam(ex.Message);
        //        return Json(msg, JsonRequestBehavior.AllowGet);
        //    }

        //}
        [HttpPost]
        public ActionResult GetProductListByCategory(long categoryId)
        {
            ViewBag.ProductList = DropDownListClass.GetProductListByCategoryAndType(categoryId);
            if (ViewBag.ProductList.Items.Count < 2)
                return null;
            return PartialView("~/Views/Shared/Partial/_ProductsOptionPartial.cshtml");
        }
        [HttpPost]
        public JsonResult DeleteInvoiceInfo(string id)
        {
            long invoiceId;
            long.TryParse(id, out invoiceId);
            var result = new DbResponse();
            if (invoiceId > 0)
            {
                result = _invoiceInfoManager.DeleteInvoiceInfo(invoiceId, _logInInfo);
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}