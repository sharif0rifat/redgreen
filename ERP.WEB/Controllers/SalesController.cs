﻿using System.Collections.Generic;
using System.Web.UI.WebControls.WebParts;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using ERP.REPOSITORIES.Repositories;
using ERPSales.Web.Helper;


namespace ERP.WEB.Controllers
{

    public class SalesController : Controller
    {
        #region Sales Setup

        public const String Name = "Sales";
        public const String EditInvoiceActionName = "Edit";
        public const String DeleteInvoiceActionName = "Delete";
        private readonly LogInInfo _logInInfo;
        private readonly IInvoiceInfoManager _invoiceInfoManager;
        private readonly ISalesInvoiceManager _salesInvoiceManagerManager;
        private readonly IInvoiceDetailsManager _invoiceDetailsManager;
        private readonly IProductManager _productManager;
        private readonly long _salesInvoicePropertyId;

        #endregion
        public SalesController(IInvoiceDetailsManager invoiceDetailsManager, HttpContext httpContext, IInvoiceInfoManager invoiceInfoManager, ISalesInvoiceManager salesInvoiceManagerManager, IProductManager productManager)
        {
            _productManager = productManager;
            _invoiceDetailsManager = invoiceDetailsManager;
            _salesInvoiceManagerManager = salesInvoiceManagerManager;
            _invoiceInfoManager = invoiceInfoManager;
            _salesInvoicePropertyId = PropertyConstants.Sales;

            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }

        public ActionResult InvoiceList(string invoiceNo = "", DateTime? datefrom = null, DateTime? dateto = null)
        {
            ViewBag.ControllerName = SalesController.Name;
            if (invoiceNo != String.Empty)
            {
                ViewBag.InvoiceList = _invoiceInfoManager.GetInvoiceInfoList(_salesInvoicePropertyId, invoiceNo);
            }
            else if (datefrom != null && dateto != null)
            {
                ViewBag.InvoiceList = _invoiceInfoManager.GetInvoiceInfoByDateRange(_salesInvoicePropertyId, datefrom ?? DateTime.Today, dateto ?? DateTime.Today);
            }
            else
            {
                var invoiceList = _invoiceInfoManager.GetInvoiceInfoList(_salesInvoicePropertyId);
                invoiceList.AddRange(_invoiceInfoManager.GetInvoiceInfoList(PropertyConstants.SalesReturn));
                // sales Replace to display in invoice list
                invoiceList.AddRange(_invoiceInfoManager.GetInvoiceInfoList(PropertyConstants.SalesReplace));
                ViewBag.InvoiceList = invoiceList;
            }

            return View("~/Views/Sales/SalesInvoiceList.cshtml");
        }

        public ActionResult AddSalesInvoice(long billId = 0)
        {
            ViewBag.ControllerName = SalesController.Name;
            ViewBag.PartySelectList = DropDownListClass.GetPartiesForSales(PropertyConstants.Dealer);
            ViewBag.CategorySelectList = DropDownListClass.GetCategories();
            ViewBag.PropertySelectList = DropDownListClass.GetInvoiceTypes("SalesInvoiceType");
            ViewBag.CurrencySelectList = DropDownListClass.GetCurrencies();
            ViewBag.SalesTypeSelectList = DropDownListClass.SalesTypes();
            ViewBag.GiftList = DropDownListClass.GetGiftProperty();
            PartialInvoiceInfo partialInvoiceInfo;
            if (billId > 0)
            {
                partialInvoiceInfo = _invoiceInfoManager.GetInvoiceByInvoiceId(billId);
                partialInvoiceInfo.IsEdit = true;
            }
            else
            {
                partialInvoiceInfo = new PartialInvoiceInfo
                {
                    InvoiceNo = _invoiceInfoManager.GetDateGeneratedInvoiceNo(PropertyConstants.Sales),
                    InvoiceDate = DateTime.Now,
                    PropertyId = PropertyConstants.Purchase,
                    IsEdit = false,
                    InvoiceDetailsList = new List<InvoiceDetailViewModel>()
                };
            }
            return View(partialInvoiceInfo);
        }

        [HttpPost]
        public JsonResult AddSalesInvoice(PartialInvoiceInfo param, List<InvoiceDetailViewModel> InvoiceDetailsList)
        {
            param.InvoiceDetailsList = InvoiceDetailsList;
            DbResponse result;
            if (param.Id > 0)
                result = _salesInvoiceManagerManager.EditSalesInvoice(param, _logInInfo);
            else
            {
                result = _salesInvoiceManagerManager.AddSalesInvoice(param, _logInInfo);
            }

            return Json(result);
        }

        public ActionResult Edit(long invoiceId)
        {
            ViewBag.ControllerName = SalesController.Name;
            ViewBag.PartySelectList = DropDownListClass.GetParties(PropertyConstants.Dealer);
            ViewBag.CategorySelectList = DropDownListClass.GetCategories();
            PartialInvoiceInfo partialInvoiceInfo = _invoiceInfoManager.GetInvoiceInfo(invoiceId);
            //partialInvoiceInfo.InvoiceDetailsList = _invoiceDetailsManager.GetInvoiceDetails(invoiceId);

            return View("~/Views/Shared/_AddInvoice.cshtml", partialInvoiceInfo);
        }

        [HttpPost]
        public JsonResult DeleteInvoiceInfo(long id)
        {
            var response = _salesInvoiceManagerManager.DeleteInvoice(id, _logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        public ActionResult LoadInvoiceInfoDetail(long invoiceInfoId)
        {

            IList<InvoiceDetailViewModel> detailLis = _invoiceDetailsManager.GetInvoiceDetails(invoiceInfoId);
            ViewBag.GiftList = DropDownListClass.GetGiftProperty();

            var view = "";
            foreach (var invoiceDetailViewModel in detailLis)
            {

                view += PartialView("~/Views/Sales/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
                    .RenderToString();
            }

            var result = Json(view, JsonRequestBehavior.AllowGet);
            result.MaxJsonLength = int.MaxValue;
            return result;
        }
        [HttpPost]
        public ActionResult GetNewInvoiceInfoDetail(long? invoiceInfoId, long productId)
        {
            PartialProduct product = _productManager.GetProduct(productId);
            ViewBag.GiftList = DropDownListClass.GetGiftProperty();
            InvoiceDetailViewModel newInvoiceDetail = new InvoiceDetailViewModel
            {
                InvoiceId = invoiceInfoId.GetValueOrDefault(),
                Status = 1,
                ProductId = productId,
                ProductName = product.ProductName,
                SellingPrice = product.SellingPrice,
                Quantity = 1,
                TotalPrice = (product.SellingPrice),
            };
            return PartialView("~/Views/Sales/Partial/_PartialInvoiceInfoDetail.cshtml", newInvoiceDetail);
        }
        [HttpPost]
        public JsonResult GetNewInvoiceInfoDetailBySerial(long invoiceId, long? productId, int? waranty, string serial)
        {
            try
            {
                if (productId==null || productId < 1)
                    throw new Exception("No Product Is selected");
                IList<InvoiceDetailViewModel> detailLis = Helper.SerialExtractor.SerialRead(invoiceId, productId??0, serial, waranty??0);
                ViewBag.GiftList = DropDownListClass.GetGiftProperty();

                var view = "";
                foreach (var invoiceDetailViewModel in detailLis)
                {
                    view += PartialView("~/Views/Sales/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
                        .RenderToString();
                }
                var response = AutoResponse.SuccessMessageWithValue("Loaded Successfully", view);
                var result = Json(response, JsonRequestBehavior.AllowGet);
                result.MaxJsonLength = int.MaxValue;
                return result;
            }
            catch (Exception ex)
            {
                var msg = AutoResponse.FailedMessageWithParam(ex.Message);
                return Json(msg, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult GetNewInvInfoDetailBySerial(string serialNo, long invoiceInfoId)

        {
            var detail = _invoiceDetailsManager.GetInvoiceDetailsBySerial(serialNo);
            PartialProduct product = _productManager.GetProduct(detail.ProductId);
            ViewBag.GiftList = DropDownListClass.GetGiftProperty();
            InvoiceDetailViewModel newInvoiceDetail = new InvoiceDetailViewModel
            {
                //InvoiceId = invoiceInfoId.GetValueOrDefault(),
                Status = 1,
                ProductId = product.Id,
                ProductName = product.ProductName,
                SellingPrice = product.SellingPrice,
                Quantity = 1,
                ProductSerial = detail.ProductSerial,
                Warranty = detail.Warranty,
                TotalPrice = (product.SellingPrice),
            };
            return PartialView("~/Views/Sales/Partial/_PartialInvoiceInfoDetail.cshtml", newInvoiceDetail);
        }
        //public JsonResult GetNewInvoiceInfoDetailByExcel(long? invoiceInfoId, long productId)
        //{
        //    try
        //    {
        //        if (productId < 1)
        //            throw new Exception("No Product Is selected");
        //        IList<InvoiceDetailViewModel> detailLis = Helper.ExcelReader.CSVReader(invoiceInfoId, productId, _productManager);
        //        ViewBag.GiftList = DropDownListClass.GetGiftProperty();

        //        var view = "";
        //        foreach (var invoiceDetailViewModel in detailLis)
        //        {

        //            view += PartialView("~/Views/Sales/Partial/_PartialInvoiceInfoDetail.cshtml", invoiceDetailViewModel)
        //                .RenderToString();
        //        }
        //        var response = AutoResponse.SuccessMessageWithValue("Loaded Successfully", view);
        //        var result = Json(response, JsonRequestBehavior.AllowGet);
        //        result.MaxJsonLength = int.MaxValue;
        //        return result;
        //    }
        //    catch (Exception ex)
        //    {
        //        var msg = AutoResponse.FailedMessageWithParam(ex.Message);
        //        return Json(msg, JsonRequestBehavior.AllowGet);
        //    }
        //}
        [HttpPost]
        public ActionResult GetWarantyBySerial(string serialNo)
        {
            var waranty = _salesInvoiceManagerManager.GetWarantyBySerial(serialNo);
            return Json(waranty);
        }
        [HttpPost]
        public ActionResult GetPartyInfo(long partyId)
        {
            IPartyManager partyManager = new PartyManager();
            var party = partyManager.GetParty(partyId);
            return Json(party);
        }
        [HttpPost]
        public ActionResult GetProductListByCategory(long categoryId)
        {
            ViewBag.ProductList = DropDownListClass.GetProductListByCategoryAndType(categoryId);

            if (ViewBag.ProductList.Items.Count < 2)
                return null;
            else
                return PartialView("~/Views/Shared/Partial/_ProductsOptionPartial.cshtml");
        }

        private string RenderPartialToString(string viewPath, object viewData)
        {
            string partialViewString = string.Empty;
            ViewData.Model = viewData;
            using (var stringWriter = new StringWriter())
            {
                ViewEngineResult viewResult = ViewEngines.Engines.FindPartialView(ControllerContext, viewPath);
                ViewContext viewContext = new ViewContext(ControllerContext, viewResult.View,
                                             ViewData, TempData, stringWriter);
                viewResult.View.Render(viewContext, stringWriter);
                viewResult.ViewEngine.ReleaseView(ControllerContext, viewResult.View);
                partialViewString = stringWriter.GetStringBuilder().ToString();
            }
            return partialViewString;
        }
    }
}