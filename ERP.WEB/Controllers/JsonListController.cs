﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.WEB.Controllers
{
    public class JsonListController : Controller
    {
        private readonly IProductManager _productManager;

        public JsonListController(IProductManager productManager)
        {
            _productManager = productManager;
        }

        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public JsonResult LoadProductByCategory(string param)
        {
            if (param == "") param = "0";
            return Json(_productManager.GetModelsByCategory(Convert.ToInt64(param)), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadProductStock(string param)
        {
            return Json(_productManager.GetIndividualProductStock(Convert.ToInt64(param)), JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult LoadProductStockSell(string param)
        {
            return Json(_productManager.GetIndividualProductStockSell(Convert.ToInt64(param)), JsonRequestBehavior.AllowGet);
        }
    }
}