﻿using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.WEB.Controllers
{
    public class HomeController : Controller
    {
        //private readonly ICurrencyManager _currencyManager;
        //public HomeController(ICurrencyManager currencyManager)
        //{
        //    _currencyManager = currencyManager;
        //}
        public ActionResult Index()
        {
            var session = System.Web.HttpContext.Current.Session;
            var root = new LogInInfo
            {
                UserId = 0,
                UserName = ConstantValue.Root,

                TemplateId = 0,
                Islogin = true,
                IpAddress = "32132132131321",
                Permissions = new List<PartialPermission> { new PartialPermission { Title = "All", PermissionCode = "ALL" } },
                ApplicationId = 0,
                ApplicationName = ConstantValue.Root
            };
            session["loginInfo"] = root;
            // For testing
            // var currencies = _currencyManager.GetCurrencies();
            //return RedirectToAction("SavePurchaseEntry", "Purchase");
            //return RedirectToAction("AddSalesInvoice", "Sales");
            return View();
        }

        public ActionResult AuthFailed()
        {
            return View();
        }
    }
}