﻿using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Mvc;
using Payment = ERP.INFRASTRUCTURE.Payment;

namespace ERP.WEB.Controllers
{
    public class PaymentController : Controller
    {
        #region payment Setup

        private readonly IPaymentManager _paymentManager;
        private readonly LogInInfo _logInInfo;

        #endregion
        public PaymentController(IPaymentManager paymentManager, HttpContext httpContext)
        {
            _paymentManager = paymentManager;
            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }
        public ActionResult DealerPaymentModule(long? partyId = null, DateTime? datefrom = null, DateTime? datetto = null, string searchCriteria = "")
        {
            ViewBag.PaymentType = PropertyConstants.Receive;// (int)Constants.TransactionType.Receive;
            ViewBag.PartyType = PropertyConstants.Dealer;
            ViewBag.GetPaymentStatus = DropDownListClass.GetPaymentStatus();
            ViewBag.Title = "Dealer Payment Module";
            List<Payment> paymentInfo = new List<Payment>();
            if (!string.IsNullOrWhiteSpace(searchCriteria))
                paymentInfo = _paymentManager.GetPaymentInfoByStatus(PropertyConstants.Receive, long.Parse(searchCriteria));
            else
                paymentInfo = _paymentManager.GetPaymentsByDate(partyId, PropertyConstants.Receive, datefrom, datetto);

            return View("PaymentModule", paymentInfo);
        }
        public ActionResult SupplierPaymentModule(long? partyId = null, DateTime? datefrom = null, DateTime? datetto = null, string searchCriteria = "")
        {
            ViewBag.PaymentType = PropertyConstants.Payment;// (int)Constants.TransactionType.Payment;
            ViewBag.PartyType = PropertyConstants.Supplier;
            ViewBag.GetPaymentStatus = DropDownListClass.GetPaymentStatus();
            ViewBag.Title = "Supplier Payment Module";
            List<Payment> paymentInfo = new List<Payment>();
            if (!string.IsNullOrWhiteSpace(searchCriteria))
                paymentInfo = _paymentManager.GetPaymentInfoByStatus(PropertyConstants.Payment, long.Parse(searchCriteria));
            else
                paymentInfo = _paymentManager.GetPaymentsByDate(partyId, PropertyConstants.Payment, datefrom, datetto);

            return View("SupplierModule", paymentInfo);
        }
        public ActionResult SavePayment(int paymentType = 0, long partyType = 0, long id = 0)
        {
            ViewBag.ControllerName = "Payment";
            var payment = new Payment { Date = DateTime.Today,CheckDate = DateTime.Today};
            ViewBag.TransactionType = paymentType;
            ViewBag.LedgerList = DropDownListClass.GetPaymentSubHeaderList();
            ViewBag.PaymentType = paymentType;
            ViewBag.PartyType = partyType;
            ViewBag.PartySelectList = DropDownListClass.GetParties(partyType);
            ViewBag.GetPaymentStatus = DropDownListClass.GetPaymentStatus();
            ViewBag.CurrencySelectList = DropDownListClass.GetCurrencies();
            if (id > 0)
            {
                payment = _paymentManager.GetPaymentById(id) ?? new Payment();
            }
            return View("SavePayment", payment);
        }
        [HttpPost]
        public ActionResult SavePayment(Payment param)
        {
            DbResponse result;
            if (param.Id > 0)
                result = _paymentManager.EditPayment(param, _logInInfo);
            else
            {
                result = _paymentManager.AddPayment(param, _logInInfo);
            }
            if (param.PaymentType == PropertyConstants.Receive)
                return RedirectToAction("DealerPaymentModule");
            else return RedirectToAction("SupplierPaymentModule");
        }

        public JsonResult DeletePayment(long id)
        {
            var logInInfo = (LogInInfo)Session["loginInfo"];
            var response = _paymentManager.DeletePayment(id, logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
    }
}