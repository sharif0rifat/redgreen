﻿using System;
using System.Web.Mvc;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.IRepositories;
using ERP.REPOSITORIES.System256;
using User = ERP.INFRASTRUCTURE.User;

namespace ERP.WEB.Controllers
{

    public class AccountController : Controller
    {
        #region Account Setup

        private const string UserPartialUri = "~/Views/Shared/Modals/_AddUser.cshtml";
        private const string ChangePasswordPartialUri = "~/Views/Shared/Modals/_ChangePassword.cshtml";
        
        private readonly IAuthRepo _authRepo;
        private readonly ITemplateRepo _templateRepo;
        private readonly IUserRepo _userRepo;

        public AccountController(ITemplateRepo templateRepo, IAuthRepo authRepo, IUserRepo userRepo)
        {
            _templateRepo = templateRepo;
            _authRepo = authRepo;
            _userRepo = userRepo;
        }
        #endregion
        #region User
        public ActionResult UserList()
        {
            var users = _userRepo.GetUsers();
            var logInInfo = (LogInInfo) Session["loginInfo"];
            ViewBag.UserName = logInInfo.UserName;
            return View(users);
        }
        public ActionResult SaveUser(long id = 0)
        {
            ViewBag.RoleList = _templateRepo.GetTemplates();
            //ViewBag.ProfileList = _branchManager.GetProfiles(true,0, new List<Profile>(), _dbEntities) ?? new List<Profile>();
            if (id <= 0) return PartialView(UserPartialUri);
            var user = _userRepo.GetUser(id).ReturnValue;
            return PartialView(UserPartialUri, user);
        }
        [HttpPost]
        public ActionResult SaveUser(PartialUser model)
        {
            //if id holds 0, it's mean add data to db. else update

            var response = _userRepo.AddUser(model);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult DeleteUser(long id, int status)
        {
            var dbResponse = _userRepo.DeleteUser(id);
            TempData["message"] = dbResponse.MessageData;
            return RedirectToAction("UserList");

        }
        public ActionResult ChangePassword(long id)
        {
            var user = _userRepo.GetUser(id);
            return PartialView(ChangePasswordPartialUri, user);
        }
        [HttpPost]
        public ActionResult ChangePassword(User model)
        {
            var response = _authRepo.ResetPasswordRequest(model.Id);
            return Json(response, JsonRequestBehavior.DenyGet);
        }
        #endregion
        public ActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public JsonResult Login(PartialUser partialUser)
        {
            var appId = Convert.ToInt64(System.Configuration.ConfigurationManager.AppSettings["AppId"]);
            var response = _authRepo.Login(partialUser.UserName, partialUser.Password, appId);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        public ActionResult Logout()
        {
            var loginInfo  = (LogInInfo)Session["loginInfo"];
            _authRepo.Logout(loginInfo.UserId);
            Session["loginInfo"] = null;
            return RedirectToAction("Login", "Account");
        }

        [HttpPost]
        public JsonResult PasswordGenerator()
        {
            var generatedPassword = System128.PasswordGenerator();
            return Json(generatedPassword);
        }
    }
}