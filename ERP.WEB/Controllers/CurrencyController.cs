﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ERP.DAL.Model;
using ERP.WEB.Models;
using ERP.REPOSITORIES.IRepositories;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;

namespace ERP.WEB.Controllers
{
    public class CurrencyController : Controller
    {
        private readonly ICurrencyManager _currencyManager;
        public const string SaveCurrencyPartial = "Partial/_SaveCurrency";
        public CurrencyController(ICurrencyManager currencyManager)
        {
            _currencyManager = currencyManager;
        }

        // GET: Currency
        #region Currency Settings
        public ActionResult CurrencyList()
        {
            ViewBag.CurrencyList = _currencyManager.GetCurrencies();
            return View();
        }
        //Add New Currency
        [HttpGet]
        public ActionResult Register(string param)
        {
            var currency = new PartialCurrency();
            if (param == Constants.EmptyString)
            {
                return PartialView(SaveCurrencyPartial, currency);
            }
            currency = _currencyManager.GetCurrency(Convert.ToInt32(param));
            currency.IsEdit = true;
            return PartialView(SaveCurrencyPartial, currency);
        }
        [HttpPost]
        public JsonResult Register(PartialCurrency currency)
        {
            var logInInfo = (LogInInfo)Session["loginInfo"];
            if (currency.ID > 0)
            {
                currency.ModifiedBy = logInInfo.UserId;
                currency.ModifiedDate = DateTime.Today;
            }
            else
            {
                currency.CreatedBy = logInInfo.UserId;
                currency.CreatedDate = DateTime.Today;
            }
            var response = _currencyManager.AddCurrency(currency);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Delete(long id)
        {
            var logInInfo = (LogInInfo)Session["loginInfo"];
            var response = _currencyManager.DeleteCurrency(id, logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }

}
