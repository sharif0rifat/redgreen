﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.WEB.Controllers
{
    public class CategoryController : Controller
    {
        private readonly ICategoryManager _categoryManager;
        public const string SaveCategoryPartial = "Partial/_SaveCategory";

        public CategoryController(ICategoryManager categoryManager)
        {
            _categoryManager = categoryManager;
        }

        // GET: Category
        #region Category Settings
        public ActionResult List()
        {
            ViewBag.CategoryList = _categoryManager.GetCategories();
            return View();
        }
        [HttpGet]
        public ActionResult Register(string param)
        {
            var category = new PartialCategory();
            if (param == Constants.EmptyString)
            {
                return PartialView(SaveCategoryPartial, category);
            }
            category = _categoryManager.GetCategory(Convert.ToInt64(param));
            category.IsEdit = true;
            return PartialView(SaveCategoryPartial, category);
        }
        [HttpPost]
        public JsonResult Register(PartialCategory category)
        {
            var logInInfo = (LogInInfo) Session["loginInfo"];
            if (category.Id > 0)
            {
                category.ModifiedBy = logInInfo.UserId;
                category.ModifiedDate = DateTime.Today;
            }
            else
            {
                category.CreatedBy = logInInfo.UserId;
                category.CreatedDate = DateTime.Today;
            }
            var response = _categoryManager.AddCategory(category);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Delete(long id)
        {
            var logInInfo = (LogInInfo)Session["loginInfo"];
            var response = _categoryManager.DeleteCategory(id, logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}