﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using Microsoft.AspNet.Identity;

namespace ERP.WEB.Controllers
{
    public class UserCommonController : Controller
    {
        // GET: UserCommon
        private readonly IUserRepo _userRepo;
        private ITemplateRepo _templateRepo;

        public UserCommonController(IUserRepo userRepo, ITemplateRepo templateRepo)
        {
            _userRepo = userRepo;
            _templateRepo = templateRepo;
        }

        public ActionResult List()
        {
            var appId = Convert.ToInt64(System.Configuration.ConfigurationManager.AppSettings["AppId"]);
            var loginInfo  = (LogInInfo)Session["loginInfo"];
            ViewBag.UserList = _userRepo.GetUsers(loginInfo.UserName, appId);
            return View();
        }
        [HttpGet]
        public ActionResult Register(long paramId=0)
        {
            ViewBag.TemplateList = DropDownListClass.GetTemplates();
            if (paramId  ==0)
            {
                return View(new PartialUser());
            }
            var response = _userRepo.GetUser(paramId);
            if (response.MessageType != 1)
            {
                return View(new PartialUser());
            }
            var partialUser = response.ReturnValue;
            return View(partialUser);
        }

        [HttpPost]
        public JsonResult Register(PartialUser param)
        {
            var loginInfo = (LogInInfo)Session["loginInfo"];
            if (param.Id == 0)
            {
                param.CreatedBy = loginInfo.UserId;
                param.CreatedDate = DateTime.Today;
            }
            else
            {
                param.ModifiedBy = loginInfo.UserId;
                param.ModifiedDate = DateTime.Today;
            }
            var response = _userRepo.AddUser(param);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult LockRelease()
        //{
        //    var agent = (LogInInfo)Session["loginInfo"];
        //    ViewBag.UserList = _userRepo.GetUsers();
        //    ViewBag.GroupList = _groupRepo.GetGroups(agent.UserName, agent.ApplicationId);
        //    return View();
        //}
        //[HttpPost]
        //public JsonResult LockRelease(int param)
        //{
        //    var response = _userRepo.LockRelease(param);
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}
        //public ActionResult ResetPassword()
        //{
        //    var agent = (LogInInfo)Session["agent"];
        //    ViewBag.UserList = _userRepo.GetUsers();
        //    ViewBag.GroupList = _groupRepo.GetGroups(agent.UserName, agent.ApplicationId);
        //    return View();
        //}
        //[HttpPost]
        //public JsonResult ResetPassword(int param)
        //{
        //    var response = _userRepo.ResetPassword(param);
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}

        //public ActionResult ReleaseLogin()
        //{
        //    var agent = (LogInInfo)Session["agent"];
        //    ViewBag.UserList = _userRepo.GetUsers();
        //    ViewBag.GroupList = _groupRepo.GetGroups(agent.UserName, agent.ApplicationId);
        //    return View();
        //}
        //[HttpPost]
        //public JsonResult ReleaseLogin(int param)
        //{
        //    var response = _userRepo.ReleaseLogin(param);
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}
    }
}