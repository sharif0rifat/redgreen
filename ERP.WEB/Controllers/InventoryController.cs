﻿using System;
using System.Web;
using System.Web.Mvc;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using Employee = ERP.INFRASTRUCTURE.Employee;

namespace ERP.WEB.Controllers
{
    public class InventoryController : Controller
    {

        public const string SaveProuctPartial = "Partial/_SaveProductSettings";

        #region Inventory Setup
        private readonly LogInInfo _logInInfo;
        private readonly ICategoryManager _categoryManager;
        private readonly IProductManager _productManager;
        private readonly IStockManager _stockManager;
        private readonly IPartyManager _partyManager;
        private readonly IInvoiceInfoManager _invoiceInfoManager;

        #endregion

        public InventoryController(ICategoryManager categoryManager, IProductManager productManager, IStockManager stockManager, IPartyManager partyManager, IInvoiceInfoManager invoiceInfoManager, HttpContext httpContext)
        {
            _categoryManager = categoryManager;
            _productManager = productManager;
            _stockManager = stockManager;
            _partyManager = partyManager;
            _invoiceInfoManager = invoiceInfoManager;
            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }

        

        #region Product
        public ActionResult ProductSettingsList()
        {
            ViewBag.ProductList = _productManager.GetProducts();
            return View();
        }
        [HttpGet]
        public ActionResult SaveProductSettings(string param)
        {
            ViewBag.CategoryList = DropDownListClass.GetCategories();
            if (param == Constants.EmptyString)
            {
                return PartialView(SaveProuctPartial);
            }
            var product = _productManager.GetProduct(Convert.ToInt64(param));
            return PartialView(SaveProuctPartial, product);
        }
        [HttpPost]
        public JsonResult SaveProductSettings(PartialProduct param)
        {
            var response = _productManager.AddProduct(param, _logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteProductSettings(long id)
        {
            var response = _productManager.DeleteProduct(id,_logInInfo);
            return Json(response,JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Stock Entry
        public ActionResult StockEntryList()
        {
            var stockEntryList = _invoiceInfoManager.GetInvoiceInfoList();
            return View(stockEntryList);
        }
        //public ActionResult SaveStockEntry(long id = 0)
        //{
        //    ViewBag.CategoryList = DropDownListClass.GetCategories();
        //    return View();
        //}

        //[HttpPost]
        //public JsonResult OnChangLoadProduct(long param)
        //{
        //    var products = _productManager.GetModelsByCategory(param);
        //    return Json(products, JsonRequestBehavior.AllowGet);
        //}
        //[HttpPost]
        //public JsonResult SaveStockEntry(PartialProduct param)
        //{
        //    param.TablePropertyId = Convert.ToInt64(Constants.StockType.StockIn);
        //    var response = _invoiceInfoManager.AddToInvoice(param,_logInInfo);
        //    return Json(response, JsonRequestBehavior.AllowGet);
        //}
        #endregion

        #region Ajax Loading Functions
        
        
        //[HttpPost]
        //public ActionResult LoadCurrentStockAmount(long modelId)
        //{
        //    var currentStockAmount = 100;//_currentStockManager.GetCurrentStockQuantityByModel(modelId);
        //    return Json(new { isSuccess = true, currentAmount = currentStockAmount });
        //}
        [HttpPost]
        public ActionResult LoadQuantityForEdit(long modelId, long quantity, long billId)
        {
            var amount = _stockManager.LoadAmountByQuantityForEdit(modelId, quantity, billId);
            return Json(new { isSuccess = true, Amount = amount });
        }
        [HttpPost]
        public ActionResult LoadInvoiceNo(int salesType = 1)
        {

            var lastInvoiceNo = _invoiceInfoManager.LoadFinalInvoiceNo(salesType) + 1;
            return Json(new { isSuccess = true, InvoiceNo = lastInvoiceNo });

        }
        #endregion

        public ActionResult LoadAmountByQuantity(long modelId, long quantity)
        {
            var amount = _stockManager.LoadAmountByQuantity(modelId, quantity);
            return Json(new { isSuccess = true, Amount = amount });
        }
    }
}