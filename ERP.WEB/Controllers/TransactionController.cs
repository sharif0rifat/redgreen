﻿using ERP.DAL.Model;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Web;
using System.Web.Mvc;

namespace ERP.WEB.Controllers
{
    public class TransactionController : Controller
    {
        #region Transaction Setup
        private DbResponse _dbResponse;
        private readonly LogInInfo _logInInfo;
        private readonly ERPEntities _dbEntities;
        private readonly ITransactionManager _transactionManager;

        #endregion
        public TransactionController(HttpContext httpContext, DbResponse dbResponse, LogInInfo logInInfo, ERPEntities dbEntities, ITransactionManager transactionManager)
        {
            _dbResponse = dbResponse;
            _dbEntities = dbEntities;

            _transactionManager = transactionManager;
            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }

        public ActionResult TransactionList()
        {
            ViewBag.LedgerList = DropDownListClass.GetLedgerGroup();
            ViewBag.TransactionList = _transactionManager.GetTransactions();
            return View();
        }
        public ActionResult TransactionForm(long? id = 0)
        {
            ViewBag.CurrentDate = DateTime.Today.ToString(PropertyConstants.DateFormat);
            ViewBag.LedgerList = DropDownListClass.GetTransactionLedgrList();
            ViewBag.CurrencySelectList = DropDownListClass.GetCurrencies();
            ViewBag.TransactionType = 3;
            var transaction = new TransactionViewModel();
            if (id > 0)
                transaction = _transactionManager.GetTransaction(id ?? 0);
            return View(transaction);
        }

        [HttpPost]
        public JsonResult SaveTransactionForm(TransactionViewModel param)
        {
            if (param.CreditHeaderId == param.DebitHeaderId)
                return Json(AutoResponse.FailedMessage("Debit and Credit Ledger Cannot be same."), JsonRequestBehavior.AllowGet);
            param.LogInInfo = _logInInfo;
            var response = param.Id > 0 ? _transactionManager.EditTransaction(param) : _transactionManager.AddTransaction(param);
            return Json(response, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteTransaction(long id)
        {
            _dbResponse = _transactionManager.DeleteTransaction(id, _dbEntities);
            //if (_dbResponse.MessageType == 1)
            //{
            //    TempData["message"] = "Transaction Deleted";
            //}
            //else
            //{
            //    TempData["message"] = "Transaction Delete failed";
            //}
            return Json(_dbResponse, JsonRequestBehavior.AllowGet);
        }
        //public ActionResult LoadSubheaderByHeader(long headerId, int type)
        //{

        //    var subHeaders = _transactionHeaderManager.GetLedgerByHeader(_dbEntities, headerId, type);
        //    return Json(new { isSuccess = true, subHeaderList = subHeaders });

        //}

    }
}