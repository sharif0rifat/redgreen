﻿using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.IRepositories;
using System;
using System.Web.Mvc;

namespace ERP.WEB.Controllers
{
    public class EmployeeController : Controller
    {
        private readonly IEmployeeManager _employeeManager;
        public const string SaveEmployeePartial = "Partial/_SaveEmployee";

        public EmployeeController(IEmployeeManager employeeManager)
        {
            _employeeManager = employeeManager;
        }

        // GET: Employee
        #region Employee Settings
        public ActionResult List()
        {
            ViewBag.EmployeeList = _employeeManager.GetEmployees();
            return View();
        }
        [HttpGet]
        public ActionResult Register(string param)
        {
            var employee = new Employee();
            if (param == Constants.EmptyString)
            {
                return PartialView(SaveEmployeePartial, employee);
            }
            employee = _employeeManager.GetEmployee(Convert.ToInt64(param));
            employee.IsEdit = true;
            return PartialView(SaveEmployeePartial, employee);
        }
        [HttpPost]
        public JsonResult Register(Employee employee)
        {
            var response = default(DbResponse);
            var logInInfo = (LogInInfo)Session["loginInfo"];
            if (employee.Id > 0)
            {
                employee.ModifiedBy = logInInfo.UserId;
                employee.ModifiedDate = DateTime.Today;
                response = _employeeManager.EditEmployee(employee, logInInfo);
            }
            else
            {
                employee.CreatedBy = logInInfo.UserId;
                employee.CreatedDate = DateTime.Today;
                response = _employeeManager.AddEmployee(employee, logInInfo);
            }

            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult Delete(long id)
        {
            var logInInfo = (LogInInfo)Session["loginInfo"];
            var response = _employeeManager.DeleteEmployee(id, logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}