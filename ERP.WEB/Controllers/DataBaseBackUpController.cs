﻿using System.Web.Configuration;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Repositories;
using System.Web.Mvc;

namespace ERP.WEB.Controllers
{
    public class DataBaseBackUpController : Controller
    {
        //
        // GET: /DataBaseBackUp/
        public ActionResult Index()
        {
            ViewData["Message"] = "";
            return View();
        }

        public ActionResult DBBackUp()
        {
            string backUpFolder = WebConfigurationManager.AppSettings["BackupPath"];
            var manager = new BackUpManager();
            var dbResponse = manager.TakeBackUp(backUpFolder);

            if (dbResponse.MessageType == 1)
                ViewData["Message"] = "success";
            {
                ViewData["SuccessMessage"] = "Database back on folder:  " + backUpFolder;
                return Json(dbResponse, JsonRequestBehavior.AllowGet);
            }
            return Json(new { Success = false, FlashLabel = "Fail", FlashMessage = "DB Back Up fail." });

        }

        public JsonResult ClearAllData()
        {
            var manager = new BackUpManager();
            DbResponse dbResponse = manager.ClearAllData();
            return Json(dbResponse);
        }
    }
}