﻿using System;
using System.Web;
using System.Web.Mvc;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using Party = ERP.INFRASTRUCTURE.Party;
using Employee = ERP.INFRASTRUCTURE.Employee;

namespace ERP.WEB.Controllers
{
    public class SettingsController : Controller
    {
        #region Settings Setup

        private const string SaveEmployeePartial = "Partial/_SaveEmployee";
        private const string SavePartyPartial = "Partial/_SaveParty";
        private const string SaveBankPartial = "Partial/_SaveBank";
        private const string SaveAreaPartial = "Partial/_SaveArea";
        private DbResponse _dbResponse;
        private readonly LogInInfo _logInInfo;
        private readonly ERPEntities _dbEntities;

        private readonly IPartyManager _partyManager;
        private readonly IEmployeeManager _employeeManager;
        private readonly ILedgerGroupManager _transactionHeaderManager;

        #endregion
        public SettingsController(IPartyManager partyManager, IEmployeeManager employeeManager, HttpContext httpContext, DbResponse dbResponse, LogInInfo logInInfo, ERPEntities dbEntities, ILedgerGroupManager transactionHeaderManager)
        {
            _dbResponse = dbResponse;
            _dbEntities = dbEntities;
            _partyManager = partyManager;
            _employeeManager = employeeManager;
            _transactionHeaderManager = transactionHeaderManager;
            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }
        #region Employee
        public ActionResult EmployeeList()
        {
            ViewBag.EmployeeList = _employeeManager.GetEmployees();
            return View();
        }
        [HttpGet]
        public ActionResult SaveEmployee(string param)
        {
            //if (param == Constants.EmptyString)
            //{
            //    return PartialView(SaveEmployeePartial, new Employee { DateOfJoining = DateTime.Today });
            //}
            //var obj = _employeeManager.GetEmployee(Convert.ToInt64(param));
            //obj.IsEdit = true; //** obj.IsEdit 
            //return PartialView(SaveEmployeePartial, obj);
            return null;
        }

        [HttpPost]
        public JsonResult SaveEmployee(Employee obj)
        {
            // addEmployee also work for update or edit.. by sabbir
            var response = _employeeManager.AddEmployee(obj, _logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteEmployee(long id)
        {
            var response = _employeeManager.DeleteEmployee(id, _logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region  Party
        public ActionResult SupplierPartyList()
        {
            ViewBag.PartyType = PropertyConstants.Supplier;
            ViewBag.PartyList = _partyManager.GetPartiesByType((int)Constants.PartyType.SupplierParty);
            return View("PartyList");
        }
        public ActionResult DealerPartyList()
        {
            ViewBag.PartyType = PropertyConstants.Dealer;
            ViewBag.PartyList = _partyManager.GetPartiesByType((int)Constants.PartyType.DealerParty);
            return View("PartyList");
        }
        [HttpGet]
        public ActionResult SaveParty(string param)
        {
            // param contain "-1^1" or "-1^2" or <any id>^partyType
            var paramList = param.Split('^');
            ViewBag.partyType = paramList[1] == PropertyConstants.Supplier.ToString() ? "Supplier" : "Client";

            if (paramList[0] == Constants.EmptyString)
            {
                return PartialView(SavePartyPartial, new LedgerViewModel { PropertyId = Convert.ToInt32(paramList[1]) });
            }
            var obj = _partyManager.GetParty(Convert.ToInt64(paramList[0]));
            obj.PropertyId = Convert.ToInt32(obj.PropertyId);
            return PartialView(SavePartyPartial, obj);
        }

        [HttpPost]
        public JsonResult SaveParty(LedgerViewModel obj)
        {
            var response = _partyManager.AddParty(obj, _logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public JsonResult DeleteParty(string id)
        {
            long partyId;
            long.TryParse(id, out partyId);
            var response = _partyManager.DeleteParty(partyId, _logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);
        }
        #endregion

    }
}