﻿using System;
using ERP.INFRASTRUCTURE;
using ERP.REPOSITORIES.Helper;
using System.Web.Mvc;

namespace ERP.WEB.Controllers
{
    public class ReportController : Controller
    {
        
        // GET: Report
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SalesReport()
        {
            ViewBag.CategoryList = DropDownListClass.GetCategories();
            ViewBag.ProductList = DropDownListClass.GetProducts();
            ViewBag.PartySelectList = DropDownListClass.GetParties(PropertyConstants.Dealer);
            var criteria = new SearchCriteria {StartDate = DateTime.Today.AddDays(-30), EndDate = DateTime.Today};
            return View(criteria);
        }
        public ActionResult PurchaseReport()
        {
            ViewBag.CategoryList = DropDownListClass.GetCategories();
            ViewBag.ProductList = DropDownListClass.GetProducts();
            ViewBag.PartySelectList = DropDownListClass.GetParties(PropertyConstants.Supplier);
            var criteria = new SearchCriteria {StartDate = DateTime.Today.AddDays(-30), EndDate = DateTime.Today};
            return View(criteria);
        }
        public ActionResult InvoiceReport()
        {
            ViewBag.CategoryList = DropDownListClass.GetCategories();
            ViewBag.ProductList = DropDownListClass.GetProducts();
            ViewBag.PartySelectList = DropDownListClass.GetParties(PropertyConstants.Dealer);
            var criteria = new SearchCriteria { StartDate = DateTime.Today.AddDays(-30), EndDate = DateTime.Today };
            return View(criteria);
        }
        public ActionResult OpeningStock()
        {
            return View();
        }
        public ActionResult ClosingStock()
        {
            return View();
        }
        public ActionResult AccountsLedger()
        {
            ViewBag.SubHeaderList = DropDownListClass.GetSubHeaderList();
            return View();
        }
        public ActionResult PartyLedger()
        
        {
            ViewBag.PartySelectList = DropDownListClass.GetParties();
            return View();
        }
        public ActionResult IncomeStatement()
        {
            return View();
        }
        public ActionResult BalanceSheet() 
        {
            return View();
        }
        public ActionResult TrialBalanceReport() 
        {
            return View();
        }

        public ActionResult StockListReport()
        {
            return View();
        }
    }
}