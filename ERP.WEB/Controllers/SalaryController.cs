﻿using System;
using System.Web;
using System.Web.Mvc;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;
using Salary = ERP.INFRASTRUCTURE.Salary;
namespace ERP.WEB.Controllers
{
    public class SalaryController : Controller
    {
        #region Salary Setup
        private DbResponse _dbResponse;
        private readonly LogInInfo _logInInfo;
        private readonly ERPEntities _dbEntities;

        private readonly IEmployeeManager _employeeManager;
        private readonly ILedgerGroupManager _transactionHeaderManager;

        private readonly ISalaryManager _salaryManager;
        private bool _flag = true;
        #endregion


        public SalaryController(IEmployeeManager employeeManager, HttpContext httpContext, DbResponse dbResponse, LogInInfo logInInfo, ERPEntities dbEntities, ILedgerGroupManager transactionHeaderManager, ISalaryManager salaryManager)
        {

            _dbResponse = dbResponse;
            _dbEntities = dbEntities;
            _employeeManager = employeeManager;
            _transactionHeaderManager = transactionHeaderManager;
            _salaryManager = salaryManager;
            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }

        public ActionResult SalaryList()
        {
            var list = _salaryManager.GetSalaries();
            return View(list);
        }
        public ActionResult SaveSalary(long id = 0)
        {
            if (id == 0)
            {
                ViewBag.EmpoyeeList = new SelectList(_employeeManager.GetEmployees(), "Id", "EmployeeName");
                ViewBag.LedgerList = DropDownListClass.GetPaymentSubHeaderList();
                ViewBag.SalaryTypeList = null;//Enum.GetValues(typeof(Constants.SalaryType)).Cast<Constants.SalaryType>().Select(v => new SelectListItem
                return PartialView("Partial/_SaveSalary", new Salary() { Month = DateTime.Today });
            }

            var obj = _salaryManager.GetSalary(id, _dbEntities);
            var employee = _employeeManager.GetEmployee(obj.EmployeeId);
            ViewBag.EmpoyeeList = new SelectList(_employeeManager.GetEmployees(), "Id", "EmployeeName", obj.EmployeeId);
            ViewBag.LedgerList = DropDownListClass.GetPaymentSubHeaderList();
            ViewBag.SalaryTypeList = null;//Enum.GetValues(typeof(Constants.SalaryType)).Cast<Constants.SalaryType>().Select(v => new SelectListItem
            //obj.SalryType = PropertyConstants.CurrentSalary;
            ViewBag.House = obj.HouseRent ?? employee.HouseRent ?? 0;
            ViewBag.Mobile = obj.MobileAllowance ?? employee.MobileInternet ?? 0;
            ViewBag.Transport = obj.TransportCost ?? employee.Transport ?? 0;
            ViewBag.FestivalBonus = obj.FestivalBonus;
            ViewBag.Basic = obj.BasicSalary ?? employee.BasicSalary ?? 0;
            obj.IsEdit = true;
            return PartialView("Partial/_SaveSalary", obj);
        }
        [HttpPost]
        public ActionResult SaveSalary(Salary salary, DateTime? FromDate = null, DateTime? ToDate = null)
        {
            FromDate = FromDate ?? DateTime.Today;
            ToDate = ToDate ?? DateTime.Today;
            try
            {
                salary.CommissionFromDate = FromDate;
                salary.CommissionToDate = ToDate;
                if (!salary.IsEdit)
                //if (false)
                {
                    ModelState.Remove("Id");
                    ModelState.Remove("IsEdit");
                    if (ModelState.IsValid)
                    {
                        salary.SalryType = PropertyConstants.CurrentSalary;
                        _dbResponse = _salaryManager.AddSalary(salary, _dbEntities);
                        return Json(_dbResponse);
                    }
                    return Json(AutoResponse.FailedMessage());
                }
                if (TryUpdateModel(salary))
                {

                    _dbResponse = _salaryManager.EditSalary(salary, _dbEntities);
                    return Json(_dbResponse);
                }
                return Redirect("SalaryList");

            }
            catch (Exception ex)
            {

                throw ex;
            }

        }
        public ActionResult DeleteSalary(long id)
        {
            _dbResponse = _salaryManager.DeleteSalary(id, _dbEntities);
            return Json(_dbResponse);

        }

        public ActionResult SaveAdvancedSalary(long id = 0)
        {
            if (id == 0)
            {
                ViewBag.EmpoyeeList = new SelectList(_employeeManager.GetEmployees(), "Id", "EmployeeName");
                ViewBag.LedgerList = DropDownListClass.GetPaymentSubHeaderList();
                ViewBag.SalaryTypeList = null;//Enum.GetValues(typeof(Constants.SalaryType)).Cast<Constants.SalaryType>().Select(v => new SelectListItem
                return PartialView("Partial/_SaveAdvancedSalary", new Salary() { Month = DateTime.Today, SalryType = (int)Constants.SalaryType.Advance });
            }

            var obj = _salaryManager.GetSalary(id, _dbEntities);
            var employee = _employeeManager.GetEmployee(obj.EmployeeId);
            ViewBag.EmpoyeeList = new SelectList(_employeeManager.GetEmployees(), "Id", "EmployeeName", obj.EmployeeId);
            ViewBag.LedgerList = DropDownListClass.GetPaymentSubHeaderList();
            ViewBag.SalaryTypeList = null;//Enum.GetValues(typeof(Constants.SalaryType)).Cast<Constants.SalaryType>().Select(v => new SelectListItem
            obj.SalryType = PropertyConstants.AdvanceSalary;
            ViewBag.Basic = obj.BasicSalary ?? employee.BasicSalary ?? 0;
            obj.IsEdit = true;
            return PartialView("Partial/_SaveAdvancedSalary", obj);
        }

        [HttpPost]
        public ActionResult SaveAdvancedSalary(Salary salary)
        {
            try
            {
                if (!salary.IsEdit)
                //if (false)
                {
                    ModelState.Remove("Id");
                    ModelState.Remove("IsEdit");
                    if (ModelState.IsValid)
                    {
                        salary.TotalSalary = salary.BasicSalary;
                        salary.SalryType = PropertyConstants.AdvanceSalary;
                        _dbResponse = _salaryManager.AddSalary(salary, _dbEntities);
                        return Json(_dbResponse);


                    }
                    return PartialView("Partial/_SaveAdvancedSalary", salary);
                }
                if (TryUpdateModel(salary))
                {

                    _dbResponse = _salaryManager.EditSalary(salary, _dbEntities);
                    return Json(_dbResponse);
                }
                return PartialView("Partial/_SaveAdvancedSalary", salary);

            }
            catch (Exception ex)
            {

                throw ex;
            }
            return View();
        }

        #region ajax
        [HttpPost]
        public ActionResult LoadAllowanceByEmployee(long employeeId)
        {
            try
            {
                var employee = _employeeManager.GetEmployee(employeeId);
                return Json(new { isSuccess = true, house = employee.HouseRent ?? 0, mobile = employee.MobileInternet ?? 0, transport = employee.Transport ?? 0, basic = employee.BasicSalary ?? 0 });
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        #endregion

        public ActionResult CalCulateEmployeePercentage(long employeeId, string datFrom, string dateTo)
        {
            decimal totalBill = 0;
            var percentage = 0;//_billingInfoManager.LoadEmployeePercentage(employeeId,Convert.ToDateTime(datFrom), Convert.ToDateTime(dateTo),_dbEntities,out totalBill);
            return Json(new { isSuccess = true, Percentage = percentage, TotalBill = totalBill });
        }
    }
}