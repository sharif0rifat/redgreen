﻿using System;
using System.Web;
using System.Web.Mvc;
using ERP.DAL.Model;
using ERP.INFRASTRUCTURE;
using ERP.INFRASTRUCTURE.Global;
using ERP.REPOSITORIES.Helper;
using ERP.REPOSITORIES.IRepositories;

namespace ERP.WEB.Controllers
{
    public class AccountingController : Controller
    {
        #region Settings Setup
        private DbResponse _dbResponse;
        private readonly LogInInfo _logInInfo;
        private readonly ILedgerGroupManager _transactionHeaderManager;
        #endregion
        public AccountingController(HttpContext httpContext,
            DbResponse dbResponse,  ERPEntities dbEntities, 
            ILedgerGroupManager transactionHeaderManager )
        {
            _dbResponse = dbResponse;
            _transactionHeaderManager = transactionHeaderManager;
            var httpContext1 = httpContext;
            if (httpContext1.Request.RequestContext.HttpContext.Session != null)
                _logInInfo = httpContext1.Request.RequestContext.HttpContext.Session["loginInfo"] as LogInInfo;
            else
            {
                _logInInfo = null;
            }
        }

        #region======== Group========
        public ActionResult LedgerGroupList()
        {

            var headerList = _transactionHeaderManager.GetLedgerGroups();

            return View(headerList);
        }
        public ActionResult SaveLedgerGroup(long id = 0)
        {
            ViewBag.ParentList = DropDownListClass.GetLedgerGroupList();
            if (id == 0)
            {
                return View(new LedgerGroupViewModel());
            }

            var obj = _transactionHeaderManager.GetLedgerGroup(id);
            if (obj != null && obj.Id > 0)
                obj.IsEdit = true;
            else
                obj.IsEdit = false;
            return View(obj);

        }
        [HttpPost]
        public ActionResult SaveLedgerGroup(LedgerGroupViewModel param)
        {
            var logInInfo = (LogInInfo)Session["loginInfo"];
            if (param.Id > 0)
            {
                param.ModifiedBy = logInInfo.UserId;
                param.ModifiedDate = DateTime.Today;
            }
            else
            {
                param.CreatedBy = logInInfo.UserId;
                param.CreatedDate = DateTime.Today;
            }
            var response = _transactionHeaderManager.AddLedgerGroup(param, _logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);


        }
        public ActionResult DeleteLedgerGroup(long id)
        {
            _dbResponse = _transactionHeaderManager.DeleteLedgerGroup(id,_logInInfo);
            return Json(_dbResponse, JsonRequestBehavior.AllowGet);
        }

        public ActionResult EditLedgerGroup(LedgerGroupViewModel model)
        {
            return View();
        }
        #endregion 

        #region =========Ledger==============
        public ActionResult LedgerList()
        {

            var headerList = _transactionHeaderManager.GetLedgers();

            return View(headerList);
        }
        public ActionResult SaveLedger(long id = 0)
        {
            ViewBag.ParentList = DropDownListClass.GetLedgerGroupListForLedger();
            //_transactionHeaderManager.GetLedgerGroupParents();
            if (id == 0)
            {
                //return PartialView("Partial/_SaveLedgerGroup", new LedgerGroupViewModel());
                return View(new LedgerViewModel());
            }
            var obj = _transactionHeaderManager.GetLedger(id);
            if (obj != null && obj.Id > 0)
                obj.IsEdit = true;
            else if (obj != null) obj.IsEdit = false;
            return View(obj);
            return PartialView("SaveLedger", obj);

        }
        [HttpPost]
        public ActionResult SaveLedger(LedgerViewModel param)
        {
            var logInInfo = (LogInInfo)Session["loginInfo"];
            if (param.Id > 0)
            {
                param.ModifiedBy = logInInfo.UserId;
                param.ModifiedDate = DateTime.Today;
            }
            else
            {
                param.CreatedBy = logInInfo.UserId;
                param.CreatedDate = DateTime.Today;
            }
            var response = _transactionHeaderManager.AddLedger(param, _logInInfo);
            return Json(response, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public JsonResult DeleteLedger(string id)
        {
            _dbResponse = _transactionHeaderManager.DeleteLedger(Convert.ToInt64(id),_logInInfo);
            return Json(_dbResponse, JsonRequestBehavior.AllowGet);
        }
        #endregion
	}
}