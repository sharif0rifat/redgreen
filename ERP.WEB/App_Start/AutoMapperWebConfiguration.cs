﻿using AutoMapper;
using ERP.INFRASTRUCTURE.Entity;
using ERP.INFRASTRUCTURE.ReportEntity;

namespace ERP.WEB.App_Start
{
    public static class AutoMapperWebConfiguration
    {
        public static void Configure()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile(new CategoryProfile());
                cfg.AddProfile(new ProductProfile());
                cfg.AddProfile(new ApplicationProfile());
                cfg.AddProfile(new TemplateProfile());
                cfg.AddProfile(new PermissionProfile());
                cfg.AddProfile(new EmployeeProfile());
                cfg.AddProfile(new PaymentProfile());
                cfg.AddProfile(new InvoiceInfoProfile());
                cfg.AddProfile(new InvoiceDetailProfile());
                cfg.AddProfile(new TransactionProfile());
                cfg.AddProfile(new LedgerGroupProfile());
                cfg.AddProfile(new LedgerProfile());
                cfg.AddProfile(new CurrencyProfile());
            });
        }
    }


    public class LedgerProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Ledger, INFRASTRUCTURE.LedgerViewModel>().ReverseMap();
        }
    }
    public class PermissionProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Permission, INFRASTRUCTURE.PartialPermission>().ReverseMap();
        }
    }
    public class LedgerGroupProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.LedgerGroup, INFRASTRUCTURE.LedgerGroupViewModel>().ReverseMap();
        }
    }
    public class TransactionProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Transaction, INFRASTRUCTURE.Entity.TransactionViewModel>().ReverseMap();
        }
    }
    public class InvoiceDetailProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.InvoiceDetail, INFRASTRUCTURE.InvoiceDetailViewModel>().ReverseMap();
        }
    }
    public class InvoiceInfoProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.InvoiceInfo, INFRASTRUCTURE.PartialInvoiceInfo>().ReverseMap();
        }
    }
    public class InvoicePrintProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.InvoiceInfo, InvoicePrint>().ReverseMap();
        }
    }
    public class PaymentProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Payment, INFRASTRUCTURE.Payment>().ReverseMap();
        }
    }
    public class TemplateProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Template, INFRASTRUCTURE.PartialTemplate>().ReverseMap();
        }
    }
    public class ApplicationProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Application, PartialApplication>().ReverseMap();
        }
    }
    public class CategoryProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Category, PartialCategory>().ReverseMap();
        }
    }
    public class ProductProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Product, INFRASTRUCTURE.PartialProduct>().ReverseMap();
        }
    }
    public class EmployeeProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Employee, INFRASTRUCTURE.Employee>().ReverseMap();
        }
    }

    public class CurrencyProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DAL.Model.Currency, PartialCurrency>().ReverseMap();
        }
    }
}
