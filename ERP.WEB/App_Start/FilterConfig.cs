﻿using System.Web.Mvc;

namespace ERP.WEB
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            //filters.Add(new Infrastructure.CustomSecurityAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
