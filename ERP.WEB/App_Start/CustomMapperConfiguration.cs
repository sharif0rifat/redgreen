﻿using AutoMapper;
using ERP.DAL.Model;

namespace ERP.WEB.App_Start
{
    public static class CustomMapperConfiguration
    {
        public static Mapper InvoiceMapperConfigure()
        {
            var configuration = new MapperConfiguration(cfg => cfg.CreateMap<ERP.INFRASTRUCTURE.PartialInvoiceInfo, DAL.Model.InvoiceInfo>()
                .ForMember(dest => dest.PartyId, opt => opt.MapFrom(src=>src.PartyId))
                .ForMember(dest => dest.InvoiceDate, opt => opt.MapFrom(src => src.InvoiceDate))
                .ForMember(dest => dest.InvoiceNo, opt => opt.MapFrom(src => src.InvoiceNo))
                .ForMember(dest => dest.PropertyId, opt => opt.MapFrom(src => src.Remarks))
                .ForMember(dest => dest.Remarks, opt => opt.MapFrom(src => src.Remarks))
                .ForMember(dest => dest.GrandTotal, opt => opt.MapFrom(src => src.GrandTotal))
                );

            return (Mapper)configuration.CreateMapper();
        }
    }
}