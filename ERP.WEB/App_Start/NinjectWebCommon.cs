using System;
using System.Web;
using Microsoft.Web.Infrastructure.DynamicModuleHelper;
using Ninject;
using Ninject.Web.Common;
using ERP.DAL.Model;
using ERP.REPOSITORIES;
using ERP.REPOSITORIES.IRepositories;
using ERP.REPOSITORIES.Repositories;
using ERP.WEB;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(NinjectWebCommon), "Start")]
[assembly: WebActivatorEx.ApplicationShutdownMethodAttribute(typeof(NinjectWebCommon), "Stop")]

namespace ERP.WEB
{
    public static class NinjectWebCommon
    {
        private static readonly Bootstrapper bootstrapper = new Bootstrapper();

        /// <summary>
        /// Starts the application
        /// </summary>
        public static void Start()
        {
            DynamicModuleUtility.RegisterModule(typeof(OnePerRequestHttpModule));
            DynamicModuleUtility.RegisterModule(typeof(NinjectHttpModule));
            bootstrapper.Initialize(CreateKernel);
        }

        /// <summary>
        /// Stops the application.
        /// </summary>
        public static void Stop()
        {
            bootstrapper.ShutDown();
        }

        /// <summary>
        /// Creates the kernel that will manage your application.
        /// </summary>
        /// <returns>The created kernel.</returns>
        private static IKernel CreateKernel()
        {
            var kernel = new StandardKernel();
            try
            {
                kernel.Bind<Func<IKernel>>().ToMethod(ctx => () => new Bootstrapper().Kernel);
                kernel.Bind<IHttpModule>().To<HttpApplicationInitializationHttpModule>();
                kernel.Bind<ERPEntities>().To<ERPEntities>();
                kernel.Bind<IApplicationRepo>().To<ApplicationRepo>();
                kernel.Bind<IAssignTemplateRepo>().To<AssignTemplateRepo>();
                kernel.Bind<IAuthRepo>().To<AuthRepo>();
                //kernel.Bind<IUserSetupManager>().To<UserSetupManager>();
                kernel.Bind<ICategoryManager>().To<CategoryManager>();
                kernel.Bind<IProductManager>().To<ProductManager>();
                kernel.Bind<IStockManager>().To<StockManager>();
                kernel.Bind<IPropertyManager>().To<PropertyManager>();
                kernel.Bind<IPartyManager>().To<PartyManager>();
                kernel.Bind<IPermissionRepo>().To<PermissionRepo>();
                kernel.Bind<IEmployeeManager>().To<EmployeeManager>();
                kernel.Bind<IInvoiceInfoManager>().To<InvoiceInfoManager>();
                kernel.Bind<ISalesInvoiceManager>().To<SalesInvoiceManager>();
                kernel.Bind<IPurchaseInvoiceManager>().To<PurchaseInvoiceManager>();
                kernel.Bind<IInvoiceDetailsManager>().To<InvoiceDetailsManager>();
                kernel.Bind<IPaymentManager>().To<PaymentManager>();
                kernel.Bind<ILedgerGroupManager>().To<LedgerGroupManager>();
                kernel.Bind<ITransactionManager>().To<TransactionManager>();
                kernel.Bind<ITemplateRepo>().To<TemplateRepo>();
                kernel.Bind<ISalaryManager>().To<SalaryManager>();
                kernel.Bind<IUserRepo>().To<UserRepo>();
                kernel.Bind<ICurrencyManager>().To<CurrencyManager>();
                RegisterServices(kernel);
                return kernel;
            }
            catch
            {
                kernel.Dispose();
                throw;
            }
        }

        /// <summary>
        /// Load your modules or register your services here!
        /// </summary>
        /// <param name="kernel">The kernel.</param>
        private static void RegisterServices(IKernel kernel)
        {
        }
    }
}
